# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.0.8] - 2025-03-06

### Added

- Added more logging for PowerBiServices, less caching and forced dynamic layouts (no issue)

## [2.0.7] - 2025-03-05

### Fixed

- Fix cookieconsent ([gbi-general#104](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/104))

## [2.0.6] - 2025-02-28

### Added

- Secrurity headers ([gbi-general#101](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/101))

## [2.0.5] - 2025-02-13

### Fixed

- Fix footer for dashboard`s page ([gbi-general#15](https://gitlab.com/operator-ict/golemio/code/golemio-bi/golemio-bi-frontend-v2/-/issues/15))

## [2.0.4] - 2025-02-12

### Changed

- Root layout separation ([gbi-general#15](https://gitlab.com/operator-ict/golemio/code/golemio-bi/golemio-bi-frontend-v2/-/issues/15))

## [2.0.3] - 2025-02-06

### Added

- Adds the ability for the garant to bulk-assign users ([gbi-general#93](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/93))

## [2.0.2] - 2025-01-30

### Added

- Add garant to dashboard(mandatory) ([gbi-general#86](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/86))

### Changed

- Fix favButton & DataContainer loader from fav ([gbi-general#72](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/72))

## [2.0.1] - 2025-01-23

### Changed

- Add organization column to user list/ remove checkbox from users and roles ([gbi-general#90](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/89))

- UI changes depending on the users rights and permissions ([gbi-general#90](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/90))

## [2.0.0] - 2025-01-16

### Added

- Add or remove user's roles ([gbi-general#83](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/83))
- Add role creation ([gbi-general#65](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/65))
- Add proxy call to OG for map data ([gbi-general#88](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/88))

### Changed

- Next-auth cookies renamed with prefix "GBI"
- Registration and password recovery email textation
- Notification banner added to login and registration pages
- UX for user registration and reset process ([gbi-general#82](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/82))
- init Kratos & Keto services for auth and permissions ([gbi-general#69](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/69))
- password recovery flow ([golemio-bi#76](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/76))
- get users data from `/admin/identity` kratos endpoint for admin panel ([golemio-bi#81](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/81))
- get roles from GBI instead of PP ([golemio-bi#85](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/85))
- get user's session from permissions API ([gbi-general#88](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/88))

### Removed

- Removed PP references and direct OG calls ([gbi-general#88](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/88))

## [1.1.13] - 2024-12-09

## Added

- Url parameters handlig for dashboard pageId and filter ([golemio-bi#334](https://gitlab.com/operator-ict/golemio/projekty/oict/p0149-chytry-svoz-odpadu/-/issues/334))

## [1.1.12] - 2024-12-02

## Fixed

- Icons for Safari and some utils - no issue

## [1.1.11] - 2024-11-04

- Request dashboard access text fixed ([golemio-bi#73](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/73))

## [1.1.10] - 2024-10-29

### Changed

- "Create user" button removed ([golemio-bi#63](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/63))

## [1.1.9] - 2024-10-21

### Fixed

- hotfix "Route /auth/request-dashboard-access couldn't be rendered statically because it used `headers`"

## [1.1.8] - 2024-10-15

### Fixed

- Fixed organization autocomplete options to registration form ([golemio-bi#52](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/52))
- Footer logo fix

## [1.1.7] - 2024-10-09

### Fixed

- Fixed default image ([golemio-bi#47](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/47))

### Added

- Added organization autocomplete options to registration form ([golemio-bi#52](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/52))

### Changed

- Logger detail includes more information ([golemio-bi#12](https://gitlab.com/operator-ict/golemio/code/golemio-bi/golemio-bi-frontend-v2/-/issues/12))

## [1.1.6] - 2024-10-03

### Fixed

- Fixed users and roles url check

## [1.1.5] - 2024-09-30

### Fixed

- Fixed favourites functionality for maps ([golemio-bi#57](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/57))

## [1.1.4] - 2024-09-24

### Added

- Tests JEST/RTL ([golemio-bi#46](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/46))

## [1.1.3] - 2024-09-17

### Fixed

- routes for maps ([golemio-bi#14](https://gitlab.com/operator-ict/golemio/code/golemio-bi/golemio-bi-frontend-v2/-/issues/14))
- dashboard thumbnails path fixed

### Changed

- Dashboard search is done by backend ([gbi-general#45](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/45))

## [] - 2024-09-12

- No changelog

## [1.1.2] - 2024-09-11

- Added search to users and roles pages ([golemio-bi#41](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/41))

- Added user conditions agreement check and update ([golemio-bi#15](https://gitlab.com/operator-ict/golemio/code/golemio-bi/golemio-bi-backend-v2/-/issues/15))

## [1.1.1] - 2024-09-05

### Added

- Added roles page ([golemio-bi#35](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/35))
- Added users page ([golemio-bi#4](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/4))
- sorting and pagination for users & roles pages ([golemio-bi#40](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/40))
- footer component to auth pages ([golemio-bi#34](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/34))
- Added User agreement conditions and handeling user info to page footer ([golemio-bi#30](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/30))
- Added user agreement conditions ([golemio-bi#1](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/1))

## [1.1.0] - 2024-09-03

- No changelog

## [1.0.7] - 2024-08-19

### Added

- add backstage metadata files
- add .gitattributes file

## [1.0.6] - 2024-08-14

### Fixed

- hotfix - data fetching is separated for dashboard and map ([golemio-bi#13](https://gitlab.com/operator-ict/golemio/code/golemio-bi/golemio-bi-frontend-v2/-/issues/13))

### Added

- possibility to request access rights for dashboard user does not have access to ([golemio-bi#2](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/2))

## [1.0.5] - 2024-08-07

### Changed

- icons for clusters and station according to is_monitored([[golemio-bi#32](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/32)])

### Added

- Added `Accept-Encoding: gzip` header and cache default setting to `no-store`  on all requests ([golemio-bi#11](https://gitlab.com/operator-ict/golemio/code/golemio-bi/golemio-bi-frontend-v2/-/issues/11))

## [1.0.4] - 2024-07-29

### Added

- redirect to link url after login ([golemio-bi#33](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/33))

## [1.0.3] - 2024-07-22

### Added

- add status endpoint([golemio-bi#8](https://gitlab.com/operator-ict/golemio/code/golemio-bi/golemio-bi-frontend-v2/-/issues/8))

### Changed

- update next and node to v20.11.1 ([golemio-bi#3](https://gitlab.com/operator-ict/golemio/code/golemio-bi/golemio-bi-frontend-v2/-/issues/3))

### Fixed

- fix color in parking`s map datail([golemio-bi#7](https://gitlab.com/operator-ict/golemio/code/golemio-bi/golemio-bi-frontend-v2/-/issues/7))

## [1.0.2] - 2024-06-28

### Fixed

- dropdown positioning and z-index fixed, header mobile wiev improved ([golemio-bi#6](https://gitlab.com/operator-ict/golemio/code/golemio-bi/golemio-bi-frontend-v2/-/issues/6))

## [1.0.1] - 2024-06-26

### Fixed

- broken dashboard links get redirected to 404 ([golemio-bi#5](https://gitlab.com/operator-ict/golemio/code/golemio-bi/golemio-bi-backend-v2/-/issues/5))
- dashboard height goes almost fullscreen ([golemio-bi#5](https://gitlab.com/operator-ict/golemio/code/golemio-bi/golemio-bi-frontend-v2/-/issues/5))

## [1.0.0] - 2024-06-19

### Added

- Added `CHANGELOG.md` file
- Added Homepage
- UI kit implemented
- Not-found page ([golemio-bi#55](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/55))
- login added ([golemio-bi#65](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/65))
- reset forgotten password added ([golemio-bi#70](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/70))
- registration added ([golemio-bi#69](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/69))
- token expiration checking added
- creating, editing and deleting of metadata tiles added ([golemio-bi#74](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/74))
- adding to and removing from favourites added ([golemio-bi#75](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/75))
- layout updates ([golemio-bi#79](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/79))
- tile constrast improved ([golemio-bi#77](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/77))
- fixes for safari browser ([golemio-bi#82](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/82))
- added GTM and cookie consent ([golemio-bi#88](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/88))
- UI udates - dropdown control ([golemio-bi#92](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/92))
- UI udates - bigger dashboard on detail page ([golemio-bi#90](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/90))
- UI udates - color contrast improvements ([golemio-bi#91](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/91))
- UI updates - users name and surname ([golemio-bi#93](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/93))
- Added history for back button from dashboard detail ([golemio-bi#95](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/95))
- Waste map - optimization ([golemio-bi#89](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/89))
- Added plausible analytics([golemio-bi#84](<https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/84}>))
- Added new button for uploadin different image in administration ([golemio-bi#96]<https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/96>)

### Fixed

- Limitation of the label on the dashboard ([golemio-bi#107]<https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/107>)
- Search by tags([golemio-bi#102]<https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/102>)
- Map CZ names fixed ([golemio-bi#101]<https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/101>)
- Problem with token unreadable fixed - no issue
- Favourites button made more noticeable in header ([golemio-bi#97](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/97))
- filters for waste sorting map  ([golemio-bi#109]<https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/109>)
- highlights button ([golemio-bi#104](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/104))

### Changed

- URL backward compatibility with version 1. ([golemio-bi#108](https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/108))

