/* eslint-disable max-len */

/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,

    images: {
        remotePatterns: [
            { protocol: "https", hostname: "api-bi.rabin.golemio.cz" },
            { protocol: "https", hostname: "api-bi2.rabin.golemio.cz" },
            { protocol: "https", hostname: "api-bi3.rabin.golemio.cz" },
            { protocol: "https", hostname: "api-bi.golemio.cz" },
            { protocol: "https", hostname: "api-bi2.golemio.cz" },
            { protocol: "https", hostname: "api-bi3.golemio.cz" },
            { protocol: "http", hostname: "golemio-bi-backend", port: "3000" },
            { protocol: "http", hostname: "golemio-bi-backend-kratos", port: "3000" },
            { protocol: "http", hostname: "localhost", port: "3002" },
        ],
        minimumCacheTTL: 120,
        deviceSizes: [828, 1200],
        formats: ["image/webp"],
    },

    i18n: {
        locales: ["cs-CZ"],
        defaultLocale: "cs-CZ",
    },

    async redirects() {
        return [
            {
                source: "/dashboard",
                destination: "/",
                permanent: true,
            },
        ];
    },

    async headers() {
        return [
            {
                source: "/:path*",
                headers: [
                    {
                        key: "X-DNS-Prefetch-Control",
                        value: "on",
                    },
                    {
                        key: "Strict-Transport-Security",
                        value: "max-age=31536000; includeSubDomains; preload",
                    },

                    {
                        key: "X-Content-Type-Options",
                        value: "nosniff",
                    },
                    {
                        key: "Referrer-Policy",
                        value: "strict-origin-when-cross-origin",
                    },
                    {
                        key: "X-XSS-Protection",
                        value: "1; mode=block",
                    },
                    {
                        key: "Permissions-Policy",
                        value: "geolocation=(self), camera=(), microphone=(), fullscreen=(self), payment=()",
                    },
                    {
                        key: "Content-Security-Policy",
                        value:
                            "default-src 'self'; " +
                            "script-src 'self' 'unsafe-inline' 'unsafe-eval' https://www.googletagmanager.com " +
                            "https://*.google-analytics.com https://tagmanager.google.com " +
                            "https://*.powerbi.com https://plausible.io https://api.mapbox.com blob: " +
                            "https://static.cloudflareinsights.com https://*.cloudflare.com " +
                            "https://challenges.cloudflare.com https://*.clarity.ms; " +
                            "style-src 'self' 'unsafe-inline' https://tagmanager.google.com " +
                            "https://fonts.googleapis.com https://api.mapbox.com " +
                            "https://*.cloudflare.com https://challenges.cloudflare.com " +
                            "https://www.googletagmanager.com; " +
                            "img-src 'self' data: blob: https: https://*.google-analytics.com " +
                            "https://www.googletagmanager.com https://*.powerbi.com " +
                            "https://api.mapbox.com https://*.tiles.mapbox.com " +
                            "https://plausible.io https://*.cloudflare.com https://challenges.cloudflare.com " +
                            "https://*.clarity.ms https://*.virtualearth.net; " +
                            "font-src 'self' data: https://fonts.gstatic.com https://*.cloudflare.com; " +
                            "frame-src 'self' https://*.powerbi.com https://app.powerbi.com; " + // FIXED: Expanded frame-src for Power BI compatibility
                            "connect-src 'self' " +
                            "https://*.google-analytics.com https://stats.g.doubleclick.net " +
                            "https://*.tiles.mapbox.com https://api.mapbox.com https://events.mapbox.com " +
                            "https://plausible.io https://login.microsoftonline.com " +
                            "https://*.powerbi.com https://*.analysis.windows.net " +
                            "https://*.cloudflare.com https://cloudflareinsights.com " +
                            "https://*.clarity.ms https://c.clarity.ms; " +
                            "worker-src 'self' blob: https://*.cloudflare.com 'unsafe-inline'; " +
                            "child-src 'self' blob: https://*.cloudflare.com https://challenges.cloudflare.com; " +
                            "manifest-src 'self'; " +
                            "base-uri 'self'; " +
                            "form-action 'self'; " +
                            "frame-ancestors 'self' https://app.powerbi.com https://*.powerbi.com; " + // FIXED: Ensuring Power BI embedding works
                            "object-src 'none'; " +
                            "upgrade-insecure-requests;",
                    },
                    {
                        key: "Cross-Origin-Opener-Policy",
                        value: "same-origin",
                    },
                    {
                        key: "Cross-Origin-Resource-Policy",
                        value: "same-origin",
                    },
                    {
                        key: "Expect-CT",
                        value: "max-age=86400, enforce",
                    },
                ],
            },
        ];
    },
};

module.exports = nextConfig;
