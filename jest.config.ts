/* eslint-disable max-len */

// /**
//  * For a detailed explanation regarding each configuration property, visit:
//  * https://jestjs.io/docs/configuration
//  */

import type { Config } from "jest";
import nextJest from "next/jest";
import { pathsToModuleNameMapper } from "ts-jest";

import { compilerOptions } from "./tsconfig.json";

const createJestConfig = nextJest({
    // Provide the path to your Next.js app to load next.config.js and .env files in your test environment
    dir: "./",
});

const config: Config = {
    // Indicates whether each individual test should be reported during the run
    verbose: undefined,

    // Indicates which provider should be used to instrument code for coverage
    coverageProvider: "v8",

    // Indicates whether the coverage information should be collected while executing the test
    collectCoverage: true,

    // The directory where Jest should output its coverage files
    coverageDirectory: "coverage",

    // A list of reporter names that Jest uses when writing coverage reports
    coverageReporters: ["text", "cobertura"],

    // A list of paths to directories that Jest should use to search for files in
    roots: ["<rootDir>/"],

    // An array of file extensions your modules use
    moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],

    // A map from regular expressions to module names or to arrays of module names that allow to stub out resources with a single module
    moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths, { prefix: "<rootDir>/" }),

    // A list of paths to modules that run some code to configure or set up the testing framework before each test
    setupFilesAfterEnv: ["<rootDir>/jest.setup.ts"],

    // The test environment that will be used for testing
    testEnvironment: "jsdom",

    // A map from regular expressions to paths to transformers
    transform: {
        "^.+\\.(ts|tsx)$": "ts-jest",
    },
    // Indicate the libraries that need to be transformed
    transformIgnorePatterns: ["node_modules/(?!jose)"],

    preset: "ts-jest",
};
export default createJestConfig(config);
