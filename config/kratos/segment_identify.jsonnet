function(ctx) {
  userId: ctx.identity.id,
  acceptedConditions: ctx.flow.transient_payload.acceptedConditions,
  traits: {
    email: ctx.identity.traits.email,
    firstName: ctx.identity.traits.firstName,
    lastName: ctx.identity.traits.lastName,
    organization: ctx.identity.traits.organization,
  },
}