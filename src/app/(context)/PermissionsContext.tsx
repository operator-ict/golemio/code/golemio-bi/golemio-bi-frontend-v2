"use client";
import { createContext, ReactNode, useContext } from "react";

type PermissionsContextType = {
    canEditUsersRole?: boolean;
    canEditRoles?: boolean;
    canEditUsersRights?: boolean;
    canViewAdminDashboards?: boolean;
} | null;
type PermissionsProviderProps = {
    permissions: PermissionsContextType;
    children: ReactNode;
};

export const PermissionsContext = createContext<PermissionsContextType>(null);

export const usePermissions = () => {
    const context = useContext<PermissionsContextType>(PermissionsContext);
    if (!context) {
        throw new Error("usePermissions must be used within a PermissionsProvider");
    }
    return context;
};

export const PermissionsProvider = ({ children, permissions }: PermissionsProviderProps) => {
    return <PermissionsContext.Provider value={permissions}>{children}</PermissionsContext.Provider>;
};
