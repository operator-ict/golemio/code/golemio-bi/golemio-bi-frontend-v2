"use client";
import { createContext, ReactNode, useContext, useEffect, useState } from "react";

import { setAgreementCookie } from "@/utils/cookies";

interface AgreementContextType {
    isAgreed: boolean;
    setAgreement: (value: boolean) => void;
}

const AgreementContext = createContext<AgreementContextType | undefined>(undefined);

export const useAgreement = () => {
    const context = useContext(AgreementContext);
    if (context === undefined) {
        throw new Error("useAgreement must be used within an AgreementProvider");
    }
    return context;
};

export const AgreementProvider = ({
    children,
    initialAgreementStatus,
}: {
    children: ReactNode;
    initialAgreementStatus: boolean;
}) => {
    const [isAgreed, setIsAgreed] = useState<boolean>(initialAgreementStatus);

    useEffect(() => {
        setAgreementCookie(initialAgreementStatus);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const setAgreement = (value: boolean) => {
        setIsAgreed(value);
        setAgreementCookie(value);
    };

    return <AgreementContext.Provider value={{ isAgreed, setAgreement }}>{children}</AgreementContext.Provider>;
};
