"use client";
import { createContext, ReactNode, useContext } from "react";

type EnvContextType = {
    [key: string]: string | number | boolean | null;
} | null;

type EnvProviderProps = {
    env: EnvContextType;
    children: ReactNode;
};

export const EnvContext = createContext<EnvContextType>(null);

export const useEnv = () => {
    return useContext<EnvContextType>(EnvContext);
};

export const EnvProvider = ({ children, env }: EnvProviderProps) => {
    return <EnvContext.Provider value={env}>{children}</EnvContext.Provider>;
};
