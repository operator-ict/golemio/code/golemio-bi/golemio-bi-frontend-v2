"use client";
import React, { createContext, FC, ReactNode, useCallback, useMemo, useState } from "react";

import { IFeatureCollection } from "@/(types)/FeaturesTypes";
import text from "@/textContent/cs.json";

type FilterOptions = {
    sensor: "sensor_only" | "all";
    fullness: {
        min: number;
        max: number;
    };
    types: string[];
    area: string[];
};

const DEFAULT_FILTER_OPTIONS: FilterOptions = {
    sensor: "sensor_only", // Default to sensor_only
    fullness: {
        min: 0,
        max: 100,
    },
    types: [],
    area: [],
};

type SetFilterOptions = <T extends keyof FilterOptions>(name: T, value: FilterOptions[T]) => void;

type ContextState = {
    filterOptions: FilterOptions;
    setFilterOptions: SetFilterOptions;
    resetFilters: () => void;
    filterDescription: string;
    filterFunction: (data: IFeatureCollection) => IFeatureCollection;
};

export const SortedWasteFiltersContext = createContext<ContextState>({
    filterOptions: DEFAULT_FILTER_OPTIONS,
    setFilterOptions: () => undefined,
    resetFilters: () => undefined,
    filterDescription: "",
    filterFunction: (data) => data,
});

interface Props {
    children: ReactNode;
}

export const SortedWasteFiltersProvider: FC<Props> = ({ children }) => {
    const [filterOptions, setFilterOptionsState] = useState<FilterOptions>(DEFAULT_FILTER_OPTIONS);

    const setFilterOptions: SetFilterOptions = useCallback((name, value) => {
        setFilterOptionsState((prevState) => {
            const newState = {
                ...prevState,
                [name]: value,
            };

            if (name === "sensor" && value === "all") {
                newState.fullness = {
                    min: 0,
                    max: 100,
                };
            }

            return newState;
        });
    }, []);

    const resetFilters = useCallback(() => {
        setFilterOptionsState(DEFAULT_FILTER_OPTIONS);
    }, []);

    const filterDescription = useMemo(() => {
        const { sensor, types, fullness, area } = filterOptions;
        const { min, max } = fullness;
        const filterDescriptionParts: string[] = [];

        // sensor
        if (sensor === "sensor_only") {
            filterDescriptionParts.push(text.map.sortedWaste.filters.withSensor.toLowerCase());
        }

        // trash types
        if (types.length > 0) {
            const typesDescription = types.map((option) => `[text.map.trashType${option}]`).join(", ");

            filterDescriptionParts.push(`${text.map.sortedWaste.filters.containing} ${typesDescription}`.toLowerCase());
        }

        // fullness
        if ((min > 0 || max < 100) && min === max) {
            filterDescriptionParts.push(`${text.map.sortedWaste.filters.percentFull} ${fullness}`);
        } else if (min > 0 && max < 100) {
            filterDescriptionParts.push(`${text.map.sortedWaste.filters.between}, ${fullness})}`);
        } else if (min > 0) {
            filterDescriptionParts.push(`${text.map.sortedWaste.filters.min}, ${fullness})}`);
        } else if (max < 100) {
            filterDescriptionParts.push(`${text.map.sortedWaste.filters.max}, ${fullness})}`);
        }

        if (area.length > 0) {
            filterDescriptionParts.push(`${text.map.sortedWaste.filters.inArea}, { area: area.join(", ") })}`);
        }

        if (filterDescriptionParts.length === 0) {
            return "";
        } else {
            return " " + filterDescriptionParts.join(", ");
        }
    }, [filterOptions]);

    const filterFunction = useCallback(
        (data: IFeatureCollection) => {
            const { area, sensor, fullness, types } = filterOptions;
            const { min, max } = fullness;

            // Filter features based on filter options
            const filteredFeatures = data?.features
                ?.map((feature) => {
                    // Clone the feature to avoid mutating the original data
                    const newFeature = {
                        ...feature,
                        properties: { ...feature.properties, containers: [...feature.properties.containers] },
                    };
                    // Filter containers within the feature
                    const filteredContainers = newFeature.properties.containers.filter((container) => {
                        // Sensors
                        if (sensor === "sensor_only" && !container.sensor_code) {
                            return false;
                        }

                        // Types
                        if (types.length && !types.includes(`${container.trash_type.id}`)) {
                            return false;
                        }

                        // Area
                        if (area.length > 0 && !area.includes(newFeature.properties.district)) {
                            return false;
                        }

                        // Fullness
                        const lastMeasurement = container.last_measurement?.percent_calculated;
                        if (min > 0 || max < 100) {
                            if (typeof lastMeasurement !== "number" || lastMeasurement < min || lastMeasurement > max) {
                                return false;
                            }
                        }

                        return true;
                    });

                    // Update the newFeature's containers to the filtered list
                    newFeature.properties.containers = filteredContainers;

                    return newFeature;
                })
                .filter((feature) => feature.properties.containers.length > 0);

            return {
                ...data,
                features: filteredFeatures,
            };
        },
        [filterOptions]
    );

    const val = useMemo(() => {
        return {
            filterOptions,
            setFilterOptions,
            resetFilters,
            filterDescription,
            filterFunction,
        };
    }, [filterOptions, resetFilters, filterDescription, setFilterOptions, filterFunction]);

    return <SortedWasteFiltersContext.Provider value={val}>{children}</SortedWasteFiltersContext.Provider>;
};
