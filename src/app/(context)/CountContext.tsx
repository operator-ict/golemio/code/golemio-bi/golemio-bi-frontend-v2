"use client";
import React, { createContext, ReactNode, useContext, useState } from "react";

interface CountContextProps {
    count: number;
    setCount: React.Dispatch<React.SetStateAction<number>>;
    countContainer: number;
    setCountContainer: React.Dispatch<React.SetStateAction<number>>;
}

const CountContext = createContext<CountContextProps | undefined>(undefined);

export const CountProvider = ({ children }: { children: ReactNode }) => {
    const [count, setCount] = useState(0);
    const [countContainer, setCountContainer] = useState(0);
    return (
        <CountContext.Provider value={{ count, setCount, countContainer, setCountContainer }}>{children}</CountContext.Provider>
    );
};

export const useCount = () => {
    const context = useContext(CountContext);
    if (!context) {
        throw new Error("useCount must be used within a CountProvider");
    }
    return context;
};
