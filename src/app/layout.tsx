import "@/styles/globals.scss";

import { Metadata } from "next";
import { ReactNode, Suspense } from "react";
import { config } from "src/config";
import { AnalyticsProvider } from "src/context/AnalyticsProvider";

import { EnvProvider } from "@/context/EnvProvider";
import { barlow, golosText } from "@/fonts/fonts";
import layout from "@/layout.module.scss";
import text from "@/textContent/cs.json";

import { getLogger } from "./(logging)/logger";
import { getErrorMessage } from "./(utils)/getErrorMessage";

const logger = getLogger("RootLayout");

export const metadata: Metadata = {
    title: text.metadata.siteName,
    description: text.metadata.siteDescription,
};

const domain = config.NEXT_PUBLIC_URL;

let plausibleDomain: string;

if (domain) {
    try {
        const url = new URL(domain);
        plausibleDomain = url.host;
    } catch (error) {
        logger.error({
            provider: "server",
            function: "plusibleDomain setting",
            component: "app-layout",
            message: `Invalid URL: ${domain}`,
            error: getErrorMessage(error),
        });
    }
}

const envContext = { API_URL: config.NEXT_PUBLIC_API_URL };

export const dynamic = "force-dynamic";

const RootLayout = ({ children }: { children: ReactNode }) => {
    return (
        <html lang="cs" className={`${golosText.variable} ${barlow.variable}`}>
            <head>
                <Suspense>
                    <AnalyticsProvider gtmId={config.NEXT_PUBLIC_GTM_ID} plausibleDomain={plausibleDomain} />
                </Suspense>
            </head>
            <body className={`${layout["main-layout"]}`}>
                <EnvProvider env={envContext}>{children}</EnvProvider>
            </body>
        </html>
    );
};

export default RootLayout;
