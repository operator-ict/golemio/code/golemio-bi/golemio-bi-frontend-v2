import styles from "./ButtonContentLoader.module.scss";

export const ButtonContentLoader: React.FC<{ isLoading: boolean; isActive: boolean; label: string }> = ({
    isLoading,
    isActive,
    label,
}) => {
    return isLoading && isActive ? (
        <span className={styles["dots"]}>
            <span>.</span>
            <span>.</span>
            <span>.</span>
        </span>
    ) : (
        <>{label}</>
    );
};
