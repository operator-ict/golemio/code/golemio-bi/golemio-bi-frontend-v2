"use client";
import React, { ChangeEvent, DetailedHTMLProps, InputHTMLAttributes, useEffect, useState } from "react";

import styles from "./CheckBox.module.scss";

interface InputElementProps extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    label?: string;
    onClick: () => void;
}

export const CheckBox = ({ checked, id, label, name, onClick, readOnly }: InputElementProps) => {
    const defaultChecked = checked ? checked : false;
    const [isChecked, setIsChecked] = useState(defaultChecked);

    const changeHandler = (e: ChangeEvent<HTMLInputElement>) => {
        setIsChecked(e.target.checked);
        onClick();
    };

    useEffect(() => {
        if (!checked) {
            setIsChecked(false);
        }
    }, [checked]);

    return (
        <li className={styles["checkbox-wrapper"]}>
            <label htmlFor={id} className={`${styles["label"]} ${readOnly ? styles["label__disabled"] : ""}`}>
                <input type="checkbox" id={id} name={name} checked={isChecked} onChange={changeHandler} disabled={readOnly} />
                <span className={styles["label-content"]}>{label}</span>
            </label>
        </li>
    );
};
