import { ButtonHTMLAttributes, DetailedHTMLProps, HTMLAttributes } from "react";

import DeleteChipIcon from "@/icons/formIcons/DeleteChipIcon";

import styles from "./Chip.module.scss";

type Tag = "button" | "div";

type CommonProps = {
    tag: Tag;
    className?: string;
    id: string;
    label: string;
    onClick?: () => void;
    onDelete?: (id: string) => void;
};

type ChipButtonProps = DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> &
    CommonProps & {
        disabled?: boolean;
    };

type ChipDivProps = DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> &
    CommonProps & {
        deleteIconLeft?: boolean;
        deleteIconRight?: boolean;
    };

type PolymorphicProps = ChipButtonProps | ChipDivProps;

const ChipButton = (props: ChipButtonProps) => {
    const { disabled, label, onClick, ...restProps } = props;
    return (
        <button
            className={`${styles.chip} ${styles["chip-button"]}`}
            onClick={onClick}
            type="button"
            disabled={disabled}
            {...restProps}
        >
            <span>{label}</span>
        </button>
    );
};

const ChipDiv = (props: ChipDivProps) => {
    const { deleteIconLeft, deleteIconRight, onDelete, ...restProps } = props;
    return (
        <div className={styles.chip} {...restProps}>
            {deleteIconLeft && (
                <button className={styles["chip-img-button"]} onClick={() => onDelete && onDelete(props.id)}>
                    <DeleteChipIcon color="secondary" width="0.75rem" height="0.75rem" />
                </button>
            )}
            <span>{props.label}</span>
            {deleteIconRight && (
                <button className={styles["chip-img-button"]} onClick={() => onDelete && onDelete(props.id)}>
                    <DeleteChipIcon color="secondary" width="0.75rem" height="0.75rem" />
                </button>
            )}
        </div>
    );
};

const isDiv = (props: PolymorphicProps): props is ChipDivProps => {
    return props.tag === "div" || props.onDelete !== undefined;
};

export const Chip = (props: PolymorphicProps) => {
    return isDiv(props) ? <ChipDiv {...props} /> : <ChipButton {...props} />;
};
