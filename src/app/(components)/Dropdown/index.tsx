"use client";
import React, { DetailedHTMLProps, HTMLAttributes, ReactNode, useEffect, useRef, useState } from "react";

import { useComponentVisible } from "@/hooks/useComponentVisible";

import styles from "./Dropdown.module.scss";

interface IDropdown extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    visible: boolean;
    hide?: () => void;
    contents?: ReactNode;
    filter?: boolean;
    dynamicWidth?: boolean;
}

export const Dropdown = ({ contents, children, visible, hide, className, filter, dynamicWidth, ...restProps }: IDropdown) => {
    const { ref } = useComponentVisible(hide || (() => {}));
    const triggerRef = useRef<HTMLDivElement>(null);
    const [dropdownWidth, setDropdownWidth] = useState<string | undefined>();

    useEffect(() => {
        if (!triggerRef.current || !dynamicWidth) return;

        const updateWidth = () => setDropdownWidth(`${triggerRef.current?.offsetWidth}px`);
        updateWidth();

        const observer = new ResizeObserver(updateWidth);
        observer.observe(triggerRef.current);

        return () => observer.disconnect();
    }, [dynamicWidth]);

    return (
        <div ref={triggerRef} className={`${styles.dropdown_wrapper} ${className}`} {...restProps}>
            {children}
            {visible && (
                <div ref={ref} className={styles[filter ? "dropdown-filter" : "dropdown"]} style={{ width: dropdownWidth }}>
                    {contents}
                </div>
            )}
        </div>
    );
};
