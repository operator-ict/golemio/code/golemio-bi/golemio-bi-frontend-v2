import React, { DetailedHTMLProps, HTMLAttributes, ReactNode, useState } from "react";

import { ChevronIcon } from "@/icons/ChevronIcon";

import styles from "./Select.module.scss";

type SelectProps = DetailedHTMLProps<HTMLAttributes<HTMLSelectElement>, HTMLSelectElement> & {
    name: string;
    options: {
        value: string;
        label: string;
    }[];
    value: string | number;
    placeholder: string;
    onChange: (e: React.ChangeEvent<HTMLSelectElement>) => void;
    label?: string;
    error?: boolean;
    errorMessage?: string;
    iconStart?: ReactNode;
    disabled?: boolean;
    className?: string;
    id: string;
};

const Select = ({ name, id, label, options, placeholder, value, iconStart, error, errorMessage, ...restProps }: SelectProps) => {
    const [isOpen, setIsOpen] = useState(false);

    const iconEnd = isOpen ? (
        <ChevronIcon color="gray-dark" width={10} direction="up" />
    ) : (
        <ChevronIcon color="gray-dark" width={10} direction="down" />
    );
    return (
        <div className={styles["select"]}>
            <div className={styles["select-field"]}>
                <label
                    htmlFor={name}
                    className={`${styles["select-label"]} ${!value ? styles["select-label__hidden"] : ""}
                            ${iconStart && styles["select-label__icon-start"]}`}
                >
                    {label}
                </label>
                <select
                    name={name}
                    id={id ?? name}
                    className={`${styles["select-input"]} ${!value && styles["select-input__label-hidden"]}
                        ${error && styles["select-input__error"]} ${iconStart && styles["select-input__icon-start"]}`}
                    onClick={() => setIsOpen(!isOpen)}
                    {...restProps}
                >
                    <option value="" className={`${styles["select-input-option"]} ${styles["select-input-placeholder"]}`}>
                        {placeholder}
                    </option>
                    {options.map((option) => (
                        <option key={option.value} value={option.value} className={`${styles["select-input-option"]}`}>
                            {option.label}
                        </option>
                    ))}
                </select>
                {iconStart && <span className={`${styles["select-icon"]} ${styles["select-icon-start"]}`}>{iconStart}</span>}
                {iconEnd && <span className={`${styles["select-icon"]} ${styles["select-icon-end"]}`}>{iconEnd}</span>}
            </div>
            {error ? <span className={styles["error"]}>{errorMessage}</span> : false}
        </div>
    );
};

export default Select;
