import { ChangeEvent, DetailedHTMLProps, FC, InputHTMLAttributes } from "react";

import { CloseSquareIcon } from "@/icons/CloseSquareIcon";
import { SearchIcon } from "@/icons/SearchIcon";

import styles from "./SearchInput.module.scss";

interface SearchInputProps extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    fullWidth?: boolean;
    resetInputField?: () => void;
    updateInputValue?: (value: string) => void;
}

export const SearchInput: FC<SearchInputProps> = ({
    fullWidth,
    id,
    name,
    onKeyDown,
    placeholder,
    resetInputField,
    updateInputValue,
    value,
    ...restProps
}: SearchInputProps) => {
    const updateValue = (event: ChangeEvent<HTMLInputElement>) => {
        updateInputValue?.(event.target.value);
    };

    return (
        <div className={styles["search-wrapper"]}>
            <input
                onChange={updateValue}
                className={`${styles["search-input"]} ${fullWidth ? styles.full : ""}`}
                type="text"
                id={id}
                name={name}
                placeholder={placeholder}
                value={value}
                onKeyDown={onKeyDown}
                {...restProps}
            />
            {value ? (
                <div className={`${styles["icon-frame"]} ${styles["icon-frame-right"]}`}>
                    <button onClick={resetInputField} data-testid="reset-button">
                        <CloseSquareIcon color="gray-middle" className={styles["close-icon"]} />
                    </button>
                </div>
            ) : (
                <div className={`${styles["icon-frame"]} ${styles["icon-frame-right"]}`}>
                    <SearchIcon color="gray-middle" className={styles["search-icon"]} />
                </div>
            )}
        </div>
    );
};
