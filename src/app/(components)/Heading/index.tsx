/* eslint-disable react/display-name */
import { DetailedHTMLProps, forwardRef, HTMLAttributes } from "react";

import styles from "./Heading.module.scss";

type ResponsiveTags = "h2-h1" | "h3-h2" | "h4-h3" | "h5-h4";
type Tags = "h1" | "h2" | "h3" | "h4" | "h5";

type Type = Tags | ResponsiveTags;

interface HeadingProps extends DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement> {
    tag?: Tags;
    type?: Type;
}

export const Heading = forwardRef<HTMLHeadingElement, HeadingProps>(
    ({ tag = "h1", type = tag as Type, className, ...restProps }, ref) => {
        const Tag = tag;
        return <Tag className={`${styles.heading} ${styles[type]} ${className ?? ""}`} {...restProps} ref={ref} />;
    }
);
