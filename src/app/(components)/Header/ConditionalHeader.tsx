"use client";

import { usePathname, useSearchParams } from "next/navigation";

import Header from "@/components/Header";
import { UserProfileType } from "@/types/UserProfileType";

import Footer from "../Footer";

type HeaderProps = {
    user?: UserProfileType;
};

export const ConditionalHeader = ({ user }: HeaderProps) => {
    const pathname = usePathname();
    const paramsNotFound = useSearchParams().get("notFound");

    const isDashboard = pathname.startsWith("/dashboard/");
    const isCreateMetaDataPage = pathname === "/create-metadata";
    const isEditMetadataPage = pathname === "/edit-metadata";

    const headerProps = isCreateMetaDataPage || isEditMetadataPage ? {} : { user };

    return <>{(paramsNotFound || !isDashboard) && <Header {...headerProps} />}</>;
};

export const ConditionalFooter = () => {
    const pathname = usePathname();

    const isDashboard = pathname.startsWith("/dashboard/");

    return <>{!isDashboard && <Footer />}</>;
};
