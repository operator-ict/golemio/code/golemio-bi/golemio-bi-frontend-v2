"use client";
/* eslint-disable react/display-name */
import { usePathname } from "next/navigation";
import React, { useEffect, useState } from "react";

import Button from "@/components/Button";
import { ArrowLeftIcon } from "@/components/icons/ArrowLeftIcon";
import { Logo } from "@/components/Logo";
import { LogoDashboard } from "@/components/LogoDashboard";
import { Title } from "@/components/Title";
import { usePermissions } from "@/context/PermissionsContext";
import SaveIcon from "@/icons/formIcons/SaveIcon";
import SubmitIcon from "@/icons/formIcons/SubmitIcon";
import text from "@/textContent/cs.json";
import { UserProfileType } from "@/types/UserProfileType";

import { HeaderUser } from "../HeaderUser";
import { NavBarAdmin } from "../NavBarAdmin";
import styles from "./Header.module.scss";

type HeaderProps = {
    user?: UserProfileType;
};

const Header = ({ user }: HeaderProps) => {
    const [isShrunk, setShrunk] = useState(false);
    const pathname = usePathname();
    const isEditPage = pathname.startsWith("/edit-metadata");
    const isMetadataPage = pathname === "/create-metadata";
    const isAdminGroupMember = usePermissions().canViewAdminDashboards;

    useEffect(() => {
        const handler = () => {
            setShrunk((isShrunk) => {
                if (!isShrunk && (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50)) {
                    return true;
                }

                if (isShrunk && document.body.scrollTop < 4 && document.documentElement.scrollTop < 4) {
                    return false;
                }

                return isShrunk;
            });
        };

        window.addEventListener("scroll", handler);
        return () => window.removeEventListener("scroll", handler);
    }, []);

    return !isMetadataPage && !isEditPage ? (
        <header className={`${styles.header} ${isShrunk ? styles["header_scrolled"] : ""}`}>
            <div className={styles["headers-block"]}>
                <Logo alt={`Logo for Golemio`} src={""} className={styles["main-logo"]} />
                {isAdminGroupMember && <NavBarAdmin />}
            </div>
            {user && <HeaderUser user={user} />}
        </header>
    ) : (
        <header className={`${styles.header} ${styles["header-meta"]} ${isShrunk ? styles["header_scrolled"] : ""}`}>
            <LogoDashboard className={styles["main-logo"]} alt={`logo Golemio`} src={""} />
            <div className={styles["title-block"]}>
                <Button
                    className={styles["button-back"]}
                    url="/"
                    label="Zpět"
                    color="tertiary"
                    iconStart={<ArrowLeftIcon color="gray-dark" />}
                />
                <span className={styles["title-heading"]}>
                    {isMetadataPage ? (
                        <Title type="t1m">{text.header.createMetadataPageTitle}</Title>
                    ) : (
                        <Title type="t1m">{text.header.editMetadataPageTitle}</Title>
                    )}
                </span>
            </div>
            <div className={styles["button-container"]}>
                <Button
                    label={isMetadataPage ? text.formComponent.submit.create : text.formComponent.submit.edit}
                    color="primary"
                    type="submit"
                    form="metadataForm"
                    iconStart={
                        isMetadataPage ? <SubmitIcon color="white" width="1rem" height="1rem" /> : <SaveIcon color="white" />
                    }
                />
            </div>
        </header>
    );
};

export default Header;
