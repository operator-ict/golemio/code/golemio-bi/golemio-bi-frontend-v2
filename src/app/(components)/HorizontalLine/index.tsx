import React from "react";

import styles from "./HorizontalLine.module.scss";

const HorizontalLine = () => {
    return <hr className={styles.hr} />;
};

export default HorizontalLine;
