"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";
import React from "react";

import { usePermissions } from "@/context/PermissionsContext";

import styles from "./NavbarAdmin.module.scss";

export const NavBarAdmin = () => {
    const pathname = usePathname();
    const canEditUsersRole = usePermissions().canEditUsersRole;
    return (
        <nav className={styles.navbar}>
            <ul className={`${styles["headers-list"]}`}>
                <li>
                    <Link href="/" className={pathname === "/" ? styles.active : ""}>
                        Úvod
                    </Link>
                </li>
                {canEditUsersRole && (
                    <li>
                        <Link href={"/users"} className={pathname.startsWith("/users") ? styles.active : ""}>
                            Uživatelé
                        </Link>
                    </li>
                )}
                {/* <li>
                    <Link href="">Dashboardy</Link>
                </li> */}
                <li>
                    <Link href="/roles" className={pathname.startsWith("/roles") ? styles.active : ""}>
                        Role
                    </Link>
                </li>
            </ul>
        </nav>
    );
};
