/* eslint-disable react/display-name */
import Image, { ImageProps } from "next/image";
import Link from "next/link";
import React, { forwardRef } from "react";

import logoPGolemio from "@/logos/golemioLogoSmall.svg";
import text from "@/textContent/cs.json";

import styles from "./LogoDashboard.module.scss";

type LogoProps = ImageProps & {
    footer?: boolean;
};

export const LogoDashboard = forwardRef<HTMLDivElement, LogoProps>(() => {
    return (
        <Link href="/" className={`${styles["frame"]}`}>
            <span className={`${styles["logo-frame"]} `}>
                <Image src={logoPGolemio} alt={`${text.logo}`} fill />
            </span>
        </Link>
    );
});
