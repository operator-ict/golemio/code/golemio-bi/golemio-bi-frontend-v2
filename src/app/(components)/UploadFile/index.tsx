"use client";
import Image from "next/image";
import Link from "next/link";
import React, { ChangeEvent, DragEvent, FC, useEffect, useRef, useState } from "react";

import Button from "@/components/Button";
import { Paragraph } from "@/components/Paragraph";
import { Title } from "@/components/Title";
import { UploadIcon } from "@/icons/formIcons/UploadIcon";
import AddFile from "@/images/AddFile.png";
import DropFile from "@/images/DropFile.png";
import ErrorFile from "@/images/ErrorFile.png";
import uploadFileIcon from "@/images/uploadfileIcon.png";
import text from "@/textContent/cs.json";
import { THUMBNAIL_RATIO_HEIGHT, THUMBNAIL_RATIO_WIDTH } from "@/utils/imageRatio";

import styles from "./UploadFile.module.scss";

interface UploadFileProps {
    onFileSelected: (file: File | null, imageUrl: string) => void;
    thumbnailUrl?: string;
    thumbnailSize?: number;
    error?: boolean;
    errorMessage?: string;
}

const ThumbnailImage: FC<{ src: string }> = ({ src }) => (
    <span className={styles["upload-file-thumbnail"]}>
        <Image src={src} fill sizes="(max-width: 576px) 100vw, (max-width: 992px) 33vw, 25vw" alt="dashboard thumbnail" />
    </span>
);

const UploadFile: FC<UploadFileProps> = ({ onFileSelected, thumbnailUrl, thumbnailSize, error, errorMessage }) => {
    const [file, setFile] = useState<File | null>();
    const [fileUrl, setFileUrl] = useState<string>(thumbnailUrl || "");
    const [loading] = useState(false);
    const fileInputRef = useRef<HTMLInputElement>(null);

    const handleFileChange = (event: ChangeEvent<HTMLInputElement>) => {
        const selectedFile = event.target.files?.[0];
        setFile(selectedFile || null);
    };

    const handleDrop = (event: DragEvent<HTMLDivElement>) => {
        event.preventDefault();
        const droppedFile = event.dataTransfer.files?.[0];
        setFile(droppedFile || null);
    };

    const handleRemoveFile = () => {
        setFile(null);
        setFileUrl("");
        onFileSelected(null, "");
    };

    useEffect(() => {
        if (file) {
            onFileSelected(file, URL.createObjectURL(file));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [file]);

    const fileSize = file?.size ? file.size : thumbnailSize;

    const onButtonClick = () => {
        if (fileInputRef.current) {
            fileInputRef.current.click();
        }
    };

    return (
        <div className={styles["upload-file-container"]}>
            <Paragraph type="p2" className={styles["upload-file-title"]}>
                {text.uploadFile.miniature}
            </Paragraph>
            {file || fileUrl ? (
                <div className={`${styles["upload-file-item"]} ${error ? styles["upload-file-item-error"] : ""}`}>
                    {!file && fileUrl && <ThumbnailImage src={fileUrl} />}
                    {file && <ThumbnailImage src={URL.createObjectURL(file)} />}
                    <div className={styles["upload-file-info"]}>
                        {error ? <Image src={ErrorFile} alt="" /> : <Image src={AddFile} width={32} height={32} alt={""} />}
                        <div className={styles["upload-file-detail"]}>
                            <span className={styles["upload-file-name"]}>
                                {file?.name ?? text.uploadFile.detail.originalImage}
                            </span>
                            {error ? (
                                <p role="alert" data-testid="error">
                                    {errorMessage}
                                </p>
                            ) : null}
                            {fileSize && (
                                <span className={styles["upload-file-size-info"]}>{`${Math.round(fileSize / 1024)} KB`}</span>
                            )}
                        </div>
                        <div className={styles["upload-file-actions"]}>
                            <Button color="tertiary" type="button" aria-label="Upload file" size="sm" onClick={onButtonClick}>
                                <UploadIcon color="secondary" width="14px" height="10px" />
                                <span className={styles["upload-file-actions-text"]}>
                                    {text.uploadFile.detail.uploadDifferentImage}
                                </span>
                            </Button>
                            <label htmlFor="thumbnail" className={styles["hidden"]}>
                                <input
                                    type="file"
                                    hidden
                                    id="thumbnail"
                                    onChange={handleFileChange}
                                    ref={fileInputRef}
                                    accept="image/png, image/jpeg"
                                    aria-label="Choose a file"
                                    data-testid="file-input"
                                />
                            </label>
                            {loading ? (
                                <div className={styles["loader"]} />
                            ) : (
                                <Button color="link" square size="sm" onClick={handleRemoveFile}>
                                    <Image src={DropFile} alt={text.uploadFile.detail.dropImage} />
                                </Button>
                            )}
                        </div>
                    </div>
                </div>
            ) : (
                <div className={styles["upload-file-input"]} onDrop={handleDrop} onDragOver={(event) => event.preventDefault()}>
                    <input
                        type="file"
                        hidden
                        id="thumbnail"
                        ref={fileInputRef}
                        onChange={handleFileChange}
                        accept="image/png, image/jpeg"
                        data-testid="file-input"
                    />
                    <label htmlFor="thumbnail" className={styles["upload-file-input-label"]}>
                        <Image src={uploadFileIcon} alt={""} />
                    </label>
                    <div className={styles["upload-file-subtitle"]}>
                        <span className={styles["upload-file-subtitle-first"]}>
                            <Title type="t3m" className={styles["upload-file-subtitle-first"]}>
                                <Link
                                    href=""
                                    onClick={() => fileInputRef.current?.click()}
                                    className={styles["upload-file-subtitle-first-link"]}
                                >
                                    {text.uploadFile.subTitle.link}
                                </Link>
                                {text.uploadFile.subTitle.paragraph_1}
                            </Title>
                        </span>
                        <span>
                            {`${text.uploadFile.subTitle.paragraph_2}
                        ${Math.round((THUMBNAIL_RATIO_WIDTH / THUMBNAIL_RATIO_HEIGHT) * 10) / 10}
                        (${THUMBNAIL_RATIO_WIDTH} : ${THUMBNAIL_RATIO_HEIGHT})`}
                        </span>
                    </div>
                </div>
            )}
        </div>
    );
};

export default UploadFile;
