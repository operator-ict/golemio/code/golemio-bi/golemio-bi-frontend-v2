"use client";
/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React, { DetailedHTMLProps, HTMLAttributes, ReactNode, useRef } from "react";
import { createPortal } from "react-dom";

import Button from "@/components/Button";
import { Title } from "@/components/Title";
import { useIsMounted } from "@/hooks/useIsMounted";
import useKeyListener from "@/hooks/useKeyListener";
import useTabKeyHandler from "@/hooks/useTabKeyHandler";
import DeleteChipIcon from "@/icons/formIcons/DeleteChipIcon";
import text from "@/textContent/cs.json";

import styles from "./Modal.module.scss";

interface Props extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    show: boolean;
    onClose?: () => void;
    label?: string;
    children: ReactNode;
}

export const Modal = ({ show, onClose, children, label }: Props) => {
    const isMounted = useIsMounted();

    const modalRef = useRef<HTMLDivElement>(null);

    const handleTabKey = useTabKeyHandler(
        modalRef,
        'a[href], button, textarea, input[type="text"], input[type="radio"], input[type="checkbox"], select'
    );

    const keyListenersMap = new Map<string, (e: KeyboardEvent) => void | undefined>([
        ["Escape", () => onClose?.()],
        ["Tab", handleTabKey],
    ]);

    useKeyListener(keyListenersMap);

    const modalContent = show ? (
        <div aria-hidden={!show} aria-describedby="modalDescription" className={styles["modal-entity"]} role="dialog">
            <div className={styles["overlay"]}></div>
            <div className={styles["modal"]} ref={modalRef}>
                <div className={styles["screenreader-text"]} id="modalDescription">
                    Toto je modální okno. Okno zavřete pomocí tlačítka Zavřít nebo klávesou Escape.
                </div>
                <div className={styles["modal-header"]}>
                    <Title type="t1m">{label}</Title>
                    {onClose && (
                        <Button
                            label={text.button.close}
                            hideLabel
                            size="sm"
                            circle
                            onClick={() => onClose()}
                            color="tertiary"
                            className={styles["modal-close-button"]}
                            iconStart={<DeleteChipIcon color="secondary" width="1rem" height="1rem" />}
                        />
                    )}
                </div>
                <div className={styles["modal-body"]}>{children}</div>
            </div>
        </div>
    ) : null;

    if (typeof window === "undefined") {
        return null;
    }

    const portal = document.getElementById("portal") as Element;

    return isMounted && portal ? createPortal(modalContent, portal) : null;
};
