"use client";
import { usePlausible } from "next-plausible";
import React, { useState } from "react";

import ListItemComponent from "@/components/ListItemComponent";
import { SearchInput } from "@/components/SearchInput";
import { useEnv } from "@/context/EnvProvider";
import { useComponentVisible } from "@/hooks/useComponentVisible";
import { ChevronIcon } from "@/icons/ChevronIcon";
import { IMetadata } from "@/types/MetadataType";
import { joinUrlPaths } from "@/utils/joinUrlPaths";

import styles from "./SearchableDropdown.module.scss";

type SearchableDropdownProps = {
    className?: string;
    options: IMetadata[] | undefined;
    label?: string;
    id: string;
    selectedVal?: IMetadata | null;
    handleChange: (value: IMetadata | null) => void;
    fullWidth?: boolean;
    title?: string;
    image?: boolean;
};

const SearchableDropdown = ({ options = [], id, title, selectedVal, handleChange, image }: SearchableDropdownProps) => {
    const [query, setQuery] = useState<string>("");
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const [titleValue, setTitleValue] = useState<string | undefined>(title);

    const filteredOptions = Array.isArray(options)
        ? options.filter((option) => option.title.toLowerCase().includes(query.toLowerCase()))
        : [];

    const plausible = usePlausible();
    const ApiUrl = useEnv()?.API_URL;

    const selectOption = (option: IMetadata) => {
        setQuery("");
        handleChange(option);
        setTitleValue(option.title);
        setIsOpen(!isOpen);
        plausible("trackSelectDashboard-title", {
            props: {
                selectDashboard: option.title,
            },
        });
    };

    const getDisplayValue = () => {
        if (query) return query;
        if (selectedVal) return selectedVal.title as string;

        return "";
    };

    const handleOpen = () => {
        setIsOpen(!isOpen);
    };

    const resetInputField = () => {
        setQuery("");
    };

    const handleVisible = () => {
        setIsOpen(!isOpen);
    };

    const { ref } = useComponentVisible(handleVisible);

    return (
        <div className={styles["searchable-container"]}>
            <button
                className={styles["searchable-dropdown-button"]}
                onClick={handleOpen}
                title={isOpen ? "Zavřít dropdown" : "Otevřít dropdown"}
            >
                <span className={styles["searchable-dropdown-button-text"]}>{titleValue}</span>
                {isOpen ? (
                    <ChevronIcon color="gray-dark" width={7} direction="up" />
                ) : (
                    <ChevronIcon color="gray-dark" width={7} direction="down" />
                )}
            </button>
            {isOpen && (
                <div className={styles["searchable-wrapper"]} ref={ref}>
                    <div className={styles["searchable-dropdown"]}>
                        <SearchInput
                            resetInputField={resetInputField}
                            placeholder="Vyhledat dashboard"
                            onChange={(e) => {
                                setQuery(e.target.value);
                                handleChange(null);
                                plausible("trackSelectDashboard-searchValue", {
                                    props: {
                                        searchValue: e.target.value,
                                    },
                                });
                            }}
                            value={getDisplayValue()}
                        />
                        <ul>
                            {filteredOptions?.map((option, index) => (
                                <ListItemComponent
                                    key={`${id}-${index}`}
                                    selectOption={selectOption}
                                    link={joinUrlPaths("/dashboard", option.client_route)}
                                    ImageSrc={
                                        option.thumbnail && ApiUrl
                                            ? joinUrlPaths(ApiUrl.toString(), option.thumbnail.url)
                                            : "/assets/images/placeholder.webp"
                                    }
                                    option={option}
                                    image={image}
                                />
                            ))}
                        </ul>
                    </div>
                </div>
            )}
        </div>
    );
};

export default SearchableDropdown;
