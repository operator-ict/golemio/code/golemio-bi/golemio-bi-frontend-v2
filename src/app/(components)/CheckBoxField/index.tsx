"use client";
import React, { DetailedHTMLProps, InputHTMLAttributes, ReactNode, useEffect } from "react";

import styles from "./CheckBoxField.module.scss";

type CheckBoxFieldProps = DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & {
    id: string;
    label: string | ReactNode;
    isChecked: boolean;
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    error?: boolean;
};

const CheckBoxField = ({ id, label, isChecked, onChange, error }: CheckBoxFieldProps) => {
    const [checked, setChecked] = React.useState(isChecked);

    useEffect(() => {
        setChecked(isChecked);
    }, [isChecked]);

    const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        setChecked(e.target.checked);
        onChange(e);
    };

    return (
        <label htmlFor={id} className={styles["checkbox-wrapper"]}>
            <input
                type="checkbox"
                id={id}
                checked={checked}
                onChange={changeHandler}
                className={`${styles["check-box"]} ${error ? styles["check-box__invalid"] : ""}`}
            />
            <span className={`${styles["label-content"]} ${error ? styles["error"] : ""}`}>{label}</span>
        </label>
    );
};

export default CheckBoxField;
