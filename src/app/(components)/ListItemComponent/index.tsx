"use client";
import Image from "next/image";
import Link from "next/link";
import React, { ReactNode } from "react";

import { IMetadata } from "@/(types)/MetadataType";

import styles from "./ListItemComponent.module.scss";
type ListItemProps = {
    selectOption?: (option: IMetadata) => void;
    option: IMetadata;
    link: string;
    ImageSrc: string;
    children?: ReactNode;
    image?: boolean;
};

const ListItemComponent = ({ selectOption, option, link, ImageSrc, image }: ListItemProps) => {
    const handleClick = () => {
        if (selectOption) {
            selectOption(option);
        }
    };
    return (
        <li className={styles["list-item-container"]} onClick={handleClick}>
            <Link href={link}>
                {image && <Image src={ImageSrc} alt="data thumbnail" width={54} height={30} />}
                {option.title}
            </Link>
        </li>
    );
};

export default ListItemComponent;
