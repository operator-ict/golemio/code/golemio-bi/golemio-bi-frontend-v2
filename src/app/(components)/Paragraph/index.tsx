/* eslint-disable react/display-name */
import { DetailedHTMLProps, forwardRef, HTMLAttributes } from "react";

import styles from "./Paragraph.module.scss";

interface ParagraphProps extends DetailedHTMLProps<HTMLAttributes<HTMLParagraphElement>, HTMLParagraphElement> {
    type: "p1" | "p2" | "p3" | "p4" | "c1" | "c1m" | "c2" | "c2m" | "c3" | "c3m";
    centered?: boolean;
    className?: string;
}

export const Paragraph = forwardRef<HTMLParagraphElement, ParagraphProps>(
    ({ type = "p2", centered, className, ...restProps }, ref) => {
        return (
            <p
                className={`${styles.paragraph} ${styles[`paragraph__${type}`]} ${centered ? styles.centered : ""} ${
                    className ?? ""
                }`}
                {...restProps}
                ref={ref}
            />
        );
    }
);
