import React, { FC } from "react";
interface Option {
    value: string;
    label: string;
}

interface FilterSelectProps {
    value: string;
    onChange: (selectedValue: string) => void;
    options: Option[];
    placeholder: string;
}
const FilterSelect: FC<FilterSelectProps> = ({ value, onChange, options, placeholder }) => {
    const handleSelect = (selectedValue: string) => {
        onChange(selectedValue);
    };

    return (
        <div className="custom-select">
            <div className="dropdown-contents">
                <select
                    value={value || "all"}
                    onChange={(e) => handleSelect(e.target.value)}
                    style={{ width: "100%", maxWidth: "100%" }}
                >
                    <option value="all">{placeholder}</option>
                    {options.map((option) => (
                        <option value={option.value} key={option.value}>
                            {option.label}
                        </option>
                    ))}
                </select>
            </div>
        </div>
    );
};

export default FilterSelect;
