/* eslint-disable react/display-name */
"use client";
import { useRouter } from "next/navigation";
import React, { ButtonHTMLAttributes, DetailedHTMLProps, forwardRef, ReactNode, Ref } from "react";

import styles from "./Button.module.scss";

export type Colors = "primary" | "secondary" | "tertiary" | "outline" | "link";

type Sizes = "sm" | "md" | "lg" | "sm-md" | "md-lg";

interface ButtonProps extends DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> {
    label?: string;
    color: Colors;
    size?: Sizes;
    iconStart?: ReactNode;
    iconEnd?: ReactNode;
    hideLabelMobile?: boolean;
    hideLabelTablet?: boolean;
    hideLabel?: boolean;
    url?: string;
    square?: boolean;
    circle?: boolean;
}

const Button = forwardRef<HTMLButtonElement, ButtonProps>((componentProps: ButtonProps, ref: Ref<HTMLButtonElement>) => {
    const {
        className,
        color,
        size = "md",
        children,
        label,
        iconStart,
        iconEnd,
        onClick,
        type = "button",
        form,
        disabled,
        hideLabelMobile,
        hideLabelTablet,
        hideLabel,
        id,
        url,
        square,
        circle,
    } = componentProps;

    const router = useRouter();

    return (
        <button
            ref={ref}
            onClick={url ? () => router.push(url) : onClick}
            type={type}
            form={form}
            className={`${styles["button"]} ${styles[`button-color_${color}`]} ${styles[`button-size_${size}`]} ${
                square ? styles["button-square"] : ""
            } ${circle ? styles["button-circle"] : ""} ${className}`}
            disabled={disabled}
            aria-label={label}
            id={id}
        >
            {iconStart && <span className={`${styles["icon-frame"]} ${styles[`icon-frame_${size}`]}`}>{iconStart}</span>}
            {!hideLabel && label && (
                <span
                    className={`${hideLabelMobile ? styles["hide-label"] : ""}
                    ${hideLabelTablet ? styles["hide-label__tablet"] : ""} ${styles["label"]}`}
                >
                    {label}
                </span>
            )}
            {children}
            {iconEnd && <span className={`${styles["icon-frame"]} ${styles[`icon-frame_${size}`]}`}>{iconEnd}</span>}
        </button>
    );
});

export default Button;
