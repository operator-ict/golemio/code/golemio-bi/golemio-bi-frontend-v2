import React from "react";

import styles from "./Spinner.module.scss";

export const Spinner = () => {
    return (
        <div className={styles["spinner-container"]}>
            <div className={styles["background-circle"]} />
            <div className={styles["spinner"]}>
                <div className={styles["right-side"]}>
                    <div className={styles["bar"]} />
                </div>
                <div className={styles["left-side"]}>
                    <div className={styles["bar"]} />
                </div>
            </div>
        </div>
    );
};
