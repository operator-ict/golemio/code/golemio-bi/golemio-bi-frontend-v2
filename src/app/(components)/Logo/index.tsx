/* eslint-disable react/display-name */
import Image, { ImageProps } from "next/image";
import Link from "next/link";
import React, { DetailedHTMLProps, FC, HTMLAttributes } from "react";

import logoPGolemio from "@/logos/standard.svg";
import text from "@/textContent/cs.json";

import styles from "./Logo.module.scss";

type LogoProps = ImageProps & DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>;

export const Logo: FC<LogoProps> = ({ className }) => {
    return (
        <Link href="/" className={`${styles["frame"]}`}>
            <span className={`${styles["logo-frame"]} ${className}`}>
                <Image src={logoPGolemio} alt={`${text.logo}`} fill />
            </span>
        </Link>
    );
};
