/* eslint-disable max-len */
"use client";

import "vanilla-cookieconsent/dist/cookieconsent.css";

import Link from "next/link";
import { FC, useCallback, useEffect, useState } from "react";
import * as CookieConsent from "vanilla-cookieconsent";

declare global {
    interface Window {
        dataLayer: any[];
    }
}

export const CookieBanner: FC<{ className?: string }> = ({ className }) => {
    const [isInitialized, setIsInitialized] = useState(false);

    const updateConsentPreferences = useCallback(() => {
        try {
            const cookieName = "golemiobi-cookieconsent-preferences";
            const rawCookie = localStorage.getItem(cookieName);
            if (!rawCookie) return;

            const cookieData = JSON.parse(rawCookie);
            cookieData.categories = {
                ...cookieData.categories,
                analytics: true,
                analytics_storage: true,
            };
            cookieData.acceptedCategories = Array.from(
                new Set([...(cookieData.acceptedCategories || []), "analytics", "analytics_storage"])
            );

            localStorage.setItem(cookieName, JSON.stringify(cookieData));
            window.dispatchEvent(new Event("storage"));
        } catch (e) {
            console.error("[CookieBanner] Error updating consent preferences:", e);
        }
    }, []);

    useEffect(() => {
        if (isInitialized || typeof CookieConsent === "undefined") return;

        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            event: "consent_initialization",
            consent_mode: {
                analytics_storage: "denied",
                ad_storage: "denied",
                functionality_storage: "granted",
                security_storage: "granted",
            },
        });

        CookieConsent.run({
            cookie: { name: "golemiobi-cookieconsent", domain: window.location.hostname, expiresAfterDays: 182 },
            categories: {
                functional_storage: { enabled: true, readOnly: true },
                analytics: { enabled: false },
            },
            guiOptions: {
                consentModal: { layout: "bar", position: "bottom center", equalWeightButtons: true },
                preferencesModal: { layout: "box", equalWeightButtons: true },
            },
            language: {
                default: "cs",
                translations: {
                    cs: {
                        consentModal: {
                            title: "Používáme cookies",
                            description:
                                "Díky využití souborů cookies zjišťujeme, co je pro uživatele zajímavé. Analýza návštěvnosti nám pomáhá web neustále zlepšovat. Nepoužíváme cookies k marketingovým účelům, ani je nepředáváme nikomu dalšímu.",
                            acceptAllBtn: "Povolit vše",
                            showPreferencesBtn: "Nastavení cookies",
                        },
                        preferencesModal: {
                            title: "Přizpůsobit nastavení cookies",
                            savePreferencesBtn: "Uložit nastavení",
                            acceptAllBtn: "Povolit vše",
                            acceptNecessaryBtn: "Povolit nezbytné",
                            closeIconLabel: "Zavřít",
                            sections: [
                                {
                                    title: "Nezbytně nutné cookies",
                                    description:
                                        "Tyto cookies pomáhají, aby webová stránka byla použitelná a fungovala správně. Ve výchozím nastavení jsou povoleny a nelze je zakázat.",
                                    linkedCategory: "functional_storage",
                                },
                                {
                                    title: "Statistika",
                                    description:
                                        "Díky těmto cookies víme, kam u nás chodíte nejraději a máme statistiky o celkové návštěvnosti našich stránek.",
                                    linkedCategory: "analytics",
                                },
                            ],
                        },
                    },
                },
            },
        });

        const handleConsent = () => window.dataLayer.push({ event: "consentChoice", consentType: "update" });
        const handleAccept = () => {
            window.dataLayer.push({ event: "consentChoice", consentType: "accept_all" });
            setTimeout(updateConsentPreferences, 200);
        };
        const handleChange = () => window.dataLayer.push({ event: "consentUpdated", consentType: "settings_changed" });

        document.addEventListener("cc:onConsent", handleConsent);
        document.addEventListener("cc:onAccept", handleAccept);
        document.addEventListener("cc:onChange", handleChange);

        setIsInitialized(true);

        return () => {
            document.removeEventListener("cc:onConsent", handleConsent);
            document.removeEventListener("cc:onAccept", handleAccept);
            document.removeEventListener("cc:onChange", handleChange);
        };
    }, [isInitialized, updateConsentPreferences]);

    return (
        <Link href="#" data-cc="show-consentModal" className={className}>
            Cookies
        </Link>
    );
};
