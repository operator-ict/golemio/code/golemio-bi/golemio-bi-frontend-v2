"use client";
import React, { FC } from "react";

import { ResetIcon } from "../icons/ResetIcon";
import styles from "./FilterReset.module.scss";

interface FilterReset {
    onClick: () => void;
}

export const FilterReset: FC<FilterReset> = ({ onClick }) => {
    return (
        <div className={styles["title-box"]}>
            <button type="button" onClick={onClick}>
                <ResetIcon />
            </button>
        </div>
    );
};
