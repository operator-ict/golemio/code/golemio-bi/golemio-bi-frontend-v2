import { signOut } from "next-auth/react";
import React, { useState } from "react";

import { Dropdown } from "@/components/Dropdown";
import { Title } from "@/components/Title";
import { ChevronIcon } from "@/icons/ChevronIcon";
import { SignOutIcon } from "@/icons/SignOutIcon";
import text from "@/textContent/cs.json";
import { UserProfileType } from "@/types/UserProfileType";
import { resetAgreementCookie } from "@/utils/cookies";

import styles from "./HeaderUser.module.scss";

type HeaderUserProps = {
    user?: UserProfileType;
};

export const HeaderUser = ({ user }: HeaderUserProps) => {
    const [visible, setVisible] = useState(false);
    const username = user?.name && user?.surname ? `${user.name} ${user.surname}` : user?.email;

    const handleDropdown = () => {
        setVisible(!visible);
    };

    const handleLogout = () => {
        resetAgreementCookie();
        signOut({ callbackUrl: "/auth/sign-in?flow=refresh" });
    };

    const userNavContent = (
        <ul className={styles["nav-user-dropdown"]}>
            <li className={styles["nav-user-dropdown-item"]}>
                <button onClick={handleLogout} className={styles["nav-user-button"]}>
                    <SignOutIcon color="error" />
                    {text.auth.signOut}
                </button>
            </li>
        </ul>
    );

    const isEmail = username?.includes("@");

    return (
        <Dropdown contents={userNavContent} visible={visible} hide={handleDropdown} className={styles["nav-user"]}>
            <button className={styles["nav-user-button"]} onClick={handleDropdown}>
                <Title type="t3m" className={isEmail ? styles["nav-user-email"] : styles["nav-user-title"]}>
                    {username}
                </Title>
                <ChevronIcon color="gray-dark" height={12} direction={visible ? "up" : "down"} />
            </button>
        </Dropdown>
    );
};
