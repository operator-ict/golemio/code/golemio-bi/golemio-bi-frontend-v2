"use client";
import React, { ChangeEvent, DetailedHTMLProps, InputHTMLAttributes, useEffect, useState } from "react";

import styles from "./CheckBoxWithSubLabel.module.scss";

interface InputElementProps extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    label?: string;
    subLabel?: string;
    onClick: () => void;
    activeUser?: boolean;
}

export const CheckBoxsubLabel = ({ checked, id, label, subLabel, name, onClick, readOnly, activeUser }: InputElementProps) => {
    const defaultChecked = checked ? checked : false;
    const [isChecked, setIsChecked] = useState(defaultChecked);

    const changeHandler = (e: ChangeEvent<HTMLInputElement>) => {
        setIsChecked(e.target.checked);
        onClick();
    };

    useEffect(() => {
        if (!checked) {
            setIsChecked(false);
        }
    }, [checked]);

    return (
        <li className={styles["checkbox-wrapper"]}>
            <label htmlFor={id} className={`${styles["label"]} ${readOnly ? styles["label__disabled"] : ""}`}>
                <div className={styles["label-container"]}>
                    <input type="checkbox" id={id} name={name} checked={isChecked} onChange={changeHandler} disabled={readOnly} />
                    <div className={styles["label-container__content"]}>
                        <span className={styles["label-container__label"]}>{label}</span>
                        {subLabel && (
                            <div className={styles["label-container__subLabel"]}>
                                <span>{subLabel}</span>
                            </div>
                        )}
                    </div>
                </div>
                {!activeUser && <span className={styles["label-container__nonactive"]}>Neaktivní uživatel</span>}
            </label>
        </li>
    );
};
