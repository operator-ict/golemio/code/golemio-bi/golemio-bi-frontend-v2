import React, { DetailedHTMLProps, FC, InputHTMLAttributes, ReactNode } from "react";

import styles from "./TextField.module.scss";

interface ITextField extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    label: string;
    hideLabel?: boolean;
    disabled?: boolean;
    error?: boolean;
    errorMessage?: string;
    iconStart?: ReactNode;
    iconEnd?: ReactNode;
    onIconClick?: () => void;
    info?: boolean;
    infoMessage?: string;
}
const TextField: FC<ITextField> = ({
    children,
    placeholder,
    label,
    hideLabel,
    type = "text",
    disabled,
    value,
    iconStart,
    iconEnd,
    onIconClick,
    id,
    onChange,
    onClick,
    autoComplete,
    className,
    error,
    errorMessage,
    info,
    infoMessage,
    ...restProps
}) => {
    return (
        <div className={`${styles["input-wrapper"]} ${disabled ? styles["input-wrapper-disabled"] : ""} ${className}`}>
            <div className={styles["input-field"]}>
                {value && (
                    <label
                        htmlFor={id}
                        className={`${styles["input-label"]} ${!value ? styles["input-label-hidden"] : ""}
                        ${iconStart && styles["input-label-icon-start"]}`}
                    >
                        {!hideLabel ? label : false}
                    </label>
                )}
                <input
                    className={`
                        ${styles["input-area"]}
                        ${(hideLabel || value) && styles["input-area__label-hidden"]}
                        ${error && styles["input-area__error"]}
                        ${iconStart && styles["input-area__icon-start"]}
                        ${value && styles["input-area__with-value"]}
                    `}
                    placeholder={placeholder}
                    value={value}
                    id={id}
                    onChange={onChange}
                    type={type}
                    disabled={disabled}
                    onClick={onClick}
                    autoComplete={autoComplete}
                    readOnly={disabled}
                    aria-label={hideLabel ? label : ""}
                    {...restProps}
                />
                {iconStart && <span className={`${styles["input-icon"]} ${styles["input-icon-start"]}`}>{iconStart}</span>}
                {children}
                {iconEnd && (
                    <span className={`${styles["input-icon"]} ${styles["input-icon-end"]}`} onClick={onIconClick}>
                        {iconEnd}
                    </span>
                )}
            </div>
            {info ? <span className={styles["info"]}>{infoMessage}</span> : false}
            {error ? <span className={styles["error"]}>{errorMessage}</span> : false}
        </div>
    );
};
export default TextField;
