import React, { ChangeEvent, DetailedHTMLProps, FC, InputHTMLAttributes, ReactNode, useState } from "react";

import { useComponentVisible } from "@/hooks/useComponentVisible";
import useKeyListener from "@/hooks/useKeyListener";
import useTabKeyHandler from "@/hooks/useTabKeyHandler";

import styles from "./TextOptionsField.module.scss";

interface ITextOptionsField extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    label: string;
    hideLabel?: boolean;
    options: string[];
    disabled?: boolean;
    error?: boolean;
    errorMessage?: string;
    iconStart?: ReactNode;
    iconEnd?: ReactNode;
    onIconClick?: () => void;
    info?: boolean;
    infoMessage?: string;
}
const TextOptionsField: FC<ITextOptionsField> = ({
    children,
    options,
    placeholder,
    label,
    hideLabel,
    type = "text",
    disabled,
    value,
    iconStart,
    iconEnd,
    onIconClick,
    id,
    onChange,
    onClick,
    autoComplete,
    className,
    error,
    errorMessage,
    info,
    infoMessage,
    ...restProps
}) => {
    const [inputValue, setInputValue] = useState(value);
    const [filteredOptions, setFilteredOptions] = useState(options);
    const [showDropdown, setShowDropdown] = useState(false);

    const handleVisible = () => {
        setShowDropdown(!showDropdown);
    };
    const { ref } = useComponentVisible<HTMLUListElement>(handleVisible);

    const handleTabKey = useTabKeyHandler(ref, "li");

    const keyListenersMap = new Map<string, (e: KeyboardEvent) => void>([
        ["Escape", () => setShowDropdown(false)],
        ["Tab", handleTabKey],
    ]);

    useKeyListener(keyListenersMap);

    const handleSelectOption = (option: string) => {
        setInputValue(option);
        onChange?.({ target: { value: option } } as ChangeEvent<HTMLInputElement>);
        setShowDropdown(false);
    };

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        setInputValue(e.target.value);
        const filtered = options.filter((option) => option.toLowerCase().includes(e.target.value.toLowerCase()));
        setFilteredOptions(filtered);
        setShowDropdown(true);
        onChange?.(e);
    };

    const handleOptionConfirmation = (e: React.KeyboardEvent, option: string) => {
        if (e.code === "Enter") {
            handleSelectOption(option);
        }
    };

    return (
        <div className={`${styles["input-wrapper"]} ${disabled ? styles["input-wrapper-disabled"] : ""} ${className}`}>
            <div className={styles["input-field"]}>
                {value && (
                    <label
                        htmlFor={id}
                        className={`${styles["input-label"]} ${!value ? styles["input-label-hidden"] : ""}
                        ${iconStart && styles["input-label-icon-start"]}`}
                    >
                        {!hideLabel ? label : false}
                    </label>
                )}
                <input
                    className={`${styles["input-area"]} ${(hideLabel || value) && styles["input-area__label-hidden"]}
                    ${error && styles["input-area__error"]} ${iconStart && styles["input-area__icon-start"]}`}
                    placeholder={placeholder}
                    value={inputValue}
                    id={id}
                    onChange={(e) => handleChange(e)}
                    type={type}
                    disabled={disabled}
                    onClick={(event) => {
                        setShowDropdown(true);
                        onClick?.(event);
                    }}
                    autoComplete={autoComplete}
                    readOnly={disabled}
                    aria-label={hideLabel ? label : ""}
                    {...restProps}
                />
                {iconStart && <span className={`${styles["input-icon"]} ${styles["input-icon-start"]}`}>{iconStart}</span>}
                {children}
                {iconEnd && (
                    <span className={`${styles["input-icon"]} ${styles["input-icon-end"]}`} onClick={onIconClick}>
                        {iconEnd}
                    </span>
                )}
            </div>
            {showDropdown && filteredOptions.length > 0 && (
                <ul id="options" className={styles["dropdown-list"]} ref={ref}>
                    {filteredOptions.map((option) => (
                        <li
                            key={option}
                            onClick={() => handleSelectOption(option)}
                            onKeyDown={(e: React.KeyboardEvent<HTMLLIElement>) => handleOptionConfirmation(e, option)}
                            className={styles["dropdown-list-item"]}
                            tabIndex={1}
                        >
                            {option}
                        </li>
                    ))}
                </ul>
            )}
            {info ? <span className={styles["info"]}>{infoMessage}</span> : false}
            {error ? <span className={styles["error"]}>{errorMessage}</span> : false}
        </div>
    );
};

export default TextOptionsField;
