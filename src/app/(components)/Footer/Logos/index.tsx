import React, { memo } from "react";

import logoGolemio from "@/logos/golemio-white.svg";
import logoOICT from "@/logos/ioct-white.svg";
import logoPrague from "@/logos/prague-white.svg";

import LogoIcon from "../LogoIcon/LogoIcon";
import styles from "./Logos.module.scss";

const logos = [
    { name: "Logo OICT", url: logoOICT, link: "https://operatorict.cz" },
    { name: "Logo Golemio", url: logoGolemio, link: "https://golemio.cz" },
    { name: "Logo Portál Hl.m.P.", url: logoPrague, link: "https://praha.eu" },
];

const Logos = () => {
    return (
        <div className={styles["logos-container"]}>
            {logos.map((icon, i) => {
                return <LogoIcon label={icon.name} url={icon.url} link={icon.link} key={i} />;
            })}
        </div>
    );
};

export default memo(Logos);
