import Image from "next/image";
import Link from "next/link";
import React, { FC, memo } from "react";

type LogoIcon = {
    link: string;
    url: string;
    label: string;
};

const LogoIcon: FC<LogoIcon> = ({ link, url, label }) => {
    return (
        <Link href={link} target="_blank" rel="norefferer noopener">
            <Image src={url} alt={label} />
        </Link>
    );
};

export default memo(LogoIcon);
