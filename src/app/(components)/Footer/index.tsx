"use client";
import Link from "next/link";
import React, { memo } from "react";

import { CookieBanner } from "@/components/CookieConsent";
import text from "@/textContent/cs.json";

import AgreementLinks from "./AgreementLinks";
import styles from "./Footer.module.scss";
import Logos from "./Logos";

const Conditions = () => {
    return (
        <div className={styles.conditions}>
            <Link href="https://api.golemio.cz/v2/docs/openapi" target="_blank">
                {text.footer.api}
            </Link>
            <CookieBanner />
            <AgreementLinks />
        </div>
    );
};

const Footer = () => {
    return (
        <footer className={styles.footer}>
            <div className={styles["container"]}>
                <div className={styles["conditions-container"]}>
                    <span className={styles["text"]}>{text.footer.dataPlatform}</span>
                    <Conditions />
                </div>
                <div className={styles["logo-container"]}>
                    <Logos />
                </div>
            </div>
        </footer>
    );
};

export default memo(Footer);
