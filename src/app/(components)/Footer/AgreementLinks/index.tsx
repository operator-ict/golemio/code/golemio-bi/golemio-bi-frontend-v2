"use client";
import Link from "next/link";
import React, { useState } from "react";

import { ConditionsDialog } from "@/(public)/auth/(components)/ConditionsDialog";
import { PersonalsDialog } from "@/(public)/auth/(components)/PersonalsDialog";

const AgreementLinks = () => {
    const [showConditions, setShowConditions] = useState(false);
    const [showPersonals, setShowPersonals] = useState(false);

    const conditionsModalHandler = () => {
        setShowConditions(true);
    };

    const personalsModalHandler = () => {
        setShowPersonals(true);
    };

    return (
        <>
            <Link href="#" onClick={conditionsModalHandler}>
                Podmínky užití
            </Link>
            <Link href="#" onClick={personalsModalHandler}>
                Zpracování osobních údajů
            </Link>
            <ConditionsDialog show={showConditions} onClose={() => setShowConditions(false)} />
            <PersonalsDialog show={showPersonals} onClose={() => setShowPersonals(false)} />
        </>
    );
};

export default AgreementLinks;
