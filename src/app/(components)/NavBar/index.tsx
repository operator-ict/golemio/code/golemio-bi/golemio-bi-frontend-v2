import Link from "next/link";
import React, { FC, useState } from "react";

import useWindowSize, { ISize } from "@/hooks/useWindowSize";
import text from "@/textContent/cs.json";

import styles from "./NavBar.module.scss";

export const NavBar: FC = () => {
    const size: ISize = useWindowSize();

    const [open, setOpen] = useState(false);

    const toggleHandler = () => {
        setOpen(!open);
    };

    return (
        <nav className={styles.navbar}>
            {size.width && size.width < 991 && (
                <button className={styles["navbar__button"]} onClick={toggleHandler} aria-label={`Menu`} type="button">
                    {/* {open ? < color={`tertiary`} /> : < color={"tertiary"} />} */}

                    <span className={styles["navbar__title"]}>MENU</span>
                </button>
            )}
            <div className={`${styles["navbar__menu"]} ${open ? styles["open"] : ""}`}>
                <ul>
                    <li>
                        <Link href={`/`} onClick={toggleHandler}>
                            {text.navBar.home}
                        </Link>
                    </li>
                </ul>
            </div>
        </nav>
    );
};
