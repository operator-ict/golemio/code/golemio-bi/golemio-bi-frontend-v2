import React, { DetailedHTMLProps, HTMLAttributes } from "react";

import styles from "./ErrorComponent.module.scss";

type ErrorComponentProps = DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> & {
    message?: string;
    errorStatusCode?: number | string;
};

export interface ErrorStatus {
    status: number | undefined;
    error: string;
}

export const ErrorComponent: React.FC<ErrorComponentProps> = ({
    message = "Something went wrong",
    errorStatusCode,
    ...props
}) => {
    let errorMessage: string;

    switch (errorStatusCode) {
        case 400:
            errorMessage = "Bad Request: The server could not understand the request.";
            break;
        case 401:
            errorMessage = "Unauthorized: Authentication is required and has failed or has not been provided.";
            break;
        case 403:
            errorMessage = "Forbidden: You don't have permission to access the requested resource.";
            break;
        case 500:
            errorMessage = "Internal Server Error: The server has encountered a situation it doesn't know how to handle.";
            break;
        default:
            errorMessage = message;
            break;
    }

    return (
        <div className={styles["error-container"]} {...props}>
            <span className={styles["error-message"]}>{errorMessage}</span>
        </div>
    );
};
