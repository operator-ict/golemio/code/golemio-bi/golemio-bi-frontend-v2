"use client";
/* eslint-disable react/display-name */

import { useRouter } from "next/navigation";
import { usePlausible } from "next-plausible";
import { useEffect, useRef } from "react";

import { FavouritesButton } from "@/(private)/(home)/(FavouritesButton)";
import { LogoDashboard } from "@/components/LogoDashboard";
import { Title } from "@/components/Title";
import { useCount } from "@/context/CountContext";
import { useScrollHandler } from "@/hooks/useScrollHandler ";
import { ArrowLeftIcon } from "@/icons/ArrowLeftIcon";
import { IFeatureCollection } from "@/types/FeaturesTypes";
import { UserProfileResponse } from "@/types/formTypes";

import ButtonLink from "../ButtonLink";
import styles from "../HeaderDashboard/HeaderDashboard.module.scss";
import NumbersOfStations from "../NumberOfStations";

type PageProps = {
    title: string;
    user: UserProfileResponse;
    data?: IFeatureCollection;
    isFavourite?: boolean;
    id: string;
    mapType?: string;
};

const extractEmail = (email: string) => {
    const atIndex = email.indexOf("@");
    if (atIndex !== -1) {
        return email.substring(atIndex + 1);
    }
    return "";
};

const HeaderMaps = ({ title, isFavourite, user, id, mapType }: PageProps) => {
    const isShrunk = useScrollHandler();
    const logoRef = useRef<HTMLDivElement | null>(null);
    const { count, countContainer } = useCount();
    const plausible = usePlausible();
    const router = useRouter();

    useEffect(() => {
        if (typeof window !== "undefined") {
            plausible("trackUsersEvent", {
                props: {
                    userName: user?.name ? [user?.name, user?.surname].filter(Boolean).join(" ") : null,
                    userEmail: user?.email,
                    userDomain: user?.email ? extractEmail(user.email) : null,
                    dashboard: title || "",
                },
            });
        }
    }, [user, title, plausible]);

    return (
        <header className={`${styles.header} ${isShrunk ? styles["header_scrolled"] : ""}`}>
            <LogoDashboard alt="logo Golemio" src="" ref={logoRef} />
            <ButtonLink
                className={styles["button-back"]}
                label="Zpět"
                color="tertiary"
                url="#"
                onMouseDown={(e) => {
                    if (e.button === 1) {
                        window.open("/", "_blank");
                    } else if (e.button === 0) {
                        if (window.history.length > 1) {
                            e.preventDefault();
                            router.back();
                        } else {
                            window.location.href = "/";
                        }
                    }
                }}
                iconStart={<ArrowLeftIcon color="gray-darker" />}
            />
            <div className={styles["header-container"]}>
                <div className={styles["header-container__title"]}>
                    <Title type="t1">{title}</Title>
                    {mapType === "parkingsMap" ? null : <NumbersOfStations count={count} countContainer={countContainer} />}
                </div>
                <div className={styles["button-container"]}>
                    <FavouritesButton isFavourite={isFavourite} id={id} isDetail={!!id} />
                </div>
            </div>
        </header>
    );
};

export default HeaderMaps;
