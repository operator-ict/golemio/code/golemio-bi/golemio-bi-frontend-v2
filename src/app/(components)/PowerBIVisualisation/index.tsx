import { useRouter } from "next/navigation";
import { Embed, models, Report } from "powerbi-client";
import { PowerBIEmbed } from "powerbi-client-react";
import { useEffect, useState } from "react";

import SvgFullScreen from "@/components/icons/FullScreen";

import styles from "./PowerBIVisualisation.module.scss";

const MINUTES_BEFORE_EXPIRATION = 5;

export interface IVisualization {
    reportId: string[];
    workspaceId: string | null | undefined;
    pageId: string | undefined;
    embedUrl: string;
    accessToken: string;
    tokenExpiration: string;
    visualisationType: string;
    filtersVisible: boolean;
    filtersExpanded: boolean;
    navContentPaneEnabled: boolean;
    background: string;
}

interface Props {
    visualization: IVisualization;
}

export const PowerBIVisualisation = ({ visualization }: Props) => {
    const [report, setReport] = useState<Report>();
    const [visualizationData, setVisualizationData] = useState<IVisualization | null>(null);
    const router = useRouter();

    const handleFullScreenClick = () => {
        if (!report) return;
        report.fullscreen();
    };

    // This effect handles setting visualization data and the report after render
    useEffect(() => {
        if (visualization) {
            setVisualizationData(visualization);
        }
    }, [visualization]);

    useEffect(() => {
        if (!report || !visualizationData?.tokenExpiration) return;
        const interval = setInterval(async () => {
            const currentTime = Date.now();
            const expiration = visualizationData ? Date.parse(visualizationData.tokenExpiration) : currentTime + 1;

            const timeUntilExpiration = expiration - currentTime;
            const timeToUpdate = MINUTES_BEFORE_EXPIRATION * 60 * 1000;

            // Update the token if it is about to expire
            if (timeUntilExpiration <= timeToUpdate && report) {
                // Hack! This re-renders the whole page
                router.refresh();
            }
        }, 30000);

        return () => clearInterval(interval);
    }, [report, visualizationData, router]);

    return (
        <div className={styles["inteligence-wrapper"]}>
            {visualization && visualization.reportId && visualization.reportId[0] && (
                <PowerBIEmbed
                    embedConfig={{
                        type: visualization?.visualisationType, // Supported types: report, dashboard, tile, visual and qna
                        id: visualization?.reportId[0],
                        embedUrl: visualization?.embedUrl,
                        accessToken: visualization?.accessToken,
                        tokenType: models.TokenType.Embed,
                        pageName: visualization.pageId,
                        settings: {
                            panes: {
                                filters: {
                                    expanded: visualization?.filtersExpanded,
                                    visible: visualization?.filtersVisible,
                                },
                            },
                            navContentPaneEnabled: visualization?.navContentPaneEnabled,
                            background: visualization.background == "transparent" ? 1 : 0,
                        },
                    }}
                    cssClassName={styles.embedcontainer}
                    getEmbeddedComponent={(embedObject: Embed) => {
                        // Now, instead of directly updating state in the render phase,
                        // we update the state in a controlled way by using useEffect to set report after mount
                        setReport(embedObject as Report);
                    }}
                />
            )}
            <button className={styles["screen-button"]} onClick={handleFullScreenClick} type="button" aria-label="full size">
                <SvgFullScreen />
            </button>
        </div>
    );
};
