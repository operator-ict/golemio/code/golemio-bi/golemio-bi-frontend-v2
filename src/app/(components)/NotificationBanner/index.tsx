import React from "react";

import { ErrorIcon } from "../icons/ErrorIcon";
import { InfoIcon } from "../icons/InfoIcon";
import { WarningIcon } from "../icons/WarningIcon";
import styles from "./NotificationBanner.module.scss";

export enum NotificationType {
    Info = "info",
    Warning = "warning",
    Error = "error",
}
type NotificationBannerProps = {
    type: NotificationType;
    text: string;
};

const NotificationBanner: React.FC<NotificationBannerProps> = ({ type, text }) => {
    const severityColors = {
        [NotificationType.Info]: "color-info",
        [NotificationType.Warning]: "color-warning",
        [NotificationType.Error]: "color-error",
    };
    const severityIcons = {
        [NotificationType.Info]: <InfoIcon />,
        [NotificationType.Warning]: <WarningIcon />,
        [NotificationType.Error]: <ErrorIcon />,
    };

    const bannerColorClass = severityColors[type];
    const bannerIcon = severityIcons[type];

    return (
        <div className={`${styles["banner-container"]} ${styles[bannerColorClass]}`}>
            <div className={`${styles["banner-content"]} ${styles[bannerColorClass]}`}>
                <span className={`${styles["icon-frame"]}`}>{bannerIcon}</span>
                <p>{text}</p>
            </div>
        </div>
    );
};

export default NotificationBanner;
