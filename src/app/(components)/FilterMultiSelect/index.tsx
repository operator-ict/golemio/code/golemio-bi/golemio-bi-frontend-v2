import React, { ChangeEvent } from "react";

import { FilterReset } from "../ResetFiltersButton";
import styles from "./Multiselect.module.scss";

interface Option {
    value: string;
    label: string;
    count?: number;
    type?: "radio" | "checkbox";
}

interface FilterMultiSelectProps {
    onChange: (values: string[]) => void;
    optionList: Option[];
    values: string[];
    title?: string;
    resetButton?: () => void;
}

const FilterMultiSelect: React.FC<FilterMultiSelectProps> = ({ onChange, optionList, values, title, resetButton }) => {
    const handleChange = (e: ChangeEvent<HTMLInputElement>, checked: boolean) => {
        if (checked) {
            const selected = [...values, e.target.value];
            onChange(selected);
        } else {
            const selected = values.filter((item) => item !== e.target.value);
            onChange(selected);
        }
    };

    return (
        <section className={styles["multiselect-container"]}>
            {resetButton && title && <FilterReset onClick={resetButton} />}
            {optionList.map((option) => {
                const inputId = `checkbox-${option.value}`;
                return (
                    <div key={option.value}>
                        <label className={styles["dropdown-label"]} htmlFor={inputId}>
                            <input
                                id={inputId}
                                className={styles["dropdown-input"]}
                                type="checkbox"
                                checked={values.includes(option.value) || (!values.length && option.value === "")}
                                onChange={(e) => handleChange(e, e.target.checked)}
                                value={option.value}
                                disabled={option.count === 0}
                            />
                            <span>{option.label}</span>
                        </label>
                    </div>
                );
            })}
        </section>
    );
};

export default FilterMultiSelect;
