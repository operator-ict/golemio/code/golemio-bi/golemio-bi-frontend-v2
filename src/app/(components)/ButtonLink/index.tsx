/* eslint-disable react/display-name */
"use client";
import Link from "next/link";
import React, { MouseEventHandler, ReactNode, useState } from "react";

import styles from "../Button/Button.module.scss";

type Colors = "primary" | "secondary" | "tertiary" | "outline" | "link";

type Sizes = "sm" | "md" | "lg" | "sm-md" | "md-lg";

interface ButtonLinkProps {
    label?: string;
    color: Colors;
    size?: Sizes;
    iconStart?: ReactNode;
    iconEnd?: ReactNode;
    hideLabelMobile?: boolean;
    hideLabelTablet?: boolean;
    hideLabel?: boolean;
    url?: string;
    square?: boolean;
    className?: string;
    onClick?: MouseEventHandler<HTMLAnchorElement>;
    onMouseDown?: MouseEventHandler<HTMLAnchorElement>;
}

const ButtonLink = (componentProps: ButtonLinkProps) => {
    const {
        className,
        color,
        size = "md",
        label,
        onClick,
        iconStart,
        iconEnd,
        hideLabelMobile,
        hideLabelTablet,
        hideLabel,
        onMouseDown,
        url,
        square,
    } = componentProps;
    const [dynamicUrl, setDynamicUrl] = useState(url);

    const handleContextMenu = () => {
        setDynamicUrl("/");
    };
    return (
        <Link
            href={dynamicUrl || ""}
            className={`${styles["button"]} ${styles[`button-color_${color}`]} ${styles[`button-size_${size}`]} ${
                square ? styles["button-square"] : ""
            } ${className}`}
            aria-label={label}
            onClick={onClick}
            onMouseDown={onMouseDown}
            onContextMenu={handleContextMenu}
        >
            {iconStart && <span className={`${styles["icon-frame"]} ${styles[`icon-frame_${size}`]}`}>{iconStart}</span>}
            {!hideLabel && label && (
                <span
                    className={`${hideLabelMobile ? styles["hide-label"] : ""}
                    ${hideLabelTablet ? styles["hide-label__tablet"] : ""} ${styles["label"]}`}
                >
                    {label}
                </span>
            )}

            {iconEnd && <span className={`${styles["icon-frame"]} ${styles[`icon-frame_${size}`]}`}>{iconEnd}</span>}
        </Link>
    );
};

export default ButtonLink;
