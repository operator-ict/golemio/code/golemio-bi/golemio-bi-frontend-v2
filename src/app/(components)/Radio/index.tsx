"use client";
import React, { ChangeEvent, DetailedHTMLProps, InputHTMLAttributes, useEffect, useState } from "react";

import styles from "./Radio.module.scss";

interface InputElementProps extends DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    label: string;
    onClick: () => void | ((value: string) => void);
}

export const Radio = ({ checked = false, id, label, name, role, onClick }: InputElementProps) => {
    const [isChecked, setIsChecked] = useState(checked);

    const changeHandler = (e: ChangeEvent<HTMLInputElement>) => {
        setIsChecked(e.target.checked);
        onClick();
    };

    useEffect(() => {
        setIsChecked(checked);
    }, [checked]);

    return (
        <li className={styles["radio"]} role={role}>
            <label htmlFor={id} className={styles["radio-label"]}>
                <input type="radio" id={id} name={name} checked={isChecked} onChange={changeHandler} value={id} />
                <span>{label}</span>
            </label>
        </li>
    );
};
