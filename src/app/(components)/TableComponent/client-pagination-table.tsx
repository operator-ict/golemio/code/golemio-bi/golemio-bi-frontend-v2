import { ColumnDef } from "@tanstack/react-table";

import { DataTable } from "@/components/TableComponent/Table";

interface ClientPaginationTableProps<TData, TValue> {
    columns: ColumnDef<TData, TValue>[];
    data: TData[];
    limit: number;
    page: number;
}
export const ClientPaginationTable = <TData, TValue>({ data, columns }: ClientPaginationTableProps<TData, TValue>) => {
    return <DataTable columns={columns} data={data} totalCount={data.length} limit={10} page={1} serverSide={false} />;
};
