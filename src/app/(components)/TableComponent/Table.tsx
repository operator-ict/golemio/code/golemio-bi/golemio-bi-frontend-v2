"use client";

import {
    ColumnDef,
    flexRender,
    getCoreRowModel,
    getPaginationRowModel,
    PaginationState,
    SortingState,
    useReactTable,
} from "@tanstack/react-table";
import { useEffect, useMemo, useState } from "react";
import { Tooltip } from "react-tooltip";

import DataTablePagination from "../TablePagination";
import styles from "./Table.module.scss";

interface DataTableProps<TData, TValue> {
    columns: ColumnDef<TData, TValue>[];
    data: TData[];
    pageCount?: number;
    totalCount?: number;
    limit: number;
    page: number;
    serverSide?: boolean;
    onPaginationChange?: (pageIndex: number, pageSize: number) => void;
    onSortingChange?: (sorting: SortingState) => void;
    currentUserID?: string | undefined;
}

export const DataTable = <TData, TValue>({
    columns,
    data,
    pageCount,
    totalCount,
    limit,
    page,
    serverSide = false,
    onPaginationChange,
    onSortingChange,
    currentUserID,
}: DataTableProps<TData, TValue>) => {
    const [sorting, setSorting] = useState<SortingState>([]);
    const [{ pageIndex, pageSize }, setPagination] = useState<PaginationState>({
        pageIndex: Number(page) - 1,
        pageSize: Number(limit),
    });

    const pagination = useMemo(
        () => ({
            pageIndex,
            pageSize,
        }),
        [pageIndex, pageSize]
    );

    useEffect(() => {
        if (serverSide && onPaginationChange) {
            onPaginationChange(pageIndex, pageSize);
        }
    }, [pageIndex, pageSize, serverSide, onPaginationChange]);

    useEffect(() => {
        if (serverSide && onSortingChange) {
            onSortingChange(sorting);
        }
    }, [sorting, serverSide, onSortingChange]);

    const table = useReactTable({
        data: data || [],
        columns,
        pageCount: serverSide ? (pageCount ?? -1) : undefined,
        state: {
            pagination,
            sorting,
        },
        onPaginationChange: setPagination,
        onSortingChange: setSorting,
        getCoreRowModel: getCoreRowModel(),
        getPaginationRowModel: !serverSide ? getPaginationRowModel() : undefined,
        manualPagination: serverSide,
        manualSorting: serverSide,
        meta: { currentUserID },
    });

    return (
        <div className={styles["data-table-container"]}>
            <Tooltip id="table-tooltip" place="bottom" className={styles["data-table-tooltip"]} delayHide={500} />
            <table className={styles["data-table"]}>
                <thead>
                    {table.getHeaderGroups().map((headerGroup) => (
                        <tr key={headerGroup.id}>
                            {headerGroup.headers.map((header) => (
                                <th
                                    key={header.id}
                                    style={{ width: header.getSize() || undefined }}
                                    onClick={header.column.getCanSort() ? () => header.column.toggleSorting() : undefined}
                                >
                                    {header.isPlaceholder
                                        ? null
                                        : flexRender(header.column.columnDef.header, header.getContext())}
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
                <tbody>
                    {table.getRowModel().rows.length > 0 ? (
                        table.getRowModel().rows.map((row) => (
                            <tr key={row.id} className={styles["data-row"]}>
                                {row.getVisibleCells().map((cell) => (
                                    <td key={cell.id} className={styles["data-table__td"]}>
                                        {flexRender(cell.column.columnDef.cell, cell.getContext())}
                                    </td>
                                ))}
                            </tr>
                        ))
                    ) : (
                        <tr>
                            <td colSpan={columns.length} className={styles["data-table__no-results"]}>
                                No results.
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
            <DataTablePagination table={table} limit={limit} totalCount={totalCount} />
        </div>
    );
};
