"use client";

import { ColumnDef } from "@tanstack/react-table";
import { useRouter } from "next/navigation";

import { DataTable } from "@/components/TableComponent/Table";

type HandlePaginationChange = (pageIndex: number, pageSize: number) => void;

interface ServerPaginationTableProps<TData, TValue> {
    columns: ColumnDef<TData, TValue>[];
    data: TData[];
    pageCount?: number;
    totalCount: number;
    limit: number;
    page: number;
    currentUserID?: string | undefined;
}

export const ServerPaginationTable = <TData, TValue>({
    data,
    columns,
    totalCount,
    limit,
    page,
    currentUserID,
}: ServerPaginationTableProps<TData, TValue>) => {
    const router = useRouter();

    const handlePaginationChange: HandlePaginationChange = (pageIndex, pageSize) => {
        const currentParams = new URLSearchParams(window.location.search);
        currentParams.set("page", (pageIndex + 1).toString());
        currentParams.set("limit", pageSize.toString());
        router.push(`?${currentParams.toString()}`, { scroll: false });
    };

    return (
        <DataTable
            columns={columns}
            data={data}
            totalCount={totalCount}
            limit={limit}
            page={page}
            serverSide
            onPaginationChange={handlePaginationChange}
            currentUserID={currentUserID}
        />
    );
};
