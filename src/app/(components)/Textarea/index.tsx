import React, { DetailedHTMLProps, FC, ReactNode, TextareaHTMLAttributes } from "react";

import styles from "./Textarea.module.scss";

interface ITextarea extends DetailedHTMLProps<TextareaHTMLAttributes<HTMLTextAreaElement>, HTMLTextAreaElement> {
    label: string;
    hideLabel?: boolean;
    iconStart?: ReactNode;
    error?: boolean;
    errorMessage?: string;
}

const Textarea: FC<ITextarea> = ({
    placeholder,
    label,
    hideLabel,
    iconStart,
    value,
    onChange,
    id,
    error,
    errorMessage,
    ...restProps
}) => {
    return (
        <div className={styles["text-area"]}>
            <div className={styles["text-area-field"]}>
                {value && (
                    <label
                        htmlFor={id}
                        className={`${styles["text-area-label"]} ${!value ? styles["text-area-label__hidden"] : ""}
                            ${iconStart && styles["text-area-label-icon-start"]}`}
                    >
                        {!hideLabel ? label : false}
                    </label>
                )}
                {iconStart && <span className={`${styles["text-area-icon"]}`}>{iconStart}</span>}
                <textarea
                    className={`
                        ${styles["text-area-input"]}
                        ${hideLabel || value ? styles["text-area-input__label-hidden"] : ""}
                        ${error && styles["text-area-input__error"]}
                        ${value && styles["text-area-input__with-value"]}
                    `}
                    id={id}
                    placeholder={placeholder}
                    onChange={onChange}
                    value={value}
                    aria-label={label}
                    {...restProps}
                />
            </div>
            {error ? <span className={styles["error"]}>{errorMessage}</span> : false}
        </div>
    );
};

export default Textarea;
