"use client";
import React from "react";

import { SortedIcon } from "@/icons/SortedIcon";

import styles from "./SortedButton.module.scss";

interface SortedButtonProps {
    title: string;
    columnName: string;
    toggleSorting: ((event: unknown) => void) | undefined;
    isSorted: false | string;
}

const SortedButton = ({ title, toggleSorting, isSorted }: SortedButtonProps) => {
    return (
        <button className={styles["sorted-button"]} onClick={toggleSorting}>
            <span className={styles["sorted-button__title"]}>{title}</span>

            {isSorted && (
                <SortedIcon
                    className={`${!isSorted ? styles["sorted-button__null"] : "sorted-button"}`}
                    desc={isSorted === "desc"}
                    color={"primary"}
                />
            )}
        </button>
    );
};

export default SortedButton;
