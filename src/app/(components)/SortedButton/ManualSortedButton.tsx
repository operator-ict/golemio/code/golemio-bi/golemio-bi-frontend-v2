"use client";
import { useRouter, useSearchParams } from "next/navigation";
import React, { useCallback, useMemo } from "react";

import { SortedIcon } from "../icons/SortedIcon";
import styles from "./SortedButton.module.scss";

interface SortedButtonProps {
    columnName: string;
    title: string;
    defaultSorting?: boolean;
}
type SortDirection = "asc" | "desc" | null;

const createQueryString = (searchParams: URLSearchParams, column: string, direction: SortDirection) => {
    const params = new URLSearchParams(searchParams.toString());
    if (direction === null) {
        params.delete("orderBy");
        params.delete("orderDir");
    } else {
        params.set("orderBy", column);
        params.set("orderDir", direction || "asc");
    }
    return params.toString();
};

const ManualSortedButton = ({ columnName, title, defaultSorting }: SortedButtonProps) => {
    const searchParams = useSearchParams();
    const direction = searchParams.get("orderDir") ?? "asc";
    const column = searchParams.get("orderBy") ?? `${defaultSorting ? columnName : ""}`;

    const router = useRouter();

    const sortConfig = useMemo(() => {
        return direction && column === columnName ? { column: columnName, direction } : { column: null, direction: null };
    }, [columnName, column, direction]);

    const toggleSortDirection = useCallback(() => {
        let newDirection: SortDirection = "asc";

        if (sortConfig.direction === "asc") {
            newDirection = "desc";
        } else if (sortConfig.direction === "desc" && columnName !== "email") {
            newDirection = null;
        }

        const queryString = createQueryString(searchParams, columnName, newDirection);

        router.push(`?${queryString}`);
        // nextjs 14 caching issue while pushing default route
        if (!queryString) {
            router.refresh();
        }
    }, [sortConfig, columnName, router, searchParams]);

    const isSorted = sortConfig.column === columnName;

    return (
        <button
            className={styles["sorted-button"]}
            onClick={toggleSortDirection}
            aria-label={`Sort by ${columnName} in ${sortConfig?.direction || "ascending"} order`}
        >
            <span className={styles["sorted-button__title"]}>{title}</span>

            {isSorted && (
                <SortedIcon
                    className={`${direction === null ? styles["sorted-button__null"] : "sorted-button"}`}
                    desc={sortConfig.direction === "desc"}
                    color={"primary"}
                />
            )}
        </button>
    );
};

export default ManualSortedButton;
