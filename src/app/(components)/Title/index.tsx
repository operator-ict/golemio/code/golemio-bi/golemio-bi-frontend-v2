/* eslint-disable react/display-name */
import { DetailedHTMLProps, forwardRef, HTMLAttributes } from "react";

import styles from "./Title.module.scss";

type Type = "t1" | "t1m" | "t2" | "t2m" | "t3" | "t3m";

interface TitleProps extends DetailedHTMLProps<HTMLAttributes<HTMLSpanElement>, HTMLSpanElement> {
    type: Type;
}

export const Title = forwardRef<HTMLHeadingElement, TitleProps>(({ type, className, ...restProps }, ref) => {
    return <span className={`${styles.title} ${styles[`title-${type}`]} ${className}`} {...restProps} ref={ref} />;
});
