import { Table } from "@tanstack/react-table";
import React from "react";

import LeftButtonIcon from "@/icons/LeftButtonIcon";
import { RightButtonIcon } from "@/icons/RightButtonIcon";
import ToFirstPageIcon from "@/icons/ToFirstPageIcon";
import ToLastPageIcon from "@/icons/ToLastPageIcon";

import Button from "../Button";
import styles from "./TablePagination.module.scss";

type DataTablePaginationProps<TData> = {
    table: Table<TData>;
    limit: number;
    totalCount?: number;
};

const DataTablePagination = <TData,>({ table, limit, totalCount }: DataTablePaginationProps<TData>) => {
    const currentPage = table.getState().pagination.pageIndex;
    const count = totalCount ? totalCount : table.getFilteredRowModel().rows.length;

    const pageCount = totalCount
        ? Math.ceil(totalCount / limit)
        : Math.ceil(table.getFilteredRowModel().rows.length / table.getState().pagination.pageSize);

    const pageSizeOptions = [10, 20, 30, 40, 50];
    const maxPagesToShow = 3;

    const getPageNumbers = () => {
        const pages: (number | "ellipsis")[] = [];

        pages.push(0);

        const startPage = Math.max(1, currentPage - maxPagesToShow);
        const endPage = Math.min(currentPage + maxPagesToShow, pageCount - 2);

        if (startPage > 1) {
            pages.push("ellipsis");
        }

        for (let i = startPage; i <= endPage; i++) {
            pages.push(i);
        }

        if (endPage < pageCount - 2) {
            pages.push("ellipsis");
        }

        if (pageCount > 1) {
            pages.push(pageCount - 1);
        }

        return pages;
    };

    const startItem = currentPage * +limit + 1;
    const endItem = Math.min((currentPage + 1) * +limit, count);

    const paginationButtons = getPageNumbers().map((page, index) => {
        if (page === "ellipsis") {
            return (
                <span key={`ellipsis-${index}`} className={styles["pagination-container__ellipsis"]}>
                    <Button
                        aria-label="Go to first page"
                        size="md"
                        color="tertiary"
                        onClick={() => table.setPageIndex(0)}
                        disabled={!table.getCanPreviousPage()}
                        label="..."
                    />
                </span>
            );
        }

        return (
            <Button
                key={page}
                onClick={() => table.setPageIndex(page as number)}
                color="tertiary"
                disabled={currentPage === page}
                className={currentPage === page ? `${styles.active} ${styles.disabled}` : ""}
            >
                {+page + 1}
            </Button>
        );
    });

    return (
        <div className={styles["pagination-container"]}>
            <div className={styles["pagination-container__rows-on-page"]}>
                <span>Rows on page </span>
                <select
                    value={table.getState().pagination.pageSize}
                    onChange={(e) => {
                        table.setPageSize(Number(e.target.value));
                    }}
                    className={styles["pagination-container__select"]}
                    data-testid="page-size-select"
                >
                    {pageSizeOptions.map((pageSize) => (
                        <option key={pageSize} value={pageSize}>
                            {pageSize}
                        </option>
                    ))}
                </select>
                <div className={styles["pagination-container__info"]}>
                    {`${startItem}-${endItem}`} of {count}
                </div>
            </div>

            <div className={styles["pagination-container__controls"]}>
                <Button
                    aria-label="Go to first page"
                    size="md"
                    color="outline"
                    onClick={() => table.setPageIndex(0)}
                    disabled={!table.getCanPreviousPage()}
                    iconStart={<ToFirstPageIcon />}
                />
                <Button
                    size="md"
                    label="Previous Page"
                    onClick={() => table.previousPage()}
                    disabled={!table.getCanPreviousPage()}
                    color="outline"
                    iconStart={<LeftButtonIcon />}
                    hideLabel
                />

                <div className={styles["pagination-container__pagination-buttons"]}>{paginationButtons}</div>

                <Button
                    size="md"
                    color="outline"
                    label="Next Page"
                    onClick={() => table.nextPage()}
                    disabled={!table.getCanNextPage()}
                    iconStart={<RightButtonIcon />}
                    hideLabel
                />
                <Button
                    size="md"
                    color="outline"
                    aria-label="Go to last page"
                    onClick={() => table.setPageIndex(pageCount - 1)}
                    disabled={!table.getCanNextPage()}
                    iconStart={<ToLastPageIcon />}
                />
            </div>
        </div>
    );
};

export default DataTablePagination;
