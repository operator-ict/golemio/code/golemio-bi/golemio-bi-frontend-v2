import React, { useCallback, useEffect, useRef, useState } from "react";

import styles from "./FilterRange.module.scss";
interface FilterRangeProps {
    onChange: (min: number, max: number) => void;
    values: number[];
    title: string;
    min: number;
    max: number;
    unit: string;
    disabled?: boolean;
}

const FilterRange: React.FC<FilterRangeProps> = ({ values, min, max, unit, onChange, disabled }) => {
    const [minVal, setMinVal] = useState(values[0]);
    const [maxVal, setMaxVal] = useState(values[1]);
    const range = useRef<HTMLDivElement | null>(null);
    const minValRef = useRef(minVal);
    const maxValRef = useRef(maxVal);

    const getPercent = useCallback((value: number) => Math.round(((value - min) / (max - min)) * 100), [min, max]);
    const labelFormat = useCallback((value: number) => `${value}${unit}`, [unit]);

    useEffect(() => {
        const minPercent = getPercent(minVal);
        const maxPercent = getPercent(maxValRef.current);
        if (range.current) {
            range.current.style.left = `${minPercent}%`;
            range.current.style.width = `${maxPercent - minPercent}%`;
        }
    }, [minVal, getPercent]);

    useEffect(() => {
        const minPercent = getPercent(minValRef.current);
        const maxPercent = getPercent(maxVal);

        if (range.current) {
            range.current.style.width = `${maxPercent - minPercent}%`;
        }
    }, [maxVal, getPercent]);

    useEffect(() => {
        onChange(minVal, maxVal);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [minVal, maxVal]);

    return (
        <div>
            <div className={styles.container}>
                <input
                    id="from-slider"
                    type="range"
                    min={min}
                    max={max}
                    value={minVal}
                    onChange={(event) => {
                        const value = Math.min(Number(event.target.value), maxVal - 1);
                        setMinVal(value);
                        minValRef.current = value;
                    }}
                    className={styles["thumb"]}
                    style={{ zIndex: `${min > max - 100}` && "15" }}
                    disabled={disabled}
                    data-testid="from-slider"
                />
                <input
                    id="to-slider"
                    type="range"
                    min={min}
                    max={max}
                    value={maxVal}
                    onChange={(event) => {
                        const value = Math.max(Number(event.target.value), minVal + 1);
                        setMaxVal(value);
                        maxValRef.current = value;
                    }}
                    className={styles["thumb"]}
                    style={{ zIndex: `${min > max - 100}` && "14" }}
                    disabled={disabled}
                    data-testid="to-slider"
                />
                <div className={styles["slider"]}>
                    <div className={styles["slider__track"]} />
                    <div ref={range} className={styles["slider__range"]} />
                    <div className={styles["slider__left-value"]}>{labelFormat(minVal)}</div>
                    <div className={styles["slider__right-value"]}>{labelFormat(maxVal)}</div>
                </div>
            </div>
        </div>
    );
};

export default FilterRange;
