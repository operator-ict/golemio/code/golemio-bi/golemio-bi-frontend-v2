/* eslint-disable react/display-name */

import { FC } from "react";

import text from "@/textContent/cs.json";

interface INumbersOfStations {
    count: number;
    countContainer: number;
}

const NumbersOfStations: FC<INumbersOfStations> = ({ count, countContainer }) => {
    return (
        <div>
            <span>{`${text.map.sortedWaste.numberofStations}: ${count}`} • </span>
            <span>{`Nádob: ${countContainer}`}</span>
        </div>
    );
};

export default NumbersOfStations;
