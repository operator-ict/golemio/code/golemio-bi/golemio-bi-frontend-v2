/* eslint-disable max-len */

import React, { SVGProps } from "react";

interface SortedIconProps {
    desc: boolean;
}
export const SortedIcon = ({ desc, width = "11", height = "12", ...restProps }: SortedIconProps & SVGProps<SVGSVGElement>) => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width={width}
        height={height}
        viewBox="0 0 11 12"
        fill="none"
        style={{
            transform: desc ? "rotate(180deg)" : "rotate(0deg)",
            transformOrigin: "center",
            transition: "transform 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
            userSelect: "none",
            display: "block",
            margin: "auto",
        }}
        {...restProps}
    >
        <path
            fill="#707D99"
            d="m5.398.438 3.938 4.125c.21.234.21.585-.024.796-.234.211-.585.211-.796-.023L5.562 2.219v7.969A.555.555 0 0 1 5 10.75a.542.542 0 0 1-.563-.563V2.22L1.462 5.336c-.211.234-.563.234-.797.023a.56.56 0 0 1-.023-.796L4.578.438A.587.587 0 0 1 5 .25c.14 0 .281.07.398.188Z"
        />
    </svg>
);
