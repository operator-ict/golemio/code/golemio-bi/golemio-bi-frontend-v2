/* eslint-disable max-len */
import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./IconFills.module.scss";

export const ArrowRightIcon = ({
    color,
    width = "15px",
    height = "14px",
    fill,
    ...restProps
}: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 15 14"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            d="M10.9768 6.42958L6.50683 1.95958L7.68516 0.78125L14.1668 7.26292L7.68516 13.7446L6.50683 12.5662L10.9768 8.09625H0.833496V6.42958H10.9768Z"
            fill={fill}
        />
    </svg>
);
