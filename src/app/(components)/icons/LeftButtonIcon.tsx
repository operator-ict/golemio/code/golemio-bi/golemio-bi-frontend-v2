/* eslint-disable max-len */

import { SVGProps } from "react";

const LeftButtonIcon = (props: SVGProps<SVGSVGElement>) => (
    <svg xmlns="http://www.w3.org/2000/svg" width={12} height={12} fill="none" {...props}>
        <path
            fill="#2C323F"
            d="m3.168 5.918 3.75-3.73c.176-.196.469-.196.664 0a.46.46 0 0 1 0 .644L4.145 6.25l3.418 3.438a.428.428 0 0 1 0 .644.428.428 0 0 1-.645 0l-3.75-3.75c-.195-.176-.195-.469 0-.664Z"
        />
    </svg>
);
export default LeftButtonIcon;
