/* eslint-disable max-len */
import React, { SVGProps } from "react";

import styles from "@/icons/IconFills.module.scss";
import { IiCon } from "@/types/IconInterface";

export const AddPlusSimpleIcon = ({
    color,
    width = "10px",
    height = "10px",
    fill,
    ...restProps
}: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        width={width}
        height={height}
        className={styles[`icon_color-${color}`]}
        style={{ width, height }}
        viewBox="0 0 10 10"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            d="M5.5625 0.6875V4.4375H9.3125C9.61719 4.4375 9.875 4.69531 9.875 5C9.875 5.32812 9.61719 5.5625 9.3125 5.5625H5.5625V9.3125C5.5625 9.64062 5.30469 9.875 5 9.875C4.67188 9.875 4.4375 9.64062 4.4375 9.3125V5.5625H0.6875C0.359375 5.5625 0.125 5.32812 0.125 5C0.125 4.69531 0.359375 4.4375 0.6875 4.4375H4.4375V0.6875C4.4375 0.382812 4.67188 0.125 5 0.125C5.30469 0.125 5.5625 0.382812 5.5625 0.6875Z"
            fill={fill}
        />
    </svg>
);
