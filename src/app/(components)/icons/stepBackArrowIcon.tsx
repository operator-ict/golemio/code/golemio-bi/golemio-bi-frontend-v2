import * as React from "react";
import { SVGProps } from "react";

import { IiCon } from "@/(types)/IconInterface";

import styles from "./IconFills.module.scss";
export const StepBackArrowIcon = ({
    color,
    width = "17px",
    height = "15px",
    fill,
    ...restProps
}: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 17 15"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            fill={fill}
            // eslint-disable-next-line max-len
            d="m.664 4.541 3.75-3.75a.723.723 0 0 1 1.054 0 .723.723 0 0 1 0 1.055l-2.46 2.46h7.195c.398 0 .75.352.75.75 0 .423-.352.75-.75.75H3.007l2.461 2.485a.723.723 0 0 1 0 1.055.723.723 0 0 1-1.054 0l-3.75-3.75a.723.723 0 0 1 0-1.055Z"
        />
    </svg>
);
