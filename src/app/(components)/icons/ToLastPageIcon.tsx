/* eslint-disable max-len */

import { SVGProps } from "react";

const ToLastPageIcon = (props: SVGProps<SVGSVGElement>) => (
    <svg {...props} xmlns="http://www.w3.org/2000/svg" width={12} height={12} fill="none">
        <path
            fill="#2C323F"
            d="M10.082 5.918a.484.484 0 0 1 0 .664l-3.75 3.75c-.195.195-.488.195-.664 0a.428.428 0 0 1 0-.645L9.086 6.27 5.668 2.832a.428.428 0 0 1 0-.644.428.428 0 0 1 .644 0l3.77 3.73Zm-7.5-3.75 3.75 3.75a.484.484 0 0 1 0 .664l-3.75 3.75c-.195.195-.488.195-.664 0a.428.428 0 0 1 0-.645L5.336 6.27 1.918 2.832a.428.428 0 0 1 0-.644.428.428 0 0 1 .645 0l.019-.02Z"
        />
    </svg>
);
export default ToLastPageIcon;
