/* eslint-disable max-len */

import { SVGProps } from "react";

const ToFirstPageIcon = (props: SVGProps<SVGSVGElement>) => (
    <svg {...props} xmlns="http://www.w3.org/2000/svg" width={12} height={12} fill="none">
        <path
            fill="#2C323F"
            d="m1.918 5.918 3.75-3.73c.176-.196.469-.196.664 0a.46.46 0 0 1 0 .644L2.895 6.25l3.417 3.438a.428.428 0 0 1 0 .644.428.428 0 0 1-.644 0l-3.75-3.75c-.195-.176-.195-.469 0-.664Zm7.5-3.75v.02c.176-.196.469-.196.664 0a.46.46 0 0 1 0 .644L6.645 6.27l3.418 3.418a.428.428 0 0 1 0 .644.428.428 0 0 1-.645 0l-3.75-3.75c-.195-.176-.195-.469 0-.664l3.75-3.75Z"
        />
    </svg>
);
export default ToFirstPageIcon;
