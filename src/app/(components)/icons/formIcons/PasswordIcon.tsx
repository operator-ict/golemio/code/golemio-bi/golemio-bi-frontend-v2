/* eslint-disable max-len */
import React, { SVGProps } from "react";

import styles from "@/icons/IconFills.module.scss";
import { IiCon } from "@/types/IconInterface";

export const PasswordIcon = ({ color, width = "18px", height = "21px", fill, ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 18 21"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            d="M5.625 5.76318V8.26318H11.875V5.76318C11.875 4.04443 10.4688 2.63818 8.75 2.63818C6.99219 2.63818 5.625 4.04443 5.625 5.76318ZM3.75 8.26318V5.76318C3.75 3.02881 5.97656 0.763184 8.75 0.763184C11.4844 0.763184 13.75 3.02881 13.75 5.76318V8.26318H15C16.3672 8.26318 17.5 9.396 17.5 10.7632V18.2632C17.5 19.6694 16.3672 20.7632 15 20.7632H2.5C1.09375 20.7632 0 19.6694 0 18.2632V10.7632C0 9.396 1.09375 8.26318 2.5 8.26318H3.75ZM1.875 10.7632V18.2632C1.875 18.6147 2.14844 18.8882 2.5 18.8882H15C15.3125 18.8882 15.625 18.6147 15.625 18.2632V10.7632C15.625 10.4507 15.3125 10.1382 15 10.1382H2.5C2.14844 10.1382 1.875 10.4507 1.875 10.7632Z"
            fill={fill}
        />
    </svg>
);
