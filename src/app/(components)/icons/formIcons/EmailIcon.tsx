/* eslint-disable max-len */
import React, { SVGProps } from "react";

import styles from "@/icons/IconFills.module.scss";
import { IiCon } from "@/types/IconInterface";

export const EmailIcon = ({ color, width = "20px", height = "16px", fill, ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 20 16"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            d="M2.5 2.13818C2.14844 2.13818 1.875 2.45068 1.875 2.76318V3.66162L8.59375 9.16943C9.41406 9.8335 10.5469 9.8335 11.3672 9.16943L18.125 3.66162V2.76318C18.125 2.45068 17.8125 2.13818 17.5 2.13818H2.5ZM1.875 6.0835V12.7632C1.875 13.1147 2.14844 13.3882 2.5 13.3882H17.5C17.8125 13.3882 18.125 13.1147 18.125 12.7632V6.0835L12.5781 10.6147C11.0547 11.8647 8.90625 11.8647 7.42188 10.6147L1.875 6.0835ZM0 2.76318C0 1.396 1.09375 0.263184 2.5 0.263184H17.5C18.8672 0.263184 20 1.396 20 2.76318V12.7632C20 14.1694 18.8672 15.2632 17.5 15.2632H2.5C1.09375 15.2632 0 14.1694 0 12.7632V2.76318Z"
            fill={fill}
        />
    </svg>
);
