/* eslint-disable max-len */
import * as React from "react";
import { SVGProps } from "react";

import { IiCon } from "@/(types)/IconInterface";
import styles from "@/icons/IconFills.module.scss";

const SubmitIcon = ({ color, width = "16px", height = "17px", fill, ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 16 17"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <defs />
        <path
            d="M13.0156 5.04102C13.3203 5.32227 13.3203 5.81445 13.0156 6.0957L7.01562 12.0957C6.73438 12.4004 6.24219 12.4004 5.96094 12.0957L2.96094 9.0957C2.65625 8.81445 2.65625 8.32227 2.96094 8.04102C3.24219 7.73633 3.73438 7.73633 4.01562 8.04102L6.5 10.502L11.9609 5.04102C12.2422 4.73633 12.7344 4.73633 13.0156 5.04102Z"
            fill={fill}
        />
    </svg>
);
export default SubmitIcon;
