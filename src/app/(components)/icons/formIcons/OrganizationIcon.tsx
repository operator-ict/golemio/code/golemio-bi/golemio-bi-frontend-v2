/* eslint-disable max-len */
import React, { SVGProps } from "react";

import styles from "@/icons/IconFills.module.scss";
import { IiCon } from "@/types/IconInterface";

export const OrganizationIcon = ({
    color,
    width = "20px",
    height = "20px",
    fill,
    ...restProps
}: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 20 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            d="M6.875 2.95068V4.51318H13.125V2.95068C13.125 2.79443 12.9688 2.63818 12.8125 2.63818H7.1875C6.99219 2.63818 6.875 2.79443 6.875 2.95068ZM5 4.51318V2.95068C5 1.77881 5.97656 0.763184 7.1875 0.763184H12.8125C13.9844 0.763184 15 1.77881 15 2.95068V4.51318H17.5C18.8672 4.51318 20 5.646 20 7.01318V17.0132C20 18.4194 18.8672 19.5132 17.5 19.5132H2.5C1.09375 19.5132 0 18.4194 0 17.0132V7.01318C0 5.646 1.09375 4.51318 2.5 4.51318H5ZM14.0625 6.38818H5.9375H5.625V17.6382H14.375V6.38818H14.0625ZM2.5 6.38818C2.14844 6.38818 1.875 6.70068 1.875 7.01318V17.0132C1.875 17.3647 2.14844 17.6382 2.5 17.6382H3.75V6.38818H2.5ZM17.5 17.6382C17.8125 17.6382 18.125 17.3647 18.125 17.0132V7.01318C18.125 6.70068 17.8125 6.38818 17.5 6.38818H16.25V17.6382H17.5Z"
            fill={fill}
        />
    </svg>
);
