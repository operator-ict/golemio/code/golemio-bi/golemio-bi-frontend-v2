/* eslint-disable max-len */
import * as React from "react";
import { SVGProps } from "react";

import { IiCon } from "@/(types)/IconInterface";
import styles from "@/icons/IconFills.module.scss";

const SaveIcon = ({ color, width = "12px", height = "11px", fill, ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 12 11"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            d="M2.25 0.306641H7.92188C8.32031 0.306641 8.71875 0.470703 9 0.751953L10.8047 2.55664C11.0859 2.83789 11.25 3.23633 11.25 3.63477V9.30664C11.25 10.1504 10.5703 10.8066 9.75 10.8066H2.25C1.40625 10.8066 0.75 10.1504 0.75 9.30664V1.80664C0.75 0.986328 1.40625 0.306641 2.25 0.306641ZM2.25 2.55664V4.05664C2.25 4.47852 2.57812 4.80664 3 4.80664H7.5C7.89844 4.80664 8.25 4.47852 8.25 4.05664V2.55664C8.25 2.1582 7.89844 1.80664 7.5 1.80664H3C2.57812 1.80664 2.25 2.1582 2.25 2.55664ZM6 6.30664C5.46094 6.30664 4.96875 6.61133 4.6875 7.05664C4.42969 7.52539 4.42969 8.11133 4.6875 8.55664C4.96875 9.02539 5.46094 9.30664 6 9.30664C6.51562 9.30664 7.00781 9.02539 7.28906 8.55664C7.54688 8.11133 7.54688 7.52539 7.28906 7.05664C7.00781 6.61133 6.51562 6.30664 6 6.30664Z"
            fill={fill}
        />
    </svg>
);
export default SaveIcon;
