/* eslint-disable max-len */
import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "../IconFills.module.scss";

export const UploadIcon = ({ color, width = "14px", height = "10px", fill, ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ minWidth: width, minHeight: height, width: width, height: height }}
        viewBox="0 0 14 10"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            d="M3.5625 9.625C2 9.625 0.75 8.375 0.75 6.8125C0.75 5.60156 1.53125 4.54688 2.625 4.17578C2.625 4.11719 2.625 4.05859 2.625 4C2.625 2.28125 4.01172 0.875 5.75 0.875C6.90234 0.875 7.91797 1.51953 8.44531 2.45703C8.73828 2.24219 9.10938 2.125 9.5 2.125C10.5352 2.125 11.375 2.96484 11.375 4C11.375 4.25391 11.3164 4.46875 11.2383 4.68359C12.3906 4.91797 13.25 5.93359 13.25 7.125C13.25 8.51172 12.1172 9.625 10.75 9.625H3.5625ZM5.10547 5.38672C4.91016 5.58203 4.91016 5.875 5.10547 6.05078C5.28125 6.24609 5.57422 6.24609 5.75 6.05078L6.51172 5.28906V7.90625C6.51172 8.17969 6.72656 8.375 6.98047 8.375C7.25391 8.375 7.44922 8.17969 7.44922 7.90625V5.28906L8.21094 6.05078C8.40625 6.24609 8.69922 6.24609 8.875 6.05078C9.07031 5.875 9.07031 5.58203 8.875 5.38672L7.3125 3.82422C7.13672 3.64844 6.84375 3.64844 6.64844 3.82422L5.08594 5.38672H5.10547Z"
            fill={fill}
        />
    </svg>
);
