import * as React from "react";
import { SVGProps } from "react";

import { IiCon } from "@/(types)/IconInterface";
import styles from "@/icons/IconFills.module.scss";

const SelectArrowIcon = ({ color, width = "14px", height = "8px", ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            fill="#707D99"
            // eslint-disable-next-line max-len
            d="m6.371 6.879-5.25-5.25a.843.843 0 0 1 0-1.23.843.843 0 0 1 1.23 0L7 5.018 11.621.4a.843.843 0 0 1 1.23 0 .843.843 0 0 1 0 1.23l-5.25 5.25a.843.843 0 0 1-1.23 0Z"
        />
    </svg>
);
export default SelectArrowIcon;
