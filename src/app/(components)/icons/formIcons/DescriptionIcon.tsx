import * as React from "react";
import { SVGProps } from "react";

import { IiCon } from "@/(types)/IconInterface";
import styles from "@/icons/IconFills.module.scss";

const DescriptionIcon = ({ color, width = "24px", height = "24px", fill, ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            fill={fill}
            // eslint-disable-next-line max-len
            d="M 3.25 5.75 C 3.25 5.242 3.641 4.812 4.187 4.812 L 19.812 4.812 C 20.32 4.812 20.75 5.242 20.75 5.75 C 20.75 6.297 20.32 6.687 19.812 6.687 L 4.187 6.687 C 3.641 6.687 3.25 6.297 3.25 5.75 Z M 3.25 12 C 3.25 11.492 3.641 11.062 4.187 11.062 L 14.812 11.062 C 15.32 11.062 15.75 11.492 15.75 12 C 15.75 12.547 15.32 12.937 14.812 12.937 L 4.187 12.937 C 3.641 12.937 3.25 12.547 3.25 12 Z M 10.75 18.25 C 10.75 18.797 10.32 19.187 9.812 19.187 L 4.187 19.187 C 3.641 19.187 3.25 18.797 3.25 18.25 C 3.25 17.742 3.641 17.312 4.187 17.312 L 9.812 17.312 C 10.32 17.312 10.75 17.742 10.75 18.25 Z"
        />
    </svg>
);
export default DescriptionIcon;
