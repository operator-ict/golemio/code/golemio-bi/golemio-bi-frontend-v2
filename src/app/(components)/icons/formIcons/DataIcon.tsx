import * as React from "react";
import { SVGProps } from "react";

import { IiCon } from "@/(types)/IconInterface";
import styles from "@/icons/IconFills.module.scss";

const DataIcon = ({ color, width = "26", height = "21", fill, ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 26 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            fill={fill}
            // eslint-disable-next-line max-len
            d="M 16.188 1.732 C 16.672 1.853 16.954 2.417 16.794 2.942 L 10.983 21.663 C 10.863 22.147 10.297 22.43 9.773 22.268 C 9.288 22.107 9.006 21.583 9.168 21.058 L 14.977 2.337 C 15.138 1.853 15.663 1.57 16.188 1.732 Z M 19.052 6.493 C 19.416 6.089 20.021 6.089 20.425 6.452 L 25.59 11.293 C 25.791 11.456 25.912 11.737 25.912 11.98 C 25.912 12.263 25.791 12.504 25.59 12.707 L 20.425 17.548 C 20.021 17.911 19.416 17.871 19.052 17.507 C 18.689 17.104 18.689 16.499 19.093 16.136 L 23.49 11.98 L 19.093 7.864 C 18.689 7.501 18.689 6.896 19.052 6.493 Z M 6.908 6.493 C 7.271 6.896 7.271 7.501 6.867 7.864 L 2.47 11.98 L 6.867 16.136 C 7.271 16.499 7.271 17.104 6.908 17.507 C 6.544 17.911 5.939 17.911 5.535 17.548 L 0.371 12.707 C 0.169 12.504 0.089 12.263 0.089 11.98 C 0.089 11.737 0.169 11.456 0.371 11.293 L 5.535 6.452 C 5.939 6.089 6.544 6.089 6.908 6.493 Z"
        />
    </svg>
);
export default DataIcon;
