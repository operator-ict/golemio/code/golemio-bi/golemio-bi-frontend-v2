/* eslint-disable max-len */
import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./IconFills.module.scss";

export const CloseDetailIcon = ({ color, width = "2rem", height = "2rem", ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 30 30"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            fillRule="evenodd"
            d="M15 30c8.284 0 15-6.716 15-15 0-8.284-6.716-15-15-15C6.716 0 0 6.716 0 15c0 8.284 6.716 15 15 15Z"
            clipRule="evenodd"
        />
        <path
            fill="#707D99"
            d="M9.922 19.953c-.39-.39-.375-1.055-.008-1.422l3.664-3.664-3.664-3.656c-.367-.367-.383-1.031.008-1.422.383-.39 1.047-.375 1.422-.008L15 13.445l3.656-3.656a1.024 1.024 0 0 1 1.422.008c.39.383.383 1.039.008 1.414l-3.656 3.656 3.656 3.664a1.014 1.014 0 0 1-.008 1.414c-.39.39-1.039.39-1.422.008L15 16.297l-3.656 3.664c-.375.367-1.04.383-1.422-.008Z"
        />
    </svg>
);
