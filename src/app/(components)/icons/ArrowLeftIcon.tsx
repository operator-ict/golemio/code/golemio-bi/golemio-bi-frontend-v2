/* eslint-disable max-len */
import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./IconFills.module.scss";

export const ArrowLeftIcon = ({
    color,
    width = "17px",
    height = "17px",
    fill,
    ...restProps
}: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 17 17"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <defs />
        <path
            d="M3.66357 8.54102L7.41357 4.79102C7.69482 4.48633 8.18701 4.48633 8.46826 4.79102C8.77295 5.07227 8.77295 5.56445 8.46826 5.8457L6.00732 8.30664H13.2026C13.6011 8.30664 13.9526 8.6582 13.9526 9.05664C13.9526 9.47852 13.6011 9.80664 13.2026 9.80664H6.00732L8.46826 12.291C8.77295 12.5723 8.77295 13.0645 8.46826 13.3457C8.18701 13.6504 7.69482 13.6504 7.41357 13.3457L3.66357 9.5957C3.35889 9.31445 3.35889 8.82227 3.66357 8.54102Z"
            fill={fill}
        />
    </svg>
);
