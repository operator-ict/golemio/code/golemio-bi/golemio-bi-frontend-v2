import React, { SVGProps } from "react";

const SelectedDashboardIcon = ({ width = "16px", height = "16px", fill = "#2C323F", ...restProps }: SVGProps<SVGSVGElement>) => (
    <svg style={{ width: width, height: height }} xmlns="http://www.w3.org/2000/svg" fill="none" {...restProps}>
        <rect width={16} height={16} fill={fill} rx={4} />
        <path fill="#fff" d="M4 8.165c0-.46.373-.833.833-.833H11.5a.833.833 0 0 1 0 1.667H4.833A.833.833 0 0 1 4 8.165Z" />
    </svg>
);
export default SelectedDashboardIcon;
