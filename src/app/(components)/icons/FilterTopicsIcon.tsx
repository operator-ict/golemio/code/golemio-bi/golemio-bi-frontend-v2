/* eslint-disable max-len */
import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./IconStrokes.module.scss";

export const FilterTopicsIcon = ({ color, width = "1rem", height = "1rem", ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 16 16"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path d="M6.88676 11.0619H2.68628" strokeWidth={1.5} strokeLinecap="round" strokeLinejoin="round" />
        <path d="M8.7605 4.60028H12.961" strokeWidth={1.5} strokeLinecap="round" strokeLinejoin="round" />
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M5.81761 4.56417C5.81761 3.7004 5.11217 3 4.24218 3C3.37219 3 2.66675 3.7004 2.66675 4.56417C2.66675 5.42794 3.37219 6.12834 4.24218 6.12834C5.11217 6.12834 5.81761 5.42794 5.81761 4.56417Z"
            strokeWidth={1.5}
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M13.3335 11.0358C13.3335 10.1721 12.6286 9.47168 11.7586 9.47168C10.8881 9.47168 10.1826 10.1721 10.1826 11.0358C10.1826 11.8996 10.8881 12.6 11.7586 12.6C12.6286 12.6 13.3335 11.8996 13.3335 11.0358Z"
            strokeWidth={1.5}
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </svg>
);
