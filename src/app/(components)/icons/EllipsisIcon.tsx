/* eslint-disable max-len */
import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./IconFills.module.scss";

export const EllipsisIcon = ({ color, width = "21px", height = "20px", fill, ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 21 20"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <defs />
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M 14.448 11.014 C 13.895 11.014 13.443 10.567 13.443 10.014 C 13.443 9.461 13.886 9.014 14.438 9.014 L 14.448 9.014 C 15.001 9.014 15.448 9.461 15.448 10.014 C 15.448 10.567 15.001 11.014 14.448 11.014 Z"
            fill={fill}
        />
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M 10.439 11.014 C 9.886 11.014 9.435 10.567 9.435 10.014 C 9.435 9.461 9.877 9.014 10.43 9.014 L 10.439 9.014 C 10.992 9.014 11.439 9.461 11.439 10.014 C 11.439 10.567 10.992 11.014 10.439 11.014 Z"
            fill={fill}
        />
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M 6.43 11.014 C 5.877 11.014 5.425 10.567 5.425 10.014 C 5.425 9.461 5.868 9.014 6.421 9.014 L 6.43 9.014 C 6.983 9.014 7.43 9.461 7.43 10.014 C 7.43 10.567 6.983 11.014 6.43 11.014 Z"
            fill={fill}
        />
    </svg>
);
