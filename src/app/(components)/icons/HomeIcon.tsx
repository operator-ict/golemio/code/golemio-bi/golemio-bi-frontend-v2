/* eslint-disable max-len */
import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./IconFills.module.scss";

export const HomeIcon = ({ color, width = "100%", height = "100%", ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 16 16"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path d="M16.348 7.25c0 .492-.41.875-.875.875h-.875l.027 4.375v.656a1.08 1.08 0 0 1-1.094 1.094H11.344a1.063 1.063 0 0 1-1.094-1.094V10.75a.9.9 0 0 0-.875-.875h-1.75a.881.881 0 0 0-.875.875v2.406a1.08 1.08 0 0 1-1.094 1.094H3.47a1.063 1.063 0 0 1-1.094-1.094V8.125H1.5a.864.864 0 0 1-.875-.875c0-.246.082-.465.273-.656l7-6.125C8.09.277 8.308.25 8.5.25c.191 0 .41.055.574.191l6.973 6.153c.219.191.328.41.3.656Z" />
    </svg>
);
