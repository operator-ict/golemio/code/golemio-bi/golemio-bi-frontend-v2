/* eslint-disable max-len */
import React, { SVGProps } from "react";

import styles from "@/icons/IconFills.module.scss";
import { IiCon } from "@/types/IconInterface";

export const InformationsIcon = ({
    color,
    width = "12px",
    height = "11px",
    fill = "#FFF",
    ...restProps
}: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 12 11"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            d="M6 10.25C4.20312 10.25 2.5625 9.3125 1.66406 7.75C0.765625 6.20703 0.765625 4.3125 1.66406 2.75C2.5625 1.20703 4.20312 0.25 6 0.25C7.77734 0.25 9.41797 1.20703 10.3164 2.75C11.2148 4.3125 11.2148 6.20703 10.3164 7.75C9.41797 9.3125 7.77734 10.25 6 10.25ZM5.21875 6.8125C4.94531 6.8125 4.75 7.02734 4.75 7.28125C4.75 7.55469 4.94531 7.75 5.21875 7.75H6.78125C7.03516 7.75 7.25 7.55469 7.25 7.28125C7.25 7.02734 7.03516 6.8125 6.78125 6.8125H6.625V5.09375C6.625 4.83984 6.41016 4.625 6.15625 4.625H5.21875C4.94531 4.625 4.75 4.83984 4.75 5.09375C4.75 5.36719 4.94531 5.5625 5.21875 5.5625H5.6875V6.8125H5.21875ZM6 2.75C5.64844 2.75 5.375 3.04297 5.375 3.375C5.375 3.72656 5.64844 4 6 4C6.33203 4 6.625 3.72656 6.625 3.375C6.625 3.04297 6.33203 2.75 6 2.75Z"
            fill={fill}
        />
    </svg>
);
