/* eslint-disable max-len */

import React, { SVGProps } from "react";

const FiltersIcon = ({ width = "1.2rem", height = "1.2rem", ...props }: SVGProps<SVGSVGElement>) => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 16 16"
        fill="none"
        style={{ width: width, height: height, display: "block" }}
        {...props}
    >
        <path
            stroke="#2C323F"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={1.5}
            d="M5.887 9.062H1.686M7.76 2.6h4.2"
        />
        <path
            stroke="#2C323F"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={1.5}
            d="M4.817 2.564A1.57 1.57 0 0 0 3.242 1c-.87 0-1.575.7-1.575 1.564a1.57 1.57 0 0 0 1.575 1.564c.87 0 1.575-.7 1.575-1.564ZM12.333 9.036a1.57 1.57 0 0 0-1.575-1.564c-.87 0-1.576.7-1.576 1.564a1.57 1.57 0 0 0 1.576 1.564c.87 0 1.575-.7 1.575-1.564Z"
            clipRule="evenodd"
        />
    </svg>
);

export default FiltersIcon;
