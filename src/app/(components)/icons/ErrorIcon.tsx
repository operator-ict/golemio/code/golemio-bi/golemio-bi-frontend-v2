/* eslint-disable max-len */
import React, { SVGProps } from "react";

export const ErrorIcon = ({ width = "16px", height = "16px", ...props }: SVGProps<SVGSVGElement>) => (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 13" width={width} height={height} fill="none" {...props}>
        <path
            fill="#CF1725"
            d="m6.781 2.074-5.414 8.887c-.054.082-.054.137-.054.219 0 .218.164.383.382.383h10.582c.22 0 .41-.165.41-.383 0-.082-.027-.137-.082-.22L7.191 2.075A.206.206 0 0 0 7 1.938c-.11 0-.164.054-.219.136Zm-1.12-.683A1.573 1.573 0 0 1 7 .625c.52 0 1.04.3 1.313.766l5.414 8.886c.164.274.273.575.273.903 0 .93-.766 1.695-1.723 1.695H1.695C.765 12.875 0 12.109 0 11.18c0-.328.082-.63.246-.903L5.66 1.391Zm2.214 8.421c0 .493-.41.876-.875.876a.864.864 0 0 1-.875-.876c0-.464.383-.874.875-.874a.9.9 0 0 1 .875.874Zm-.219-5.03v2.624c0 .383-.3.657-.656.657a.632.632 0 0 1-.656-.657V4.781c0-.355.273-.656.656-.656.355 0 .656.3.656.656Z"
        />
    </svg>
);
