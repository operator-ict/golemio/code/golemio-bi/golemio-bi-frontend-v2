/* eslint-disable max-len */
import React, { SVGProps } from "react";

import styles from "@/icons/IconFills.module.scss";
import { IiCon } from "@/types/IconInterface";

export const EditPenIcon = ({ color, width = "12px", height = "11px", fill, ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 12 11"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            d="M1.70312 7.30078C1.78125 7.02734 1.9375 6.77344 2.13281 6.57812L8.07031 0.640625C8.55859 0.152344 9.35938 0.152344 9.84766 0.640625L10.6094 1.40234C10.668 1.46094 10.7266 1.53906 10.7656 1.59766C11.0977 2.08594 11.0391 2.75 10.6094 3.17969L4.67188 9.11719C4.65234 9.13672 4.61328 9.15625 4.59375 9.19531C4.39844 9.35156 4.18359 9.46875 3.94922 9.54688L2.42578 9.99609L1.58594 10.25C1.42969 10.2891 1.25391 10.25 1.13672 10.1133C1 9.99609 0.960938 9.82031 1.01953 9.66406L1.25391 8.82422L1.70312 7.30078ZM2.60156 7.57422L2.46484 8.02344L2.15234 9.09766L3.22656 8.78516L3.67578 8.64844C3.8125 8.60938 3.91016 8.55078 4.00781 8.45312L8.48047 3.98047L7.26953 2.76953L2.79688 7.24219C2.77734 7.24219 2.77734 7.26172 2.75781 7.28125C2.67969 7.35938 2.64062 7.45703 2.60156 7.57422Z"
            fill={fill}
        />
    </svg>
);
