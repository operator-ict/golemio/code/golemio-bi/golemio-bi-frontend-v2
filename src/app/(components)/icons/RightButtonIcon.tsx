/* eslint-disable max-len */

import { SVGProps } from "react";

export const RightButtonIcon = (props: SVGProps<SVGSVGElement>) => (
    <svg xmlns="http://www.w3.org/2000/svg" width={12} height={12} fill="none" {...props}>
        <path
            fill="#2C323F"
            d="M8.832 5.918a.484.484 0 0 1 0 .664l-3.75 3.75c-.195.195-.488.195-.664 0a.428.428 0 0 1 0-.645L7.836 6.27 4.418 2.832a.428.428 0 0 1 0-.644.428.428 0 0 1 .644 0l3.77 3.73Z"
        />
    </svg>
);
