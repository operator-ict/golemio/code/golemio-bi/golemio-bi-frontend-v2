/* eslint-disable max-len */
import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./IconStrokes.module.scss";

export const AllRecordsIcon = ({ color, width = "1rem", height = "1rem", ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 17 16"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M2.75 4.33333C2.75 2.58319 2.76874 2 5.08333 2C7.39793 2 7.41667 2.58319 7.41667 4.33333C7.41667 6.08348 7.42405 6.66667 5.08333 6.66667C2.74262 6.66667 2.75 6.08348 2.75 4.33333Z"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M10.0833 4.33333C10.0833 2.58319 10.102 2 12.4166 2C14.7312 2 14.7499 2.58319 14.7499 4.33333C14.7499 6.08348 14.7573 6.66667 12.4166 6.66667C10.0759 6.66667 10.0833 6.08348 10.0833 4.33333Z"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M2.75 11.6666C2.75 9.91644 2.76874 9.33325 5.08333 9.33325C7.39793 9.33325 7.41667 9.91644 7.41667 11.6666C7.41667 13.4167 7.42405 13.9999 5.08333 13.9999C2.74262 13.9999 2.75 13.4167 2.75 11.6666Z"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M10.0833 11.6666C10.0833 9.91644 10.102 9.33325 12.4166 9.33325C14.7312 9.33325 14.7499 9.91644 14.7499 11.6666C14.7499 13.4167 14.7573 13.9999 12.4166 13.9999C10.0759 13.9999 10.0833 13.4167 10.0833 11.6666Z"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </svg>
);
