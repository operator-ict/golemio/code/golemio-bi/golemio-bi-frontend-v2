/* eslint-disable max-len */
import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./IconStrokes.module.scss";

export const StarOutlineIcon = ({
    color = "gray-dark",
    width = "14px",
    height = "14px",
    fill,
    ...restProps
}: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        viewBox="0 0 14 14"
        fill={fill || "none"}
        xmlns="http://www.w3.org/2000/svg"
        {...restProps}
    >
        <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M7.98617 1.78459L9.20445 4.21842C9.32384 4.45736 9.55431 4.62303 9.82157 4.66137L12.5469 5.05366C13.2202 5.15088 13.4882 5.96695 13.0009 6.43455L11.0301 8.32821C10.8364 8.51443 10.7483 8.78212 10.7941 9.04501L11.2592 11.7185C11.3737 12.3798 10.6698 12.8844 10.068 12.5715L7.63214 11.3084C7.39335 11.1845 7.10735 11.1845 6.86786 11.3084L4.432 12.5715C3.83015 12.8844 3.12626 12.3798 3.2415 11.7185L3.7059 9.04501C3.75171 8.78212 3.66355 8.51443 3.46988 8.32821L1.49912 6.43455C1.01181 5.96695 1.27976 5.15088 1.95311 5.05366L4.67843 4.66137C4.94569 4.62303 5.17685 4.45736 5.29625 4.21842L6.51383 1.78459C6.8151 1.18281 7.6849 1.18281 7.98617 1.78459Z"
            strokeLinecap="round"
            strokeLinejoin="round"
        />
    </svg>
);
