/* eslint-disable max-len */
import React, { SVGProps } from "react";

export const FavouritesWidgets = ({ ...restProps }: SVGProps<SVGSVGElement>) => (
    <svg viewBox="0 0 152 118" width="152px" height="118px" fill="none" xmlns="http://www.w3.org/2000/svg" {...restProps}>
        <circle cx={76} cy={52} r={52} fill="#DBE6F2" />
        <g filter="url(#a)">
            <path
                fill="#F9F5FF"
                fillRule="evenodd"
                d="M77.6 16c-10.773 0-20.302 5.323-26.101 13.483A25.672 25.672 0 0 0 45.6 28.8C31.461 28.8 20 40.261 20 54.4 20 68.539 31.462 80 45.6 80h64c12.371 0 22.4-10.029 22.4-22.4 0-12.371-10.029-22.4-22.4-22.4-.879 0-1.746.05-2.598.149C102.098 23.968 90.78 16 77.6 16Z"
                clipRule="evenodd"
            />
            <circle cx={45.6} cy={54.4} r={25.6} fill="url(#b)" />
            <circle cx={77.6} cy={48} r={32} fill="url(#c)" />
            <circle cx={109.6} cy={57.6} r={22.4} fill="url(#d)" />
        </g>
        <circle cx={21} cy={19} r={5} fill="#EFF7FF" />
        <circle cx={18} cy={109} r={7} fill="#EFF7FF" />
        <circle cx={145} cy={35} r={7} fill="#EFF7FF" />
        <circle cx={134} cy={8} r={4} fill="#EFF7FF" />
        <g filter="url(#e)">
            <rect width={48} height={48} x={52} y={62} fill="#335F92" fillOpacity={0.3} rx={24} />
            <path
                stroke="#fff"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="m85 95-4.35-4.35M83 85a8 8 0 1 1-16 0 8 8 0 0 1 16 0Z"
            />
        </g>
        <defs>
            <linearGradient id="b" x1={25.943} x2={71.2} y1={37.486} y2={80} gradientUnits="userSpaceOnUse">
                <stop stopColor="#DDEDFF" />
                <stop offset={0.351} stopColor="#fff" stopOpacity={0} />
            </linearGradient>
            <linearGradient id="c" x1={53.029} x2={109.6} y1={26.857} y2={80} gradientUnits="userSpaceOnUse">
                <stop stopColor="#DDEDFF" />
                <stop offset={0.351} stopColor="#fff" stopOpacity={0} />
            </linearGradient>
            <linearGradient id="d" x1={92.4} x2={132} y1={42.8} y2={80} gradientUnits="userSpaceOnUse">
                <stop stopColor="#DDEDFF" />
                <stop offset={0.351} stopColor="#fff" stopOpacity={0} />
            </linearGradient>
            <filter id="a" width={152} height={104} x={0} y={16} colorInterpolationFilters="sRGB" filterUnits="userSpaceOnUse">
                <feFlood floodOpacity={0} result="BackgroundImageFix" />
                <feColorMatrix in="SourceAlpha" result="hardAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                <feMorphology in="SourceAlpha" radius={4} result="effect1_dropShadow_523_11236" />
                <feOffset dy={8} />
                <feGaussianBlur stdDeviation={4} />
                <feColorMatrix values="0 0 0 0 0.0627451 0 0 0 0 0.0941176 0 0 0 0 0.156863 0 0 0 0.04 0" />
                <feBlend in2="BackgroundImageFix" result="effect1_dropShadow_523_11236" />
                <feColorMatrix in="SourceAlpha" result="hardAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                <feMorphology in="SourceAlpha" radius={4} result="effect2_dropShadow_523_11236" />
                <feOffset dy={20} />
                <feGaussianBlur stdDeviation={12} />
                <feColorMatrix values="0 0 0 0 0.0627451 0 0 0 0 0.0941176 0 0 0 0 0.156863 0 0 0 0.1 0" />
                <feBlend in2="effect1_dropShadow_523_11236" result="effect2_dropShadow_523_11236" />
                <feBlend in="SourceGraphic" in2="effect2_dropShadow_523_11236" result="shape" />
            </filter>
            <filter id="e" width={64} height={64} x={44} y={54} colorInterpolationFilters="sRGB" filterUnits="userSpaceOnUse">
                <feFlood floodOpacity={0} result="BackgroundImageFix" />
                <feGaussianBlur in="BackgroundImageFix" stdDeviation={4} />
                <feComposite in2="SourceAlpha" operator="in" result="effect1_backgroundBlur_523_11236" />
                <feBlend in="SourceGraphic" in2="effect1_backgroundBlur_523_11236" result="shape" />
            </filter>
        </defs>
    </svg>
);
