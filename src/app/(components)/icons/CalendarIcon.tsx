/* eslint-disable max-len */
import * as React from "react";
import { SVGProps } from "react";
const CalendarIcon = (props: SVGProps<SVGSVGElement>) => (
    <svg xmlns="http://www.w3.org/2000/svg" width={16} height={16} fill="none" {...props}>
        <path
            fill="#707D99"
            d="M5.656 2.406V3.5h3.938V2.406c0-.355.273-.656.656-.656.355 0 .656.3.656.656V3.5H12c.957 0 1.75.793 1.75 1.75V14c0 .984-.793 1.75-1.75 1.75H3.25c-.984 0-1.75-.766-1.75-1.75V5.25c0-.957.766-1.75 1.75-1.75h1.094V2.406c0-.355.273-.656.656-.656.355 0 .656.3.656.656ZM2.813 7v7c0 .246.19.438.437.438H12a.45.45 0 0 0 .438-.438V7H2.812Z"
        />
    </svg>
);
export default CalendarIcon;
