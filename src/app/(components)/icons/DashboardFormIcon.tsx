/* eslint-disable max-len */

import * as React from "react";
import { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

const DashboardFormIcon = ({ width = "24px", height = "24px", ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg {...restProps} xmlns="http://www.w3.org/2000/svg" width={width} height={height} fill="none">
        <path
            fill="#707D99"
            d="M8.875 5.625h-5V10h5V5.625Zm-5-1.875h5c1.016 0 1.875.86 1.875 1.875V10c0 1.055-.86 1.875-1.875 1.875h-5A1.851 1.851 0 0 1 2 10V5.625c0-1.016.82-1.875 1.875-1.875Zm5 11.25h-5v4.375h5V15Zm-5-1.875h5c1.016 0 1.875.86 1.875 1.875v4.375c0 1.055-.86 1.875-1.875 1.875h-5A1.851 1.851 0 0 1 2 19.375V15c0-1.016.82-1.875 1.875-1.875Zm10-7.5v1.25h6.25v-1.25h-6.25Zm-1.875 0c0-1.016.82-1.875 1.875-1.875h6.25c1.016 0 1.875.86 1.875 1.875v1.25c0 1.055-.86 1.875-1.875 1.875h-6.25A1.851 1.851 0 0 1 12 6.875v-1.25Zm8.125 6.25h-6.25v1.25h6.25v-1.25ZM13.875 10h6.25c1.016 0 1.875.86 1.875 1.875v1.25C22 14.18 21.14 15 20.125 15h-6.25A1.851 1.851 0 0 1 12 13.125v-1.25c0-1.016.82-1.875 1.875-1.875Zm0 8.125v1.25h6.25v-1.25h-6.25Zm-1.875 0c0-1.016.82-1.875 1.875-1.875h6.25c1.016 0 1.875.86 1.875 1.875v1.25c0 1.055-.86 1.875-1.875 1.875h-6.25A1.851 1.851 0 0 1 12 19.375v-1.25Z"
        />
    </svg>
);
export default DashboardFormIcon;
