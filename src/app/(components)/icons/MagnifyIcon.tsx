/* eslint-disable max-len */
import React, { SVGProps } from "react";

import { IiCon } from "@/types/IconInterface";

import styles from "./IconFills.module.scss";

export const MagnifyIcon = ({ color, width = "14px", height = "14px", ...restProps }: IiCon & SVGProps<SVGSVGElement>) => (
    <svg
        className={styles[`icon_color-${color}`]}
        style={{ width: width, height: height }}
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        {...restProps}
    >
        <path
            // eslint-disable-next-line max-len
            d="M4.148 6.854a.723.723 0 0 1 1.055 0 .723.723 0 0 1 0 1.054l-2.648 2.649H3.75c.398 0 .75.351.75.75 0 .421-.352.75-.75.75h-3c-.117 0-.21 0-.305-.047a.67.67 0 0 1-.398-.399C0 11.518 0 11.424 0 11.307v-3c0-.399.328-.75.75-.75.398 0 .75.351.75.75v1.195l2.648-2.648ZM11.93.525c.047.094.07.188.07.282v3c0 .422-.352.75-.75.75a.74.74 0 0 1-.75-.75V2.635L7.828 5.283a.723.723 0 0 1-1.055 0 .723.723 0 0 1 0-1.054l2.649-2.672H8.25a.74.74 0 0 1-.75-.75c0-.399.328-.75.75-.75h3c.094 0 .188.023.281.07a.67.67 0 0 1 .399.398Z"
        />
    </svg>
);
