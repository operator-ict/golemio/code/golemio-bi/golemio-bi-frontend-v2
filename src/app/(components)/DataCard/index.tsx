"use client";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import React from "react";

import { FavouritesButton } from "@/(private)/(home)/(FavouritesButton)";
import { Card } from "@/components/Card";
import { Chip } from "@/components/Chip";
import { Paragraph } from "@/components/Paragraph";
import { IMetadata } from "@/types/MetadataType";
import { ITag } from "@/types/TagType";

import { EditDashboardsBtn } from "../EditDashboardsBtn/index";
import { Title } from "../Title";
import styles from "./DataCard.module.scss";

type Props = {
    metadata: IMetadata;
    isFavourite: boolean;
    imagePath: string;
    canEditDashboards?: boolean;
};

export const DataCard = ({ metadata, imagePath, canEditDashboards, isFavourite }: Props) => {
    const router = useRouter();

    const linkEnabled =
        (metadata.type === "dashboard" || metadata.type === "application") && metadata.component !== "iframeComponent";

    const chipHandler = (tag: ITag) => {
        router.push(`?tag=${tag.title}`);
    };

    return (
        <Card tag="div" className={styles["data-card"]}>
            <Link href={`/dashboard${metadata.client_route}`} className={styles["data-card__image-frame"]}>
                <Image
                    src={imagePath}
                    alt={`titulni obrázek k ${metadata.title}`}
                    fill
                    sizes="(max-width: 576px) 100vw, (max-width: 992px) 33vw, 25vw"
                />
            </Link>
            <div className={styles["data-card__text"]}>
                <div className={styles["data-card__button-area"]}>
                    <div className={styles["data-card__chip-area"]}>
                        {metadata?.tags?.map((tag, i) => (
                            <Chip tag="button" id={String(i)} label={tag.title} key={i} onClick={() => chipHandler(tag)} />
                        ))}
                    </div>
                    <div className={styles["data-card__actions"]}>
                        <FavouritesButton isFavourite={isFavourite} id={metadata._id} />
                        {canEditDashboards && <EditDashboardsBtn id={metadata._id} />}
                    </div>
                </div>
                {linkEnabled ? (
                    <Link href={`/dashboard${metadata.client_route}`}>
                        <Title type="t1m">{metadata.title}</Title>
                    </Link>
                ) : (
                    <Title type="t1m">{metadata.title}</Title>
                )}
                <Paragraph type="p2" className={styles["data-card__paragraph"]}>
                    {metadata.description}
                </Paragraph>
            </div>
        </Card>
    );
};
