/* eslint-disable react/display-name */
"use client";
import React, { useState } from "react";

import { FavouritesButton } from "@/(private)/(home)/(FavouritesButton)";
import { IMetadata } from "@/(types)/MetadataType";

import SearchableDropdown from "../SearchableDropdown";
import styles from "./DropdownComponent.module.scss";

interface DropdownComponentProps {
    metadataFilter?: IMetadata[];
    title: string;
    isFavourite?: boolean;
    id: string;
}

const DropdownComponent = ({ metadataFilter, title, isFavourite, id }: DropdownComponentProps) => {
    const [selectedVal, setSelectedVal] = useState<IMetadata | null>(null);
    const filteredData = metadataFilter?.filter((item) => item.data !== null);

    return (
        <div className={styles["dropdown-container"]}>
            <SearchableDropdown
                className={styles["searchable-component"]}
                options={filteredData}
                title={title}
                id="SearchableDropdown"
                selectedVal={selectedVal}
                handleChange={(val) => setSelectedVal(val)}
            />
            <FavouritesButton isFavourite={isFavourite} id={id} isDetail={!!id} />
        </div>
    );
};

export default DropdownComponent;
