"use client";
import Link from "next/link";
import { useRouter } from "next/navigation";
import React, { FC, useState } from "react";

import { Dropdown } from "@/(components)/Dropdown";
import { useUpdatedQuery } from "@/hooks/useUpdatedQuery";
import { DeleteIcon } from "@/icons/DeleteIcon";
import { EditIcon } from "@/icons/EditIcon";

import Button from "../Button";
import { EllipsisIcon } from "../icons/EllipsisIcon";
import styles from "./EditDashboardsBtn.module.scss";

type EditDashboardsBtnProps = {
    id: string;
};
export const EditDashboardsBtn: FC<EditDashboardsBtnProps> = ({ id }) => {
    const [visible, setVisible] = useState(false);

    const router = useRouter();

    const delQuery = useUpdatedQuery("delete", id);

    const settingsMenu = [
        { title: "Upravit", icon: <EditIcon color="gray-darker" />, link: `/edit-metadata/${id}` },
        { title: "Smazat", icon: <DeleteIcon color="error" /> },
    ];
    const content = (
        <ul className={styles["edit-actions"]}>
            {settingsMenu.map((item, i) => (
                <li key={i}>
                    {item.link && item.icon ? (
                        <Link href={item.link} className={styles["edit-actions-link"]}>
                            {item.icon}
                            {item.title}
                        </Link>
                    ) : (
                        <button onClick={() => router.push(`?${delQuery}`)} className={styles["edit-actions-button"]}>
                            {item.icon}
                            {item.title}
                        </button>
                    )}
                </li>
            ))}
        </ul>
    );

    const handleDropdown = () => {
        setVisible(!visible);
    };

    return (
        <Dropdown contents={content} visible={visible} hide={handleDropdown} className={styles["edit-dropdown"]}>
            <Button
                color="tertiary"
                className={styles["dropdown-button"]}
                label={visible ? "Zavřít dropdown" : "Otevřít dropdown"}
                hideLabel
                onClick={handleDropdown}
                iconStart={<EllipsisIcon color="gray-darker" />}
            />
        </Dropdown>
    );
};
