"use client";
/* eslint-disable react/display-name */

import { useRouter } from "next/navigation";
import { usePlausible } from "next-plausible";
import React, { useEffect, useRef, useState } from "react";

import { FavouritesButton } from "@/(private)/(home)/(FavouritesButton)";
import { LogoDashboard } from "@/components/LogoDashboard";
import SearchableDropdown from "@/components/SearchableDropdown";
import { useScrollHandler } from "@/hooks/useScrollHandler ";
import { ArrowLeftIcon } from "@/icons/ArrowLeftIcon";
import { IFeatureCollection } from "@/types/FeaturesTypes";
import { UserProfileResponse } from "@/types/formTypes";
import { IMetadata } from "@/types/MetadataType";

import ButtonLink from "../ButtonLink";
import styles from "./HeaderDashboard.module.scss";

type PageProps = {
    metadataFilter?: IMetadata[];
    title: string;
    type: string;
    user: UserProfileResponse;
    data?: IFeatureCollection;
    isFavourite?: boolean;
    id: string;
};

const extractEmail = (email: string) => {
    const atIndex = email.indexOf("@");
    if (atIndex !== -1) {
        return email.substring(atIndex + 1);
    }
    return "";
};

const HeaderDashboard = ({ metadataFilter, title, type, isFavourite, user, id }: PageProps) => {
    const isShrunk = useScrollHandler();
    const logoRef = useRef<HTMLDivElement | null>(null);
    const [selectedVal, setSelectedVal] = useState<IMetadata | null>(null);
    const plausible = usePlausible();
    const router = useRouter();

    useEffect(() => {
        if (typeof window !== "undefined") {
            plausible("trackUsersEvent", {
                props: {
                    userName: user?.name ? [user?.name, user?.surname].filter(Boolean).join(" ") : null,
                    userEmail: user?.email,
                    userDomain: user?.email ? extractEmail(user.email) : null,
                    dashboard: title || "",
                },
            });
        }
    }, [user, title, plausible]);

    return (
        <header className={`${styles.header} ${isShrunk ? styles["header_scrolled"] : ""}`}>
            <LogoDashboard alt="logo Golemio" src="" ref={logoRef} />
            <ButtonLink
                className={styles["button-back"]}
                label="Zpět"
                color="tertiary"
                url="#"
                onMouseDown={(e) => {
                    if (e.button === 1) {
                        window.open("/", "_blank");
                    } else if (e.button === 0) {
                        if (window.history.length > 1) {
                            e.preventDefault();
                            router.back();
                        } else {
                            window.location.href = "/";
                        }
                    }
                }}
                iconStart={<ArrowLeftIcon color="gray-darker" />}
            />
            {type === "dashboard" && (
                <div className={styles["header-searchable-container"]}>
                    <SearchableDropdown
                        options={metadataFilter}
                        title={title}
                        id="id"
                        selectedVal={selectedVal}
                        handleChange={(val) => setSelectedVal(val)}
                    />
                    <div className={styles["button-container"]}>
                        <FavouritesButton isFavourite={isFavourite} id={id} isDetail={!!id} />
                    </div>
                </div>
            )}
        </header>
    );
};

export default HeaderDashboard;
