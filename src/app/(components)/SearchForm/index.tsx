/* eslint-disable react-hooks/exhaustive-deps */
"use client";
import { useRouter, useSearchParams } from "next/navigation";
import { usePlausible } from "next-plausible";
import React, { forwardRef, KeyboardEvent, useEffect, useState } from "react";

import { SearchInput } from "@/components/SearchInput";

import styles from "./SearchForm.module.scss";

type SearchFormProps = {
    search?: string;
    label?: string;
    fullWidth?: boolean;
    placeholder?: string;
    className?: string;
};

// eslint-disable-next-line react/display-name
export const SearchForm = forwardRef<HTMLDivElement, SearchFormProps>((SearchFormProps, ref) => {
    const { search, label, fullWidth, placeholder, className } = SearchFormProps;
    const router = useRouter();
    const searchParams = useSearchParams();
    const tag = searchParams.get("tag");
    const fav = searchParams.get("fav");
    const sp = searchParams.get("search");
    const plausible = usePlausible();
    const [inputValue, setInputValue] = useState(sp ?? "");

    const updateInputValue = (value: string) => {
        setInputValue(value);
    };

    const searchClicked = () => {
        const currentParams = new URLSearchParams(window.location.search);

        if (inputValue !== "") {
            currentParams.set("search", inputValue);
        } else {
            currentParams.delete("search");
        }

        if (fav) {
            currentParams.set("fav", fav);
        }

        currentParams.delete("page");
        currentParams.delete("limit");

        if (inputValue) {
            router.push(`?${currentParams.toString()}`);
        }
    };

    const onKeyHandler = (event: KeyboardEvent) => {
        if (event.key === "Enter") {
            searchClicked();
        }
    };

    const resetInputField = () => {
        updateInputValue("");
        router.push(`?`);
    };

    useEffect(() => {
        const delayInputTimeout = setTimeout(() => {
            if (!tag) {
                searchClicked();
            }
            return;
        }, 1000);
        return () => clearTimeout(delayInputTimeout);
    }, [inputValue]);

    useEffect(() => {
        if (!search) {
            setInputValue("");
        } else {
            setInputValue(search as string);
            plausible("trackSearchEvent", {
                props: {
                    searchValue: inputValue,
                },
            });
        }
    }, [search]);

    return (
        <div className={`${styles["search-form"]} ${fullWidth ? styles.full : ""} ${className ?? ""}`} ref={ref}>
            <SearchInput
                name="dataSearch"
                aria-label={label ?? "Vyhledávací pole"}
                placeholder={placeholder}
                onKeyDown={onKeyHandler}
                resetInputField={resetInputField}
                updateInputValue={updateInputValue}
                value={inputValue}
            />
        </div>
    );
});
