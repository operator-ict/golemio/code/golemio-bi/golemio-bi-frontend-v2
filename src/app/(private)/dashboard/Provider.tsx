"use client";

import { SortedWasteFiltersProvider } from "@/context/SortedWasteFiltersProvider";

type ProviderProps = {
    children: React.ReactNode;
};
export const Provider = ({ children }: ProviderProps) => {
    return <SortedWasteFiltersProvider>{children}</SortedWasteFiltersProvider>;
};
