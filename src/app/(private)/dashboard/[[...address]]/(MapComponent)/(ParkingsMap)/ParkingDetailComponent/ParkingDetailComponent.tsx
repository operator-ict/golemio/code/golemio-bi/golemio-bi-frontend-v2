/* eslint-disable max-len */
"use client";
import React, { FC } from "react";

import { IParkingProperties } from "@/(types)/ParkingsType";
import text from "@/textContent/cs.json";

import styles from "./ParkingDetailComponent.module.scss";

interface DetailProps {
    properties: IParkingProperties;
}

function timeAgo(dateString: string, now: Date): string {
    const past = new Date(dateString);
    const diffInMilliseconds = now.getTime() - past.getTime();
    const diffInMinutes = Math.floor(diffInMilliseconds / 1000 / 60);

    if (diffInMinutes < 60) {
        return `před ${diffInMinutes} minutami`;
    } else if (diffInMinutes < 1440) {
        const hours = Math.floor(diffInMinutes / 60);
        return `před ${hours} hodinami`;
    } else {
        const days = Math.floor(diffInMinutes / 1440);
        return `před ${days} dny`;
    }
}

export const ParkingDetail: FC<DetailProps> = ({ properties }) => {
    const now = new Date();
    return (
        <div className={styles["depo-container"]}>
            <div className={styles["depo-container__data-box"]}>
                <span>{text.map.parkingMap.capacity}</span>
                <span>{properties.total_spot_number}</span>
            </div>
            <div className={styles["depo-container__data-box"]}>
                <span>{text.map.parkingMap.freeSpots}</span>
                <span>{properties.available_spots_number || "0"}</span>
            </div>

            <div className={styles["depo-container__data-box"]}>
                <span>{text.map.parkingMap.occupied}</span>
                <span>{properties.total_spot_number - properties.available_spots_number || "0"}</span>
            </div>
            <div className={styles["depo-container__data-box"]}>
                <span>{text.map.parkingMap.lastUpdate}</span>
                <span>{timeAgo(properties.available_spots_last_updated, now)}</span>
            </div>
        </div>
    );
};
