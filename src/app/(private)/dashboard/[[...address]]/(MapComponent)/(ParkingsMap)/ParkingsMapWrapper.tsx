"use client";
import React, { FC, ReactNode, use, useMemo } from "react";

import { IParkingCollection } from "@/(types)/ParkingsType";
import { Title } from "@/components/Title";
import text from "@/textContent/cs.json";

import styles from "../../DashboardPage.module.scss";
import MapComponent from "..";
import { parkingRideBlue, parkingRideIcon } from "../(MapIcons)/MapIcons";

export const getParkingIcon = (parkingTypeId: string | undefined) => {
    switch (parkingTypeId) {
        case "sorted-waste":
            return "sorted-waste";
        case "park_and_ride":
            return "parking-ride-marker";
        case "park_paid_private":
            return "parking-paid-marker";
        case "active-icon":
            return "active-icon";
        default:
            return "parking-marker";
    }
};

const transformData = (data: IParkingCollection) => {
    return {
        ...data,
        features: data.features.map((feature) => {
            return {
                geometry: feature.properties.centroid,
                properties: {
                    ...feature.properties,
                    icon: getParkingIcon(feature.properties.parking_type),
                },
                type: feature.type,
            };
        }),
    };
};
interface WrapperProps {
    children?: ReactNode;
    data: Promise<IParkingCollection>;
    mapboxToken: string;
}

export const ParkingsMapWrapper: FC<WrapperProps> = ({ data, mapboxToken }) => {
    const dataFromPromise = use(data);

    const transformedData = useMemo(() => {
        return transformData(dataFromPromise);
    }, [dataFromPromise]);

    return (
        <section className={styles["map-container"]}>
            <div className={styles["title-container"]}>
                <Title type={"t1"}>{text.map.sortedWaste.mainTitle}</Title>
            </div>
            <MapComponent
                map="parking-ride"
                mapboxToken={mapboxToken}
                data={transformedData}
                customIcons={{
                    images: [
                        ["parking-ride-marker", parkingRideIcon as HTMLImageElement],
                        ["active-icon", parkingRideBlue as HTMLImageElement],
                    ],
                    markerIcon: "{icon}",
                    clusterIcon: "parking-ride-marker",
                    activeIcon: "active-icon",
                }}
            />
        </section>
    );
};
