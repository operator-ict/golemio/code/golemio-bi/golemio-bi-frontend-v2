import { FC, useContext, useEffect, useMemo, useRef, useState } from "react";

import Button from "@/(components)/Button";
import { Dropdown } from "@/(components)/Dropdown";
import FilterMultiSelect from "@/(components)/FilterMultiSelect";
import SelectFilters from "@/(components)/icons/ActiveFilter";
import { ChevronIcon } from "@/(components)/icons/ChevronIcon";
import { SearchInput } from "@/(components)/SearchInput";
import { Title } from "@/(components)/Title";
import { SortedWasteFiltersContext } from "@/(context)/SortedWasteFiltersProvider";
import { ICityDistrictsFeatureCollection } from "@/(types)/cityDistricts";
import { sortAndFilterDistricts } from "@/(utils)/districtsSort";
import text from "@/textContent/cs.json";

import styles from "../Filters.module.scss";

interface FilteredProps {
    districts: ICityDistrictsFeatureCollection;
    onChange: (active: boolean) => void;
    visible: boolean;
    onToggle: () => void;
}
const AreaFilter: FC<FilteredProps> = ({ districts, onChange, visible, onToggle }) => {
    const { filterOptions, setFilterOptions } = useContext(SortedWasteFiltersContext);
    const buttonRef = useRef<HTMLButtonElement>(null);
    const [inputValue, setInputValue] = useState<string>("");

    const districtList = districts?.features?.map((district) => ({
        label: district.properties.name,
        value: district.properties.slug,
    }));

    const filteredDistricts = useMemo(() => {
        return sortAndFilterDistricts(districtList, inputValue);
    }, [inputValue, districtList]);

    const updateInputValue = (value: string) => {
        setInputValue(value);
    };

    const resetInputField = () => {
        updateInputValue("");
    };
    useEffect(() => {
        onChange(filterOptions.area.length > 0 && filterOptions.area[0] !== "");
    }, [filterOptions.area, onChange]);

    const handleFilterChange = (values: string[]): void => {
        if (values.includes("")) {
            setFilterOptions("area", []);
        } else {
            setFilterOptions("area", values);
        }
    };
    return (
        <Dropdown
            visible={visible}
            className={styles["filter-card"]}
            filter
            contents={
                <div className={styles["multiselect-list"]}>
                    <SearchInput
                        name="dataSearch"
                        aria-label="Vyhledejte oblast"
                        placeholder="Vyhledejte oblast"
                        resetInputField={resetInputField}
                        updateInputValue={updateInputValue}
                        value={inputValue}
                    />
                    <FilterMultiSelect
                        values={filterOptions.area}
                        onChange={handleFilterChange}
                        optionList={[
                            { label: text.map.sortedWaste.filters.all, value: "", type: "checkbox" },
                            ...filteredDistricts.map((item) => ({
                                label: item.label || "Unknown-label",
                                value: item.value || "unknown-value",
                            })),
                        ]}
                    />
                </div>
            }
        >
            <Button className={styles["filter-button"]} onClick={onToggle} size="sm" color="link" ref={buttonRef}>
                <div className={styles["filter-button__title"]}>
                    <Title type="t3m">{text.map.sortedWaste.filters.area}</Title>
                    <div>{filterOptions.area.length > 0 && filterOptions.area[0] !== "" ? <SelectFilters /> : null}</div>
                </div>
                <ChevronIcon color="gray-dark" height={14} direction={visible ? "up" : "down"} />
            </Button>
        </Dropdown>
    );
};

export default AreaFilter;
