import { FC, useContext, useEffect } from "react";

import Button from "@/(components)/Button";
import { Dropdown } from "@/(components)/Dropdown";
import FilterRange from "@/(components)/FilterRange";
import SelectFilters from "@/(components)/icons/ActiveFilter";
import { ChevronIcon } from "@/(components)/icons/ChevronIcon";
import { Title } from "@/(components)/Title";
import { SortedWasteFiltersContext } from "@/(context)/SortedWasteFiltersProvider";
import text from "@/textContent/cs.json";

import styles from "../Filters.module.scss";

type DropdownVisibility = {
    fullness: boolean;
    area: boolean;
    sensor: boolean;
    type: boolean;
};
interface IFullnessFilter {
    onChange: (active: boolean) => void;
    visible: boolean;
    onToggle: () => void;
    setDropdownVisibility: React.Dispatch<React.SetStateAction<DropdownVisibility>>;
}

const FullnessFilter: FC<IFullnessFilter> = ({ onChange, visible, onToggle, setDropdownVisibility }) => {
    const { filterOptions, setFilterOptions } = useContext(SortedWasteFiltersContext);

    const fullnessEnabled = filterOptions.sensor === "sensor_only";

    useEffect(() => {
        onChange(filterOptions.fullness && (filterOptions.fullness.min > 0 || filterOptions.fullness.max < 100));
    }, [filterOptions, onChange]);

    useEffect(() => {
        if (!fullnessEnabled) {
            setDropdownVisibility((prevState: DropdownVisibility) => ({ ...prevState, fullness: false }));
        }
    }, [fullnessEnabled, setDropdownVisibility]);

    return (
        <Dropdown
            className={styles["filter-card"]}
            visible={visible}
            filter
            contents={
                <div className={styles["range-list"]}>
                    <FilterRange
                        title={text.map.sortedWaste.fullness}
                        values={[filterOptions.fullness.min, filterOptions.fullness.max]}
                        max={100}
                        min={0}
                        unit="%"
                        onChange={(min, max) => setFilterOptions("fullness", { min, max })}
                        disabled={!fullnessEnabled}
                    />
                </div>
            }
        >
            <Button onClick={onToggle} color="link" disabled={!fullnessEnabled} className={styles["filter-button"]} size="sm">
                <div className={styles["filter-button__title"]}>
                    <Title type="t3m">{text.map.sortedWaste.fullness}</Title>
                    <div>
                        {filterOptions.fullness && (filterOptions.fullness.min > 0 || filterOptions.fullness.max < 100) ? (
                            <SelectFilters />
                        ) : null}
                    </div>
                </div>
                <ChevronIcon color="gray-dark" height={14} direction={visible ? "up" : "down"} />
            </Button>
        </Dropdown>
    );
};

export default FullnessFilter;
