import { FC, useContext, useEffect, useRef } from "react";

import Button from "@/(components)/Button";
import { Dropdown } from "@/(components)/Dropdown";
import { ChevronIcon } from "@/(components)/icons/ChevronIcon";
import { Radio } from "@/(components)/Radio";
import { Title } from "@/(components)/Title";
import { SortedWasteFiltersContext } from "@/(context)/SortedWasteFiltersProvider";
import text from "@/textContent/cs.json";

import styles from "../Filters.module.scss";

interface ISensorFilter {
    onChange: (active: boolean) => void;
    visible: boolean;
    onToggle: () => void;
}
const SensorFilter: FC<ISensorFilter> = ({ onChange, visible, onToggle }) => {
    const { filterOptions, setFilterOptions } = useContext(SortedWasteFiltersContext);
    const buttonRef = useRef<HTMLButtonElement>(null);

    const optionsSensor: { label: string; value: "all" | "sensor_only" }[] = [
        { label: text.map.sortedWaste.filters.all, value: "all" },
        { label: text.map.sortedWaste.filters.sensorOnly, value: "sensor_only" },
    ];

    useEffect(() => {
        onChange(filterOptions.sensor === "sensor_only");
    }, [filterOptions.sensor, onChange]);
    return (
        <Dropdown
            visible={visible}
            className={styles["filter-card"]}
            filter
            contents={
                <div className={styles["containers-list"]}>
                    <ul role="listbox">
                        {optionsSensor.map((option) => (
                            <Radio
                                key={option.value}
                                title={text.map.sortedWaste.filters.containers}
                                id={option.value}
                                label={option.label}
                                name={option.label}
                                checked={option.value === filterOptions.sensor}
                                onClick={() => setFilterOptions("sensor", option.value as "all" | "sensor_only")}
                                role="option"
                            />
                        ))}
                    </ul>
                </div>
            }
        >
            <Button className={styles["filter-button"]} ref={buttonRef} size="sm" onClick={onToggle} color="link">
                <Title type="t3m">{text.map.sortedWaste.filters.containers}</Title>
                <ChevronIcon color="gray-dark" height={14} direction={visible ? "up" : "down"} />
            </Button>
        </Dropdown>
    );
};
export default SensorFilter;
