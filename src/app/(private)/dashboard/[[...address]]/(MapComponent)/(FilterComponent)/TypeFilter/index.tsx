import { FC, useContext, useEffect } from "react";

import Button from "@/(components)/Button";
import { Dropdown } from "@/(components)/Dropdown";
import FilterMultiSelect from "@/(components)/FilterMultiSelect";
import SelectFilters from "@/(components)/icons/ActiveFilter";
import { ChevronIcon } from "@/(components)/icons/ChevronIcon";
import { Title } from "@/(components)/Title";
import { SortedWasteFiltersContext } from "@/(context)/SortedWasteFiltersProvider";
import text from "@/textContent/cs.json";

import styles from "../Filters.module.scss";

interface ITypeFilter {
    onChange: (active: boolean) => void;
    visible: boolean;
    onToggle: () => void;
}

const TypeFilter: FC<ITypeFilter> = ({ onChange, visible, onToggle }) => {
    const { filterOptions, setFilterOptions } = useContext(SortedWasteFiltersContext);

    const handleFilterChange = (values: string[]): void => {
        if (values.includes("")) {
            setFilterOptions("types", []);
        } else {
            setFilterOptions("types", values);
        }
    };

    useEffect(() => {
        onChange(filterOptions.types.length > 0 && filterOptions.types[0] !== "");
    }, [filterOptions.types, onChange]);

    return (
        <Dropdown
            className={styles["filter-card"]}
            filter
            contents={
                <div className={styles["multiselect-list"]}>
                    <FilterMultiSelect
                        title={text.map.sortedWaste.filters.wasteTypes}
                        values={filterOptions.types}
                        onChange={handleFilterChange}
                        optionList={[
                            {
                                label: text.map.sortedWaste.filters.all,
                                value: "",
                                type: "checkbox",
                            },
                            ...[5, 6, 9, 3, 4, 1, 7, 8, 2].map((item) => ({
                                label: text.map.sortedWaste.trashType[
                                    item as unknown as keyof typeof text.map.sortedWaste.trashType
                                ],
                                value: String(item),
                            })),
                        ]}
                    />
                </div>
            }
            visible={visible}
        >
            <Button className={styles["filter-button"]} onClick={onToggle} size="sm" color="link">
                <div className={styles["filter-button__title"]}>
                    <Title type="t3m">{text.map.sortedWaste.filters.wasteTypes}</Title>
                    <div> {filterOptions.types.length > 0 && filterOptions.types[0] !== "" ? <SelectFilters /> : null}</div>
                </div>
                <ChevronIcon color="gray-dark" height={14} direction={visible ? "up" : "down"} />
            </Button>
        </Dropdown>
    );
};

export default TypeFilter;
