import { FC, useContext, useEffect, useState } from "react";

import { ChevronIcon } from "@/(components)/icons/ChevronIcon";
import FiltersIcon from "@/(components)/icons/FirtesIcon";
import { FilterReset } from "@/(components)/ResetFiltersButton";
import { SortedWasteFiltersContext } from "@/(context)/SortedWasteFiltersProvider";
import { useComponentVisible } from "@/(hooks)/useComponentVisible";
import { ICityDistrictsFeatureCollection } from "@/(types)/cityDistricts";

import AreaFilter from "./(FilterComponent)/AreaFilter";
import FullnessFilter from "./(FilterComponent)/FullnessFilter";
import SensorFilter from "./(FilterComponent)/SensorFilter";
import TypeFilter from "./(FilterComponent)/TypeFilter";
import styles from "./FilterComponent.module.scss";

interface IFilterComponent {
    districts: ICityDistrictsFeatureCollection;
}

const FilterComponent: FC<IFilterComponent> = ({ districts }) => {
    const { resetFilters, filterOptions } = useContext(SortedWasteFiltersContext);
    const [fullnessFilterActive, setFullnessFilterActive] = useState<boolean>(false);
    const [areaFilterActive, setAreaFilterActive] = useState<boolean>(false);
    // eslint-disable-next-line max-len
    const [sensorFilterActive, setSensorFilterActive] = useState<boolean>(filterOptions.sensor === "sensor_only"); // Default to true
    const [typeFilterActive, setTypeFilterActive] = useState<boolean>(false);
    const [activeFiltersCount, setActiveFiltersCount] = useState<number>(0);
    const [isOpen, setIsOpen] = useState<boolean>(false);

    const [dropdownVisibility, setDropdownVisibility] = useState({
        fullness: false,
        area: false,
        sensor: false,
        type: false,
    });

    const handleDropdownHide = () => {
        setIsOpen(false);
    };

    const { ref } = useComponentVisible(handleDropdownHide);

    useEffect(() => {
        const activeFilters = [fullnessFilterActive, areaFilterActive, sensorFilterActive, typeFilterActive].filter(
            Boolean
        ).length;
        setActiveFiltersCount(activeFilters);
    }, [fullnessFilterActive, areaFilterActive, sensorFilterActive, typeFilterActive]);

    const handleFullnessFilterChange = (active: boolean) => {
        setFullnessFilterActive(active);
    };

    const handleAreaFilterChange = (active: boolean) => {
        setAreaFilterActive(active);
    };

    const handleSensorFilterChange = (active: boolean) => {
        setSensorFilterActive(active);
    };

    const handleTypeFilterChange = (active: boolean) => {
        setTypeFilterActive(active);
    };

    const toggleDropdownVisibility = (filter: keyof typeof dropdownVisibility) => {
        setDropdownVisibility((prevState) => ({
            ...prevState,
            [filter]: !prevState[filter],
        }));
    };

    const handleResetFilters = () => {
        setFullnessFilterActive(false);
        setAreaFilterActive(false);
        setSensorFilterActive(false);
        setTypeFilterActive(false);
        resetFilters();
    };

    useEffect(() => {
        if (!isOpen) {
            setDropdownVisibility({
                fullness: false,
                area: false,
                sensor: false,
                type: false,
            });
        }
    }, [isOpen]);

    return (
        <div className={`${styles["filter-dropdown"]} ${isOpen ? styles["filter-dropdown__open"] : ""}`}>
            <div className={styles["filter-dropdown__container"]} onClick={() => setIsOpen(!isOpen)}>
                <div className={styles["filter-dropdown__container-title"]}>
                    <FiltersIcon />
                    <span>Filtrace mapy</span>
                    <div className={styles["filter-dropdown__container-counter"]}>
                        <span>{activeFiltersCount}</span>
                    </div>
                </div>
                <div className={styles["filter-dropdown__reset-open"]}>
                    <FilterReset onClick={handleResetFilters} />
                    <button
                        id={"id-1"}
                        aria-expanded={isOpen}
                        type="button"
                        onClick={() => setIsOpen(!isOpen)}
                        className={styles["filter-dropdown__toggle-button"]}
                    >
                        <ChevronIcon color={"black"} direction={isOpen ? "up" : "down"} />
                    </button>
                </div>
            </div>
            {isOpen && (
                <div ref={ref} className={styles["filter-dropdown__menu"]}>
                    <hr className={styles["separator"]} />
                    <FullnessFilter
                        onChange={handleFullnessFilterChange}
                        visible={dropdownVisibility.fullness}
                        onToggle={() => toggleDropdownVisibility("fullness")}
                        setDropdownVisibility={setDropdownVisibility}
                    />
                    <AreaFilter
                        districts={districts}
                        onChange={handleAreaFilterChange}
                        visible={dropdownVisibility.area}
                        onToggle={() => toggleDropdownVisibility("area")}
                    />
                    <SensorFilter
                        onChange={handleSensorFilterChange}
                        visible={dropdownVisibility.sensor}
                        onToggle={() => toggleDropdownVisibility("sensor")}
                    />
                    <TypeFilter
                        onChange={handleTypeFilterChange}
                        visible={dropdownVisibility.type}
                        onToggle={() => toggleDropdownVisibility("type")}
                    />
                </div>
            )}
        </div>
    );
};

export default FilterComponent;
