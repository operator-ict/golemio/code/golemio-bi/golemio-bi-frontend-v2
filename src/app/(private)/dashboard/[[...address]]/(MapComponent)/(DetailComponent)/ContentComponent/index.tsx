/* eslint-disable max-len */
"use client";
import React, { FC } from "react";

import { Title } from "@/(components)/Title";
import { IFeatureProperties } from "@/(types)/FeaturesTypes";
import { IParkingProperties } from "@/(types)/ParkingsType";

import { ParkingDetail } from "../../(ParkingsMap)/ParkingDetailComponent/ParkingDetailComponent";
import Accordion from "../AccordionComponent/AccordionComponent";
import styles from "./ContentComponent.module.scss";

interface DetailProps {
    children?: React.ReactNode;
    properties: IFeatureProperties | IParkingProperties;
}

export const ContentComponent: FC<DetailProps> = ({ properties }) => {
    const isParkingProperties = (props: IFeatureProperties | IParkingProperties): props is IParkingProperties => {
        return (props as IParkingProperties).address_formatted !== undefined;
    };
    return (
        <div className={styles.content}>
            <>
                {!isParkingProperties(properties) ? (
                    <div className={styles["title"]}>
                        <Title type="t3">{properties.name}</Title>
                        <Title type="t3">{` - ${properties.district.charAt(0).toUpperCase() + properties.district.slice(1)}`}</Title>
                    </div>
                ) : (
                    <div className={styles["title-parking"]}>
                        <Title type="t3">{properties.name}</Title>
                        <p>{properties.address.street_address}</p>
                        <p>{properties.address_formatted.split(",").slice(1).join(",").trim()}</p>
                    </div>
                )}
            </>
            <div className={styles["station-number"]}>
                {!isParkingProperties(properties) ? <p>{properties.station_number}</p> : null}
            </div>
            {!isParkingProperties(properties) ? <Accordion properties={properties} /> : <ParkingDetail properties={properties} />}
        </div>
    );
};
