"use client";
import { FC, useState } from "react";

import { IFeatureProperties } from "@/(types)/FeaturesTypes";
import text from "@/textContent/cs.json";

import styles from "./AccordionComponent.module.scss";
import { AccordionItem } from "./AccordionItem";
interface AccordionItemProps {
    properties: IFeatureProperties;
}

const Accordion: FC<AccordionItemProps> = ({ properties }) => {
    const [activeIndex, setActiveIndex] = useState<number | null>(null);
    const [fetchStatuses, setFetchStatuses] = useState<{ [key: number]: number | null }>({});

    const handleItemClick = (index: number) => {
        setActiveIndex((prevIndex) => (prevIndex === index ? null : index));
    };

    const handleFetchStatus = (containerId: number, status: number | null) => {
        setFetchStatuses((prev) => ({ ...prev, [containerId]: status }));
    };

    const hasError = Object.values(fetchStatuses).some((status) => status !== null && status !== 403);
    const hasAccessDenied = Object.values(fetchStatuses).some((status) => status === 403);

    return (
        <div className={`${styles["accordion-container"]}`}>
            {hasAccessDenied && (
                <div className={`${styles["wrapper"]}`}>
                    <span className={`${styles["error-message"]}`}>{text.map.spotDetail.accessDenied}</span>
                </div>
            )}
            {hasError && (
                <div className={`${styles["wrapper"]}`}>
                    <span className={`${styles["error-message"]}`}>{text.map.spotDetail.noData}</span>
                </div>
            )}

            {!hasAccessDenied &&
                !hasError &&
                properties.containers.map((item, index) => (
                    <AccordionItem
                        key={index}
                        isOpen={activeIndex === index}
                        onClick={() => handleItemClick(index)}
                        title={item.trash_type.description}
                        ksnkoId={item.ksnko_id}
                        properties={properties.containers}
                        trashType={item.trash_type}
                        containerId={item.container_id}
                        isMonitored={item.is_monitored}
                        fetchStatus={fetchStatuses[item.container_id]}
                        setFetchStatus={(status: number | null) => handleFetchStatus(item.container_id, status)}
                    />
                ))}
        </div>
    );
};

export default Accordion;
