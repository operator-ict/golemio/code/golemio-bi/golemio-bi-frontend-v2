"use client";
import "moment/locale/cs";

import moment from "moment";
import { FC, useEffect, useMemo, useRef, useState } from "react";

import { getMeasurementsId } from "@/(actions)/formSorteewasteAction";
import SelectArrowIcon from "@/(components)/icons/formIcons/SelectArrowIcon";
import { Title } from "@/(components)/Title";
import { IFeatureContainer } from "@/(types)/FeaturesTypes";
import { IMeasurementsId } from "@/(types)/MeasurementsType";
import text from "@/textContent/cs.json";

import { Chart, ITrashType } from "../Chart";
import { ChartGraphOptions } from "../TransformData";
import styles from "./AccordionComponent.module.scss";

interface IAccordionItemProps {
    title: string;
    ksnkoId: number;
    isOpen: boolean;
    onClick: () => void;
    properties: IFeatureContainer[];
    trashType: ITrashType;
    containerId: number;
    isMonitored?: boolean;
    fetchStatus: number | null;
    setFetchStatus: (status: number | null) => void;
}
export const AccordionItem: FC<IAccordionItemProps> = ({
    title,
    ksnkoId,
    containerId,
    trashType,
    isMonitored,
    isOpen,
    onClick,
    setFetchStatus,
}) => {
    moment.locale("cs");
    const contentHeight = useRef<HTMLDivElement | null>(null);
    const [measurements, setMeasurements] = useState<IMeasurementsId[]>([]);

    useEffect(() => {
        if (!isMonitored) return;

        const dateFrom = ChartGraphOptions.reduce((min, curr) => {
            return min < curr.value ? min : curr.value;
        }, new Date());
        const fetchData = async (ksnkoId: number, update: string) => {
            try {
                const response = await getMeasurementsId(ksnkoId, update);
                if (response.status === 200 && Array.isArray(response.data)) {
                    setMeasurements(response.data);
                    setFetchStatus(null);
                } else {
                    setMeasurements([]);
                    setFetchStatus(response.status);
                }
            } catch (error) {
                setMeasurements([]);
                setFetchStatus(500);
                console.error("Error fetching measurements:", error);
            }
        };
        fetchData(containerId, dateFrom.toISOString());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [containerId, isMonitored]);

    const lastMeasurement = useMemo(() => {
        const measurement = measurements?.find((measurement) => measurement.percent_calculated !== null);

        if (!measurement) {
            return null;
        }

        return {
            measuredAt: moment(measurement.measured_at_utc),
            value: measurement.percent_calculated,
        };
    }, [measurements]);
    return (
        <div className={`${styles["wrapper"]}`}>
            <button className={`${styles["question-container"]} ${isOpen ? "active" : ""}`} onClick={onClick}>
                <div>
                    <div className={`${styles["question-container__title"]}`}>
                        <Title type="t3">{title}</Title>
                        <p>{`KSNKO ID: ${ksnkoId}`}</p>
                    </div>
                    {lastMeasurement && (
                        <p className={`${styles["question-container__update"]}`}>
                            {`${text.map.spotDetail.lastUpdate} ${lastMeasurement.measuredAt.fromNow()}`}
                            {lastMeasurement.measuredAt < moment().subtract(1, "day") && <div>{`Error`}</div>}
                        </p>
                    )}
                </div>
                <div className={`${styles["question-container__percent"]}`}>
                    {lastMeasurement && (
                        <div>
                            <p>{lastMeasurement.value}%</p>
                        </div>
                    )}
                    {isMonitored === true && (
                        <SelectArrowIcon className={`${styles.arrow} ${isOpen ? styles.active : ""}`} color={"primary"} />
                    )}
                </div>
            </button>
            {isMonitored === true && (
                <div
                    ref={contentHeight}
                    className={`${styles["answer-container"]}`}
                    style={isOpen ? { height: contentHeight.current?.scrollHeight } : { height: "0px" }}
                >
                    <Chart measurements={measurements} trashType={trashType} />
                </div>
            )}
        </div>
    );
};
