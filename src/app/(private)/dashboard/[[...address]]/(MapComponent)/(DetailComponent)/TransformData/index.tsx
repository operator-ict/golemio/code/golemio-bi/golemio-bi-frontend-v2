import moment from "moment";

import { IMeasurementsId } from "@/(types)/MeasurementsType";

interface ISelectedTimeRange {
    key: string;
    value: Date;
}
const getPastDate = (value: moment.DurationInputArg1, unit: moment.DurationInputArg2) => moment().subtract(value, unit).toDate();

export const ChartGraphOptions = [
    {
        key: "day",
        value: getPastDate(1, "days"),
    },
    {
        key: "3days",
        value: getPastDate(3, "days"),
    },
    {
        key: "7days",
        value: getPastDate(7, "days"),
    },
    {
        key: "2weeks",
        value: getPastDate(2, "weeks"),
    },
    {
        key: "month",
        value: getPastDate(1, "months"),
    },
    {
        key: "2months",
        value: getPastDate(2, "months"),
    },
    {
        key: "3months",
        value: getPastDate(3, "months"),
    },
];

export const transformData = (measureData: IMeasurementsId[], selectedTimeRange: ISelectedTimeRange) => {
    if (!measureData) {
        console.error("Invalid measureData:", measureData);
        return [];
    }
    return measureData
        .filter((item) => new Date(item?.measured_at_utc) >= selectedTimeRange.value)
        .reverse()
        .map((item, _i, arr) => {
            const date = moment(item?.measured_at_utc).format(arr.length < 10 ? "D.M. H:mm" : "D.M.");

            return {
                fullness: item?.percent_calculated,
                date,
                fullDate: moment(item?.measured_at_utc).format("D.M. H:mm"),
            };
        });
};
