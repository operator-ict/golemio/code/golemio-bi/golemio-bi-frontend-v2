"use client";
import React, { FC } from "react";

import { CloseDetailIcon } from "@/(components)/icons/CloseDetailIcon";
import ClickAwayListener from "@/(utils)/ClickAwayListener";

import styles from "./Detail.module.scss";

interface DetailProps {
    children?: React.ReactNode;
    hideDetail: () => void;
}

export const DetailWrapper: FC<DetailProps> = ({ children, hideDetail }) => {
    return (
        <ClickAwayListener onClickAway={hideDetail}>
            <div className={`${styles["wrapper"]}`}>
                {children}
                <span className={`${styles["close"]}`} onClick={hideDetail} role="button" tabIndex={0}>
                    <CloseDetailIcon color={"tertiary"} />
                </span>
            </div>
        </ClickAwayListener>
    );
};
