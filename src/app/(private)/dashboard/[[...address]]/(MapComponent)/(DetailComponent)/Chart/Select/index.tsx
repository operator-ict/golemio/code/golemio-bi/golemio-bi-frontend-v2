import { ChangeEvent } from "react";

import { IMeasurementsId } from "@/(types)/MeasurementsType";
import text from "@/textContent/cs.json";

import { ChartGraphOptions } from "../../TransformData";
import { Chart, ITrashType } from "..";
import styles from "../Chart.module.scss";

export interface ChartGraphOption {
    key: string;
    value: Date;
}
interface SelectComponentProps {
    measurements: IMeasurementsId[];
    trashType: ITrashType;
    setTimeRange: (option: ChartGraphOption) => void;
}

export const RenderChartData: React.FC<SelectComponentProps> = ({ measurements, setTimeRange, trashType }) => {
    const onSelectChange = (e: ChangeEvent<HTMLSelectElement>) => {
        setTimeRange(ChartGraphOptions.find((item) => item.key === e.target.value) as ChartGraphOption);
    };
    const selectedTimeRange = ChartGraphOptions[2];

    return (
        <div className={styles.container}>
            <div className={styles.timePickerWrapper}>
                <span className={styles.text}>{text.map.wasteCollection.fullnessPast}</span>
                <form>
                    <select
                        className={styles.timePicker}
                        onChange={onSelectChange}
                        value={selectedTimeRange.key}
                        {...{ disablePortal: true }}
                    >
                        {ChartGraphOptions.map((option) => (
                            <option key={option.key} value={option.key}>
                                {`text.sortedWaste.timeRange${option.key}`}
                            </option>
                        ))}
                    </select>
                </form>
            </div>
            {!!measurements && <pre>{JSON.stringify(measurements, null, 2)}</pre>}
            <div className={styles.wrapper}>
                {measurements && measurements && <Chart measurements={measurements} trashType={trashType} />}
            </div>
        </div>
    );
};
