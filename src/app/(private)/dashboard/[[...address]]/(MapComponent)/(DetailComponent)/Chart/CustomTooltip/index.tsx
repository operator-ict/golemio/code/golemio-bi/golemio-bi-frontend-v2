"use client";
import { TooltipProps } from "recharts";

import text from "@/textContent/cs.json";

import styles from "./CustomTooltip.module.scss";

export const CustomTooltip = ({ active, payload }: TooltipProps<number, string>) => {
    if (active && payload) {
        return (
            <>
                {payload.map((item, index) => (
                    <div className={`${styles["custom-tooltip"]}`} key={index}>
                        <p className={`${styles["custom-tooltip__title"]}`}>{item.payload.fullDate}:00</p>
                        <div className={`${styles["custom-tooltip__item"]}`}>
                            <span>{text.map.wasteCollection[item.name as keyof typeof text.map.wasteCollection]}:</span>
                            <p>{item.value !== undefined ? ` ${Math.floor(item.value)} %` : "N/A"}</p>
                        </div>
                    </div>
                ))}
            </>
        );
    }

    return null;
};
