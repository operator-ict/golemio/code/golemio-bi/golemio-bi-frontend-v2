/* eslint-disable max-len */
import React, { ChangeEvent, FC, useState } from "react";
import { Area, AreaChart, ReferenceLine, ResponsiveContainer, Tooltip, TooltipProps, XAxis, YAxis } from "recharts";

import { IMeasurementsId } from "@/(types)/MeasurementsType";
import text from "@/textContent/cs.json";

import { ChartGraphOptions, transformData } from "../TransformData";
import styles from "./Chart.module.scss";
import { CustomTooltip } from "./CustomTooltip";

export interface ChartGraphOption {
    key: string;
    value: Date;
}

const getTrashColor = (trashId: number | null) => {
    switch (trashId) {
        case 1:
            return "#318a2c";
        case 2:
            return "#e20612";
        case 3:
            return "#777";
        case 4:
            return "#ee8a28";
        case 5:
            return "#1d71b9";
        case 6:
            return "#fec736";
        case 7:
            return "#c9e9ff";
        default:
            return "#808000";
    }
};

interface IChartProps {
    measurements: IMeasurementsId[];
    trashType: ITrashType;
}

export interface ITrashType {
    description: string;
    id: number;
}

interface IChartState {
    selectedTimeRange: ChartGraphOption;
}

export const Chart: FC<IChartProps> = ({ measurements, trashType }) => {
    const trashtype = (trashType && trashType.id) || null;
    const trashColor = getTrashColor(trashtype);
    const gradientId = `gradient-${trashType.id}`;

    const [chartState, setChartState] = useState<IChartState>({
        selectedTimeRange: ChartGraphOptions[2],
    });

    const handleTimeRangeChange = (e: ChangeEvent<HTMLSelectElement>) => {
        const selectedOption = ChartGraphOptions.find((item) => item.key === e.target.value);
        if (selectedOption) {
            setChartState({ selectedTimeRange: selectedOption });
        }
    };

    return (
        <div className={styles.wrapper}>
            <div className={styles.wrapper__timePicker}>
                <form>
                    <div className={styles.selectContainer}>
                        <select onChange={handleTimeRangeChange} value={chartState.selectedTimeRange.key}>
                            {ChartGraphOptions.map((option) => (
                                <option key={option.key} value={option.key}>
                                    {`${text.map.wasteCollection.fullnessPast} ${text.map.timeRange[option.key as keyof typeof text.map.timeRange]}`}
                                </option>
                            ))}
                        </select>
                    </div>
                </form>
            </div>
            <ResponsiveContainer height={180} width="100%">
                <AreaChart
                    width={280}
                    height={154}
                    data={transformData(measurements, chartState.selectedTimeRange)}
                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                >
                    <defs>
                        <linearGradient id={gradientId} x1="0" x2="0" y1="0" y2="1">
                            <stop offset="5%" stopColor={trashColor} stopOpacity={0.8} />
                            <stop offset="95%" stopColor={trashColor} stopOpacity={0} />
                        </linearGradient>
                    </defs>
                    <XAxis dataKey="date" />
                    <YAxis
                        domain={[0, 100]}
                        orientation="right"
                        padding={{ top: 0, bottom: 0 }}
                        tickLine={false}
                        unit="%"
                        width={15}
                    />
                    <Area dataKey="fullness" fill={`url(#${gradientId})`} fillOpacity={1} stroke={trashColor} type="linear" />
                    <Tooltip
                        content={({ active, payload, label }: TooltipProps<number, string>) => (
                            <CustomTooltip active={active} payload={payload} label={label} />
                        )}
                    />
                    <ReferenceLine y={100} stroke="#2C323F" strokeDasharray="3 3" />
                </AreaChart>
            </ResponsiveContainer>
        </div>
    );
};
