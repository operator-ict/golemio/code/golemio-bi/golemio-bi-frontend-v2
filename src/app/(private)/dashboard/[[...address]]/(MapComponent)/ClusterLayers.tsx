import React from "react";
import { Layer } from "react-map-gl";

interface ClusterProps {
    sourceId: string;
    monitoredImage: string;
    defaultImage: string;
    map: string;
}

export const ClusterLayers: React.FC<ClusterProps> = (props: ClusterProps) => {
    const { sourceId, monitoredImage, defaultImage, map } = props;

    return (
        <>
            <Layer
                id={`${sourceId}-symbol`}
                type="symbol"
                source={sourceId}
                filter={["has", "point_count"]}
                layout={{
                    "icon-image":
                        map === "sorted-waste" ? ["case", ["get", "isMonitored"], monitoredImage, defaultImage] : defaultImage,
                    "icon-allow-overlap": true,
                    "text-allow-overlap": true,
                }}
            />
            <Layer
                id={`${sourceId}-circle`}
                type="circle"
                source={sourceId}
                filter={["has", "point_count"]}
                paint={{
                    "circle-color": "#005FCC",
                    "circle-radius": ["step", ["get", "point_count"], 10, 100, 13],
                    // offset in px, has to be equal to (text-offset * text-size) to be centered
                    "circle-translate": [18, 18],
                    "circle-stroke-color": "#fff",
                    "circle-stroke-width": 2,
                }}
            />
            <Layer
                id={`${sourceId}-text`}
                type="symbol"
                source={sourceId}
                filter={["has", "point_count"]}
                layout={{
                    "text-field": "{point_count_abbreviated}",
                    // offset will be multiplied by text size
                    "text-offset": [1.5, 1.5],
                    "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
                    "text-size": 12,
                    "icon-allow-overlap": true,
                    "text-allow-overlap": true,
                }}
                paint={{
                    "text-color": "#fff",
                }}
            />
        </>
    );
};
