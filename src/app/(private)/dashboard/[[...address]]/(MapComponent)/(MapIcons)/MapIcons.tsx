"use client";

import defaultClusterB64 from "./defaultClusterIcon.png";
import defaultB64 from "./defaultIcon.png";
import parkingPaidB64 from "./parkingPaid.png";
import parkingRideB64 from "./parkingRide.png";
import parkingRideBlueB64 from "./parkingRideBlue.png";
import sensorIconB64 from "./sensorIcon.png";
import activeSensorIconFromBase64 from "./sensorIconActive.png";
import sortedWasteB64 from "./sortedWaste.png";
import sortedWasteBlueB64 from "./sortedWaste-active.png";

export const GetImageFromBase64 = (src: string) => {
    if (typeof window !== "undefined") {
        const img = new window.Image();
        img.src = src;
        return img;
    }
};
export const defaultIcon = GetImageFromBase64(defaultB64.src);
export const defaultClusterIcon = GetImageFromBase64(defaultClusterB64.src);
export const parkingPaidIcon = GetImageFromBase64(parkingPaidB64.src);
export const parkingRideIcon = GetImageFromBase64(parkingRideB64.src);
export const parkingRideBlue = GetImageFromBase64(parkingRideBlueB64.src);
export const sortedWasteIcon = GetImageFromBase64(sortedWasteB64.src);
export const sortedWasteBlue = GetImageFromBase64(sortedWasteBlueB64.src);
export const sensorIcon = GetImageFromBase64(sensorIconB64.src);
export const activeSensorIcon = GetImageFromBase64(activeSensorIconFromBase64.src);
