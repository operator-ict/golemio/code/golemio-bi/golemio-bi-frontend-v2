"use client";

import "mapbox-gl/dist/mapbox-gl.css";

import type { FeatureCollection } from "geojson";
import * as React from "react";
import { FC, ReactNode, useCallback, useMemo, useRef, useState } from "react";
import Map, { Layer, MapMouseEvent, MapRef, NavigationControl, Source } from "react-map-gl";

import { IFeatureCollection, IFeatureProperties, IGeoData } from "@/(types)/FeaturesTypes";
import { ICustomIcons, MapImageType } from "@/(types)/MapImagesType";
import { IParkingCollection, IParkingProperties } from "@/(types)/ParkingsType";

import { DetailWrapper } from "./(DetailComponent)";
import { ContentComponent } from "./(DetailComponent)/ContentComponent";
import {
    activeSensorIcon,
    defaultClusterIcon,
    defaultIcon,
    parkingRideBlue,
    sensorIcon,
    sortedWasteBlue,
} from "./(MapIcons)/MapIcons";
import { ClusterLayers } from "./ClusterLayers";

interface MapComponentProps {
    mapboxToken: string;
    data: IFeatureCollection | IParkingCollection;
    children?: ReactNode;
    customIcons: ICustomIcons;
    map: string;
}
export interface IFeatureWithProperties {
    properties: IFeatureProperties;
}

const defaultImages = [
    ["default-icon", defaultIcon],
    ["default-cluster-icon", defaultClusterIcon],
    ["active-icon", parkingRideBlue || sortedWasteBlue],
    ["sensor-icon", sensorIcon],
    ["active-sensor-icon", activeSensorIcon],
];

const DEFAULT_ZOOM = 10;
const MAX_BOUNDS: [number, number, number, number] = [13.38938, 49.46719, 15.662555, 50.57593];

const MapComponent: FC<MapComponentProps> = ({ mapboxToken, data, children, customIcons, map }) => {
    const mapRef = useRef<MapRef | null>(null);
    const [zoom, setZoom] = useState(DEFAULT_ZOOM);
    const [viewState, setViewState] = useState({
        longitude: 14.4378,
        latitude: 50.0755,
        zoom: zoom,
    });
    const [selectedFeature, selectFeature] = useState<IGeoData<IFeatureProperties | IParkingProperties> | null>(null);
    const [activeFeatureIndex, setActiveFeatureIndex] = useState<number | null>(null);
    const { images, markerIcon, sensorIcon, clusterIcon } = customIcons || {
        images: defaultImages,
        markerIcon: "default-icon",
        clusterIcon: "default-cluster-icon",
        sensorIcon: "sensor-icon",
    };

    const geoJsonSource = useMemo(() => {
        return {
            type: data?.type,
            features: data?.features?.map((item, featureIndex: number) => {
                const isMonitored =
                    map === "sorted-waste"
                        ? (item.properties as IFeatureProperties).containers.some((container) => container.is_monitored)
                        : null;
                return {
                    ...item,
                    properties: {
                        ...item.properties,
                        featureIndex,
                        isMonitored,
                        icon:
                            activeFeatureIndex === featureIndex
                                ? isMonitored
                                    ? "active-sensor-icon"
                                    : "active-icon"
                                : map === "sorted-waste"
                                  ? isMonitored
                                      ? "sensor-icon"
                                      : "default-icon"
                                  : "parking-ride-marker",
                    },
                };
            }),
        };
    }, [data, activeFeatureIndex, map]);

    const mapRefCallback = useCallback(
        (ref: MapRef | null) => {
            if (ref !== null) {
                mapRef.current = ref;
                const map = ref;

                const loadImage = () => {
                    if (Array.isArray(images)) {
                        images.forEach((image: MapImageType | string | HTMLImageElement) => {
                            let imageName: string;
                            let imageData: HTMLImageElement | undefined;

                            if (Array.isArray(image)) {
                                [imageName, imageData] = image;
                            } else if (typeof image === "string") {
                                imageName = image;
                            } else {
                                imageName = image.id || "unknown-image-id" || "default-icon";
                                imageData = image;
                            }

                            if (!map.hasImage(imageName) && imageData) {
                                map.loadImage(imageData.src, (error, result) => {
                                    if (error) throw error;

                                    map.addImage(imageName, result as ImageBitmap, {
                                        sdf: false,
                                    });
                                });
                            }
                        });
                    }
                };
                loadImage();

                // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/no-unused-vars
                map.on("styleimagemissing", (e: any) => {
                    loadImage();
                });
            }
        },
        [images]
    );

    const onFeatureClick = useCallback(
        (e: MapMouseEvent) => {
            e.originalEvent.preventDefault();
            e.originalEvent.stopPropagation();

            const feature = e.features && e.features[0];
            const index = feature ? feature.properties && feature.properties.featureIndex : null;

            if (index !== null && index !== undefined) {
                selectFeature(data.features[index]);
                setActiveFeatureIndex(index);
            }
        },
        [data]
    );

    return (
        <Map
            {...viewState}
            style={{ width: "100vw", height: "100vh", position: "fixed" }}
            mapStyle="mapbox://styles/golemio/clx90gswj01w301qs759ehlud"
            mapboxAccessToken={mapboxToken}
            initialViewState={{
                longitude: 14.4378,
                latitude: 50.0755,
                zoom: 14,
            }}
            onMove={(event) => setViewState(event.viewState)}
            ref={mapRefCallback}
            maxBounds={MAX_BOUNDS}
            dragRotate={false}
            minZoom={9}
            interactiveLayerIds={["features-layer"]}
            onClick={onFeatureClick}
            onZoomEnd={(e) => setZoom(Math.round(e.viewState.zoom))}
        >
            <NavigationControl />
            <Source
                id="data"
                type="geojson"
                data={geoJsonSource as FeatureCollection}
                cluster={true}
                clusterProperties={{
                    isMonitored: ["any", ["get", "isMonitored"]],
                }}
                clusterMaxZoom={18}
                clusterRadius={50}
            />
            <Layer
                source="data"
                id="features-layer"
                type="symbol"
                filter={["!", ["has", "point_count"]]}
                layout={{
                    "icon-image": markerIcon,
                    "text-offset": [0, 2],
                    "text-field": "{name}",
                    "text-letter-spacing": 0.05,
                    "text-font": ["Roboto Regular", "Arial Unicode MS Bold"],
                    "text-size": 12,
                    "text-anchor": "top",
                    "icon-allow-overlap": true,
                    "text-allow-overlap": true,
                }}
            />
            <ClusterLayers map={map} sourceId="data" monitoredImage={sensorIcon as string} defaultImage={clusterIcon as string} />
            {children}
            {selectedFeature && (
                <DetailWrapper hideDetail={() => selectFeature(null)}>
                    <ContentComponent properties={selectedFeature?.properties} />
                </DetailWrapper>
            )}
        </Map>
    );
};
export default MapComponent;
