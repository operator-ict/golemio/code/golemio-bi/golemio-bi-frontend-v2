"use client";
import React, { FC, ReactNode, use, useContext, useEffect, useMemo } from "react";

import NumbersOfStations from "@/(components)/NumberOfStations";
import { useCount } from "@/(context)/CountContext";
import { Title } from "@/components/Title";
import { SortedWasteFiltersContext } from "@/context/SortedWasteFiltersProvider";
import text from "@/textContent/cs.json";
import { ICityDistrictsFeatureCollection } from "@/types/cityDistricts";
import { IFeatureCollection } from "@/types/FeaturesTypes";

import styles from "../DashboardPage.module.scss";
import MapComponent from ".";
import { activeSensorIcon, sensorIcon, sortedWasteBlue, sortedWasteIcon } from "./(MapIcons)/MapIcons";
import FilterComponent from "./FilterComponent";

interface WrapperProps {
    children?: ReactNode;
    data: Promise<IFeatureCollection>;
    mapboxToken: string;
    districts: Promise<ICityDistrictsFeatureCollection>;
}

export const MapWrapper: FC<WrapperProps> = ({ data, districts, mapboxToken }) => {
    const { filterFunction } = useContext(SortedWasteFiltersContext);
    const districtsFromPromise = use(districts);
    const dataFromPromise = use(data);
    const { count, setCount } = useCount();
    const { countContainer, setCountContainer } = useCount();
    const filteredData = useMemo(() => {
        return filterFunction(dataFromPromise);
    }, [filterFunction, dataFromPromise]);

    useEffect(() => {
        const containers = filteredData.features.map((item) => item.properties.containers.length);
        setCount(filteredData?.features?.length || 0);
        setCountContainer(containers.reduce((acc, current) => acc + current, 0));
    }, [filteredData, setCount, setCountContainer]);

    return (
        <section className={styles["map-container"]}>
            <div className={styles["title-container"]}>
                <Title type={"t1"}>{text.map.sortedWaste.mainTitle}</Title>
                <NumbersOfStations count={count} countContainer={countContainer} />
            </div>
            <FilterComponent districts={districtsFromPromise} />
            <MapComponent
                map="sorted-waste"
                mapboxToken={mapboxToken}
                data={filteredData}
                customIcons={{
                    images: [
                        ["sorted-waste", sortedWasteIcon as HTMLImageElement],
                        ["active-icon", sortedWasteBlue as HTMLImageElement],
                        ["default-icon", sortedWasteIcon as HTMLImageElement],
                        ["sensor-icon", sensorIcon as HTMLImageElement],
                        ["active-sensor-icon", activeSensorIcon as HTMLImageElement],
                    ],
                    markerIcon: "{icon}",
                    activeIcon: "active-icon",
                    clusterIcon: "sorted-waste",
                    sensorIcon: "sensor-icon",
                    activeSensorIcon: "active-sensor-icon",
                }}
            />
        </section>
    );
};
