import Image from "next/image";

import Button from "@/components/Button";
import Footer from "@/components/Footer";
import Header from "@/components/Header";
import { Heading } from "@/components/Heading";
import { HomeIcon } from "@/icons/HomeIcon";
import noAccessImage from "@/images/no-access-rights-icon.svg";
import text from "@/textContent/cs.json";
import { UserProfileResponse } from "@/types/formTypes";

import { RequestDAccessBtn } from "../(RequestDAccessBtn)";
import styles from "./NoAccessRights.module.scss";

type NoAccessRightsProps = {
    user?: UserProfileResponse;
    dashboardUrl: string;
};

export type AccessRightsValues = {
    userId: string;
    email: string;
    dashboardUrl: string;
};

export const NoAccessRights = ({ user, dashboardUrl }: NoAccessRightsProps) => {
    const accessRightsValues: AccessRightsValues = {
        userId: user?.id.toString() || "",
        email: user?.email || "",
        dashboardUrl: dashboardUrl,
    };

    return (
        <div className={styles["body-container"]}>
            <Header user={user} />
            <main className={styles["main"]}>
                <Image src={noAccessImage} alt="No access" />
                <Heading tag="h1" type="h4-h3" className={styles["heading"]}>
                    {text.noAccessRights.heading}
                </Heading>
                <div className={styles["button-container"]}>
                    <Button url="/" color="tertiary" iconStart={<HomeIcon color="secondary" />}>
                        {text.navBar.home}
                    </Button>
                    <RequestDAccessBtn accessRightsValues={accessRightsValues} />
                </div>
            </main>
            <Footer />
        </div>
    );
};
