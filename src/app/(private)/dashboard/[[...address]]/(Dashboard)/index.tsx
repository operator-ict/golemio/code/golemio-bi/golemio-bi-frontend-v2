"use client";

import dynamic from "next/dynamic";
import React from "react";

import { ErrorComponent, ErrorStatus } from "@/components/ErrorComponent";
import { IVisualization } from "@/components/PowerBIVisualisation";

import styles from "./Dashboard.module.scss";

const PowerBIVisualisation = dynamic(
    () => import("@/components/PowerBIVisualisation").then((dashboard) => dashboard.PowerBIVisualisation),
    { ssr: false }
);
type DashboardProps = {
    data: IVisualization | ErrorStatus;
};
function isErrorStatus(data: IVisualization | ErrorStatus): data is ErrorStatus {
    return "status" in data;
}

export const Dashboard = ({ data }: DashboardProps) => {
    return (
        <section className={styles["dashboard-container"]}>
            {isErrorStatus(data) && data.status !== 200 ? (
                <div className={styles["dashboard-middle-section"]}>
                    <ErrorComponent errorStatusCode={data.status} style={{ color: "black" }} />
                </div>
            ) : (
                <div className={styles["dashboard-middle-section"]}>
                    <PowerBIVisualisation visualization={data as IVisualization} />
                </div>
            )}
        </section>
    );
};
