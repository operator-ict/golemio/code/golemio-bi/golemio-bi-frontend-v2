"use client";
import { useRouter } from "next/navigation";
import toast from "react-hot-toast";

import { accessDashboardReq } from "@/actions/formAuthActions";
import Button from "@/components/Button";
import NotificationBanner, { NotificationType } from "@/components/NotificationBanner";
import { ArrowRightIcon } from "@/icons/ArrowRightIcon";
import text from "@/textContent/cs.json";

import { AccessRightsValues } from "../(NoAccessRights)";

type Props = {
    accessRightsValues: AccessRightsValues;
};

export const RequestDAccessBtn = ({ accessRightsValues }: Props) => {
    const router = useRouter();

    const DashboardAccessAnswer = async (accessRightsValues: AccessRightsValues) => {
        const result = await accessDashboardReq(accessRightsValues);
        if (result?.success) {
            router.push("/auth/request-dashboard-access");
        } else {
            toast.custom(<NotificationBanner type={"error" as NotificationType} text={text.auth.accessDashboardRequestFailed} />);
        }
    };

    return (
        <Button
            color="primary"
            onClick={() => DashboardAccessAnswer(accessRightsValues)}
            iconEnd={<ArrowRightIcon color="white" />}
        >
            {text.auth.accessDashboardRequestBtn}
        </Button>
    );
};
