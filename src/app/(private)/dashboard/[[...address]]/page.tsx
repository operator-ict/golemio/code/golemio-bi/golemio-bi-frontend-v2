import { notFound } from "next/navigation";
import { Suspense } from "react";

import DropdownComponent from "@/components/DropdownComponent";
import HeaderDashboard from "@/components/HeaderDashboard";
import { IVisualization } from "@/components/PowerBIVisualisation";
import { getLogger } from "@/logging/logger";
import { favouriteServise } from "@/services/favourite-service";
import { metadataService } from "@/services/metadata-service";
import { powerbiEmbedService } from "@/services/powerbi-service";
import { UserProfileService } from "@/services/user-profile-service";
import { IMetadata } from "@/types/MetadataType";
import { checkIfFavourite } from "@/utils/checkIfFavourite";

import Loading from "../loading";
import { Dashboard } from "./(Dashboard)/index";
import { NoAccessRights } from "./(NoAccessRights)";
import styles from "./DashboardPage.module.scss";

const logger = getLogger("DashboardPage");

type DashboardPageProps = { params: { address: string[] }; searchParams: { [key: string]: string | string[] | undefined } };

export const dynamic = "force-dynamic";

const DashboardPage = async ({ params, searchParams }: DashboardPageProps) => {
    const client_route = `/${params.address.join("/")}`;

    const filter = searchParams.filter as string | undefined;
    const pageId = searchParams.pageId as string | undefined;

    const user = await UserProfileService.getUserProfile();

    const dashboardExists = await metadataService.verifyMetadataExists(client_route);

    if (!dashboardExists) {
        logger.warn(`Dashboard with client route: ${client_route} does not exist`);
        return notFound();
    }

    const metadata = await metadataService.getMetadataByClientRoute(client_route);

    if (dashboardExists && !metadata) {
        logger.warn(`Metadata not found for existing client route: ${client_route}, user has probably no access rights`);
        return <NoAccessRights user={user} dashboardUrl={client_route} />;
    }

    const metadataFilter = await metadataService.getMetadataByTypeComponent("dashboard", "powerbiComponent");

    const dashboardCredentials = await powerbiEmbedService.getEmbedInfo(
        String(metadata?.data.workspace_id),
        String(metadata?.data.report_id)
    );

    const embedUrl = filter ? `${dashboardCredentials.embedUrl}&filter=${filter}` : dashboardCredentials.embedUrl;

    const visualization = dashboardCredentials?.embedToken
        ? {
              reportId: metadata?.data.report_id ? [metadata.data.report_id] : [],
              workspaceId: metadata?.data.workspace_id || null,
              pageId,
              embedUrl,
              accessToken: dashboardCredentials.embedToken.token,
              tokenExpiration: dashboardCredentials.embedToken.expiration,
              visualisationType: "report",
              filtersVisible: false,
              filtersExpanded: false,
              navContentPaneEnabled: false,
              background: "",
          }
        : null;

    logger.debug({
        provider: "server",
        data_type: "Visualization:",
        client_route,
        data:
            `reportId: ${visualization?.reportId} | workspaceId: ${visualization?.workspaceId} | ` +
            `pageId: ${visualization?.pageId} | embedUrl: ${visualization?.embedUrl}`,
    });

    if (!visualization) {
        return notFound();
    }

    await metadataService.trackMetadata(metadata?._id);

    const { id, isFavourite } = await checkIfFavourite(favouriteServise, metadata);

    return (
        <>
            <HeaderDashboard
                type={metadata?.type as string}
                metadataFilter={metadataFilter as IMetadata[]}
                title={metadata?.title as string}
                isFavourite={isFavourite}
                id={id}
                user={user}
            />
            <main className={styles.main} id="main">
                <Suspense fallback={<Loading />}>
                    <section id="dashboard" className={styles["dashboard-container"]}>
                        <DropdownComponent
                            metadataFilter={metadataFilter as IMetadata[]}
                            title={metadata?.title as string}
                            isFavourite={isFavourite}
                            id={metadata?._id as string}
                        />
                        <Dashboard data={visualization as IVisualization} />
                    </section>
                </Suspense>
            </main>
        </>
    );
};

export default DashboardPage;
