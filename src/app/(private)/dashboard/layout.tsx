import "@/styles/globals.scss";

import { ReactNode } from "react";

import { CountProvider } from "@/context/CountContext";

import { Provider } from "./Provider";

type PageProps = {
    children: ReactNode;
};

const DashboardLayout = async ({ children }: PageProps) => {
    return (
        <Provider>
            <CountProvider>{children}</CountProvider>
        </Provider>
    );
};

export default DashboardLayout;
