import { Suspense } from "react";
import { config } from "src/config";

import HeaderMaps from "@/components/HeaderMaps";
import { favouriteServise } from "@/services/favourite-service";
import { mapService } from "@/services/maps-service";
import { metadataService } from "@/services/metadata-service";
import { UserProfileService } from "@/services/user-profile-service";
import { checkIfFavourite } from "@/utils/checkIfFavourite";

import { ParkingsMapWrapper } from "../[[...address]]/(MapComponent)/(ParkingsMap)/ParkingsMapWrapper";
import { NoAccessRights } from "../[[...address]]/(NoAccessRights)";
import styles from "../[[...address]]/DashboardPage.module.scss";
import Loading from "../loading";

const mapboxToken = config.NEXT_PUBLIC_MAPBOX_TOKEN;

const ParkingsMapPage = async () => {
    const metadata = await metadataService.getMetadataByClientRoute("/parkings");

    const user = await UserProfileService.getUserProfile();

    if (!metadata) {
        return <NoAccessRights user={user} dashboardUrl={"/parkings"} />;
    }

    const parkingsDataPromise = mapService.getParkingsData();

    const { id, isFavourite } = await checkIfFavourite(favouriteServise, metadata);

    return (
        <>
            <HeaderMaps
                title={metadata?.title as string}
                isFavourite={isFavourite}
                id={id}
                mapType={metadata?.component as string}
                user={user}
            />
            <main className={styles.main} id="main">
                <Suspense fallback={<Loading />}>
                    <ParkingsMapWrapper data={parkingsDataPromise} mapboxToken={mapboxToken} />
                </Suspense>
            </main>
        </>
    );
};
export default ParkingsMapPage;
