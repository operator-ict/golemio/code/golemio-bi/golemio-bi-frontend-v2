import { Suspense } from "react";
import { config } from "src/config";

import HeaderMaps from "@/components/HeaderMaps";
import Loading from "@/private/loading";
import { favouriteServise } from "@/services/favourite-service";
import { mapService } from "@/services/maps-service";
import { metadataService } from "@/services/metadata-service";
import { UserProfileService } from "@/services/user-profile-service";
import { checkIfFavourite } from "@/utils/checkIfFavourite";

import { MapWrapper } from "../[[...address]]/(MapComponent)/MapWrapper";
import { NoAccessRights } from "../[[...address]]/(NoAccessRights)";
import styles from "../[[...address]]/DashboardPage.module.scss";

const SortedWasteStations = async () => {
    const metadata = await metadataService.getMetadataByClientRoute("/sortedwastestations");

    const user = await UserProfileService.getUserProfile();

    if (!metadata) {
        return <NoAccessRights user={user} dashboardUrl={"/sortedwastestations"} />;
    }

    const mapboxToken = config.NEXT_PUBLIC_MAPBOX_TOKEN;
    const featureCollectionDataPromise = mapService.getSortedWasteStations();
    const cityDistrictsPromise = mapService.getCityDistricts();

    const { id, isFavourite } = await checkIfFavourite(favouriteServise, metadata);

    return (
        <>
            <main className={styles.main} id="main">
                <HeaderMaps
                    title={metadata?.title as string}
                    isFavourite={isFavourite}
                    id={id}
                    mapType={metadata?.component as string}
                    user={user}
                />
                <Suspense fallback={<Loading />}>
                    <MapWrapper data={featureCollectionDataPromise} districts={cityDistrictsPromise} mapboxToken={mapboxToken} />
                </Suspense>
            </main>
        </>
    );
};
export default SortedWasteStations;
