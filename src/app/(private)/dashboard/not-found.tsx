"use client";

import { useRouter, useSearchParams } from "next/navigation";
import { useEffect } from "react";

import Button from "@/components/Button";
import { Heading } from "@/components/Heading";
import { Paragraph } from "@/components/Paragraph";
import { HomeIcon } from "@/icons/HomeIcon";
import { NotFoundIcon } from "@/icons/NotFoundImage";
import layout from "@/layout.module.scss";
import text from "@/textContent/cs.json";

import styles from "../../ErrorPages.module.scss";

const NotFoundPrivate = () => {
    const searchParams = useSearchParams();
    const router = useRouter();

    const searchParamsString = searchParams.toString();

    useEffect(() => {
        if (!searchParams.get("notFound")) {
            const newParams = new URLSearchParams(searchParams);
            newParams.set("notFound", "true");
            router.replace(`?${newParams.toString()}`);
        }
    }, [searchParamsString, router]);
    return (
        <main className={layout.main} id="main">
            <section className={` ${styles["error-page-container"]}`}>
                <div className={`${styles["image-container"]}`}>
                    <NotFoundIcon />
                </div>
                <Heading tag="h1" type="h4-h3">
                    {text.notFoundPage.title}
                </Heading>
                <Paragraph type={"p1"}>{text.notFoundPage.subTitle}</Paragraph>
                <Button
                    color={"primary"}
                    label="Přejít zpět na úvodní stránku"
                    size="md-lg"
                    url={"/"}
                    iconStart={<HomeIcon color="white" />}
                />
            </section>
            <div id="portal" />
        </main>
    );
};

export default NotFoundPrivate;
