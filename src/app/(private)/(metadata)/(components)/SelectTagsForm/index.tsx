"use client";
import React, { Dispatch, SetStateAction, useRef, useState } from "react";

import Button from "@/components/Button";
import { CheckBox } from "@/components/CheckBox";
import { Chip } from "@/components/Chip";
import { Dropdown } from "@/components/Dropdown";
import { ChevronIcon } from "@/icons/ChevronIcon";
import { TagsIcon } from "@/icons/formIcons/TagsIcon";
import { ITag } from "@/types/TagType";

import styles from "./SelectTagsForm.module.scss";

type SelectTagsFormProps = {
    tags?: ITag[];
    selectedTags?: ITag[];
    setSelectedTags: Dispatch<SetStateAction<ITag[] | undefined>>;
    label?: string;
};

export const SelectTagsForm = ({ tags, selectedTags, setSelectedTags, label }: SelectTagsFormProps) => {
    const dropdownRef = useRef<HTMLButtonElement | null>(null);

    const [visible, setVisible] = useState(false);

    const handleTagCheckbox = (tag: ITag) => {
        const updatedTags = selectedTags?.some((selectedTag) => selectedTag.title === tag.title)
            ? selectedTags?.filter((selectedTag) => selectedTag.title !== tag.title)
            : [...(selectedTags || []), tag];
        setSelectedTags(updatedTags);
    };

    const content = (
        <ul className={styles["tags-list"]}>
            {tags?.map((tag) => (
                <CheckBox
                    key={tag._id}
                    id={tag._id}
                    name={tag.title}
                    label={tag.title}
                    checked={selectedTags?.some((selectedTag) => selectedTag.title === tag.title)}
                    onClick={() => handleTagCheckbox(tag)}
                />
            ))}
        </ul>
    );

    const handleDropdown = () => {
        setVisible(!visible);
    };

    const handleChipDelete = (id: string) => {
        const updatedTags = selectedTags?.filter((_, index) => String(index) !== id);
        setSelectedTags(updatedTags);
    };

    const withTags = selectedTags && selectedTags?.length > 0;

    return (
        <div className={styles["select-container"]}>
            <Dropdown dynamicWidth hide={handleDropdown} contents={content} visible={visible} className={styles["tags"]}>
                {label ? <span className={styles["tags-label"]}>{label}</span> : false}
                <Button
                    color="outline"
                    label={"Vyberte ze seznamu"}
                    ref={dropdownRef}
                    iconStart={<TagsIcon color={"black-gray"} height="24px" width="24px" />}
                    iconEnd={<ChevronIcon color="gray-middle" direction={visible ? "up" : "down"} />}
                    onClick={handleDropdown}
                    className={`${styles["dropdown-button"]}  ${withTags && styles["dropdown-button__with-value"]}`}
                />
            </Dropdown>
            <span className={`${styles["tags-container"]} ${withTags ? styles["tags-container__with-tags"] : ""}`}>
                {withTags &&
                    selectedTags.map((item, index) => (
                        <Chip
                            tag="div"
                            id={String(index)}
                            key={item._id}
                            label={item.title}
                            deleteIconLeft
                            onDelete={() => handleChipDelete(String(index))}
                        />
                    ))}
            </span>
        </div>
    );
};
