/* eslint-disable react/display-name */
"use client";
import { Form, Formik, FormikProps } from "formik";
import { useRouter } from "next/navigation";
import React, { FC, use, useState } from "react";

import { SelectTagsForm } from "@/(private)/(metadata)/(components)/SelectTagsForm";
import { ThumbnailInfo } from "@/(private)/(metadata)/edit-metadata/[_id]/page";
import CustomSelectForm from "@/(private)/roles/(CustomSelectForm)";
import { showToast } from "@/(utils)/showToast";
import { createMetaData, deleteThumbnail, updateMetadata, uploadThumbnail } from "@/actions/formDataActions";
import { RolesIcon } from "@/components/icons/RolesIcon";
import { NotificationType } from "@/components/NotificationBanner";
import Textarea from "@/components/Textarea";
import TextField from "@/components/TextField";
import UploadFile from "@/components/UploadFile";
import DataIcon from "@/icons/formIcons/DataIcon";
import DescriptionIcon from "@/icons/formIcons/DescriptionIcon";
import { TitleIcon } from "@/icons/formIcons/TitleIcon";
import { UrlIcon } from "@/icons/formIcons/UrlIcon";
import { formThumbnail, MetadataFormValues } from "@/types/formTypes";
import { DataComponent, IData, IMetadata, IMetadataCreate } from "@/types/MetadataType";
import { ITag } from "@/types/TagType";
import { Garant, Role } from "@/types/Users";
import { EntityType } from "@/utils/CountLabel";
import { MetadataValSchema } from "@/utils/validation-schemas";

import SelectComponentForm from "../SelectComponentForm";
import styles from "./MetadataForm.module.scss";

interface MetadataFormProps {
    tagsPromise: Promise<ITag[]>;
    metadata?: IMetadata;
    thumbnailInfo?: ThumbnailInfo;
    roles: Role[] | undefined;
    garants: Garant[] | undefined;
}

const MetadataForm: FC<MetadataFormProps> = ({ tagsPromise, metadata, thumbnailInfo, roles, garants }) => {
    const tags = use(tagsPromise);
    const router = useRouter();

    const [selectedTags, setSelectedTags] = useState<ITag[] | undefined>(metadata?.tags);

    const stringifyData = (data: object) => JSON.stringify(data, undefined, 4);

    const getDataInitialValue = (data: IData) => {
        if (data.report_id && data.workspace_id) {
            return stringifyData({ report_id: data.report_id, workspace_id: data.workspace_id });
        } else if (data.districtsRoute && data.measurementsRoute) {
            return stringifyData({ districtsRoute: data.districtsRoute, measurementsRoute: data.measurementsRoute });
        } else if (data.url) {
            return stringifyData({ url: data.url });
        } else {
            return "";
        }
    };

    const dataInitialValue = metadata && metadata.data ? getDataInitialValue(metadata.data) : "";

    const initialValues = {
        title: metadata?.title || "",
        description: metadata?.description || "",
        client_route: metadata?.client_route || "",
        component: metadata?.component || "",
        type: metadata?.type || "",
        tags: metadata?.tags?.map((tag) => tag._id) || ([] as string[]),
        thumbnail: { file: null as File | null, imageUrl: metadata?.thumbnail?.url || "" },
        data: dataInitialValue,
        roles: metadata?.roles?.map((role) => role.id) || [],
        garant: metadata?.garant || "",
    };
    const handleUploadThumbnail = async (metadataId: string, file: File) => {
        const fileData = new FormData();
        fileData.append("file", file);
        const result = await uploadThumbnail(metadataId, fileData);

        if (result?.success) {
            showToast("info" as NotificationType, "Miniatura byla úspěšně nahrána");
        }

        if (result?.error) {
            showToast("error" as NotificationType, `Chyba při ukládání obrázku: ${result.error}`);
        }
        return result;
    };

    const handleDeleteThumbnail = async (metadataId: string) => {
        const result = await deleteThumbnail(metadataId);

        if (result?.success) {
            showToast("info" as NotificationType, "Miniatura byla úspěšně smazána");
        }

        if (result?.error) {
            showToast("error" as NotificationType, `Chyba při mazání obrázku: ${result.error}`);
        }
    };

    const handleMetadataCreation = async (body: IMetadataCreate, thumbnailFile: File | null) => {
        const res = await createMetaData(body);
        if (res?.error) {
            showToast("error" as NotificationType, `Chyba při ukládání dat: ${res.error}`);
            return;
        }

        if (res?.id && thumbnailFile) {
            await handleUploadThumbnail(res.id, thumbnailFile);
        }

        showToast("info" as NotificationType, "Data byla úspěšně uložena");
        router.replace(`/edit-metadata/${res?.id}`);
    };

    const handleMetadataUpdate = async (
        metadataId: string,
        body: IMetadataCreate,
        thumbnail: formThumbnail,
        metadataImgUrl: string
    ) => {
        const res = await updateMetadata(metadataId, body);

        if (thumbnail.file) {
            await handleUploadThumbnail(metadataId, thumbnail.file);
        }

        if (metadataImgUrl && !thumbnail.imageUrl) {
            await handleDeleteThumbnail(metadataId);
        }

        if (res?.error) {
            showToast("error" as NotificationType, `Chyba při změně dat: ${res.error}`);
            return false;
        } else {
            showToast("info" as NotificationType, "Data byla úspěšně změněna");
            return true;
        }
    };

    const garantOprions = garants?.map((item) => ({
        value: item.id.toString(),
        label: `${item.name} ${item.surname}`,
    }));

    const componentOptions = roles
        ?.map((item) => ({
            value: item.id.toString(),
            label: item.label,
        }))
        .sort((a, b) => a.label.localeCompare(b.label));

    return (
        <div className={styles["form-container"]}>
            <Formik
                initialValues={initialValues}
                validationSchema={MetadataValSchema}
                validateOnBlur={true}
                onSubmit={async (values) => {
                    const body = {
                        title: values.title,
                        description: values.description,
                        client_route: values.client_route,
                        tags: selectedTags?.map((tag) => tag._id) || [],
                        component: values.component as DataComponent,
                        type: values.component === "powerbiComponent" ? "dashboard" : "application",
                        data: values.data ? JSON.parse(values.data) : {},
                        roles: values.roles,
                        garant: values.garant,
                    };
                    if (metadata?._id) {
                        await handleMetadataUpdate(metadata._id, body, values.thumbnail, metadata?.thumbnail?.url);
                    } else {
                        await handleMetadataCreation(body, values.thumbnail.file);
                    }
                }}
            >
                {({ values, errors, touched, setFieldValue }: FormikProps<MetadataFormValues>) => (
                    <Form id="metadataForm" className={styles["form-items"]}>
                        <TextField
                            type="text"
                            id="title"
                            label="Název"
                            placeholder={metadata?.title || "Název"}
                            value={values.title}
                            onChange={(e) => setFieldValue("title", e.target.value)}
                            iconStart={<TitleIcon color="black-gray" height="24px" />}
                            error={touched.title && !!errors.title}
                            errorMessage={errors.title?.toString()}
                        />
                        <TextField
                            type="text"
                            id="description"
                            label="Popis"
                            placeholder={metadata?.description || "Popis"}
                            value={values.description}
                            onChange={(e) => setFieldValue("description", e.target.value)}
                            iconStart={<DescriptionIcon color="black-gray" height="24px" />}
                            error={touched.description && !!errors.description}
                            errorMessage={errors.description?.toString()}
                            info={true}
                            infoMessage="Max. 128 znaků"
                        />

                        <CustomSelectForm
                            label="Role"
                            onSelected={(values) => setFieldValue("roles", values)}
                            componentOptions={componentOptions}
                            value={values.roles}
                            entity={EntityType.Role}
                            withCheckboxes
                            errorMessage={errors.roles?.toString()}
                            error={touched.roles && !!errors.roles}
                            selectedCountLabel
                            iconStart={<RolesIcon color="gray-dark" />}
                        />
                        <TextField
                            type="text"
                            id="client_route"
                            label="URL pro uživatele"
                            placeholder={metadata?.client_route || "URL pro uživatele je automaticky generována"}
                            value={values.client_route}
                            onChange={(e) => setFieldValue("client_route", e.target.value)}
                            iconStart={<UrlIcon color="black-gray" height="24px" />}
                            error={touched.client_route && !!errors.client_route}
                            errorMessage={errors.client_route?.toString()}
                        />
                        <CustomSelectForm
                            label="Garant"
                            onSelected={(values) => setFieldValue("garant", values)}
                            componentOptions={garantOprions}
                            value={values.garant}
                            entity={EntityType.Garant}
                            errorMessage={errors.garant?.toString()}
                            error={touched.garant && !!errors.garant}
                            selectedCountLabel
                        />
                        <SelectComponentForm
                            label="Komponenta"
                            value={values.component}
                            onSelected={(value) => setFieldValue("component", value)}
                            error={touched.component && !!errors.component}
                            errorMessage={errors.component?.toString()}
                        />
                        <SelectTagsForm tags={tags} selectedTags={selectedTags} setSelectedTags={setSelectedTags} label="Tagy" />
                        <Textarea
                            id="data"
                            label="Data"
                            placeholder="Data"
                            value={values.data}
                            onChange={(e) => setFieldValue("data", e.target.value)}
                            iconStart={<DataIcon color="black-gray" width="26px" height="21px" />}
                            error={touched.data && !!errors.data}
                            errorMessage={errors.data?.toString()}
                        />
                        <UploadFile
                            onFileSelected={(file, imageUrl) => {
                                setFieldValue("thumbnail", {
                                    file,
                                    imageUrl,
                                });
                            }}
                            thumbnailUrl={thumbnailInfo?.url}
                            thumbnailSize={thumbnailInfo?.size}
                            error={touched.thumbnail && !!errors.thumbnail}
                            errorMessage={errors.thumbnail?.toString()}
                        />
                    </Form>
                )}
            </Formik>
        </div>
    );
};

export default MetadataForm;
