import React, { useEffect, useState } from "react";

import Button from "@/components/Button";
import { Dropdown } from "@/components/Dropdown";
import { Radio } from "@/components/Radio";
import { ChevronIcon } from "@/icons/ChevronIcon";
import ComponentIcon from "@/icons/formIcons/ComponetIcon";

import styles from "./SelectComponentForm.module.scss";

type SelectComponentFormProps = {
    label?: string;
    value?: string;
    onSelected: (value: string) => void;
    error?: boolean;
    errorMessage?: string;
};

const ComponentOptions = [
    { value: "powerbiComponent", label: "Power BI" },
    { value: "parkingsMap", label: "Mapa Parkovišť" },
    { value: "sortedWasteMap", label: "Mapa tříděného odpadu" },
];

const SelectComponentForm = ({ label, value, onSelected, error, errorMessage }: SelectComponentFormProps) => {
    const [visible, setVisible] = useState(false);
    const [component, setComponent] = useState({ value, label });

    const indexOfComponent = ComponentOptions.findIndex((option) => option.value === component.value);

    const handleDropdown = () => {
        setVisible(!visible);
    };

    const content = (
        <ul className={styles["select-listbox"]} role="listbox" id={label}>
            {ComponentOptions.map((option) => (
                <Radio
                    key={option.value}
                    id={option.value}
                    label={option.label}
                    name={label}
                    checked={component.value === option.value}
                    onClick={() => setComponent(option)}
                    role="option"
                />
            ))}
        </ul>
    );

    useEffect(() => {
        if (component.value) {
            onSelected(component.value);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [component.value]);

    return (
        <div className={styles["select-container"]}>
            <Dropdown
                hide={handleDropdown}
                contents={content}
                visible={visible}
                className={styles["select-dropdown"]}
                dynamicWidth
            >
                {value ? <span className={styles["select-dropdown-label"]}>{label}</span> : false}
                <Button
                    color="outline"
                    label={component.value ? ComponentOptions[indexOfComponent].label : label}
                    iconStart={<ComponentIcon color="black-gray" height="24px" />}
                    iconEnd={<ChevronIcon color="gray-middle" direction={visible ? "up" : "down"} />}
                    onClick={handleDropdown}
                    className={`${styles["dropdown-button"]}
                    ${!value ? styles["dropdown-button__label-hidden"] : ""}
                    ${error && styles["dropdown-button__error"]}
                    ${value && styles["dropdown-button__with-value"]}`}
                    role="combobox"
                    aria-labelledby="select button"
                    aria-haspopup="listbox"
                    aria-expanded="false"
                    aria-controls={label}
                />
            </Dropdown>
            {error ? <span className={styles["error"]}>{errorMessage}</span> : false}
        </div>
    );
};

export default SelectComponentForm;
