import React, { FC } from "react";

import MetadataForm from "@/(private)/(metadata)/(components)/MetadataForm";
import { tagsService } from "@/(services)/tags-service";
import layout from "@/layout.module.scss";
import { usersService } from "@/services/users-service";

export const dynamic = "force-dynamic";

const CreateMetaDataPage: FC = async () => {
    const tagsPromise = tagsService.getAllTags();
    const [roles, garants] = await Promise.all([usersService.getRoles(), usersService.getGarants()]);
    return (
        <main className={layout.main} id="main">
            <div className={layout.container}>
                <MetadataForm tagsPromise={tagsPromise} roles={roles.payload || []} garants={garants} />
            </div>
        </main>
    );
};

export default CreateMetaDataPage;
