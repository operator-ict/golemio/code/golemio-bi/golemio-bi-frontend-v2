import { redirect } from "next/navigation";
import React, { FC } from "react";
import { config } from "src/config";

import MetadataForm from "@/(private)/(metadata)/(components)/MetadataForm";
import layout from "@/layout.module.scss";
import { metadataService } from "@/services/metadata-service";
import { tagsService } from "@/services/tags-service";
import { thumbnailService } from "@/services/thumbnail-service";
import { usersService } from "@/services/users-service";

export const dynamic = "force-dynamic";
interface PageProps {
    params: { _id?: string };
}

export type ThumbnailInfo = {
    url: string;
    size?: number;
};

const EditMetaDataPage: FC<PageProps> = async ({ params }) => {
    const tagsPromise = tagsService.getAllTags();
    const [roles, metadataResponse, garants] = await Promise.all([
        usersService.getRoles(),
        metadataService.getMetadataByRoute(String(params._id), "reload"),
        usersService.getGarants(),
    ]);

    const metadata = metadataResponse.payload;

    if (!metadata) {
        redirect("/");
    }
    let thumbnailInfo: ThumbnailInfo = { url: "", size: 0 };
    if (metadata?.thumbnail) {
        const thumbnail = await thumbnailService.getThumbnail(String(metadata._id));
        thumbnailInfo = {
            url: `${config.NEXT_PUBLIC_API_URL}${metadata?.thumbnail?.url}`,
            size: thumbnail?.size,
        };
    }

    return (
        <main className={layout.main} id="main">
            <div className={layout.container}>
                <MetadataForm
                    tagsPromise={tagsPromise}
                    metadata={metadata}
                    thumbnailInfo={thumbnailInfo}
                    roles={roles.payload || []}
                    garants={garants}
                />
            </div>
        </main>
    );
};

export default EditMetaDataPage;
