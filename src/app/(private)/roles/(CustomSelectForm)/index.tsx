"use client";
/* eslint-disable max-len */
import React, { FC, ReactNode, useCallback, useEffect, useState } from "react";

import Button from "@/components/Button";
import { CheckBox as DefaultCheckBox } from "@/components/CheckBox";
import { Dropdown } from "@/components/Dropdown";
import { ChevronIcon } from "@/components/icons/ChevronIcon";
import { GarantIcon } from "@/components/icons/GarantIcon";
import SelectedDashboardIcon from "@/components/icons/SelectedDashboardIcon";
import { SearchInput } from "@/components/SearchInput";
import { EntityType, getLabel } from "@/utils/CountLabel";
import { removeDiacritics } from "@/utils/removeDiacritics";
import { sortOptionsWithSelectedOnTop } from "@/utils/sortOptionsWithSelectedOnTop";

import styles from "./CustomSelectForm.module.scss";

export interface CheckBoxProps extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    label?: string;
    subLabel?: string;
    onClick: () => void;
    activeUser?: boolean;
    className?: string;
}

interface CustomSelectFormProps {
    label?: string;
    onSelected: (value: string | string[]) => void;
    componentOptions: { value: string; label: string; organization?: string; activeUser?: boolean }[] | undefined;
    value: string | string[];
    entity: EntityType;
    withCheckboxes?: boolean;
    errorMessage?: string;
    error?: boolean;
    readOnly?: boolean;
    usersComponent?: boolean;
    iconStart?: ReactNode;
    activeUser?: boolean;
    CheckBoxComponent?: React.ComponentType<CheckBoxProps>;
    selectedCountLabel: boolean;
}

const CustomSelectForm: FC<CustomSelectFormProps> = ({
    label,
    onSelected,
    componentOptions,
    value,
    entity,
    withCheckboxes,
    errorMessage,
    error,
    readOnly,
    iconStart,
    CheckBoxComponent,
    selectedCountLabel,
}) => {
    const [visible, setVisible] = useState(false);
    const [selectedOptions, setSelectedOptions] = useState<string | string[]>(value || (typeof value === "string" ? "" : []));
    const [query, setQuery] = useState<string>("");

    const buttonStartIcon = entity === "garant" ? <GarantIcon color="black-gray" /> : iconStart;
    useEffect(() => {
        if (JSON.stringify(selectedOptions) !== JSON.stringify(value)) {
            setSelectedOptions(value || (typeof value === "string" ? "" : []));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [value]);

    const memoizedOnSelected = useCallback((value: string | string[]) => onSelected(value), [onSelected]);

    useEffect(() => {
        if (JSON.stringify(selectedOptions) !== JSON.stringify(value)) {
            memoizedOnSelected(selectedOptions);
        }
    }, [selectedOptions, value, memoizedOnSelected]);

    const filteredOptions = query
        ? componentOptions?.filter(
              (option) =>
                  removeDiacritics(option.label.toLowerCase()).includes(removeDiacritics(query.toLowerCase())) ||
                  (option.organization &&
                      removeDiacritics(option.organization.toLowerCase()).includes(removeDiacritics(query.toLowerCase())))
          )
        : componentOptions;

    const handleToggleDropdown = () => setVisible(!visible);

    const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => setQuery(e.target.value);

    const handleCheckboxChange = (id: string) => {
        if (typeof selectedOptions === "string") {
            setSelectedOptions([id]);
            return;
        }
        setSelectedOptions((prevSelected) => {
            if (typeof prevSelected === "string") {
                return [id];
            }
            return prevSelected.includes(id) ? prevSelected.filter((item) => item !== id) : [...prevSelected, id];
        });
    };

    const selectedLabels =
        typeof selectedOptions === "string"
            ? componentOptions?.find((option) => option.value === selectedOptions)?.label || ""
            : selectedOptions
                  .map((id) => componentOptions?.find((option) => option.value === id)?.label || "")
                  .filter(Boolean)
                  .join(", ");

    const displayLabels =
        entity === EntityType.Users && Array.isArray(selectedOptions) && selectedOptions.length > 0
            ? `Vybráno ${selectedOptions.length} uživatelé`
            : Array.isArray(selectedOptions) && selectedOptions.length > 1
              ? `${selectedLabels.substring(0, 50)}...`
              : selectedLabels;

    const handleResetSelection = () => {
        setSelectedOptions([]);
        setQuery("");
    };

    const resetInputField = () => {
        setQuery("");
    };

    const CheckBoxToRender = CheckBoxComponent || DefaultCheckBox;

    return (
        <div className={styles["select-container"]}>
            <Dropdown
                hide={handleToggleDropdown}
                className={styles["select-container__dropdown"]}
                dynamicWidth
                contents={
                    <div className={styles["select-container__list"]}>
                        <SearchInput
                            resetInputField={resetInputField}
                            value={query}
                            onChange={handleSearchChange}
                            placeholder="Vyhledejte"
                        />

                        {withCheckboxes && selectedCountLabel && entity !== EntityType.Users && (
                            <div className={styles["select-container__selected"]}>
                                {selectedOptions.length > 0 ? (
                                    <>
                                        <button onClick={handleResetSelection} disabled={readOnly}>
                                            <SelectedDashboardIcon fill={readOnly ? "#b8becc" : undefined} />
                                        </button>
                                        <span>{getLabel(selectedOptions.length, entity)}</span>
                                    </>
                                ) : (
                                    getLabel(selectedOptions.length, entity)
                                )}
                            </div>
                        )}

                        <ul className={styles["select-container__options"]}>
                            {filteredOptions &&
                                sortOptionsWithSelectedOnTop(filteredOptions, selectedOptions).map((option) =>
                                    withCheckboxes ? (
                                        <CheckBoxToRender
                                            key={option.value}
                                            id={option.value}
                                            label={option.label}
                                            name={label}
                                            checked={selectedOptions.includes(option.value)}
                                            onClick={() => handleCheckboxChange(option.value)}
                                            role="option"
                                            readOnly={readOnly}
                                            subLabel={option.organization}
                                            activeUser={option.activeUser}
                                        />
                                    ) : (
                                        <li
                                            key={option.value}
                                            onClick={() => {
                                                setSelectedOptions(option.value);
                                                handleToggleDropdown();
                                            }}
                                            role="option"
                                            tabIndex={0}
                                            aria-selected={selectedOptions.includes(option.value)}
                                            className={styles["select-container__option"]}
                                        >
                                            {option.label}
                                        </li>
                                    )
                                )}
                        </ul>
                    </div>
                }
                visible={visible}
            >
                {value && value.length > 0 ? <span className={styles["select-dropdown-label"]}>{label}</span> : false}

                <Button
                    color="outline"
                    label={selectedOptions.length > 0 ? displayLabels : label}
                    iconStart={buttonStartIcon}
                    iconEnd={<ChevronIcon color="gray-middle" direction={visible ? "up" : "down"} />}
                    onClick={entity === EntityType.Users && readOnly ? undefined : handleToggleDropdown}
                    disabled={entity === EntityType.Users && readOnly}
                    className={`
                        ${styles["dropdown-button"]}
                        ${!selectedOptions ? styles["dropdown-button__label-hidden"] : "dropdown-button__label"}
                        ${selectedOptions.length > 0 && styles["dropdown-button__with-value"]}
                        ${error && styles["dropdown-button__error"]}
                        `}
                    role="combobox"
                    aria-labelledby="select button"
                    aria-haspopup="listbox"
                    aria-expanded={visible}
                    aria-controls={label}
                />
            </Dropdown>
            {error ? <span className={styles["error"]}>{errorMessage}</span> : false}
        </div>
    );
};

export default CustomSelectForm;
