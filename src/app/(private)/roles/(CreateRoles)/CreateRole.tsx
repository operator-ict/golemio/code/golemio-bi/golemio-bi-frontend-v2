"use client";

import React, { useState, useTransition } from "react";
import toast from "react-hot-toast";

import { getMetaData } from "@/actions/formDashboardsActions";
import { getAllUsers } from "@/actions/formUserActions";
import Button from "@/components/Button";
import { AddPlusSimpleIcon } from "@/components/icons/AddPlusSimpleIcon";
import text from "@/textContent/cs.json";
import { IMetadata } from "@/types/MetadataType";
import { User } from "@/types/Users";

import CreateRolesModal from "./CreateRolesModal";

const CreateRole = () => {
    const [isModalOpen, setModalOpen] = useState(false);
    const [isPending, startTransition] = useTransition();
    const [dashboards, setDashboards] = useState<IMetadata[]>([]);
    const [users, setUsers] = useState<User[] | null>([]);

    const handleOpenModal = () => {
        startTransition(async () => {
            try {
                const res = await getMetaData();
                const users = await getAllUsers();
                setDashboards(res);
                setUsers(users);
            } catch (error) {
                console.error(error);
                toast.error("Chyba při načítání dashboardů.");
            }
            setModalOpen(true);
        });
    };
    return (
        <>
            <Button
                color="secondary"
                label={text.button["rolesPage"]}
                hideLabelTablet
                iconStart={<AddPlusSimpleIcon color="white" width="11px" height="11px" />}
                onClick={handleOpenModal}
                disabled={isPending}
            />
            {isModalOpen && (
                <CreateRolesModal users={users} show={isModalOpen} onClose={() => setModalOpen(false)} dashboards={dashboards} />
            )}
        </>
    );
};

export default CreateRole;
