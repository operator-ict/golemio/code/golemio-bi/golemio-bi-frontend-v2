import { useRouter } from "next/navigation";
import React, { FC, useEffect, useMemo, useState, useTransition } from "react";

import { CreateRole, RolePatch } from "@/actions/formDashboardsActions";
import Button from "@/components/Button";
import { CheckBox } from "@/components/CheckBox";
import { CheckBoxsubLabel } from "@/components/CheckBoxWithSubLabel";
import DashboardFormIcon from "@/components/icons/DashboardFormIcon";
import SubmitIcon from "@/components/icons/formIcons/SubmitIcon";
import QuestionIcon from "@/components/icons/QuestionIcon";
import { UsersIcon } from "@/components/icons/UsersIcon";
import { Modal } from "@/components/Modal";
import { NotificationType } from "@/components/NotificationBanner";
import TextField from "@/components/TextField";
import { usePermissions } from "@/context/PermissionsContext";
import { IMetadata } from "@/types/MetadataType";
import { RoleDashboards, User } from "@/types/Users";
import { EntityType } from "@/utils/CountLabel";
import { showToast } from "@/utils/showToast";

import CustomSelectForm from "../(CustomSelectForm)";
import styles from "./CreateRoles.module.scss";

export interface IFormData {
    label: string;
    description: string;
    isPublic: string;
    dashboards: string[];
    users: User[];
}
interface IEditRolesModal {
    show: boolean;
    id?: string;
    initialData?: {
        id: number;
        label: string;
        description: string;
        isPublic?: boolean;
        dashboards?: RoleDashboards[];
        users: User[] | undefined;
    };
    onClose: () => void;
    dashboards: IMetadata[] | undefined;
    users: User[] | null;
}

const CreateRolesModal: FC<IEditRolesModal> = ({ show, id, initialData, onClose, dashboards, users }) => {
    const canEditRoles = usePermissions().canEditRoles;
    const canEditUsersRole = usePermissions().canEditUsersRole;

    const [isPending, startTransition] = useTransition();
    const [formState, setFormState] = useState<{
        label: string;
        description: string;
        isPublic: boolean;
        dashboards: string[];
        users: string[];
    }>({
        label: "",
        description: "",
        isPublic: false,
        dashboards: [],
        users: [],
    });
    const isFormUnchanged = useMemo(() => {
        const initial = {
            label: initialData?.label || "",
            description: initialData?.description || "",
            isPublic: initialData?.isPublic || false,
            dashboards: initialData?.dashboards?.map((d) => d.id) || [],
            users: initialData?.users ? initialData?.users.map((user) => user.id) : [],
        };
        return JSON.stringify(initial) === JSON.stringify(formState);
    }, [initialData, formState]);

    const router = useRouter();

    const dashboardsComponentOptions = dashboards?.map((item) => ({
        value: item._id,
        label: item.title,
    }));

    const userComponentOptions = users?.map((item) => ({
        value: item.id,
        label: item.name || item.surname ? `${item.name ?? ""} ${item.surname ?? ""}`.trim() : item.email,
        organization: item.organization || "Neznámá organizace",
        activeUser: "invitation_accepted_at" in item,
    }));

    useEffect(() => {
        if (initialData) {
            setFormState({
                label: initialData.label,
                description: initialData.description,
                isPublic: initialData.isPublic || false,
                dashboards: initialData.dashboards ? initialData.dashboards.map((d) => d.id).filter(Boolean) : [],
                users: initialData.users ? initialData.users.map((user) => user.toString()) : [],
            });
        }
    }, [initialData]);

    const handleFieldChange = <K extends keyof typeof formState>(field: K, value: (typeof formState)[K]) => {
        setFormState((prev) => ({ ...prev, [field]: value }));
    };

    const validateForm = () => {
        if (!formState.label.trim()) {
            showToast("error" as NotificationType, "Název role je povinný.");
            return false;
        }
        if (formState.description.length > 128) {
            showToast("error" as NotificationType, "Popis role nesmí přesáhnout 128 znaků.");
            return false;
        }
        return true;
    };

    const handleSubmit = async () => {
        if (!validateForm()) return;

        let formData;

        if (canEditUsersRole && !canEditRoles) {
            const initialUsers = initialData?.users ? initialData.users.map((user) => user.id) : [];
            const isUsersChanged = JSON.stringify(initialUsers) !== JSON.stringify(formState.users);

            if (!isUsersChanged) {
                showToast("info" as NotificationType, "Nebyla provedena žádná změna uživatelů.");
                return;
            }

            formData = { users: formState.users };
        } else {
            formData = {
                label: formState.label,
                description: formState.description,
                dashboards: Array.from(new Set([...formState.dashboards])),
                isPublic: formState.isPublic,
                users: formState.users,
            };
        }
        startTransition(async () => {
            try {
                const result = !id
                    ? await CreateRole(JSON.stringify(formData))
                    : await RolePatch(initialData?.id.toString() || "", JSON.stringify(formData));

                if (result) {
                    showToast(
                        "info" as NotificationType,
                        id
                            ? canEditUsersRole && !canEditRoles
                                ? "Seznam uživatelů v roli byl úspěšně aktualizován."
                                : "Role byla úspěšně aktualizována."
                            : "Nová role byla úspěšně vytvořena."
                    );
                    onClose();
                } else {
                    showToast(
                        "error" as NotificationType,
                        id ? "Při aktualizaci role došlo k chybě." : "Při vytváření role došlo k chybě."
                    );
                }
                router.refresh();
            } catch (error) {
                showToast("error" as NotificationType, "Při odesílání formuláře došlo k chybě.");
                console.debug(error);
            }
        });
    };

    const adminModallLabel = id ? "Upravit roli" : "Vytvoření role";
    const infoModalLabel = "Detail role";

    return (
        <Modal show={show} label={canEditRoles ? adminModallLabel : infoModalLabel} onClose={onClose}>
            <form className={styles["modal-form"]} onSubmit={(e) => e.preventDefault()}>
                <TextField
                    id="title"
                    label="Název"
                    value={formState.label}
                    onChange={(e) => handleFieldChange("label", e.target.value)}
                    placeholder="Název"
                    className={styles["modal-form__text-field"]}
                    disabled={!canEditRoles}
                />
                <TextField
                    id="description"
                    label="Popis"
                    value={formState.description}
                    onChange={(e) => handleFieldChange("description", e.target.value)}
                    placeholder="Popis"
                    info
                    infoMessage="Max. 128 znaků"
                    iconEnd={<QuestionIcon />}
                    className={styles["modal-form__text-field"]}
                    disabled={!canEditRoles}
                />
                <CustomSelectForm
                    label="Uživatelé"
                    onSelected={(values) => {
                        handleFieldChange("users", values as string[]);
                    }}
                    componentOptions={userComponentOptions}
                    value={formState.users}
                    entity={EntityType.Users}
                    withCheckboxes
                    selectedCountLabel={false}
                    readOnly={!canEditUsersRole}
                    iconStart={<UsersIcon color="gray-dark" />}
                    CheckBoxComponent={CheckBoxsubLabel}
                />
                <CustomSelectForm
                    label="Dashboardy"
                    onSelected={(values) => {
                        handleFieldChange("dashboards", values as string[]);
                    }}
                    componentOptions={dashboardsComponentOptions}
                    value={formState.dashboards}
                    entity={EntityType.Dashboard}
                    withCheckboxes
                    selectedCountLabel
                    readOnly={!canEditRoles}
                    iconStart={<DashboardFormIcon color="black-gray" height="24px" />}
                />
                {canEditRoles && (
                    <div className={styles["modal-form__check-box"]}>
                        <CheckBox
                            label="Veřejná role"
                            checked={formState.isPublic}
                            onClick={() => handleFieldChange("isPublic", !formState.isPublic)}
                        />
                    </div>
                )}

                <div className={styles["modal-form__button"]}>
                    <Button color="secondary" type="button" onClick={onClose}>
                        Zpět
                    </Button>
                    {(canEditRoles || canEditUsersRole) && (
                        <Button
                            color="primary"
                            type="submit"
                            onClick={handleSubmit}
                            iconStart={<SubmitIcon color="white" width="1rem" height="1rem" />}
                            disabled={isFormUnchanged || isPending}
                        >
                            {id ? "Uložit změny" : "Založit"}
                        </Button>
                    )}
                </div>
            </form>
        </Modal>
    );
};

export default CreateRolesModal;
