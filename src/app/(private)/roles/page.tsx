import { Suspense } from "react";

import { NavBarAdmin } from "@/components/NavBarAdmin";
import { SearchForm } from "@/components/SearchForm";
import { ServerPaginationTable } from "@/components/TableComponent/server-pagination-table";
import { PermissionsService } from "@/services/permissions-service";
import { usersService } from "@/services/users-service";
import styles from "@/styles/AdminPages.module.scss";
import text from "@/textContent/cs.json";

import { RolesColumns } from "./(Columns)/RolesColumns";
import CreateRole from "./(CreateRoles)/CreateRole";

export const dynamic = "force-dynamic";

interface RolesPageProps {
    searchParams: {
        page: string;
        limit: string;
        orderDir: "asc" | "desc";
        orderBy: string;
        search: string;
    };
}
const RolesPage = async ({ searchParams }: RolesPageProps) => {
    const page = searchParams.page ? Number(searchParams.page) : 1;
    const limit = searchParams.limit ? Number(searchParams.limit) : 10;
    const searchParam = searchParams.search;
    const orderDir = searchParams.orderDir ?? "asc";
    const orderBy = searchParams.orderBy ?? "label";
    searchParams.limit = limit.toString();
    searchParams.page = page.toString();
    searchParams.orderDir = orderDir;
    searchParams.orderBy = orderBy;

    const [roles, canEditRoles] = await Promise.all([
        usersService.getRoles(searchParams),
        PermissionsService.getRolesEditPermissions(),
    ]);
    const totalCounts = roles.count ? parseInt(roles.count) : 1;
    const pageCount = Math.ceil(totalCounts / +limit);
    return (
        <main className={styles["admin-page-container"]}>
            <div className={styles["page-nav"]}>
                <NavBarAdmin />
            </div>
            <div className={styles["data-container"]}>
                <div className={styles["data-container-header"]}>
                    <div className={styles["data-container-header__title"]}>
                        <h1>{text.rolesPage.title}</h1>
                        {canEditRoles && <CreateRole />}
                    </div>
                    <SearchForm
                        search={searchParam}
                        placeholder={text.search.placeholderUsersPage}
                        className={styles["data-container-header-search"]}
                    />
                </div>
                <SearchForm
                    search={searchParam}
                    placeholder={text.search.placeholderUsersPage}
                    className={styles["search-tablet"]}
                />
                <Suspense>
                    <ServerPaginationTable
                        columns={RolesColumns}
                        data={roles.payload ?? []}
                        totalCount={totalCounts}
                        limit={limit}
                        page={page}
                        pageCount={pageCount}
                    />
                </Suspense>
            </div>
        </main>
    );
};

export default RolesPage;
