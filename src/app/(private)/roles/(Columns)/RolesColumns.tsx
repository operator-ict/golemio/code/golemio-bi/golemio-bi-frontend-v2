"use client";

import { ColumnDef } from "@tanstack/react-table";

import { ActionIcon } from "@/(components)/icons/ActionIcon";
import { Role } from "@/(types)/Users";
import ManualSortedButton from "@/components/SortedButton/ManualSortedButton";

import ActionsCell from "./ActionsCell";
import styles from "./RolesColumns.module.scss";

const formatDate = (dateString: string) => {
    if (!dateString) return "";
    const date = new Date(dateString);
    return new Intl.DateTimeFormat("cs-CZ", {
        day: "2-digit",
        month: "2-digit",
        year: "numeric",
        hour: "2-digit",
        minute: "2-digit",
    }).format(date);
};

export const RolesColumns: ColumnDef<Role>[] = [
    {
        accessorKey: "label",
        header: () => <ManualSortedButton columnName={"label"} title={"Název"} />,
        size: 365,
    },
    {
        accessorKey: "updated_at",
        header: () => <ManualSortedButton columnName={"updated_at"} title={"Poslední změna"} />,
        size: 155,
        cell: ({ row }) => {
            return <span>{formatDate(row.original.updated_at as string)}</span>;
        },
    },
    {
        accessorKey: "description",
        header: () => <ManualSortedButton columnName={"description"} title={"Popis"} />,
        size: 550,
    },
    {
        accessorKey: "dashboard_count",
        header: " Počet dashboardů",
        size: 200,
    },
    {
        accessorKey: "actions",
        header: () => (
            <span className={styles["actions-header"]}>
                <ActionIcon color={"primary"} />
                Akce
            </span>
        ),
        size: 185,
        cell: ({ row }) => <ActionsCell row={row} />,
    },
];
