import { Row } from "@tanstack/react-table";
import { FC, useState, useTransition } from "react";

import { getMetaData, GetRoleForUpdate } from "@/actions/formDashboardsActions";
import { getAllUsers } from "@/actions/formUserActions";
import Button from "@/components/Button";
import { EditPenIcon } from "@/components/icons/EditPenIcon";
import { RemoveIcon } from "@/components/icons/RemoveIcon";
import { usePermissions } from "@/context/PermissionsContext";
import { InformationsIcon } from "@/icons/InformationsIcon";
import { IMetadata } from "@/types/MetadataType";
import { Role, UpdateRole, User } from "@/types/Users";

import CreateRolesModal from "../(CreateRoles)/CreateRolesModal";
import styles from "./RolesColumns.module.scss";
interface ActionsCellProps {
    row: Row<Role>;
}
const ActionsCell: FC<ActionsCellProps> = ({ row }) => {
    const canEditRoles = usePermissions().canEditRoles;

    const [isModalOpen, setModalOpen] = useState(false);
    const [data, setData] = useState<UpdateRole>();
    const [users, setUsers] = useState<User[] | null>(null);
    const [allDashboards, setAllDashboards] = useState<IMetadata[] | undefined>();
    const [isPending, startTransition] = useTransition();

    const handleOpenModal = async () => {
        startTransition(async () => {
            try {
                const res = await GetRoleForUpdate(row.original.id.toString());
                const dashboardsAll = await getMetaData();
                const users = await getAllUsers();
                setUsers(users);
                setData(res as UpdateRole);
                setAllDashboards(dashboardsAll);
                setModalOpen(true);
            } catch (error) {
                console.error("Error fetching role data:", error);
            }
        });
    };

    const handleCloseModal = () => {
        setModalOpen(false);
    };

    return (
        <div className={styles["button-container"]}>
            {canEditRoles && (
                <Button
                    color="tertiary"
                    iconStart={<RemoveIcon color="gray-dark" />}
                    size="sm"
                    className={styles["button-container__remove-button"]}
                />
            )}
            <Button
                label={canEditRoles ? "Upravit" : "Informace"}
                size="sm"
                color="secondary"
                iconStart={
                    canEditRoles ? <EditPenIcon color="white" width="11px" height="11px" /> : <InformationsIcon color="white" />
                }
                onClick={handleOpenModal}
                disabled={isPending}
            />
            {isModalOpen && (
                <CreateRolesModal
                    show={isModalOpen}
                    id={row.id}
                    onClose={handleCloseModal}
                    dashboards={allDashboards}
                    users={users}
                    initialData={{
                        id: row.original.id,
                        label: row.original.label,
                        description: row.original.description,
                        dashboards: data?.dashboards,
                        users: data?.users,
                    }}
                />
            )}
        </div>
    );
};

export default ActionsCell;
