"use client";
import { useRouter, useSearchParams } from "next/navigation";
import { usePlausible } from "next-plausible";
import React, { useEffect, useState, useTransition } from "react";

import { useRemoveParam } from "@/(hooks)/useRemoveParam";
import Button from "@/components/Button";
import { ButtonContentLoader } from "@/components/ButtonContentLoader";
import { useUpdatedQuery } from "@/hooks/useUpdatedQuery";
import { AllRecordsIcon } from "@/icons/AllRecordsIcon";
import { StarOutlineIcon } from "@/icons/StarOutlineIcon";

import styles from "./Favourites.module.scss";

export const FavouritesContainer = () => {
    const router = useRouter();
    const searchParams = useSearchParams();
    const [fav, setFav] = useState<string | null>(searchParams.get("fav"));
    const [isPending, startTransition] = useTransition();
    const [activeButton, setActiveButton] = useState<"all" | "my" | null>(null);

    const myQuery = useUpdatedQuery("fav", "my");
    const allQuery = useRemoveParam("fav");

    const plausible = usePlausible();

    useEffect(() => {
        setFav(searchParams.get("fav"));
    }, [searchParams]);

    return (
        <div>
            <div className={styles["favourites-container"]}>
                <Button
                    size="sm"
                    color={fav === "my" ? "tertiary" : "secondary"}
                    iconStart={
                        isPending && activeButton === "all" ? null : (
                            <AllRecordsIcon color={fav === "my" ? "gray-dark" : "white"} />
                        )
                    }
                    onClick={() => {
                        setActiveButton("all");
                        plausible("Click-to-All", {
                            props: {
                                favorites: "all",
                            },
                        });

                        startTransition(() => {
                            router.push(`?${allQuery}`);
                        });
                    }}
                    disabled={isPending && activeButton === "all"}
                >
                    <ButtonContentLoader isLoading={isPending} isActive={activeButton === "all"} label="Vše" />
                </Button>
                <Button
                    size="sm"
                    color={fav === "my" ? "secondary" : "tertiary"}
                    iconStart={
                        isPending && activeButton === "my" ? null : (
                            <StarOutlineIcon color={fav === "my" ? "white" : "gray-dark"} />
                        )
                    }
                    onClick={() => {
                        setActiveButton("my");
                        plausible("Click-to-My", {
                            props: {
                                favorites: "my",
                            },
                        });
                        startTransition(() => {
                            router.push(`?${myQuery}`);
                        });
                    }}
                    disabled={isPending && activeButton === "my"}
                >
                    <ButtonContentLoader isLoading={isPending} isActive={activeButton === "my"} label="Moje" />
                </Button>
            </div>
        </div>
    );
};
