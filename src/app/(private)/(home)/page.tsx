import { Suspense } from "react";

import Button from "@/components/Button";
import { NavBarAdmin } from "@/components/NavBarAdmin";
import { AddPlusSimpleIcon } from "@/icons/AddPlusSimpleIcon";
import layout from "@/layout.module.scss";
import Loading from "@/private/loading";
import { metadataService } from "@/services/metadata-service";
import { PermissionsService } from "@/services/permissions-service";
import { tagsService } from "@/services/tags-service";
import { IFavourites, userService } from "@/services/user-service";
import navStyles from "@/styles/AdminPages.module.scss";
import text from "@/textContent/cs.json";
import { DataType, IMetadata } from "@/types/MetadataType";

import { DataContainer } from "./(DataContainer)";
import { DeleteDialog } from "./(DeleteDialog)";
import { FavouritesContainer } from "./(FavouritesContainer)";
import { FilterAndSearch } from "./(FilterAndSearch)/FilterAndSearch";
import styles from "./Home.module.scss";

type PageProps = {
    searchParams: { tag?: string; type?: DataType; search?: string; fav?: string; delete?: string };
};
export const dynamic = "force-dynamic";

const Home = async ({ searchParams }: PageProps) => {
    const canEditDashboards = await PermissionsService.getDashboardsEditPermissions();

    const tagParam = searchParams.tag ? searchParams.tag.split(",") : undefined;
    const typeParam = searchParams.type;
    const searchParam = searchParams.search;
    const metadataPromise = searchParam
        ? metadataService.getMetadataBySearch(searchParam)
        : metadataService.getMetadataByFilterAndType(typeParam, tagParam);
    const tagsPromise = tagsService.getUserTags();

    const deleteItemId = searchParams.delete;
    const getNameById = async (id: string) => {
        const metadata = await metadataService.getMetadataByRoute(id);
        return metadata.payload?.title;
    };
    const toBeDeletedName = deleteItemId ? await getNameById(deleteItemId) : undefined;

    const favouritesPromise = userService.getFavourites();
    return (
        <main className={layout.main} id="main">
            <div className={`${layout.container} ${styles["local-container"]}`}>
                <div className={navStyles["page-nav"]}>
                    <NavBarAdmin />
                </div>
                <section className={styles["filter-search-favourites-container"]} aria-label="filter search and favourites">
                    <section className={styles["search-container"]} aria-label="search">
                        <FilterAndSearch searchParam={searchParam} tagsPromise={tagsPromise} />
                        {canEditDashboards && (
                            <Button
                                color="secondary"
                                label={text.button["create-metadata"]}
                                url={`/create-metadata`}
                                className={styles["button-add"]}
                                hideLabelTablet
                                iconStart={<AddPlusSimpleIcon color="white" width="11px" height="11px" />}
                            />
                        )}
                    </section>
                    <section className={styles["favourites-container"]} aria-label="favourites">
                        <FavouritesContainer />
                    </section>
                </section>
                <section className={styles["data-container"]} aria-label="data">
                    <Suspense key={searchParams.fav} fallback={<Loading />}>
                        <DataContainer
                            dataPromise={metadataPromise as Promise<IMetadata[]>}
                            favouritesPromise={favouritesPromise as Promise<IFavourites>}
                            canEditDashboards={canEditDashboards}
                            fav={!!searchParams.fav}
                        />
                    </Suspense>
                </section>
                <DeleteDialog toBeDeletedName={toBeDeletedName} />
            </div>
        </main>
    );
};

export default Home;
