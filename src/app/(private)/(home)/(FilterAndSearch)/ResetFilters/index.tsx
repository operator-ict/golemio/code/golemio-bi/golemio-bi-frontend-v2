"use client";
import { useRouter, useSearchParams } from "next/navigation";
import React from "react";

import Button from "@/components/Button";
import { ClearFilterIcon } from "@/icons/ClearFilterIcon";
import text from "@/textContent/cs.json";

import styles from "./ResetFilters.module.scss";

export const ResetFilters = () => {
    const router = useRouter();
    const searchParams = useSearchParams();
    const show = searchParams.get("tag") || searchParams.get("type") || searchParams.get("search");

    const handleClearFilters = () => {
        router.push(`?`);
    };

    return (
        <>
            {show ? (
                <Button
                    color="secondary"
                    hideLabelTablet
                    label={text.filters.clearFilters}
                    className={styles["clear-button"]}
                    onClick={handleClearFilters}
                    iconStart={<ClearFilterIcon color="white" />}
                />
            ) : (
                false
            )}
        </>
    );
};
