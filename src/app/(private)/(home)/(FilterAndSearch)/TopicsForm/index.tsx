"use client";
import { useRouter, useSearchParams } from "next/navigation";
import { usePlausible } from "next-plausible";
import React, { useState } from "react";

import { CheckBox } from "@/(components)/CheckBox";
import { Dropdown } from "@/(components)/Dropdown";
import { ITag } from "@/(types)/TagType";
import Button from "@/components/Button";
import { FilterTopicsIcon } from "@/components/icons/FilterTopicsIcon";
import text from "@/textContent/cs.json";

import styles from "./TopicsForm.module.scss";

type TopicsFormProps = {
    tags?: ITag[];
};

export const TopicsForm = ({ tags }: TopicsFormProps) => {
    const router = useRouter();
    const searchParams = useSearchParams();

    const tagParam = searchParams.get("tag");

    const tagParams = tagParam?.split(",");
    const tagsCount = tagParams?.length;
    const plausible = usePlausible();

    const [visible, setVisible] = useState(false);

    const handleTagCheckbox = (tag: string) => {
        if (tagParams?.includes(tag)) {
            const newTagParam = tagParams.filter((tagParam) => tagParam !== tag).join(",");
            router.push(`?tag=${newTagParam}`);
            return;
        } else {
            const newTagParam = tagParams && tagParams[0] !== "" ? `${tagParams},${tag}` : tag;
            plausible("trackСhoosingTopic", {
                props: {
                    tag: newTagParam,
                },
            });
            router.push(`?tag=${newTagParam}`);
            return;
        }
    };

    const content = (
        <ul className={styles["topics-list"]}>
            {tags?.map((tag) => (
                <CheckBox
                    key={tag._id}
                    id={tag._id}
                    name={tag.title}
                    label={tag.title}
                    checked={tagParam?.includes(tag.title)}
                    onClick={() => handleTagCheckbox(tag.title)}
                />
            ))}
        </ul>
    );

    const handleDropdown = () => {
        setVisible(!visible);
    };

    return (
        <Dropdown contents={content} visible={visible} hide={handleDropdown} className={styles["topics"]}>
            <Button
                color="tertiary"
                label={text.filters.topics}
                iconStart={<FilterTopicsIcon color="gray-darker" />}
                onClick={handleDropdown}
                className={`${tagParam ? styles["filters-on"] : ""}`}
            >
                <span className={styles["count-area"]}>{`${tagParam ? `(${tagsCount})` : `   `}`}</span>
            </Button>
        </Dropdown>
    );
};
