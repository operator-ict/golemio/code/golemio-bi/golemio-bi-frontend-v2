import React, { use } from "react";

import { SearchForm } from "@/components/SearchForm";
import text from "@/textContent/cs.json";
import { ITag } from "@/types/TagType";

import { ResetFilters } from "../ResetFilters";
import { TopicsForm } from "../TopicsForm";
import styles from "./FilterAndSearch.module.scss";

type FilterAndSearchProps = {
    searchParam?: string;
    tagsPromise: Promise<ITag[]>;
};

export const FilterAndSearch = ({ searchParam, tagsPromise }: FilterAndSearchProps) => {
    const tags = use(tagsPromise);
    return (
        <div className={styles["filter-search-container"]}>
            <TopicsForm tags={tags} />
            <SearchForm search={searchParam} placeholder={text.search.placeholderShort} className={styles["search"]} />
            <ResetFilters />
        </div>
    );
};
