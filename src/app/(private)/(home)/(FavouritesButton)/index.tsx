import React, { useState } from "react";

import { updateFavourites } from "@/actions/formDataActions";
import Button from "@/components/Button";
import { AddPlusSimpleIcon } from "@/components/icons/AddPlusSimpleIcon";
import { NotificationType } from "@/components/NotificationBanner";
import { StarOutlineIcon } from "@/icons/StarOutlineIcon";
import { showToast } from "@/utils/showToast";

type Props = {
    isFavourite?: boolean;
    id: string;
    isDetail?: boolean;
};
export const FavouritesButton = ({ isFavourite = false, id, isDetail }: Props) => {
    const [localFavourite, setLocalFavourite] = useState(isFavourite);
    const [isLoading, setIsLoading] = useState(false);

    const getButtonColor = () => {
        if (!isDetail) return "link";
        if (localFavourite) return "tertiary";
        return "tertiary";
    };

    const getIconStart = () => {
        if (!isDetail) {
            return (
                <StarOutlineIcon
                    width="1rem"
                    height="1rem"
                    color={localFavourite ? "yellow" : "gray-dark"}
                    fill={localFavourite ? "#E8D000" : ""}
                />
            );
        }

        if (localFavourite) {
            return <AddPlusSimpleIcon color="secondary" width="14px" height="14px" style={{ transform: "rotate(45deg)" }} />;
        }

        return <StarOutlineIcon width="1rem" height="1rem" color="gray-dark" />;
    };

    const handleFavouriteToggle = async () => {
        setLocalFavourite(!localFavourite);
        setIsLoading(true);
        try {
            const res = await updateFavourites(id);
            if (!res || !res.success) {
                throw new Error(res?.error ? res.error : JSON.stringify(res));
            }
        } catch (error) {
            setLocalFavourite(localFavourite);
            showToast("error" as NotificationType, `Chyba při přidávání do oblíbených: ${error}`);
        } finally {
            setIsLoading(false);
        }
    };

    const buttonColor = getButtonColor();
    const buttonLabel = localFavourite ? "Оdebrat z oblíbených" : "Přidat do oblíbených";
    const iconStart = getIconStart();

    return (
        <Button
            color={buttonColor}
            type="button"
            label={buttonLabel}
            hideLabel={!isDetail}
            square={!isDetail}
            iconStart={iconStart}
            onClick={handleFavouriteToggle}
            disabled={isLoading}
        />
    );
};
