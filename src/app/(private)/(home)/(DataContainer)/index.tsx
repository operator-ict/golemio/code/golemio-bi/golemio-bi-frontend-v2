import { use } from "react";
import { config } from "src/config";

import { DataCard } from "@/components/DataCard";
import { IFavourites } from "@/services/user-service";
import text from "@/textContent/cs.json";
import { IMetadata } from "@/types/MetadataType";

import { EmptyResultPrompt } from "../(EmptyResultPrompt)";

type DataContainerProps = {
    dataPromise: Promise<IMetadata[]>;
    favouritesPromise: Promise<IFavourites>;
    canEditDashboards?: boolean;
    fav: boolean;
};
export const dynamic = "force-dynamic";

export const DataContainer = ({ dataPromise, favouritesPromise, canEditDashboards, fav }: DataContainerProps) => {
    const apiUrl = config.NEXT_PUBLIC_API_URL;
    const metadata = use(dataPromise);
    const haveData = metadata && metadata.length > 0;

    const favouriteData = use(favouritesPromise);
    const favouriteTiles = favouriteData ? favouriteData?.favouriteTiles : [];
    const favourites = metadata?.filter((item) => favouriteTiles.includes(item._id));
    const haveFavourites = favourites && favourites.length > 0;

    const renderDataCard = (item: IMetadata) => (
        <DataCard
            key={item._id}
            metadata={item}
            isFavourite={favouriteTiles.includes(item._id)}
            imagePath={item.thumbnail?.url ? `${apiUrl}${item?.thumbnail?.url}` : "/assets/images/placeholder.webp"}
            canEditDashboards={canEditDashboards}
        />
    );

    if (fav) {
        if (!haveData || (haveData && !haveFavourites)) {
            return <EmptyResultPrompt title={text.emptySearch.emptyTitle} paragraph_1={text.emptySearch.paragraph_1} />;
        } else if (!haveFavourites) {
            return (
                <EmptyResultPrompt
                    title={text.favouritesWidgetEmpty.emptyTitle}
                    paragraph_1={text.favouritesWidgetEmpty.paragraph_1}
                    paragraph_2={text.favouritesWidgetEmpty.paragraph_2}
                />
            );
        } else {
            return favourites?.map(renderDataCard);
        }
    } else {
        if (!haveData) {
            return <EmptyResultPrompt title={text.emptySearch.emptyTitle} paragraph_1={text.emptySearch.paragraph_1} />;
        } else {
            return metadata?.map(renderDataCard);
        }
    }
};
