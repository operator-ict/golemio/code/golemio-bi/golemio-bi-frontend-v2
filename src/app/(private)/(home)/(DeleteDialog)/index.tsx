"use client";
import { useRouter, useSearchParams } from "next/navigation";
import React from "react";

import { deleteMetadata } from "@/actions/formDataActions";
import Button from "@/components/Button";
import { Modal } from "@/components/Modal";
import { NotificationType } from "@/components/NotificationBanner";
import { Paragraph } from "@/components/Paragraph";
import { Title } from "@/components/Title";
import { useRemoveParam } from "@/hooks/useRemoveParam";
import { showToast } from "@/utils/showToast";

import styles from "./DeleteDialog.module.scss";

export const DeleteDialog = ({ toBeDeletedName }: { toBeDeletedName?: string }) => {
    const router = useRouter();
    const deleteItemId = useSearchParams().get("delete");

    const removeDelQuery = useRemoveParam("delete");

    return (
        <Modal show={!!deleteItemId} label="Potvrzení smazání" onClose={() => router.push(`?${removeDelQuery}`)}>
            <form
                action={async () => {
                    const res = deleteItemId && (await deleteMetadata(deleteItemId));
                    if (res && res.success) {
                        showToast("info" as NotificationType, "Dlaždice byla úspěšně smazána");
                    } else {
                        showToast("error" as NotificationType, `Chyba při mazání dlaždice: ${res && res.error}`);
                        return;
                    }
                    router.push(`?${removeDelQuery}`);
                }}
                className={styles["modal-form"]}
            >
                <Paragraph type="p1">Opravdu chcete smazat dlaždici:</Paragraph>
                <Title type="t1" className={styles["modal-form-title"]}>
                    {toBeDeletedName}
                </Title>
                <Paragraph type="p1" className={styles["modal-form-warning"]}>
                    Tato akce je nevratná!
                </Paragraph>
                <Button color="secondary" type="submit" className={styles["modal-form-button"]}>
                    {"Smazat metadata"}
                </Button>
            </form>
        </Modal>
    );
};
