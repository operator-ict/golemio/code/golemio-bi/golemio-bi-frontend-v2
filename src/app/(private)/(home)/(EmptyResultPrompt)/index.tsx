import React from "react";

import { Paragraph } from "@/components/Paragraph";
import { Title } from "@/components/Title";
import { FavouritesWidgets } from "@/icons/FavouritesWidgets";

import styles from "./EmptyResultPrompt.module.scss";

type Props = {
    title: string;
    paragraph_1: string;
    paragraph_2?: string;
};

export const EmptyResultPrompt = ({ title, paragraph_1, paragraph_2 }: Props) => {
    return (
        <div className={styles["empty-container"]}>
            <div className={styles["empty-image-container"]}>
                <FavouritesWidgets />
            </div>
            <Title type="t3m">{title}</Title>
            <Paragraph type={"p3"}>{paragraph_1}</Paragraph>
            <Paragraph type={"p3"}>{paragraph_2}</Paragraph>
        </div>
    );
};
