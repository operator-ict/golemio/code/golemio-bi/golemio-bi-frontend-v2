import React, { useEffect, useMemo, useState } from "react";

import Button from "@/components/Button";
import { Dropdown } from "@/components/Dropdown";
import { ChevronIcon } from "@/components/icons/ChevronIcon";
import { GroupIcon } from "@/components/icons/GroupIcon";

import styles from "./SelectGroup.module.scss";

type SelectGroupFormProps = {
    label?: string;
    onSelected: (value: string) => void;
    value: string;
    disabled?: boolean;
};

const groupOptions = [
    { value: "admin", label: "Admin" },
    { value: "datar", label: "Datař" },
    { value: "default_user", label: "Uživatel" },
    { value: "garant", label: "Garant" },
];

export const SelectGroupForm = ({ label, onSelected, value, disabled }: SelectGroupFormProps) => {
    const [visible, setVisible] = useState(false);
    const [selectedOption, setSelectedOption] = useState<string>(value);

    useEffect(() => {
        if (selectedOption !== value) {
            setSelectedOption(value);
        }
    }, [value, selectedOption]);

    const handleToggleDropdown = () => setVisible((prev) => !prev);

    const handleOptionClick = (id: string) => {
        setSelectedOption(id);
        onSelected(id);
        setVisible(false);
    };

    const sortedOptions = useMemo(() => {
        return [
            ...groupOptions.filter((option) => option.value === selectedOption),
            ...groupOptions.filter((option) => option.value !== selectedOption),
        ];
    }, [selectedOption]);

    const selectedLabel = useMemo(
        () => groupOptions.find((option) => option.value === selectedOption)?.label || "Zadejte oprávnění",
        [groupOptions, selectedOption]
    );

    return (
        <div className={styles["select-container"]}>
            <Dropdown
                hide={handleToggleDropdown}
                className={styles["select-container__dropdown"]}
                contents={
                    <div className={styles["select-container__list"]}>
                        <ul className={styles["select-container__options"]} role="listbox">
                            {sortedOptions.map((option) => (
                                <li
                                    key={option.value}
                                    id={option.value}
                                    className={`${styles["select-container__option"]} ${
                                        option.value === selectedOption ? styles["bla"] : ""
                                    }`}
                                    onClick={() => handleOptionClick(option.value)}
                                    role="option"
                                    aria-selected={option.value === selectedOption}
                                >
                                    {option.label}
                                </li>
                            ))}
                        </ul>
                    </div>
                }
                visible={visible}
                dynamicWidth
            >
                <span className={styles["select-dropdown-label"]}>{label}</span>
                <Button
                    color="outline"
                    label={selectedLabel}
                    iconStart={<GroupIcon color="black-gray" />}
                    iconEnd={<ChevronIcon color="gray-middle" direction={visible ? "up" : "down"} />}
                    onClick={handleToggleDropdown}
                    className={`${styles["dropdown-button"]} ${value && styles["dropdown-button__with-value"]}`}
                    aria-haspopup="listbox"
                    aria-expanded={visible}
                    disabled={disabled}
                />
            </Dropdown>
        </div>
    );
};
