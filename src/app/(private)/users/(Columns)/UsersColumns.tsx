"use client";

import { ColumnDef, Row } from "@tanstack/react-table";

import ManualSortedButton from "@/components/SortedButton/ManualSortedButton";
import { ActionIcon } from "@/icons/ActionIcon";
import styles from "@/styles/AdminPages.module.scss";
import { User } from "@/types/Users";

import { UsersRolesList } from "../(UsersRolesList)";
import UsersActionsCell from "./UsersActionsCell";

const formatDate = (dateString: string) => {
    if (!dateString) return "";
    const date = new Date(dateString);
    return new Intl.DateTimeFormat("cs-CZ", {
        day: "2-digit",
        month: "2-digit",
        year: "numeric",
        hour: "2-digit",
        minute: "2-digit",
    }).format(date);
};

const collator = new Intl.Collator("cs", { sensitivity: "base" });

const localeStringSort = (rowA: Row<User>, rowB: Row<User>, columnId: string) => {
    const a: string = rowA.getValue(columnId);
    const b: string = rowB.getValue(columnId);
    if (a === null && b === null) return 0;
    if (a === null) return 1;
    if (b === null) return -1;
    return collator.compare(a, b);
};

export const UsersColumns: ColumnDef<User>[] = [
    {
        accessorKey: "name",
        header: () => <ManualSortedButton columnName={"name"} title={"Jméno"} />,
        size: 128,
        sortingFn: localeStringSort,
    },
    {
        accessorKey: "surname",
        header: () => <ManualSortedButton columnName={"surname"} title={"Příjmení"} />,
        size: 132,
        sortingFn: localeStringSort,
    },
    {
        accessorKey: "organization",
        size: 330,
        header: () => <ManualSortedButton columnName={"organization"} title={"Organizace"} />,
        id: "organization",
    },
    {
        accessorKey: "email",
        size: 330,
        header: () => <ManualSortedButton columnName={"email"} title={"Email"} />,
        id: "email",
    },

    {
        accessorKey: "created_at",
        header: () => <ManualSortedButton columnName={"created_at"} title={"Vytvořen"} />,
        cell: ({ row }) => {
            return <span>{formatDate(row.original.created_at)}</span>;
        },
        size: 130,
    },
    {
        accessorKey: "invitation_accepted_at",
        header: () => <ManualSortedButton columnName={"invitation_accepted_at"} title={"Aktivován"} />,
        cell: ({ row }) => {
            return <span>{formatDate(row.original.invitation_accepted_at)}</span>;
        },
        size: 130,
    },
    {
        accessorKey: "last_activity",
        header: () => <ManualSortedButton columnName={"last_activity"} title={"Poslední aktivita"} />,
        cell: ({ row }) => {
            return <span>{formatDate(row.original.last_activity)}</span>;
        },
        size: 130,
    },
    {
        accessorKey: "roleIds",
        header: "Počet rolí",
        cell: ({ row }) => {
            const roleIds = row.original.roleIds || [];
            return roleIds.length > 0 ? (
                <UsersRolesList userId={row.original.id.toString()}>{roleIds.length}</UsersRolesList>
            ) : (
                <span>0</span>
            );
        },
        size: 135,
    },

    {
        accessorKey: "actions",
        header: () => (
            <span className={styles["actions-header"]}>
                <ActionIcon color={"primary"} />
                Akce
            </span>
        ),
        size: 180,
        cell: ({ row, table }) => <UsersActionsCell row={row} currentUserId={table.options.meta?.currentUserID} />,
    },
];
