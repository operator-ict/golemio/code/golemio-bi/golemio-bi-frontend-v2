import { Row } from "@tanstack/react-table";
import { FC, useState, useTransition } from "react";

import { getUserRoles } from "@/actions/formUserActions";
import Button from "@/components/Button";
import { usePermissions } from "@/context/PermissionsContext";
import { AddRoleIcon } from "@/icons/AddRoleIcon";
import { EditPenIcon } from "@/icons/EditPenIcon";
import styles from "@/styles/AdminPages.module.scss";
import { User, UserRole } from "@/types/Users";

import { EditUserDialog } from "../(EditUser)/EditUserDialog";
interface UserActionsProps {
    row: Row<User>;
    currentUserId: string | undefined;
}
const UsersActionsCell: FC<UserActionsProps> = ({ row, currentUserId }) => {
    const canEditUsersRoles = usePermissions().canEditUsersRole;
    const canEditUsersRights = usePermissions().canEditUsersRights;

    const [isDialogOpen, setDialogOpen] = useState(false);
    const [allRoles, setAllRoles] = useState<UserRole[]>();
    const [userGroup, setUserGroup] = useState<string>();
    const [isPending, startTransition] = useTransition();

    const handleOpenModal = async () => {
        startTransition(async () => {
            try {
                const res = await getUserRoles(row.original.id.toString());
                setAllRoles(res !== null ? res.roles : []);
                setUserGroup(res !== null ? res.group_name : "");
                setDialogOpen(true);
            } catch (error) {
                console.error("Error fetching role data:", error);
            }
        });
    };

    const handleDialogClose = () => {
        setDialogOpen(false);
    };

    return (
        <div className={styles["button-container"]}>
            {canEditUsersRoles && (
                <Button
                    label={canEditUsersRights ? "Upravit" : "Přiřadit roli"}
                    size="sm"
                    color="secondary"
                    iconStart={
                        canEditUsersRights ? (
                            <EditPenIcon color="white" width="11px" height="11px" />
                        ) : (
                            <AddRoleIcon color="white" />
                        )
                    }
                    onClick={handleOpenModal}
                    disabled={isPending}
                />
            )}
            {isDialogOpen && (
                <EditUserDialog
                    show={isDialogOpen}
                    onClose={handleDialogClose}
                    roles={allRoles}
                    name={row.original.name}
                    surname={row.original.surname}
                    initialData={{
                        id: row.original.id.toString(),
                        roleIds: row.original.roleIds,
                        userGroup: userGroup,
                        currentUserID: currentUserId,
                    }}
                />
            )}
        </div>
    );
};

export default UsersActionsCell;
