import { NavBarAdmin } from "@/components/NavBarAdmin";
import { SearchForm } from "@/components/SearchForm";
import { ServerPaginationTable } from "@/components/TableComponent/server-pagination-table";
import { TokenService } from "@/services/token-service";
import { usersService } from "@/services/users-service";
import styles from "@/styles/AdminPages.module.scss";
import text from "@/textContent/cs.json";
import { User } from "@/types/Users";

import { UsersColumns } from "./(Columns)/UsersColumns";

interface UsersPageProps {
    searchParams: {
        page: string;
        limit: string;
        orderDir: "asc" | "desc";
        orderBy: string;
        search: string;
    };
}

export const revalidate = 0;

const UsersPage = async ({ searchParams }: UsersPageProps) => {
    const page = searchParams.page ? Number(searchParams.page) : 1;
    const limit = searchParams.limit ? Number(searchParams.limit) : 10;

    const [userId, users] = await Promise.all([TokenService.getUserId(), usersService.getUsers()]);

    const searchParam = searchParams.search;
    const orderDir = searchParams.orderDir ?? "asc";
    const orderBy = searchParams.orderBy ?? "label";

    searchParams.limit = limit.toString();
    searchParams.page = page.toString();
    searchParams.orderDir = orderDir;
    searchParams.orderBy = orderBy;
    const processUserData = (
        users: User[],
        orderDir: "asc" | "desc",
        orderBy: string,
        searchParam: string,
        page: number,
        limit: number
    ) => {
        const filteredData = searchParam
            ? users.filter((user) =>
                  Object.values(user).some((value) => String(value).toLowerCase().includes(searchParam.toLowerCase()))
              )
            : users;
        const sortedData = filteredData.sort((a, b) => {
            const aValue = a[orderBy as keyof User] ?? "";
            const bValue = b[orderBy as keyof User] ?? "";

            if (orderDir === "asc") {
                return aValue > bValue ? 1 : -1;
            }
            return aValue < bValue ? 1 : -1;
        });

        const paginatedData = sortedData.slice((page - 1) * limit, page * limit);

        return {
            paginatedData,
            totalCount: filteredData.length,
        };
    };
    const { paginatedData, totalCount } = processUserData(users, orderDir, orderBy, searchParam, page, limit);
    const pageCount = Math.ceil(totalCount / +limit);

    return (
        <main className={styles["admin-page-container"]}>
            <div className={styles["page-nav"]}>
                <NavBarAdmin />
            </div>
            <div className={styles["data-container"]}>
                <div className={styles["data-container-header"]}>
                    <h1>{text.usersPage.title}</h1>
                    <SearchForm
                        search={searchParam}
                        placeholder={text.search.placeholderUsersPage}
                        className={styles["data-container-header-search"]}
                    />
                </div>
                <ServerPaginationTable
                    columns={UsersColumns}
                    data={paginatedData as User[]}
                    totalCount={totalCount}
                    limit={limit}
                    page={page}
                    pageCount={pageCount}
                    currentUserID={userId}
                />
            </div>
        </main>
    );
};

export default UsersPage;
