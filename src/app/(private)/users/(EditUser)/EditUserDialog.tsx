import { useRouter } from "next/navigation";
import React, { useMemo, useState, useTransition } from "react";

import CustomSelectForm from "@/(private)/roles/(CustomSelectForm)";
import { createUserGroup, updateUserRoles } from "@/actions/formUserActions";
import Button from "@/components/Button";
import { RolesIcon } from "@/components/icons/RolesIcon";
import { UsersIcon } from "@/components/icons/UsersIcon";
import { Modal } from "@/components/Modal";
import { NotificationType } from "@/components/NotificationBanner";
import TextField from "@/components/TextField";
import { usePermissions } from "@/context/PermissionsContext";
import SubmitIcon from "@/icons/formIcons/SubmitIcon";
import { UserRole } from "@/types/Users";
import { EntityType } from "@/utils/CountLabel";
import { showToast } from "@/utils/showToast";

import { SelectGroupForm } from "../(SelectGroup)";
import styles from "./EditUserDialog.module.scss";

type EditUserModalProps = {
    show: boolean;
    onClose: () => void;
    name: string;
    surname: string;
    roles?: UserRole[];
    initialData: {
        id: string;
        roleIds: string[] | undefined;
        userGroup: string | undefined;
        currentUserID?: string;
    };
};

export const EditUserDialog: React.FC<EditUserModalProps> = ({ show, onClose, roles, name, surname, initialData }) => {
    const canEditUsersRights = usePermissions().canEditUsersRights;

    const [isPending, startTransition] = useTransition();
    const router = useRouter();
    const [formState, setFormState] = useState({
        roleIds: initialData?.roleIds || [],
        id: initialData.id,
        groupName: initialData?.userGroup,
    });

    const componentOptions = useMemo(() => roles?.map((item) => ({ value: item.id, label: item.label })) || [], [roles]);

    const isFormUnchanged = useMemo(() => {
        const initial = {
            roleIds: initialData?.roleIds || [],
            id: initialData.id,
            groupName: initialData?.userGroup,
        };
        return JSON.stringify(initial) === JSON.stringify(formState);
    }, [initialData, formState]);

    const handleFieldChange = (field: keyof typeof formState, value: string | string[]) => {
        setFormState((prev) => ({ ...prev, [field]: value }));
    };

    const isEditingSelf = initialData.currentUserID === formState.id;
    const isEditingGroup = formState.groupName !== initialData.userGroup;

    const handleSubmit = async () => {
        startTransition(async () => {
            try {
                const res = await updateUserRoles(formState.id, formState.roleIds);
                if (!res.success) {
                    showToast(NotificationType.Error, `Aktualizace rolí uživatele selhala, chyba: ${res.error}.`);
                    throw new Error(res.error || "Unknown error during role update");
                }

                if (canEditUsersRights && !isEditingSelf && isEditingGroup) {
                    const groupRes = await createUserGroup(formState.id, {
                        groupName: formState.groupName as string,
                    });

                    if (groupRes.error) {
                        showToast(NotificationType.Error, `Aktualizace práva uživatele selhala, chyba: ${groupRes.error}.`);
                        throw new Error(groupRes.error);
                    }
                }

                showToast(
                    NotificationType.Info,
                    `Role ${canEditUsersRights ? "a práva " : ""}uživatele byly úspěšně aktualizovány.`
                );
                onClose();
                router.refresh();
            } catch (error) {
                console.error("Error updating user role:", error);
            }
        });
    };
    return (
        <Modal show={show} label={canEditUsersRights ? "Upravit uživatele" : "Přiřadit roli"} onClose={onClose}>
            <form className={styles["modal-form"]} onSubmit={(e) => e.preventDefault()}>
                <div className={styles["modal-form__fields"]}>
                    <div className={styles["modal-form__fields-text"]}>
                        <TextField
                            id="fullName"
                            label="Jméno"
                            value={name || ""}
                            placeholder="Jméno"
                            disabled
                            iconStart={<UsersIcon color="gray-dark" />}
                        />
                        <TextField
                            id="surname"
                            label="Příjmení"
                            value={surname || ""}
                            placeholder="Příjmení"
                            disabled
                            iconStart={<UsersIcon color="gray-dark" />}
                        />
                    </div>
                    {canEditUsersRights && (
                        <SelectGroupForm
                            label="Práva"
                            value={formState.groupName || ""}
                            onSelected={(value) => handleFieldChange("groupName", value)}
                            disabled={!canEditUsersRights || isEditingSelf}
                        />
                    )}
                    <CustomSelectForm
                        label="Role"
                        onSelected={(values) => handleFieldChange("roleIds", values)}
                        componentOptions={componentOptions}
                        value={formState.roleIds}
                        entity={EntityType.Role}
                        withCheckboxes
                        selectedCountLabel
                        iconStart={<RolesIcon color="gray-dark" width="24px" height="24px" />}
                    />
                </div>
                <div className={styles["modal-form__buttons"]}>
                    <Button color="secondary" type="button" onClick={onClose}>
                        Zpět
                    </Button>
                    <Button
                        color="primary"
                        type="submit"
                        onClick={handleSubmit}
                        iconStart={<SubmitIcon color="white" width="1rem" height="1rem" />}
                        disabled={isFormUnchanged || isPending}
                    >
                        {isPending ? "Ukládání..." : "Uložit"}
                    </Button>
                </div>
            </form>
        </Modal>
    );
};
