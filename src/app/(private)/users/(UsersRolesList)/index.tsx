"use client";

import { ReactNode, useState, useTransition } from "react";

import { getUserRoles } from "@/actions/formUserActions";
import { UserRole } from "@/types/Users";

import styles from "./UsersRolesList.module.scss";

type UsersRolesListProps = {
    userId: string;
    children: ReactNode;
};

export const UsersRolesList = ({ children, userId }: UsersRolesListProps) => {
    const [isPending, startTransition] = useTransition();
    const [roles, setRoles] = useState<UserRole[]>([]);

    const handleMouseEnter = () => {
        startTransition(async () => {
            const userRoles = await getUserRoles(userId);
            setRoles(userRoles !== null ? userRoles.roles.filter((role) => role.assigned === true) : []);
        });
    };

    const getVisibleRoles = () => {
        const MAX_VISIBLE_ROLES = 5;
        if (roles.length <= MAX_VISIBLE_ROLES) {
            return { visibleRoles: roles, remainingCount: 0 };
        }
        return {
            visibleRoles: roles.slice(0, MAX_VISIBLE_ROLES),
            remainingCount: roles.length - MAX_VISIBLE_ROLES,
        };
    };

    const { visibleRoles, remainingCount } = getVisibleRoles();

    const getRemainingText = (count: number) => (count > 0 ? `+${count} ${count > 4 ? "dalších" : "další"}` : "");

    const tooltipContent = isPending
        ? `<span class="${styles["tooltip-content"]}">Nahrávám...</span>`
        : `<span class="${styles["tooltip-content"]}">
            <span>${visibleRoles.map((role) => role.label).join(", ")}</span>
            <span class="${styles["tooltip-content-rest"]}">${getRemainingText(remainingCount)}</span>
        </span>`;

    return (
        <span
            data-tooltip-id="table-tooltip"
            data-tooltip-html={tooltipContent}
            onMouseEnter={handleMouseEnter}
            className={styles["tooltip-wrapper"]}
        >
            {children}
        </span>
    );
};
