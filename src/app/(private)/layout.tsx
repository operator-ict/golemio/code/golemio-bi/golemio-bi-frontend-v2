import "@/styles/globals.scss";

import { ReactNode } from "react";
import { Toaster } from "react-hot-toast";
import { config } from "src/config";

import { ConditionsDialogStrict } from "@/(public)/auth/(components)/ConditionsDialogStrict";
import { ConditionalFooter, ConditionalHeader } from "@/components/Header/ConditionalHeader";
import NotificationBanner, { NotificationType } from "@/components/NotificationBanner";
import Skiplink from "@/components/Skiplink";
import { AgreementProvider } from "@/context/AgreementContext";
import { PermissionsProvider } from "@/context/PermissionsContext";
import { getLogger } from "@/logging/logger";
import { PermissionsService } from "@/services/permissions-service";
import { UserProfileService } from "@/services/user-profile-service";
import { UserProfileType } from "@/types/UserProfileType";
import { fetchAgreementStatus } from "@/utils/agreementStatus";
import { getErrorMessage } from "@/utils/getErrorMessage";

const logger = getLogger("private-layout");

export const dynamic = "force-dynamic";

const PrivateLayout = async ({ children }: { children: ReactNode }) => {
    let user: UserProfileType | undefined;

    try {
        user = await UserProfileService.getUserProfile();
    } catch (error) {
        logger.error({
            provider: "server",
            function: "user data fetching",
            component: "private-layout",
            message: `Error while fetching user profile: ${getErrorMessage(error)}`,
        });
    }

    const agreementStatus: boolean = await fetchAgreementStatus();
    const [canEditUsersRole, canEditRoles, canEditUsersRights, canViewAdminDashboards] = await Promise.all([
        PermissionsService.getUsersEditRolePermissions(),
        PermissionsService.getRolesEditPermissions(),
        PermissionsService.getUsersEditRightsPermissions(),
        PermissionsService.getDashboardsViewAdminPermissions(),
    ]);

    return (
        <PermissionsProvider permissions={{ canEditUsersRole, canEditRoles, canEditUsersRights, canViewAdminDashboards }}>
            <AgreementProvider initialAgreementStatus={agreementStatus}>
                <Skiplink />
                <ConditionalHeader user={user} />

                {config.NOTIFICATION_BANNER_SEVERITY ? (
                    <NotificationBanner
                        type={config.NOTIFICATION_BANNER_SEVERITY as NotificationType}
                        text={config.NOTIFICATION_BANNER_TEXT}
                    />
                ) : null}
                {children}
                <Toaster
                    position="top-center"
                    containerStyle={{
                        top: "72px",
                        left: 0,
                        right: 0,
                    }}
                />
                <ConditionsDialogStrict />
                <div id="portal" />
                <ConditionalFooter />
            </AgreementProvider>
        </PermissionsProvider>
    );
};

export default PrivateLayout;
