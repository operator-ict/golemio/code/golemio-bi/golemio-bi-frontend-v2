export const dynamic = "force-dynamic"; // defaults to auto
export async function GET() {
    return new Response("Running...", {
        status: 200,
        headers: {
            "content-type": "text/plain",
        },
    });
}
