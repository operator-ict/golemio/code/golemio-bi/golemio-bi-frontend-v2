"use server";

import { ContinueWith, RegistrationFlow, ResponseError, UiNode, UiText } from "@ory/client-fetch";

import { getOryKratosFrontendApi } from "@/api/auth/OryApisProvider";
import { getLogger } from "@/logging/logger";
import { AccessService } from "@/services/access-service";
import { TokenService } from "@/services/token-service";
import { userService } from "@/services/user-service";
import text from "@/textContent/cs.json";
import { AccessDashboardRequest, AccessRequest, AccessRequestResponse, ResponseEntity } from "@/types/formTypes";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { deleteOrySessionCookie, getOrySessionCookie, setOrySessionCookie } from "@/utils/oryCookies";

const logger = getLogger("formAuthActions");

export const checkLoggedIn = async () => {
    const logData = { provider: "server", function: "checkLoggedIn" };

    try {
        const isLoggedIn = await TokenService.getIsLoggedIn();
        if (isLoggedIn) {
            logger.debug({ ...logData, message: "User is logged in" });
            return { success: true };
        } else {
            logger.error({ ...logData, message: "User is not logged in" });
            return { error: "User is not logged in" };
        }
    } catch (error) {
        logger.error({ ...logData, error: getErrorMessage(error) });
        return { error: getErrorMessage(error) };
    }
};

export const forgottenPassReq = async (email: string, flow: string) => {
    const logData = { provider: "server", function: "forgottenPassReq" };

    try {
        const oryFrontendApi = getOryKratosFrontendApi();
        const data = await oryFrontendApi.updateRecoveryFlow({
            flow,
            updateRecoveryFlowBody: {
                method: "code",
                email,
            },
        });

        const uiError = collectRecoveryInfoErrors(data.ui.messages || []);
        if (uiError) {
            return { error: uiError };
        }

        return { success: true };
    } catch (error) {
        if (error instanceof ResponseError) {
            return { error: await handleRecoveryFlowError(logData, error) };
        }

        logger.error({ ...logData, error: getErrorMessage(error) });
        return { error: getErrorMessage(error) };
    }
};

export const forgottenPassCodeReq = async (code: string, flowId: string) => {
    const logData = { provider: "server", function: "forgottenPassCodeReq" };

    try {
        const oryFrontendApi = getOryKratosFrontendApi();
        const data = await oryFrontendApi.updateRecoveryFlow({
            flow: flowId,
            updateRecoveryFlowBody: {
                method: "code",
                code,
            },
        });

        const uiError = collectRecoveryInfoErrors(data.ui.messages || []);
        if (uiError) {
            logger.error({ ...logData, error: uiError });
            return { error: uiError };
        }

        const actions = collectRecoveryInfo(data.continue_with || []);

        setOrySessionCookie(actions.sessionToken);
        logger.debug({ ...logData, message: "Code verified" });
        return { success: true, settingsToken: actions.settingsFlowId };
    } catch (error) {
        if (error instanceof ResponseError) {
            return { error: await handleRecoveryFlowError(logData, error) };
        }

        logger.error({ ...logData, error: getErrorMessage(error) });
        return { error: getErrorMessage(error) };
    }
};

export const verifyResetCodeReq = async (flowId: string) => {
    const logData = { provider: "server", function: "verifyResetCodeReq" };

    try {
        const oryFrontendApi = getOryKratosFrontendApi();
        const flowData = await oryFrontendApi.getRecoveryFlow({ id: flowId });

        if (flowData.state === "passed_challenge") {
            const sessionToken = getOrySessionCookie();

            return { success: true, sessionToken };
        } else {
            logger.error({ ...logData, message: `Reset password session expired.` });
            return { error: text.auth.resetPasswordErrTokenInvalid };
        }
    } catch (error) {
        if (error instanceof ResponseError) {
            return { error: await handleRecoveryFlowError(logData, error) };
        }

        logger.error({ ...logData, error: getErrorMessage(error) });
        return { error: getErrorMessage(error) };
    }
};

export const forgottenPassReset = async (flowId: string, sessionToken: string, password: string) => {
    const logData = { provider: "server", function: "forgottenPassReset" };

    try {
        const oryFrontendApi = getOryKratosFrontendApi();
        await oryFrontendApi.updateSettingsFlow({
            flow: flowId,
            updateSettingsFlowBody: {
                method: "password",
                password,
            },
            xSessionToken: sessionToken,
        });

        deleteOrySessionCookie();

        return { success: true };
    } catch (error) {
        if (error instanceof ResponseError) {
            const errorBody = await error.response.json();
            let messages: Array<UiText> = [];
            if (errorBody.ui?.messages) {
                messages = messages.concat(errorBody.ui.messages);
            }
            if (errorBody.ui?.nodes) {
                messages = messages.concat(errorBody.ui.nodes.map((el: UiNode) => el.messages).flat());
            }

            const uiError = collectResetPasswordErrors(messages);
            if (uiError) {
                return { error: uiError };
            }

            return { error: await handleRecoveryFlowError(logData, error) };
        }

        logger.error({ ...logData, error: getErrorMessage(error) });
        return { error: getErrorMessage(error) };
    }
};

export const accessReqKratos = async (accessRequest: AccessRequest, flowId: string | null) => {
    const logData = {
        provider: "server",
        function: "accessReqKratos",
    };
    logger.debug({ ...logData, message: "Sending access request to Kratos" });
    if (!flowId) {
        return { success: false, error: "Flow was not started." };
    }
    const ory = getOryKratosFrontendApi();
    try {
        const response = await ory.updateRegistrationFlow({
            flow: flowId,
            updateRegistrationFlowBody: {
                method: "password",
                password: accessRequest.password,
                traits: {
                    email: accessRequest.email,
                    firstName: accessRequest.name,
                    lastName: accessRequest.surname,
                    organization: accessRequest.organization,
                },
                transient_payload: {
                    acceptedConditions: accessRequest.conditions_accepted,
                },
            },
        });

        const continueId =
            response.continue_with?.[0]?.action === "show_verification_ui" ? response.continue_with[0].flow.id : undefined;

        logger.debug({
            ...logData,
            continueId,
            data: response,
        });

        return {
            success: true,
            continueId,
        };
    } catch (error) {
        if (error instanceof ResponseError) {
            const body: RegistrationFlow = await error.response.json();
            logger.debug(body);

            if (error.response.status === 400) {
                let messages: Array<UiText> = [];

                if (body.ui?.messages) {
                    messages = messages.concat(body.ui.messages);
                }
                if (body.ui?.nodes) {
                    messages = messages.concat(body.ui.nodes.map((el) => el.messages).flat());
                }
                if (messages.some((el) => el.id === 4000007)) {
                    // account already exists see https://www.ory.sh/docs/kratos/concepts/ui-user-interface
                    // email sent via kratos hook calling /v1/hook/pre-registration
                    logger.warn({ ...logData, data: messages });
                    return { success: true };
                }

                logger.error({
                    ...logData,
                    error: getErrorMessage(error),
                    messages,
                    messageTypes: messages.map((m) => ({ id: m.id, type: m.type, text: m.text })),
                });
                return { error: getErrorMessage(error), status: error.response.status };
            } else {
                // 410, 422 and others
                logger.error({ ...logData, error: getErrorMessage(error) });
                return { error: getErrorMessage(error), status: error.response.status };
            }
        } else {
            logger.error({ ...logData, error: getErrorMessage(error) });
            return { error: getErrorMessage(error) };
        }
    }
};

export const accessDashboardReq = async (accessDashboardRequest: AccessDashboardRequest) => {
    const logData = {
        provider: "server",
        function: "accessDashboardReq",
    };
    try {
        const res = await AccessService.accessDashboardRequest(JSON.stringify(accessDashboardRequest));
        if (res.status === 200 && (res.payload as AccessRequestResponse).isOk) {
            logger.debug({
                ...logData,
                message: `Dashboard access request for user: ${accessDashboardRequest.userId} was successful`,
            });
            return { success: true };
        } else {
            const errors = JSON.stringify((res.payload as AccessRequestResponse).errors);
            logger.error({
                ...logData,
                message:
                    `Dashboard access request for user: ${accessDashboardRequest.userId} failed, ` +
                    `response error ${errors} | status: ${res?.status}`,
                errors: errors,
            });
            return { error: errors, status: res?.status };
        }
    } catch (error) {
        logger.error({
            ...logData,
            error: getErrorMessage(error),
        });
        return { error: getErrorMessage(error) };
    }
};

export const getUserAgreementStatus = async () => {
    const logData = {
        provider: "server",
        function: "getUserAgreementStatus",
    };
    try {
        const res = await userService.getUserAgreementStatus();
        if (res.status === 200 && typeof res.payload === "object" && "acceptedConditions" in res.payload) {
            logger.debug({
                ...logData,
                message: `Agreement status fetched, status value: ${res.payload.acceptedConditions}`,
            });
            return { success: true, "agreement-status": res.payload.acceptedConditions };
        } else {
            logger.error({
                ...logData,
                message: `Failed to fetch agreement status, response error: ${res.error} | status: ${res.status}`,
            });
            return { error: res.error };
        }
    } catch (error) {
        logger.error({ ...logData, error: getErrorMessage(error) });
        return { error: getErrorMessage(error) };
    }
};

export const acceptConditions = async () => {
    const logData = {
        provider: "server",
        function: "acceptConditions",
    };
    try {
        const res = (await userService.updateUserAgreementStatus()) as ResponseEntity<"">;
        if (res.status === 204) {
            logger.debug({
                ...logData,
                message: `Agreement status updated | response status: ${res.status}`,
            });
            return { success: true };
        } else {
            logger.error({
                ...logData,
                message: `Failed to update agreement status, response error: ${res.error} | status: ${res.status}`,
            });
            return { error: res.error };
        }
    } catch (error) {
        logger.error({ ...logData, error: getErrorMessage(error) });
        return { error: getErrorMessage(error) };
    }
};

export const verifyEmail = async (
    code: string,
    flow: string,
    refreshFlow: boolean = true
): Promise<{ success: boolean; error?: string }> => {
    const service = getOryKratosFrontendApi();
    const logData = { provider: "server", function: "verifyEmail" };
    try {
        const data = await service.updateVerificationFlow(
            {
                flow: flow,
                updateVerificationFlowBody: {
                    method: "code",
                    code,
                },
            },
            {
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
            } as RequestInit
        );
        logger.debug({ ...logData, data });

        const uiError = collectVerifyInfoErrors(data.ui.messages || []);
        if (uiError) {
            return {
                success: false,
                error: uiError,
            };
        }

        return {
            success: data.ui.messages?.some((el) => el.id === 1080002) ?? false,
        };
    } catch (error) {
        if (error instanceof ResponseError) {
            const resolution = await handleVerifyFlowError(logData, error);
            if (resolution.newFlowId && refreshFlow) {
                return verifyEmail(code, resolution.newFlowId, false);
            }

            return { success: false, error: resolution.message };
        }

        logger.error({ ...logData, error: getErrorMessage(error) });
        return { success: false, error: getErrorMessage(error) };
    }
};

const collectRecoveryInfo = (items: ContinueWith[]): { settingsFlowId: string; sessionToken: string } => {
    return items?.reduce(
        (acc, curr) => {
            switch (curr.action) {
                case "show_settings_ui":
                    return { ...acc, settingsFlowId: curr.flow.id };

                case "set_ory_session_token": {
                    return { ...acc, sessionToken: curr.ory_session_token };
                }
            }
            return acc;
        },
        { settingsFlowId: "", sessionToken: "" }
    );
};

const collectRecoveryInfoErrors = (uiMessage: Array<UiText>) => {
    const uiError = uiMessage.find((el) => el.type === "error");
    if (!uiError) return;

    switch (uiError?.id) {
        case 4060001:
            return text.auth.resetPasswordErrAlreadyUsed;
        case 4060002:
        case 4060005:
        case 4060004:
            return text.auth.resetPasswordErrTokenInvalid;
        case 4060006:
            return text.auth.resetPasswordErrCodeInvalid;
        default:
            return uiError?.text || "";
    }
};

const collectResetPasswordErrors = (uiMessage: Array<UiText>) => {
    const uiError = uiMessage.find((el) => el.type === "error");
    if (!uiError) return;

    switch (uiError?.id) {
        case 4000031:
        case 4000034:
            return text.auth.resetPasswordErrPassNotAllowed;
        case 4000032:
            return text.auth.resetPasswordErrPassLengthMin + `: ${(uiError.context as Record<string, string>)?.min_length}.`;
        case 4000033:
            return text.auth.resetPasswordErrPassLengthMax + `: ${(uiError.context as Record<string, string>)?.max_length}.`;
        default:
            return uiError?.text || "";
    }
};

const handleRecoveryFlowError = async (logData: Record<string, string>, err: ResponseError) => {
    const errorObject = await err.response.json();
    logger.error({ ...logData, error: errorObject.error?.reason });

    switch (err.response.status) {
        case 400:
        case 401:
        case 404:
        case 410:
            return text.auth.resetPasswordErrTokenInvalid;
        default:
            // TODO nahradi za českou general message
            return errorObject.error.reason ? errorObject.error.reason : errorObject.error.message;
    }
};

const handleVerifyFlowError = async (logData: Record<string, string>, err: ResponseError) => {
    const errorObject = await err.response.json();
    logger.error({ ...logData, error: errorObject.error?.reason });

    if (errorObject?.error.id === "self_service_flow_replaced" && errorObject.use_flow_id) {
        return { newFlowId: errorObject.use_flow_id };
    }

    switch (err.response.status) {
        case 400:
        case 401:
        case 404:
        case 410:
            return { message: text.auth.verifyFailureRetryLater };
        default:
            // TODO nahradi za českou general message
            return { message: errorObject.error.reason ? errorObject.error.reason : errorObject.error.message };
    }
};

const collectVerifyInfoErrors = (uiMessage: Array<UiText>) => {
    const uiError = uiMessage.find((el) => el.type === "error");
    if (!uiError) return;

    switch (uiError?.id) {
        case 4070001:
        case 4070006:
            return text.auth.verifyInvalidOrUsedToken;
        case 4070002:
            return text.auth.verifyAlreadyCompleted;
        case 4070003:
            return text.auth.verifyFailureRetryLater;
        case 4070005:
            return text.auth.verifyExpired;
        default:
            return uiError?.text || "";
    }
};
