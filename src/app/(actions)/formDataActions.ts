"use server";
import { revalidatePath } from "next/cache";

import { userService } from "@/(services)/user-service";
import { getLogger } from "@/logging/logger";
import { metadataService } from "@/services/metadata-service";
import { thumbnailService } from "@/services/thumbnail-service";
import { CreateMetadataResponse } from "@/types/formTypes";
import { IMetadataCreate } from "@/types/MetadataType";
import { getErrorMessage } from "@/utils/getErrorMessage";

const logger = getLogger("formActions");

export const createMetaData = async (body: IMetadataCreate) => {
    const logData = {
        provider: "server",
        function: "createMetaData",
    };
    try {
        const res = await metadataService.createMetadata<CreateMetadataResponse>(JSON.stringify(body));
        if (res.status === 201) {
            logger.debug({
                ...logData,
                message: `Metadata created | response status ${res.status}`,
            });
            revalidatePath("/create-metadata", "layout");
            return { success: true, id: (res.payload as CreateMetadataResponse).id };
        } else {
            logger.error({
                ...logData,
                message: `Metadata creation failed, response error ${res.error} | status ${res.status}`,
            });
            return { error: res.error };
        }
    } catch (error) {
        logger.error({ ...logData, error: getErrorMessage(error) });
        return { error: getErrorMessage(error) };
    }
};

export const updateMetadata = async (id: string, body: IMetadataCreate) => {
    const logData = {
        provider: "server",
        function: "updateMetadata",
    };
    try {
        const res = await metadataService.updateMetadata(id, JSON.stringify(body));
        if (res.status === 204) {
            logger.debug({
                ...logData,
                message: `Metadata updated | response status ${res.status}`,
            });
            revalidatePath(`/`, "layout");
            return { success: true };
        } else {
            logger.error({
                ...logData,
                message: `Metadata update failed, response error ${res.error} | status ${res.status}`,
            });
            return { error: res.error };
        }
    } catch (error) {
        logger.error({ ...logData, error: getErrorMessage(error) });
        return { error: getErrorMessage(error) };
    }
};

export const deleteMetadata = async (id: string) => {
    const logData = {
        provider: "server",
        function: "deleteMetadata",
    };
    try {
        const res = await metadataService.deleteMetadata(id);
        if (res.status === 204) {
            logger.debug({
                ...logData,
                message: `Metadata deleted | response status ${res.status}`,
            });
            revalidatePath(`/`, "layout");
            return { success: true };
        } else {
            logger.error({
                ...logData,
                message: `Metadata delete failed, response error ${res.error} | status ${res.status}`,
            });
            return { error: res.error };
        }
    } catch (error) {
        logger.error({ ...logData, error: getErrorMessage(error) });
        return { error: getErrorMessage(error) };
    }
};

export const uploadThumbnail = async (id: string, body: FormData) => {
    try {
        await thumbnailService.uploadThumbnail(id, body);
        revalidatePath(`/`, "layout");
        return { success: true };
    } catch (error) {
        logger.error({ provider: "server", function: "uploadThumbnail", error: getErrorMessage(error) });
        return { error: getErrorMessage(error) };
    }
};

export const deleteThumbnail = async (id: string) => {
    try {
        await thumbnailService.deleteThumbnail(id);
        revalidatePath(`/edit-metadata/${id}`, "layout");
        return { success: true };
    } catch (error) {
        logger.error({ provider: "server", function: "deleteThumbnail", error: getErrorMessage(error) });
        return { error: getErrorMessage(error) };
    }
};

export const updateFavourites = async (id: string) => {
    const logData = {
        provider: "server",
        function: "updateFavourites",
    };
    try {
        const res = await userService.updateFavourites(id);
        if (res?.status === 204) {
            logger.debug({ ...logData, message: `Favourite status for id: ${id} updated` });
            revalidatePath(`/`, "layout");
            return { success: true };
        } else {
            logger.error({
                ...logData,
                message: `Favourite update for id: ${id} failed, response error ${res?.error} | status ${res?.status}`,
            });
            return { error: res?.error };
        }
    } catch (error) {
        logger.error({ ...logData, error: getErrorMessage(error) });
        return { error: getErrorMessage(error) };
    }
};
