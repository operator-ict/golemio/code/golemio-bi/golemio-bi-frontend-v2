"use server";

import { getLogger } from "@/logging/logger";
import { ApiClient } from "@/services/api-client";
import { IMetadata } from "@/types/MetadataType";
import { UpdateRole } from "@/types/Users";
import { getErrorMessage } from "@/utils/getErrorMessage";

const logger = getLogger("server-action-roles");

const logData = {
    provider: "server",
    function: "roles-server-action",
};

export const getMetaData = async (): Promise<IMetadata[]> => {
    try {
        logger.debug({
            actions: "getMetaData",
            requestUrl: `/metadata`,
            method: "GET",
            message: "Fetching metadata",
        });

        const res = await ApiClient.get(`/metadata`);
        return res.payload as IMetadata[];
    } catch (error) {
        logger.error({
            message: "Failed to fetch metadata",
            error: getErrorMessage(error),
        });
        return [];
    }
};

export const CreateRole = async (body: BodyInit | undefined) => {
    const headers = {
        "Content-Type": "application/json",
    };

    logger.debug({
        provider: "server",
        actions: "CreateRole",
        requestUrl: `/admin/role`,
        method: "POST",
        message: `Called with body ${body?.toString()}`,
    });

    try {
        return await ApiClient.post(`/admin/role`, body, headers);
    } catch (error) {
        logger.error({ ...logData, message: "Failed to create role", error: getErrorMessage(error) });
        return error;
    }
};

export const GetRoleForUpdate = async (roleId: string): Promise<UpdateRole | []> => {
    logger.debug({
        provider: "server",
        requestUrl: `/admin/role/${roleId}`,
        method: "GET",
        actions: "GetRoleForUpdate",
        message: `Called with roleId: ${roleId}`,
    });

    try {
        const res = await ApiClient.get(`/admin/role/${roleId}`);
        return res.payload as UpdateRole;
    } catch (error) {
        logger.error({ ...logData, message: "Failed to fetch role with ID ${roleId}", error: getErrorMessage(error) });
        return [];
    }
};

export const RolePatch = async (roleId: string, body: BodyInit | undefined) => {
    const headers = { "Content-Type": "application/json" };
    try {
        logger.debug({
            provider: "server",
            actions: "UpdateRole",
            requestUrl: `/admin/role/${roleId}`,
            method: "PATCH",
            message: `Updating role`,
        });
        return await ApiClient.patch(`/admin/role/${roleId}`, body, headers);
    } catch (error) {
        logger.error({ ...logData, message: "Failed to update role with ID ${roleId}", error: getErrorMessage(error) });
        return error;
    }
};
