"use server";
import { getLogger } from "@/logging/logger";
import { mapService } from "@/services/maps-service";
import { getErrorMessage } from "@/utils/getErrorMessage";

const logger = getLogger("formActions");

export const getMeasurementsId = async (containerId: number, update: string) => {
    const logData = {
        provider: "server",
        function: "getMeasurementsId",
    };
    try {
        const res = await mapService.getMeasurementsClient(containerId, update);

        if (res.data && res.status === 200) {
            logger.debug({
                ...logData,
                message: `Measurements for container id ${containerId} fetched | response status ${res.status}`,
            });
            return { status: res.status, data: res.data };
        } else {
            logger.error({
                ...logData,
                message:
                    `Failed to fetch measurements for container id ${containerId}, ` +
                    `response error ${res.error} | status ${res.status}`,
            });
            return { status: res.status, error: "Empty or invalid response received." };
        }
    } catch (error) {
        const errorMessage = getErrorMessage(error);
        logger.error({
            ...logData,
            error: `Error fetching measurements: ${errorMessage}`,
        });
        return { status: 500, error: errorMessage };
    }
};
