"use server";
import { getLogger } from "@/logging/logger";
import { ApiClient } from "@/services/api-client";
import { User } from "@/types/Users";

import { getErrorMessage } from "../(utils)/getErrorMessage";

const logger = getLogger("server-action-users");
const logHeader = {
    provider: "server",
    function: "users-server-actions",
};

export const getAllUsers = async (): Promise<User[] | null> => {
    const logData = {
        ...logHeader,
        actions: "getUserRoles",
        requestUrl: `/admin/user`,
    };

    try {
        const res = await ApiClient.get<User[]>(`/admin/user`);

        if (res.status === 200 && res.payload) {
            logger.debug({
                ...logData,
                message: "getAllUsers fetched successfully",
            });
            return res.payload;
        }

        logger.error({
            ...logData,
            message: `getAllUsers fetch failed, status: ${res.status}, returning null`,
        });
        return null;
    } catch (error) {
        logger.error({
            ...logData,
            error: `Failed to fetch getAllUsers: ${getErrorMessage(error)}, returning null`,
        });
        return null;
    }
};

export const getUserRoles = async (userId: string): Promise<User | null> => {
    const logData = {
        ...logHeader,
        actions: "getUserRoles",
        requestUrl: `/admin/user/${userId}`,
    };

    try {
        const res = await ApiClient.get<User>(`/admin/user/${userId}`);

        if (res.status === 200 && res.payload) {
            logger.debug({
                ...logData,
                message: "User roles fetched successfully",
            });
            return res.payload;
        }

        logger.error({
            ...logData,
            message: `User roles fetch failed, status: ${res.status}, returning null`,
        });
        return null;
    } catch (error) {
        logger.error({
            ...logData,
            error: `Failed to fetch user roles with error: ${getErrorMessage(error)}, returning null`,
        });
        return null;
    }
};
export const updateUserRoles = async (userId: string, roles: string[]) => {
    const body = JSON.stringify({ roles });
    const headers = { "Content-Type": "application/json" };
    const logData = {
        ...logHeader,
        actions: "updateUserRoles",
        requestUrl: `/admin/user/${userId}/role`,
    };
    logger.debug({
        ...logData,
        message: `Called with body ${body.toString()}`,
    });
    try {
        const res = await ApiClient.post(`/admin/user/${userId}/role`, body, headers);
        if (res.status === 204) {
            logger.debug({
                ...logData,
                message: "User roles updated successfully",
            });
            return { success: true };
        }
        logger.error({ ...logData, message: `User roles update failed, status: ${res.status}` });
        return { success: false, error: `User roles update failed, status: ${res.status}` };
    } catch (error) {
        logger.error({ ...logData, error: `Failed to update user roles with error: ${getErrorMessage(error)}` });
        return { success: false, error: getErrorMessage(error) };
    }
};

export const createUserGroup = async (userId: string, group: { groupName: string }) => {
    const body = JSON.stringify(group);
    const headers = { "Content-Type": "application/json" };
    const logData = {
        ...logHeader,
        actions: "createUserGroup",
        requestUrl: `/admin/user/${userId}/group`,
    };
    logger.debug({
        ...logData,
        message: `Called with body ${body}`,
    });

    try {
        const res = await ApiClient.patch(`/admin/user/${userId}/group`, body, headers);
        if (res.status === 204) {
            logger.debug({
                ...logData,
                message: "User group updated successfully",
            });
            return { success: true };
        }
        logger.error({ ...logData, message: `User roles update failed, status: ${res.status}` });
        return { success: false, error: `User roles update failed, status: ${res.status}` };
    } catch (error) {
        logger.error({ ...logData, error: `Failed to update user roles with error: ${getErrorMessage(error)}` });
        return { success: false, error: getErrorMessage(error) };
    }
};
