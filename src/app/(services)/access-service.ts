import { getLogger } from "@/logging/logger";
import { AccessRequestResponse } from "@/types/formTypes";

import { ApiClient } from "./api-client";

const logger = getLogger("AccessService");
export class AccessService {
    static async accessRequest(body: BodyInit | undefined) {
        const headers = {
            "Content-Type": "application/json",
        };
        return ApiClient.post<AccessRequestResponse>("user", body, headers);
    }

    static async accessDashboardRequest(body: BodyInit | undefined) {
        const headers = {
            "Content-Type": "application/json",
        };
        logger.debug({
            provider: "server",
            class: "AccessService",
            method_name: "accessDashboardRequest",
            message: `Called with body ${body?.toString()}`,
        });
        return ApiClient.post<AccessRequestResponse>("accessrequest?type=dashboard", body, headers);
    }
}
