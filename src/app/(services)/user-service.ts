import { getLogger } from "@/logging/logger";
import { TokenService } from "@/services/token-service";
import { getErrorMessage } from "@/utils/getErrorMessage";

import { ApiClient } from "./api-client";

const logger = getLogger("user-service");

export type IFavourites = {
    favouriteTiles: string[];
};

export type AgreementStatus = {
    acceptedConditions: boolean;
};

class UserService {
    logHeaders = {
        provider: "server",
        class: "UserService",
    };
    async getFavourites() {
        const logData = {
            ...this.logHeaders,
            method: "getFavourites",
            requestUrl: "user",
        };
        try {
            const res = await ApiClient.get<IFavourites>("user");

            if (res.status === 200 && Array.isArray((res.payload as IFavourites).favouriteTiles)) {
                logger.debug({
                    ...logData,
                    message:
                        `Response status: ${res.status} | ` +
                        `User has ${(res.payload as IFavourites).favouriteTiles.length} favourites`,
                });
                return res.payload as IFavourites;
            } else {
                logger.error({ ...logData, message: `Failed to fetch favourites: ${res.status}` });
                return { favouriteTiles: [] } as IFavourites;
            }
        } catch (error) {
            logger.error({ ...logData, error: getErrorMessage(error) });
            return error;
        }
    }

    async updateFavourites(_id: string) {
        const headers = { "Content-Type": "application/json" };

        const favourites = await this.getFavourites();

        if (favourites) {
            const isFavourite = (favourites as IFavourites).favouriteTiles.includes(_id);
            const newFavouriteTiles = isFavourite
                ? (favourites as IFavourites).favouriteTiles.filter((item) => item !== _id)
                : [...(favourites as IFavourites).favouriteTiles, _id];
            const body = JSON.stringify({ favouriteTiles: newFavouriteTiles });
            return await ApiClient.put<IFavourites>("user", body, headers);
        }
    }

    async getUserAgreementStatus() {
        return await ApiClient.get<AgreementStatus>("user");
    }

    async updateUserAgreementStatus() {
        const logData = {
            ...this.logHeaders,
            method: "updateUserAgreementStatus",
        };
        const headers = { "Content-Type": "application/json" };
        try {
            const userId = await TokenService.getUserId();
            logger.debug({
                ...logData,
                requestUrl: `user/${userId}/conditions`,
                message: `Updating user agreement status`,
            });
            return await ApiClient.patch(`user/${userId}/conditions`, undefined, headers);
        } catch (error) {
            logger.error({ ...logData, error: getErrorMessage(error) });
            return error;
        }
    }
}

export const userService = new UserService();
