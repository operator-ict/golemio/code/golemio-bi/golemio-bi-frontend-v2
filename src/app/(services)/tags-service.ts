import { getLogger } from "@/logging/logger";
import { ITag } from "@/types/TagType";

import { ApiClient } from "./api-client";
import { TokenService } from "./token-service";

const logger = getLogger("tags-service");

class TagsService {
    logHeaders = {
        provider: "server",
        class: "TagsService",
    };

    async getAllTags() {
        const res = await ApiClient.get<Array<ITag>>("tag");

        const logData = {
            ...this.logHeaders,
            method: "getAllTags",
            requestUrl: "tag",
        };

        if (res.status === 200 && Array.isArray(res.payload)) {
            logger.debug({
                ...logData,
                message: `Response status: ${res.status} | data Arr length ${res.payload.length}`,
            });
            return res.payload;
        }
        logger.error({
            ...logData,
            message: `Request failed, returning empty Arr, response error: ${res.error} | status: ${res.status}`,
        });
        return [];
    }

    async getUserTags() {
        const userId = await TokenService.getUserId();
        const res = await ApiClient.get<Array<ITag>>(`user/${userId}/tag`);

        const logData = {
            ...this.logHeaders,
            method: "getUserTags",
            requestUrl: `user/${userId}/tag`,
        };

        if (res.status === 200 && Array.isArray(res.payload)) {
            logger.debug({
                ...logData,
                message: `Response status: ${res.status} | data Arr length ${res.payload.length}`,
            });
            return res.payload;
        }
        logger.error({
            ...logData,
            message: `Request failed, returning empty Arr, response error: ${res.error} | status: ${res.status}`,
        });
        return [];
    }
}

const tagsService = new TagsService();

export { tagsService };
