/* eslint-disable @typescript-eslint/no-explicit-any */

import { getLogger } from "@/logging/logger";

import { getAccessToken } from "../(powerbiembed)/authentication";
import { EmbedConfig } from "../(powerbiembed)/embedConfig";
import { getAuthHeader } from "../(powerbiembed)/utils";

const logger = getLogger("powerbiembed");

class PowerBIEmbedService {
    /**
     * Generate embed token and embed urls for reports
     * @return Details like Embed URL, Access token and Expiry
     */

    logHeaders = {
        provider: "server",
        class: "PowerBIEmbedService",
    };
    async getEmbedInfo(workspaceId: string, reportId: string): Promise<EmbedConfig> {
        const logData = {
            ...this.logHeaders,
            method: "getEmbedInfo",
            workspaceId,
            reportId,
        };
        // Get the Report Embed details
        try {
            logger.debug({
                ...logData,
                message: "Getting Embed Info",
            });
            return await this.getEmbedParamsForSingleReport(workspaceId, reportId);
        } catch (error: any) {
            logger.error({
                ...logData,
                error:
                    `Failed to get Embed Info, returning empty config, error status: ${error.status} | ` +
                    `statusText: ${error.statusText}`,
            });
            return new EmbedConfig();
        }
    }

    /**
     * Get embed params for a single report for a single workspace
     * @param {string} workspaceId
     * @param {string} reportId
     * @param {string} additionalDatasetId - Optional Parameter
     * @return EmbedConfig object
     */
    async getEmbedParamsForSingleReport(workspaceId: string, reportId: string): Promise<EmbedConfig> {
        const reportInGroupApi = `https://api.powerbi.com/v1.0/myorg/groups/${workspaceId}/reports/${reportId}`;
        const headers = await this.getRequestHeader();

        // Get report info by calling the PowerBI REST API
        const result = await fetch(reportInGroupApi, {
            method: "GET",
            headers,
            cache: "no-store",
        });

        if (!result.ok) {
            logger.error({
                ...this.logHeaders,
                method: "getEmbedParamsForSingleReport",
                workspaceId,
                reportId,
                error: `Failed to get Embed Info, status: ${result.status} | statusText: ${result.statusText}`,
            });

            throw result;
        }

        // Convert result in json to retrieve values
        const resultJson = await result.json();

        // Add report data for embedding
        const reportEmbedConfig = new EmbedConfig();
        reportEmbedConfig.reportId = resultJson.id;
        reportEmbedConfig.embedUrl = resultJson.embedUrl;

        // Create list of datasets
        const datasetIds = [resultJson.datasetId];

        // Get Embed token multiple resources
        reportEmbedConfig.embedToken = await this.getEmbedTokenForSingleReportSingleWorkspace(reportId, datasetIds, workspaceId);
        logger.debug({
            ...this.logHeaders,
            method: "getEmbedParamsForSingleReport",
            workspaceId,
            reportId,
            message: "Embed Info retrieved successfully",
        });
        return reportEmbedConfig;
    }

    /**
     * Get Embed token for single report, multiple datasets, and an optional target workspace
     * @param {string} reportId
     * @param {Array<string>} datasetIds
     * @param {string} targetWorkspaceId - Optional Parameter
     * @return EmbedToken
     */
    async getEmbedTokenForSingleReportSingleWorkspace(reportId: string, datasetIds: string[], targetWorkspaceId: string) {
        // Add report id in the request
        const formData: {
            datasets: Array<{ id: string }>;
            reports: Array<{ id: string }>;
            targetWorkspaces: Array<{ id: string }>;
        } = {
            datasets: [],
            reports: [
                {
                    id: reportId,
                },
            ],
            targetWorkspaces: [],
        };

        // Add dataset ids in the request
        formData["datasets"] = [];
        for (const datasetId of datasetIds) {
            formData["datasets"].push({
                id: datasetId,
            });
        }

        // Add targetWorkspace id in the request
        if (targetWorkspaceId) {
            formData["targetWorkspaces"] = [];
            formData["targetWorkspaces"].push({
                id: targetWorkspaceId,
            });
        }

        const embedTokenApi = "https://api.powerbi.com/v1.0/myorg/GenerateToken";

        const headers = await this.getRequestHeader();

        // Generate Embed token for single report, workspace, and multiple datasets. Refer https://aka.ms/MultiResourceEmbedToken
        const result = await fetch(embedTokenApi, {
            method: "POST",
            headers: headers,
            cache: "no-store",
            body: JSON.stringify(formData),
        });

        if (!result.ok) {
            logger.error({
                ...this.logHeaders,
                method: "getEmbedTokenForSingleReportSingleWorkspace",
                reportId,
                datasetIds,
                targetWorkspaceId,
                error: `Failed to get Embed Token, status: ${result.status} | statusText: ${result.statusText}`,
            });
            throw result;
        }

        logger.debug({
            ...this.logHeaders,
            method: "getEmbedTokenForSingleReportSingleWorkspace",
            reportId,
            datasetIds,
            targetWorkspaceId,
            message: "Embed Token retrieved successfully",
        });
        return result.json();
    }

    /**
     * Get Request header
     * @return Request header with Bearer token
     */
    async getRequestHeader() {
        // Store authentication token
        let tokenResponse: any;

        // Store the error thrown while getting authentication token
        let errorResponse: string;

        // Get the response from the authentication request
        try {
            tokenResponse = await getAccessToken();
        } catch (err: any) {
            if (
                Object.prototype.hasOwnProperty.call(err, "error_description") &&
                Object.prototype.hasOwnProperty.call(err, "error")
            ) {
                errorResponse = err.error_description;
            } else {
                errorResponse = "Invalid PowerBI Username provided";
            }
            throw new Error(errorResponse);
        }

        // Extract AccessToken from the response
        const token = tokenResponse.accessToken;
        return {
            "Content-Type": "application/json",
            Authorization: getAuthHeader(token),
            "Cache-Control": "no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0",
        };
    }
}

export const powerbiEmbedService = new PowerBIEmbedService();
