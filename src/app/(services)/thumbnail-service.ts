import { getLogger } from "@/(logging)/logger";

import { ApiClient } from "./api-client";

const logger = getLogger("thumbnail-service");

class ThumbnailService {
    logHeaders = {
        provider: "server",
        class: "ThumbnailService",
    };
    async getThumbnail(id: string) {
        const logData = {
            ...this.logHeaders,
            method: "getThumbnail",
            requestUrl: `metadata/${id}/thumbnail`,
            headers: { "Content-Type": "image/*" },
        };
        const res = await ApiClient.get(`metadata/${id}/thumbnail`, undefined, { "Content-Type": "image/*" });

        if (res.status === 200 && res.payload instanceof Blob) {
            logger.debug({ ...logData, message: `Response status: ${res.status}` });
            return res.payload as Blob;
        } else {
            logger.error({ ...logData, message: `Request failed, response error: ${res.error} status: ${res.status}` });
            return undefined;
        }
    }
    async uploadThumbnail(id: string, body: FormData) {
        const logData = {
            ...this.logHeaders,
            method: "uploadThumbnail",
            requestUrl: `metadata/${id}/thumbnail`,
        };
        const res = await ApiClient.put(`metadata/${id}/thumbnail`, body);

        if (res.status === 201) {
            logger.debug({ ...logData, message: `Response status: ${res.status}` });
            return res.payload;
        } else {
            logger.error({ ...logData, message: `Request failed, response error: ${res.error} | status: ${res.status}` });
            throw new Error(`Failed to put thumbnail: ${res.status}`);
        }
    }

    async deleteThumbnail(id: string) {
        const logData = {
            ...this.logHeaders,
            method: "deleteThumbnail",
            requestUrl: `metadata/${id}/thumbnail`,
            headers: { "Content-Type": "image/*" },
        };
        const res = await ApiClient.delete(`metadata/${id}/thumbnail`, { "Content-Type": "image/*" });

        if (res.status === 200) {
            logger.debug({ ...logData, message: `Response status: ${res.status}` });
            return res.payload;
        } else {
            logger.error({ ...logData, message: `Request failed, response error: ${res.error} | status: ${res.status}` });
            throw new Error(`Failed to delete thumbnail: ${res.status}`);
        }
    }
}

const thumbnailService = new ThumbnailService();

export { thumbnailService };
