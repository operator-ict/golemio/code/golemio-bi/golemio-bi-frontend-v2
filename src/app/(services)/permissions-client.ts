import { config } from "src/config";

import { getLogger } from "@/logging/logger";
import { getErrorMessage } from "@/utils/getErrorMessage";
const API_BASE = config.NEXT_PUBLIC_API_URL;

const logger = getLogger("permissions-client");

export class PermissionsClient {
    static async checkPermissions(object: string, relation: string, token: string, userId: string): Promise<boolean> {
        const url = new URL(`${API_BASE}/permissions`);

        const headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("x-access-token", token);
        headers.append("User-Agent", "gbiFe");

        const logData = {
            provider: "server",
            class: "PermissionsClient",
            actions: "checkPermissions",
            requestUrl: url.toString(),
            method: "POST",
            headers: {
                "Content-Type": headers.get("Content-Type"),
                "Accept-Encoding": headers.get("Accept-Encoding"),
                "User-Agent": headers.get("User-Agent"),
            },
            message: `Checking permission for private:${object}:${relation} with userId ${userId}`,
        };

        try {
            const permissionsResponse = await fetch(url.toString(), {
                method: "POST",
                headers,
                body: JSON.stringify([
                    {
                        namespace: "private",
                        object,
                        relation,
                        subjectId: userId,
                    },
                ]),
                cache: "no-store",
            });

            if (permissionsResponse.ok) {
                const permissions = await permissionsResponse.json();
                logger.debug({
                    ...logData,
                    message: `Permission for private:${object}:${relation} with userId ${userId} is ${permissions[0]}`,
                });
                return permissions[0];
            }
            logger.error({
                ...logData,
                message:
                    `Permission for private:${object}:${relation} with userId ${userId} ` +
                    `is false, status: ${permissionsResponse.status}`,
            });
            return false;
        } catch (error) {
            logger.error({
                ...logData,
                message: `Failed to check permission for private:${object}:${relation} with userId ${userId}`,
                error: getErrorMessage(error),
            });
            return false;
        }
    }
}
