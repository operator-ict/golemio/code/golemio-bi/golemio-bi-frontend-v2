import { getServerSession } from "next-auth";

import { authOptions } from "@/api/auth/[...nextauth]/authOptions";
import { getLogger } from "@/logging/logger";

const logger = getLogger("token-service");

export class TokenService {
    static sessionToken: string | undefined;
    static userId: string | undefined;
    static isLoggedIn: boolean | undefined;

    static async initialize() {
        const authSession = await getServerSession(authOptions);
        this.sessionToken = authSession?.token;
        this.userId = authSession?.id?.toString();
        this.isLoggedIn = authSession?.isLoggedIn;
        logger.debug("TokenService initialized successfully");
    }

    static async getToken(): Promise<string | undefined> {
        await this.initialize();
        return this.sessionToken;
    }

    static async getUserId(): Promise<string | undefined> {
        await this.initialize();
        return this.userId;
    }

    static async getIsLoggedIn(): Promise<boolean | undefined> {
        await this.initialize();
        return this.isLoggedIn;
    }
}
