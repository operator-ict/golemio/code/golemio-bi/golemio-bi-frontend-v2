import { IFavourites, userService } from "./user-service";

export class FavouriteService {
    async isFavourite(id: string) {
        const favourites = await userService.getFavourites();
        const favouriteTiles = favourites ? (favourites as IFavourites).favouriteTiles : [];
        return favouriteTiles.includes(id);
    }
}

export const favouriteServise = new FavouriteService();
