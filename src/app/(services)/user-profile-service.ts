import { getOryKratosFrontendApi } from "@/api/auth/OryApisProvider";
import { getLogger } from "@/logging/logger";
import { UserProfileResponse } from "@/types/formTypes";
import { getErrorMessage } from "@/utils/getErrorMessage";

import { TokenService } from "./token-service";

const logger = getLogger("user-profile-service");

export class UserProfileService {
    static async getUserProfile() {
        const logData = {
            provider: "server",
            function: "getUserProfile",
        };
        try {
            const sessionToken = await TokenService.getToken();
            const oryFrontendApi = getOryKratosFrontendApi();
            const data = await oryFrontendApi.toSession({ xSessionToken: sessionToken });

            return {
                id: data.identity?.id,
                name: data.identity?.traits.firstName,
                surname: data.identity?.traits.lastName,
                email: data.identity?.traits.email,
            } as UserProfileResponse;
        } catch (error) {
            logger.error({ ...logData, message: `Error while fetching user profile: ${getErrorMessage(error)}` });
            throw new Error(`Error while fetching user profile: ${getErrorMessage(error)}`);
        }
    }
}
