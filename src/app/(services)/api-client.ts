import qs from "qs";
import { config } from "src/config";

import { getLogger } from "@/logging/logger";
import { ApiGetRequestParams, ResponseEntity } from "@/types/formTypes";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { joinUrlPaths } from "@/utils/joinUrlPaths";

import { TokenService } from "./token-service";

const API_BASE = config.NEXT_PUBLIC_API_URL;

const logger = getLogger("api-client");

export class ApiClient {
    static async request<T>(
        method: "GET" | "PUT" | "DELETE" | "POST" | "PATCH",
        path: string,
        options: RequestInit & { params?: ApiGetRequestParams }
    ): Promise<ResponseEntity<T>> {
        const query = qs.stringify(
            {
                ...(options.params?.pagination ? { pagination: options.params.pagination } : {}),
                ...(options.params?.filters ? { filters: options.params.filters } : {}),
                ...(options.params?.clientRoute ? { clientRoute: options.params.clientRoute } : {}),
                ...(options.params?.page && { page: options.params.page }),
                ...(options.params?.limit && { limit: options.params.limit }),
                ...(options.params?.orderDir && { orderDir: options.params.orderDir }),
                ...(options.params?.orderBy && { orderBy: options.params.orderBy }),
                ...(options.params?.search && { search: options.params.search }),
            },
            { encodeValuesOnly: true }
        );

        const url = new URL(joinUrlPaths(API_BASE, path));

        if (query) {
            url.search = query;
        }

        const isPostPutPatch = method === "POST" || method === "PUT" || method === "PATCH";

        const sessionToken = await TokenService.getToken();

        const headers = new Headers();
        if (sessionToken) {
            headers.append("x-access-token", sessionToken);
        }
        headers.append("Accept-Encoding", "gzip");
        headers.append("User-Agent", "gbiFe");
        for (const [name, value] of Object.entries(options.headers ?? {})) {
            headers.append(name, value);
        }

        const body = isPostPutPatch ? options.body : undefined;

        const isImageRequest = headers && headers.get("Content-Type") === "image/*";

        const logData = {
            provider: "server",
            class: "ApiClient",
            method: `request - ${method}`,
            requestUrl: url.toString(),
            headers: {
                "Content-Type": headers.get("Content-Type"),
                "Accept-Encoding": headers.get("Accept-Encoding"),
                "User-Agent": headers.get("User-Agent"),
            },
        };

        return fetch(url.toString(), {
            method,
            headers,
            body,
            cache: options.cache ?? "no-store",
        })
            .then(async (response) => {
                const isEmptyResponse = response.status === 204;

                const getPayload = async () => {
                    if (isImageRequest) return await response.blob();
                    if (isEmptyResponse) return null;
                    return await response.json();
                };
                if (response.ok) {
                    const payload = await getPayload();
                    logger.debug({
                        ...logData,
                        message: `Response status: ${response.status}, statusText: ${response.statusText}`,
                    });
                    return {
                        status: response.status,
                        payload,
                        count: (response.headers.get("x-count") as string | null) || null,
                    };
                } else {
                    const errorPayload = await response
                        .json()
                        .catch(() => ({ error: "Unknown error", errors: { info: "Not access request" } }));

                    const message = errorPayload.errors
                        ? `Payload error: ${errorPayload.error}, response status: ${response.status}, ` +
                          `accessRequestErrors: ${JSON.stringify(errorPayload.errors)}`
                        : `Payload error: ${errorPayload.error}, response status: ${response.status}`;

                    logger.error({
                        ...logData,
                        message,
                    });
                    return {
                        status: response.status,
                        error: errorPayload.error,
                        errors: errorPayload.errors,
                    };
                }
            })
            .catch((error) => {
                logger.error({
                    ...logData,
                    error: getErrorMessage(error),
                });
                throw new Error(`Failed to fetch the data: ${getErrorMessage(error)}`);
            });
    }

    static async get<T>(path: string, params?: ApiGetRequestParams, headers?: HeadersInit, cache?: RequestCache) {
        return this.request<T>("GET", path, { params, headers, cache });
    }

    static async put<T>(path: string, body: BodyInit | undefined, headers?: HeadersInit) {
        return this.request<T>("PUT", path, { body, headers });
    }

    static async post<T>(path: string, body: BodyInit | undefined, headers?: HeadersInit) {
        return this.request<T>("POST", path, { body, headers });
    }

    static async patch<T>(path: string, body: BodyInit | undefined, headers?: HeadersInit) {
        return this.request<T>("PATCH", path, { body, headers });
    }

    static async delete<T>(path: string, headers?: HeadersInit) {
        return this.request<T>("DELETE", path, { headers });
    }
}
