import { ApiClient } from "@/services/api-client";
import { ApiGetRequestParams, ResponseEntity } from "@/types/formTypes";
import { Garant, Role, User } from "@/types/Users";

export class usersService {
    static async getRoles(params?: ApiGetRequestParams): Promise<ResponseEntity<Role[]>> {
        return await ApiClient.get<Role[]>(`admin/role`, params);
    }

    static async getUsers() {
        const response = await ApiClient.get<User[]>(`admin/user`);
        return response.payload ?? [];
    }

    static async getGarants() {
        const response = await ApiClient.get<Garant[]>(`admin/user/garant`);
        return response.payload ?? [];
    }
}
