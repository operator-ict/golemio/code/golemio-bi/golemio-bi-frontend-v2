import { getLogger } from "@/logging/logger";

import { ApiClient } from "./api-client";
import { TokenService } from "./token-service";

const logger = getLogger("PermissionsService");

export class PermissionsService {
    private static logData = {
        provider: "server",
        class: "PermissionsService",
    };
    private static async checkPermission(object: string, relation: string): Promise<boolean> {
        const userId = await TokenService.getUserId();
        const permissions = await ApiClient.post<boolean[]>(
            `/permissions`,
            JSON.stringify([
                {
                    namespace: "private",
                    object,
                    relation,
                    subjectId: userId,
                },
            ]),
            { "Content-Type": "application/json" }
        );
        logger.debug({
            ...this.logData,
            message: `private:${object}:${relation} permissions: ${permissions.payload?.[0]}`,
        });
        return permissions.payload?.[0] ?? false;
    }

    static async getDashboardsViewAdminPermissions(): Promise<boolean> {
        return this.checkPermission("dashboards", "viewAdmin");
    }

    static async getDashboardsEditPermissions(): Promise<boolean> {
        return this.checkPermission("dashboards", "edit");
    }

    static async getUsersViewAdminPermissions(): Promise<boolean> {
        return this.checkPermission("users", "viewAdmin");
    }

    static async getUsersEditRolePermissions(): Promise<boolean> {
        return this.checkPermission("users", "editRole");
    }

    static async getRolesEditPermissions(): Promise<boolean> {
        return this.checkPermission("roles", "edit");
    }

    static async getUsersEditRightsPermissions(): Promise<boolean> {
        return this.checkPermission("users", "editRights");
    }
}
