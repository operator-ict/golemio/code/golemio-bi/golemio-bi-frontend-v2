import { ICityDistrictsFeatureCollection } from "@/(types)/cityDistricts";
import { IFeatureCollection } from "@/(types)/FeaturesTypes";
import { IMeasurementsIdResponse } from "@/(types)/MeasurementsType";
import { IParkingCollection } from "@/(types)/ParkingsType";
import { getLogger } from "@/logging/logger";

import { ApiClient } from "./api-client";

const logger = getLogger("maps-service");

class MapService {
    logHeaders = {
        provider: "server",
        class: "mapService",
    };

    async getSortedWasteStations(): Promise<IFeatureCollection> {
        const logData = {
            ...this.logHeaders,
            method: "getSortedWasteStations",
            requestUrl: "/v2/sortedwastestations",
        };
        const response = await ApiClient.post<IFeatureCollection>("/proxy", JSON.stringify({ path: "/v2/sortedwastestations" }), {
            "Content-Type": "application/json",
        });
        if (response.status && response.payload) {
            logger.debug({
                ...logData,
                message: `Response status: ${response.status}`,
            });
            return response.payload;
        }
        logger.error({
            ...logData,
            message: `Request failed, returning empty Arr, response status: ${response.status}`,
        });
        return {
            type: "FeatureCollection",
            features: [],
        };
    }
    async getCityDistricts(): Promise<ICityDistrictsFeatureCollection> {
        const logData = {
            ...this.logHeaders,
            method: "getCityDistricts",
            requestUrl: "/v2/citydistricts",
        };
        const response = await ApiClient.post<ICityDistrictsFeatureCollection>(
            "/proxy",
            JSON.stringify({ path: "/v2/citydistricts" }),
            { "Content-Type": "application/json" }
        );
        if (response.status && response.payload) {
            logger.debug({
                ...logData,
                message: `Response status: ${response.status}`,
            });
            return response.payload;
        }
        logger.error({
            ...logData,
            message: `Request failed, returning empty Arr, response status: ${response.status}`,
        });
        return {
            type: "FeatureCollection",
            features: [],
        };
    }
    async getParkingsData(): Promise<IParkingCollection> {
        const logData = {
            ...this.logHeaders,
            method: "getParkingsData",
            requestUrl: "/v2/parking?source=tsk&category=park_and_ride",
        };
        const response = await ApiClient.post<IParkingCollection>(
            "/proxy",
            JSON.stringify({ path: "/v2/parking?source=tsk&category=park_and_ride" }),
            { "Content-Type": "application/json" }
        );
        if (response.status && response.payload) {
            logger.debug({
                ...logData,
                message: `Response status: ${response.status}`,
            });
            return response.payload;
        }
        logger.error({
            ...logData,
            message: `Request failed, returning empty Arr, response status: ${response.status}`,
        });
        return {
            type: "Feature",
            features: [],
        };
    }

    async getMeasurementsClient(containerId: number, update: string | Date): Promise<IMeasurementsIdResponse> {
        const requestUrl = `v2/sortedwastestations/measurements?containerId=${containerId}&from=${update}`;
        const logData = {
            ...this.logHeaders,
            method: "getMeasurementsClient",
            requestUrl,
        };

        const response = await ApiClient.post<IFeatureCollection>("/proxy", JSON.stringify({ path: requestUrl }), {
            "Content-Type": "application/json",
        });
        if (response.status === 200 && Array.isArray(response.payload)) {
            logger.debug({
                ...logData,
                message: `Response status: ${response.status}`,
            });
            return { status: response.status, data: response.payload };
        }
        logger.error({
            ...logData,
            message: `Request failed, returning empty Arr, response status: ${response.status}`,
        });
        return {
            status: response.status,
            data: [],
        };
    }
}

const mapService = new MapService();

export { mapService };
