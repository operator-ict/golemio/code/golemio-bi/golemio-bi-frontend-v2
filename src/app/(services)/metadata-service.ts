import { getLogger } from "@/logging/logger";
import { DataComponent, DataType, IMetadata, MetadataExistsResponse } from "@/types/MetadataType";
import { ITag } from "@/types/TagType";

import { ApiClient } from "./api-client";
import { TokenService } from "./token-service";

const logger = getLogger("metadata-service");

type DataTag = string[];

class MetadataService {
    logHeaders = {
        provider: "server",
        class: "MetadataService",
    };
    async getMetadataPromise(cache?: RequestCache, searchString?: string) {
        const userId = await TokenService.getUserId();
        let url = `user/${userId}/metadata?sortBy=visits`;
        if (searchString) {
            url = url.concat(`&searchString=${searchString}`);
        }
        logger.debug({
            ...this.logHeaders,
            method: "getMetadataPromise",
            requestUrl: url,
            message: `Fetching metadata, primary promise`,
        });
        return ApiClient.get<Array<IMetadata>>(url, undefined, undefined, cache);
    }

    async getMetadataByTypeComponent(type: DataType, component?: DataComponent) {
        logger.debug({
            ...this.logHeaders,
            method: "getMetadataByTypeComponent",
            message: `Fetching metadata by type: ${type} and component: ${component}`,
        });
        return this.getMetadataByFilterAndType(type, undefined, component);
    }

    async getMetadataBySearch(search: string) {
        const res = await this.getMetadataPromise(undefined, search);

        if (res.status === 200 && Array.isArray(res.payload)) {
            const dataBySearch: IMetadata[] = res.payload;

            logger.debug({
                ...this.logHeaders,
                method: "getMetadataBySearch",
                message:
                    `Response status: ${res.status} | data arr length ${dataBySearch.length} | ` +
                    `first title: ${dataBySearch.length > 0 ? dataBySearch[0].title : "Zero data fetched"}`,
            });
            return dataBySearch;
        }
    }

    async getMetadataByClientRoute(client_route: string) {
        const logData = {
            ...this.logHeaders,
            method: "getMetadataByClientRoute",
            client_route,
        };
        const res = await this.getMetadataPromise();
        logger.debug({
            provider: "server",
            class: "MetadataService",
            method: "getMetadataByClientRoute",
            message: `status: ${res.status} title: ${res.payload?.length && res.payload[0].title}`,
        });
        if (res.status === 200 && res.payload) {
            const dataByRoute = (res.payload as IMetadata[]).find((item) => item.client_route === client_route);
            logger.debug({
                ...logData,
                message: `Response status:  ${res.status} | title: ${dataByRoute?.title}`,
            });
            return dataByRoute ? (dataByRoute as IMetadata) : null;
        } else {
            logger.error({
                ...logData,
                message: `Failed to fetch metadata, response status: ${res.status}`,
            });
            return undefined;
        }
    }

    async getMetadataByFilterAndType(type?: DataType, tag?: DataTag, component?: DataComponent) {
        const res = await this.getMetadataPromise();

        if (res.status && Array.isArray(res.payload)) {
            let filteredData: IMetadata[] = type ? res.payload.filter((item) => item.type.includes(type)) : res.payload;

            if (tag && tag.length > 0) {
                filteredData = filteredData.filter((item) => item.tags.some((t: ITag) => tag.includes(t.title)));
            }

            if (component) {
                filteredData = filteredData.filter((item) => item.component === component);
            }

            logger.debug({
                ...this.logHeaders,
                method: "getMetadataByFilterAndType",
                message:
                    `Response status: ${res.status} | Title: ` +
                    `${filteredData.length > 0 ? filteredData[0].title : "Zero data fetched"}` +
                    ` | Filtered data length: ${filteredData.length}`,
            });
            return filteredData;
        }
    }

    async getMetadataByRoute(_id: string, cache?: RequestCache) {
        logger.debug({
            ...this.logHeaders,
            method: "getMetadataByRoute",
            requestUrl: `/metadata/${_id}`,
            message: `Fetching metadata with id: ${_id}`,
        });
        return ApiClient.get<IMetadata>(`/metadata/${_id}`, undefined, undefined, cache);
    }
    async createMetadata<T>(body: BodyInit | undefined) {
        const headers = {
            "Content-Type": "application/json",
        };
        logger.debug({
            ...this.logHeaders,
            method: "createMetadata",
            message: `Creating metadata with body: ${JSON.stringify(body, null, 2)}`,
        });
        return await ApiClient.post<T>("metadata", body, headers);
    }

    async updateMetadata(_id: string, body: BodyInit | undefined) {
        const headers = {
            "Content-Type": "application/json",
        };
        logger.debug({
            ...this.logHeaders,
            method: "updateMetadata",
            message: `Updating metadata with body: ${JSON.stringify(body, null, 2)}`,
        });
        return await ApiClient.put(`metadata/${_id}`, body, headers);
    }

    async deleteMetadata(_id: string) {
        logger.debug({
            ...this.logHeaders,
            method: "deleteMetadata",
            message: `Deleting metadata with id: ${_id}`,
        });
        return await ApiClient.delete(`metadata/${_id}`);
    }
    async trackMetadata(_id: string | undefined) {
        logger.debug({
            ...this.logHeaders,
            method: "trackMetadata",
            message: `Tracking metadata with id: ${_id}`,
        });
        return await ApiClient.put(`metadata/${_id}/track`, undefined);
    }

    async verifyMetadataExists(clientRoute: string) {
        const res = await ApiClient.get<MetadataExistsResponse>(`check/metadata`, { clientRoute });
        if (res.status === 200 && res.payload) {
            logger.debug({
                ...this.logHeaders,
                method: "verifyMetadataExists",
                requestUrl: `check/metadata`,
                clientRoute,
                message: `Metadata exist: ${(res.payload as MetadataExistsResponse).exists}`,
            });
            return (res.payload as MetadataExistsResponse).exists;
        }
        logger.error({
            provider: "server",
            class: "MetadataService",
            method: "verifyMetadataExists",
            message: `Res.status: ${res.status as number}`,
        });
        return undefined;
    }
}

const metadataService = new MetadataService();

export { metadataService };
