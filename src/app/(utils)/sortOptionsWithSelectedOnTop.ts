export const sortOptionsWithSelectedOnTop = <T extends { value: string; label: string }>(
    options: T[],
    selectedOptions: string | string[]
) => {
    if (!Array.isArray(selectedOptions)) {
        selectedOptions = [selectedOptions];
    }
    return [...options].sort((a, b) => {
        const aSelected = selectedOptions.includes(a.value);
        const bSelected = selectedOptions.includes(b.value);

        if (aSelected !== bSelected) {
            return bSelected ? 1 : -1;
        }
        return a.label.localeCompare(b.label);
    });
};
