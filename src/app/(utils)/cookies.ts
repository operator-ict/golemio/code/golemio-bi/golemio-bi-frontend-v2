export const setAgreementCookie = (value: boolean) => {
    document.cookie = `golemiobi-acceptedConditions=${value}; path=/; max-age=${30 * 24 * 60 * 60}`;
};

export const resetAgreementCookie = () => {
    document.cookie = `golemiobi-acceptedConditions=false; path=/; max-age=0`;
};
