/* eslint-enable @typescript-eslint/no-explicit-any */
export const getFileDimensions = (file: Blob): Promise<{ width: number; height: number }> => {
    const url = window.URL.createObjectURL(file);
    return new Promise<{ width: number; height: number }>((resolve, reject) => {
        const img = new Image();
        img.src = url;
        img.onerror = (e: any) => reject(new Error("Image failed to load", e));
        img.onload = () => {
            resolve({
                width: img.naturalWidth,
                height: img.naturalHeight,
            });
        };
    }).finally(() => {
        window.URL.revokeObjectURL(url);
    });
};
