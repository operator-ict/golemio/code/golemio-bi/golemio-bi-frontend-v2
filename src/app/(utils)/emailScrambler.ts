export const emailScrambler = (email: string) => {
    if (!email || !email.includes("@")) {
        return "Not an email";
    }
    const [name, domain] = email.split("@");
    const scrambledName = starString(name);
    const scrambledDomain = domain
        .split(".")
        .map((part) => starString(part))
        .join(".");
    return `${scrambledName}@${scrambledDomain}`;
};

const starString = (string: string) => {
    if (string.length === 2) {
        return string.slice(0, 1) + "*";
    }
    return string.slice(0, 2) + "*";
};
