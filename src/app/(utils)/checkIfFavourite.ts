type FavouriteService = {
    isFavourite: (id: string) => Promise<boolean>;
};

export async function checkIfFavourite(
    favouriteService: FavouriteService,
    metadata?: { _id?: string } | null
): Promise<{ id: string; isFavourite: boolean }> {
    const id = metadata?._id ?? "";
    const isFavourite = await favouriteService.isFavourite(id);
    return { id, isFavourite };
}
