import toast from "react-hot-toast";

import NotificationBanner, { NotificationType } from "@/(components)/NotificationBanner";

export const showToast = (type: NotificationType, text: string) => {
    toast.custom(<NotificationBanner type={type} text={text} />);
};
