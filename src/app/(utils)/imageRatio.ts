import { getLogger } from "@/logging/logger";
import { getErrorMessage } from "@/utils/getErrorMessage";

import { getFileDimensions } from "./getFileDimensions";

const logger = getLogger("get-file-dimensions");

export const THUMBNAIL_RATIO_WIDTH = 394;
export const THUMBNAIL_RATIO_HEIGHT = 180;

export const imageRatio = async (value: Blob | undefined) => {
    const valid = true;
    if (value as Blob) {
        try {
            const { width, height } = await getFileDimensions(value as Blob);

            if (height && width) {
                const ratio = width / height;
                const valid =
                    Math.round(ratio * 10) / 10 === Math.round((THUMBNAIL_RATIO_WIDTH / THUMBNAIL_RATIO_HEIGHT) * 10) / 10;
                return valid;
            }
        } catch (error) {
            logger.error({ provider: "server", function: "imageRatio", error: getErrorMessage(error) });
            return false;
        }
    }

    return valid;
};
