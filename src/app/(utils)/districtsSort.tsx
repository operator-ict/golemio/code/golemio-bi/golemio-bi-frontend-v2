interface District {
    label: string | null;
    value: string | null;
}

export const sortAndFilterDistricts = (districtList: District[], inputValue: string): District[] => {
    // Function to determine if a label is of a numeric district
    const isNumericLabel = (label: string | null) => {
        return /^Praha \d+/.test(label || "");
    };

    // Function to extract the numeric part from a numeric district label

    const extractNumeric = (label: string | null) => {
        if (label === null) return null;
        const match = label.match(/\d+/);
        return match ? parseInt(match[0], 10) : null;
    };
    // Filtering based on input value
    const filteredDistricts = districtList.filter((district) => {
        const labelLower = district.label?.toLowerCase() || "";
        return labelLower.includes(inputValue.toLowerCase());
    });

    // Sorting logic
    filteredDistricts.sort((a, b) => {
        const isANumeric = isNumericLabel(a.label);
        const isBNumeric = isNumericLabel(b.label);

        // Both are numeric districts
        if (isANumeric && isBNumeric) {
            return (extractNumeric(a.label) || 0) - (extractNumeric(b.label) || 0);
        }

        // Only A is numeric, it should come first
        if (isANumeric) return -1;

        // Only B is numeric, it should come first
        if (isBNumeric) return 1;

        // Both are non-numeric, sort alphabetically
        return (a.label || "").localeCompare(b.label || "");
    });

    return filteredDistricts;
};
