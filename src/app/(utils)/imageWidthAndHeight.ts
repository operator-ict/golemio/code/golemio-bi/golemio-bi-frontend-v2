export const imageWidthAndHeight = (provideFile: Blob) => {
    type ImageDimensions = { width: number | null; height: number | null };
    const imgDimensions: ImageDimensions = { width: null, height: null };

    return new Promise<ImageDimensions>((resolve) => {
        const reader = new FileReader();

        reader.readAsDataURL(provideFile);
        reader.onload = function () {
            const img = new Image();
            img.src = reader.result ? reader.result.toString() : "";

            img.onload = function () {
                imgDimensions.width = img.width;
                imgDimensions.height = img.height;

                resolve(imgDimensions);
            };
        };
    });
};
