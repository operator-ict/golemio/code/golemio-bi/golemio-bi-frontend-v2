export enum EntityType {
    Dashboard = "dashboard",
    Role = "role",
    Garant = "garant",
    Users = "users",
}

export function getLabel(count: number, type: EntityType): string {
    switch (type) {
        case EntityType.Dashboard:
            if (count === 0) return "Žádné vybrané dashboardy";
            if (count === 1) return "Vybrán 1 dashboard";
            if (count >= 2 && count <= 4) return `Vybrány ${count} dashboardy`;
            return `Vybráno ${count} dashboardů`;

        case EntityType.Role:
            if (count === 0) return "Žádné vybrané role";
            if (count === 1) return "Vybrána 1 role";
            if (count >= 2 && count <= 4) return `Vybrány ${count} role`;
            return `Vybráno ${count} rolí`;

        case EntityType.Users:
            if (count === 0) return "Žádní vybraní uživatelé";
            if (count === 1) return "Vybrán 1 uživatel";
            if (count >= 2 && count <= 4) return `Vybráni ${count} uživatelé`;
            return `Vybráno ${count} uživatelů`;

        default:
            return "";
    }
}
