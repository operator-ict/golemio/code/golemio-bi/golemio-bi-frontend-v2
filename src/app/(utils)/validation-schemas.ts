import * as Yup from "yup";

import text from "@/textContent/cs.json";

import { imageRatio, THUMBNAIL_RATIO_HEIGHT, THUMBNAIL_RATIO_WIDTH } from "./imageRatio";

export const nameSchema = Yup.object({
    name: Yup.string().required("Vyplňte jméno"),
});

export const surnameSchema = Yup.object({
    surname: Yup.string().required("Vyplňte příjmení"),
});

export const emailSchema = Yup.object({
    email: Yup.string().email("Neplatný formát e-mail adresy").required("Vyplňte e-mail"),
});

export const codeSchema = Yup.object({
    code: Yup.number().required("Zadejte kód"),
});

export const passwordSchema = Yup.object({
    password: Yup.string().required("Vyplňte heslo").min(6, "Heslo musí obsahovat alespoň 6 znaků"),
});

export const organizationSchema = Yup.object({
    organization: Yup.string().required("Vyplňte název organizace"),
});

export const websiteUrlSchema = Yup.object({
    "website-url": Yup.string().trim().matches(/^$/, {
        message: "Caught in honeypot",
    }),
});

export const confirmAddressSchema = Yup.object({
    "confirm-address": Yup.string().trim().matches(/^$/, {
        message: "Caught in honeypot",
    }),
});

export const confirmCodeSchema = Yup.object({
    "confirm-code": Yup.string().trim().matches(/^$/, {
        message: "Caught in honeypot",
    }),
});

export const confirmationSchema = Yup.object({
    confirmation: Yup.boolean().oneOf([true], "Musíte souhlasit s podmínkami"),
});

const relativePathRegex = /^(\/[^/\0]+)*\/?$/;

export const MetadataValSchema = Yup.object().shape({
    title: Yup.string().required("Vyplňte název"),
    description: Yup.string().required("Vyplňte popis").max(128, "Délka popisu je omezena na 128 znaků"),
    client_route: Yup.string().matches(relativePathRegex, "Adresa musí začínat znakem '/'").required("Vyplňte URL pro uživatele"),
    component: Yup.string().required("Vyberte komponentu"),
    thumbnail: Yup.mixed()
        .test(
            "thumbnail validation",
            `${text.uploadFile.subTitle.paragraph_2} ${Math.round((THUMBNAIL_RATIO_WIDTH / THUMBNAIL_RATIO_HEIGHT) * 10) / 10}
                        (${THUMBNAIL_RATIO_WIDTH} : ${THUMBNAIL_RATIO_HEIGHT})`,
            async (value) => {
                if (value && "file" in value && value.file instanceof Blob) {
                    return await imageRatio(value.file);
                }
                return true;
            }
        )
        .test("thumbnail filesize validation", "Soubor nesmí být větší než 750 kB", async (value) => {
            if (value && "file" in value && value.file instanceof Blob) {
                const imageSize = value.file.size;
                return imageSize < 768000;
            }
            return true;
        }),

    roles: Yup.array()
        .of(Yup.string().uuid("Každá role musí být platné UUID"))
        .min(1, "Je vyžadována alespoň jedna role")
        .required("Role jsou povinné"),
    garant: Yup.string().required("Vyberte garanta dashboardu"),
});
