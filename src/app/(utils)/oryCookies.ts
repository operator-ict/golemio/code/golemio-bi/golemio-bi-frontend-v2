import { cookies } from "next/headers";

export const ORY_SESSION_COOKIE_NAME = "ory_session";

export const setOrySessionCookie = (value: string) => {
    const cookieStore = cookies();
    cookieStore.set({
        name: "ory_session",
        value,
        maxAge: 60 * 60,
    });
};

export const getOrySessionCookie = () => {
    const cookieStore = cookies();
    return cookieStore.get(ORY_SESSION_COOKIE_NAME)?.value;
};

export const deleteOrySessionCookie = () => {
    const cookieStore = cookies();
    cookieStore.delete(ORY_SESSION_COOKIE_NAME);
};
