import path from "path";

export const joinUrlPaths = (firstPath: string, ...restPaths: string[]): string => {
    if (!firstPath) {
        throw new Error("First path is required");
    }
    if (!restPaths.length) {
        throw new Error("At least one more path is required");
    }
    const isAbsolute = firstPath.trim().startsWith("https://") || firstPath.trim().startsWith("http://");
    if (!isAbsolute) {
        return path.posix.join(firstPath, ...restPaths);
    }
    const base = new URL(firstPath);
    return new URL(path.posix.join(base.pathname, ...restPaths), base).toString();
};
