import { getUserAgreementStatus } from "@/actions/formAuthActions";
import { getLogger } from "@/logging/logger";
import { getErrorMessage } from "@/utils/getErrorMessage";

const logger = getLogger("fetch-agreement-status");

export const fetchAgreementStatus = async (): Promise<boolean> => {
    try {
        const agreementStatusRes = await getUserAgreementStatus();
        if (agreementStatusRes.success) {
            logger.debug({
                provider: "server",
                function: "fetchAgreementStatus",
                message: `Agreement status: ${agreementStatusRes["agreement-status"]}`,
            });
            return agreementStatusRes["agreement-status"];
        }
    } catch (error) {
        logger.error({
            provider: "server",
            function: "fetchAgreementStatus",
            error: getErrorMessage(error),
        });
    }
    return false;
};
