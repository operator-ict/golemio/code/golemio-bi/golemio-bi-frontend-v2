import "@/styles/globals.scss";

import Image from "next/image";
import { ReactNode } from "react";
import { Toaster } from "react-hot-toast";
import { config } from "src/config";

import Footer from "@/(components)/Footer";
import NotificationBanner, { NotificationType } from "@/components/NotificationBanner";
import Skiplink from "@/components/Skiplink";
import logoPGolemio from "@/logos/standard.svg";
import text from "@/textContent/cs.json";

import styles from "./PublicLayout.module.scss";

const PublicLayout = ({ children }: { children: ReactNode }) => {
    const isNotificationVisible = config.NOTIFICATION_BANNER_SEVERITY;

    return (
        <>
            <Skiplink />
            <Toaster
                position="top-center"
                containerStyle={{
                    top: isNotificationVisible ? 48 : 0,
                    left: 0,
                    right: 0,
                }}
            />
            {isNotificationVisible ? (
                <NotificationBanner
                    type={config.NOTIFICATION_BANNER_SEVERITY as NotificationType}
                    text={config.NOTIFICATION_BANNER_TEXT}
                />
            ) : null}
            <main className={styles.main} id="main">
                <div className={styles["logo"]}>
                    <Image src={logoPGolemio} alt={`${text.logo}`} fill />
                </div>
                {children}
            </main>
            <div id="portal" />
            <Footer />
        </>
    );
};

export default PublicLayout;
