import { Heading } from "@/components/Heading";
import text from "@/textContent/cs.json";

import publiclayout from "../../PublicLayout.module.scss";
import { EmailSent } from "../(components)/EmailSent";

export const dynamic = "force-dynamic";

const RequestDashboardAccessPage = () => {
    return (
        <div className={`${publiclayout["form-container"]} ${publiclayout["form-container-Gap4"]}`}>
            <div className={publiclayout["form-container-top"]}>
                <Heading tag="h4">{text.auth.accessDashboardRequest}</Heading>
                <span>{text.auth.accessDashboardRequestInfo}</span>
            </div>
            {<EmailSent home />}
        </div>
    );
};

export default RequestDashboardAccessPage;
