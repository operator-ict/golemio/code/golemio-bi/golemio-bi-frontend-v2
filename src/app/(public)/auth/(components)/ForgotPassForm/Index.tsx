"use client";
import { Form, Formik, FormikProps } from "formik";
import { useRouter, useSearchParams } from "next/navigation";
import React from "react";
import toast from "react-hot-toast";
import * as Yup from "yup";

import { forgottenPassReq } from "@/actions/formAuthActions";
import Button from "@/components/Button";
import { ArrowRightIcon } from "@/components/icons/ArrowRightIcon";
import { EmailIcon } from "@/components/icons/formIcons/EmailIcon";
import NotificationBanner, { NotificationType } from "@/components/NotificationBanner";
import TextField from "@/components/TextField";
import text from "@/textContent/cs.json";
import { confirmAddressSchema, emailSchema } from "@/utils/validation-schemas";

import styles from "../Form.module.scss";

type Values = {
    email: string;
    "confirm-address": string;
};

const ForgotPassValSchema = Yup.object({
    ...emailSchema.fields,
    ...confirmAddressSchema.fields,
});

const ForgotPassInitValues = {
    email: "",
    "confirm-address": "",
};

export const ForgotPassForm = () => {
    const router = useRouter();
    const queryParams = useSearchParams();
    const flow = queryParams.get("flow") ?? "";

    return (
        <Formik
            initialValues={ForgotPassInitValues}
            validationSchema={ForgotPassValSchema}
            validateOnBlur={true}
            onSubmit={async (values) => {
                if (values.email) {
                    const result = await forgottenPassReq(values.email, flow);
                    if (result?.success) {
                        toast.custom(
                            <NotificationBanner type={NotificationType.Info} text={text.auth.resetEmailStatusSuccess} />
                        );
                        const updatedParams = new URLSearchParams(queryParams.toString());
                        updatedParams.set("codeSent", "success");
                        router.push(`?${updatedParams.toString()}`);
                    } else {
                        toast.custom(
                            <NotificationBanner
                                type={NotificationType.Error}
                                text={`Chyba při odesílání e-mailu: ${result?.error}`}
                            />
                        );
                    }
                }
            }}
        >
            {({ values, errors, touched, setFieldValue }: FormikProps<Values>) => (
                <Form id="forgotPassForm" className={styles["form"]}>
                    <div className={styles["fields"]}>
                        <TextField
                            type="text"
                            id="email"
                            label="E-mail"
                            placeholder="E-mail"
                            value={values.email}
                            onChange={(e) => setFieldValue("email", e.target.value)}
                            className={styles["field"]}
                            error={touched.email && !!errors.email}
                            errorMessage={errors.email?.toString()}
                            iconStart={<EmailIcon color="gray-middle" />}
                        />
                        <TextField
                            type="text"
                            id="confirm-address"
                            label="confirm-address"
                            placeholder="Adresa"
                            autoComplete="nope"
                            tabIndex={-1}
                            value={values["confirm-address"]}
                            onChange={(e) => {
                                setFieldValue("confirm-address", e.target.value);
                                toast.custom(
                                    <NotificationBanner
                                        type={"error" as NotificationType}
                                        text={"Hidden field caught in honeypot"}
                                    />
                                );
                            }}
                            className={styles["hfield"]}
                        />
                    </div>
                    <Button
                        type="submit"
                        color="primary"
                        size="lg"
                        form="forgotPassForm"
                        className={styles["form-button"]}
                        iconEnd={<ArrowRightIcon color="white" />}
                    >
                        {text.auth.resetButton}
                    </Button>
                </Form>
            )}
        </Formik>
    );
};
