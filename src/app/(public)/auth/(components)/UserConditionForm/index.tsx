"use client";
import { Form, Formik } from "formik";
import React from "react";

import { NotificationType } from "@/(components)/NotificationBanner";
import { showToast } from "@/(utils)/showToast";
import { acceptConditions } from "@/actions/formAuthActions";
import Button from "@/components/Button";
import CheckBoxField from "@/components/CheckBoxField";
import { useAgreement } from "@/context/AgreementContext";
import SubmitIcon from "@/icons/formIcons/SubmitIcon";
import text from "@/textContent/cs.json";
import { confirmationSchema } from "@/utils/validation-schemas";

import styles from "./UserConditionsForm.module.scss";

const UserConditionsValues = {
    confirmation: false,
};

export const UserConditionsForm = () => {
    const { setAgreement } = useAgreement();
    return (
        <Formik
            initialValues={UserConditionsValues}
            validationSchema={confirmationSchema}
            validateOnBlur={true}
            validateOnChange={true}
            onSubmit={async (values) => {
                const confirmation = values.confirmation;
                if (!confirmation) {
                    return;
                }
                const res = await acceptConditions();
                if (res.success) {
                    setAgreement(true);
                } else {
                    showToast("error" as NotificationType, `Nepodařilo se uložit souhlas s podmínkami, chyba: ${res.error}`);
                }
            }}
        >
            {({ values, errors, touched, setFieldValue }) => (
                <Form className={styles["user-conditions-form"]} id="userConditionsForm">
                    <CheckBoxField
                        id="confirmation"
                        label="Souhlasím s Podmínkami užití a potvrzuji, že jsem se s nimi seznámil/a."
                        isChecked={values.confirmation}
                        onChange={(e) => {
                            setFieldValue("confirmation", e.target.checked);
                        }}
                        error={touched.confirmation && !!errors.confirmation}
                    />
                    <Button
                        type="submit"
                        color="primary"
                        size="md"
                        form="userConditionsForm"
                        className={styles["user-conditions-form-btn"]}
                        iconStart={<SubmitIcon color="white" width="1rem" height="1rem" />}
                    >
                        {text.usageAgreement.button}
                    </Button>
                </Form>
            )}
        </Formik>
    );
};
