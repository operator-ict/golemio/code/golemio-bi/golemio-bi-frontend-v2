import parse from "html-react-parser";
import DOMPurify from "isomorphic-dompurify";
import React from "react";

import { Modal } from "@/components/Modal";
import { Paragraph } from "@/components/Paragraph";
import { Title } from "@/components/Title";
import text from "@/textContent/cs.json";

import styles from "../ConditionsDialog/ConditionsDialog.module.scss";

type PersonalsDialogProps = {
    show: boolean;
    onClose: () => void;
};

export const PersonalsDialog = ({ show, onClose }: PersonalsDialogProps) => {
    const { sanitize } = DOMPurify;
    return (
        <Modal show={show} onClose={onClose} label={text.personalDataProcessing.mainTitle}>
            <div className={styles["dialog-frame"]}>
                <div className={styles["dialog-text-area"]}>
                    <Title type="t3m">{text.personalDataProcessing.title01}</Title>
                    <Paragraph type="c1">{text.personalDataProcessing.par01}</Paragraph>
                    <Title type="t3m">{text.personalDataProcessing.title02}</Title>
                    <Paragraph type="c1">{text.personalDataProcessing.par02}</Paragraph>
                    <Paragraph type="c1">{parse(sanitize(text.personalDataProcessing.par03))}</Paragraph>
                    <Paragraph type="c1">{text.personalDataProcessing.par04}</Paragraph>
                </div>
            </div>
        </Modal>
    );
};
