"use client";
import { Field, Form, Formik, FormikProps } from "formik";
import { useRouter } from "next/navigation";
import React, { ChangeEvent, ClipboardEvent, KeyboardEvent, useEffect, useRef } from "react";

import Button from "@/components/Button";
import { ArrowLeftIcon } from "@/icons/ArrowLeftIcon";
import { ArrowRightIcon } from "@/icons/ArrowRightIcon";
import text from "@/textContent/cs.json";

import styles from "../Form.module.scss";

const createOTPConfig = (length: number, isNumericOnly: boolean) => ({
    length,
    isNumericOnly,
    pattern: {
        input: isNumericOnly ? /^\d$/ : /^[a-zA-Z0-9]$/,
        fullCode: new RegExp(`^${isNumericOnly ? "\\d" : "[a-zA-Z0-9]"}{${length}}$`),
        clean: isNumericOnly ? /\D/g : /[^a-zA-Z0-9]/g,
    },
});

const OTP_CONFIG = createOTPConfig(6, true);

const CodeInitValues: Values = {
    code: new Array(OTP_CONFIG.length).fill(""),
};

type Values = {
    code: string[];
};

type OTPFormProps = {
    onSubmit: (code: string) => Promise<void>;
    flowId: string;
    formId?: string;
};

export const OTPForm = ({ onSubmit, formId = "OTPForm" }: OTPFormProps) => {
    const router = useRouter();
    const inputsRef = useRef<HTMLInputElement[]>([]);

    const allValuesFilled = (values: Values) => values.code.every((value) => value !== "");

    const handlePaste = (e: ClipboardEvent<HTMLInputElement>, setFieldValue: (field: string, value: string) => void) => {
        const pasteData = e.clipboardData.getData("Text");
        if (OTP_CONFIG.pattern.fullCode.test(pasteData)) {
            const digits = pasteData.split("");
            digits.forEach((digit, index) => {
                setFieldValue(`code.${index}`, digit);
            });
        }
        e.preventDefault();
    };

    const handleKeyDown = (
        e: KeyboardEvent<HTMLInputElement>,
        index: number,
        values: Values,
        setFieldValue: (field: string, value: string) => void
    ) => {
        if (e.key === "Backspace" && !values.code[index] && index > 0) {
            inputsRef.current[index - 1]?.focus();
            setFieldValue(`code.${index - 1}`, "");
        } else if (OTP_CONFIG.pattern.input.test(e.key) && index < OTP_CONFIG.length - 1) {
            setTimeout(() => {
                inputsRef.current[index + 1]?.focus();
            }, 0);
        }
    };

    useEffect(() => {
        inputsRef.current[0]?.focus();
    }, []);

    return (
        <>
            <Formik
                initialValues={CodeInitValues}
                onSubmit={async (values) => {
                    if (!allValuesFilled(values)) return;
                    const code = values.code.join("");
                    await onSubmit(code);
                }}
            >
                {({ values, setFieldValue }: FormikProps<Values>) => (
                    <Form className={styles["form"]} id={formId}>
                        <div className={styles["form-code-inputs"]}>
                            {values.code.map((digit, index) => (
                                <Field
                                    key={index}
                                    name={`code.${index}`}
                                    id={`code.${index}`}
                                    type="text"
                                    maxLength={1}
                                    value={digit}
                                    inputMode={OTP_CONFIG.isNumericOnly ? "numeric" : "text"}
                                    pattern={OTP_CONFIG.pattern.input.source}
                                    innerRef={(el: HTMLInputElement) => {
                                        inputsRef.current[index] = el;
                                    }}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                                        const value = e.target.value.replace(OTP_CONFIG.pattern.clean, "");
                                        setFieldValue(`code.${index}`, value);
                                    }}
                                    onKeyDown={(e: KeyboardEvent<HTMLInputElement>) =>
                                        handleKeyDown(e, index, values, setFieldValue)
                                    }
                                    onPaste={(e: ClipboardEvent<HTMLInputElement>) => handlePaste(e, setFieldValue)}
                                    className={styles["form-code-input"]}
                                />
                            ))}
                        </div>
                        <div className={styles["form-buttons"]}>
                            <Button
                                type="button"
                                color="tertiary"
                                size="lg"
                                className={styles["form-button"]}
                                iconStart={<ArrowLeftIcon color="black" />}
                                onClick={() => router.back()}
                                aria-label={text.button.back}
                            >
                                {text.button.back}
                            </Button>
                            <Button
                                type="submit"
                                color={allValuesFilled(values) ? "primary" : "secondary"}
                                size="lg"
                                form={formId}
                                disabled={!allValuesFilled(values)}
                                className={styles["button"]}
                                iconEnd={<ArrowRightIcon color="white" />}
                                aria-label={text.auth.accountConfirmationButton}
                            >
                                {text.auth.accountConfirmationButton}
                            </Button>
                        </div>
                    </Form>
                )}
            </Formik>
        </>
    );
};
