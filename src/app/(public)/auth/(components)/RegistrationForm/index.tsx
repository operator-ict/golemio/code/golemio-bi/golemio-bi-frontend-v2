"use client";
import { Form, Formik, FormikProps } from "formik";
import Link from "next/link";
import { useRouter, useSearchParams } from "next/navigation";
import React, { useState } from "react";
import toast from "react-hot-toast";
import * as Yup from "yup";

import { accessReqKratos } from "@/actions/formAuthActions";
import Button from "@/components/Button";
import CheckBoxField from "@/components/CheckBoxField";
import NotificationBanner, { NotificationType } from "@/components/NotificationBanner";
import TextField from "@/components/TextField";
import TextOptionsField from "@/components/TextOptionsField";
import { ArrowRightIcon } from "@/icons/ArrowRightIcon";
import { EyeIcon } from "@/icons/EyeIcon";
import { EyeOffIcon } from "@/icons/EyeOffIcon";
import { EmailIcon } from "@/icons/formIcons/EmailIcon";
import { OrganizationIcon } from "@/icons/formIcons/OrganizationIcon";
import { PasswordIcon } from "@/icons/formIcons/PasswordIcon";
import text from "@/textContent/cs.json";
import { Result } from "@/types/formTypes";
import {
    confirmationSchema,
    emailSchema,
    nameSchema,
    organizationSchema,
    passwordSchema,
    surnameSchema,
    websiteUrlSchema,
} from "@/utils/validation-schemas";

import { ConditionsDialog } from "../ConditionsDialog";
import styles from "../Form.module.scss";

type Values = {
    name: string;
    surname: string;
    email: string;
    password: string;
    organization: string;
    confirmation: boolean;
    "website-url": string;
};

const RegistrationValSchema = Yup.object({
    ...nameSchema.fields,
    ...surnameSchema.fields,
    ...emailSchema.fields,
    ...passwordSchema.fields,
    ...organizationSchema.fields,
    ...confirmationSchema.fields,
    ...websiteUrlSchema.fields,
});

const RegistrationInitValues = {
    name: "",
    surname: "",
    email: "",
    password: "",
    organization: "",
    confirmation: false,
    "website-url": "",
};

export const RegistrationForm = ({ organizations }: { organizations: Array<string> }) => {
    const router = useRouter();
    const [showConditions, setShowConditions] = useState(false);
    const [passVisible, setPassVisible] = useState(false);
    const queryParams = useSearchParams();
    const flowId = queryParams.get("flow");

    const linkedModalHandler = () => {
        setShowConditions(true);
    };

    const handleAccessRequest = async (values: Values, flowId: string | null) => {
        const { name, surname, email, password, organization, confirmation } = values;

        if (!name || !surname || !email || !organization || !confirmation || !password) {
            return;
        }

        const result = await accessReqKratos(
            {
                name,
                surname,
                email,
                password,
                organization,
                conditions_accepted: confirmation,
            },
            flowId
        );

        handleAccessRequestResult(result);
    };

    const handleAccessRequestResult = (result: Result) => {
        if (result?.success) {
            toast.custom(<NotificationBanner type={NotificationType.Info} text={text.auth.accessRequestInfoToast} />);
            const updatedParams = new URLSearchParams(queryParams.toString());
            updatedParams.set("status", "success");
            updatedParams.set("continueId", result.continueId ?? "");
            router.push(`?${updatedParams.toString()}`);
        } else if (result?.status === 410) {
            // The flow expired, let's request a new one. see https://www.ory.sh/docs/kratos/concepts/ui-user-interface
            router.push("/auth/request-access");
            router.refresh();
            // TODO vyresetovat i formulář nebo aspoň odznačit checkbox souhlasu
            toast.custom(<NotificationBanner type={NotificationType.Warning} text={text.auth.accessRequestErr410} />);
        } else {
            handleError(result.status);
        }
    };

    const handleError = (status?: number) => {
        let errorMessage = text.auth.accessRequestErr500;

        switch (status) {
            case 400:
                errorMessage = text.auth.accessRequestErr400;
                break;
            case 403:
                errorMessage = text.auth.accessRequestErr403;
                break;
        }

        toast.custom(<NotificationBanner type={NotificationType.Error} text={errorMessage} />);
    };

    return (
        <>
            <Formik
                initialValues={RegistrationInitValues}
                validationSchema={RegistrationValSchema}
                validateOnBlur={true}
                onSubmit={async (values) => {
                    await handleAccessRequest(values, flowId);
                }}
            >
                {({ values, errors, touched, setFieldValue }: FormikProps<Values>) => (
                    <Form id="registrationForm" className={styles["form"]}>
                        <div className={styles["fields"]}>
                            <TextField
                                type="text"
                                id="name"
                                label="Jméno"
                                placeholder="Jméno"
                                autoComplete="given-name"
                                value={values.name}
                                onChange={(e) => setFieldValue("name", e.target.value)}
                                className={styles["field"]}
                                error={touched.name && !!errors.name}
                                errorMessage={errors.name?.toString()}
                            />
                            <TextField
                                type="text"
                                id="surname"
                                label="Příjmení"
                                placeholder="Příjmení"
                                autoComplete="family-name"
                                value={values.surname}
                                onChange={(e) => setFieldValue("surname", e.target.value)}
                                className={styles["field"]}
                                error={touched.surname && !!errors.surname}
                                errorMessage={errors.surname?.toString()}
                            />
                            <TextField
                                type="text"
                                id="email"
                                label="E-mail"
                                placeholder="E-mail"
                                autoComplete="email"
                                value={values.email}
                                onChange={(e) => setFieldValue("email", e.target.value)}
                                className={styles["field"]}
                                error={touched.email && !!errors.email}
                                errorMessage={errors.email?.toString()}
                                iconStart={<EmailIcon color="gray-middle" />}
                            />
                            <TextField
                                type={passVisible ? "text" : "password"}
                                id="password"
                                label="Heslo"
                                placeholder="Heslo"
                                autoComplete="nope"
                                value={values.password}
                                onChange={(e) => setFieldValue("password", e.target.value)}
                                className={styles["field"]}
                                error={touched.password && !!errors.password}
                                errorMessage={errors.password?.toString()}
                                iconStart={<PasswordIcon color="gray-middle" />}
                                iconEnd={passVisible ? <EyeIcon color="gray-middle" /> : <EyeOffIcon color="gray-middle" />}
                                onIconClick={() => setPassVisible(!passVisible)}
                            />
                            <TextOptionsField
                                type="text"
                                id="organization"
                                label="Organizace"
                                placeholder="Organizace"
                                options={organizations}
                                value={values.organization}
                                onChange={(e) => setFieldValue("organization", e.target.value)}
                                className={styles["field"]}
                                error={touched.organization && !!errors.organization}
                                errorMessage={errors.organization?.toString()}
                                iconStart={<OrganizationIcon color="gray-middle" />}
                                autoComplete="off"
                            />
                            <CheckBoxField
                                id="confirmation"
                                label={
                                    <>
                                        Souhlasím s{" "}
                                        <Link href="#" onClick={linkedModalHandler} className={styles["link-underlined"]}>
                                            Podmínkami užití
                                        </Link>{" "}
                                        a potvrzuji, že jsem se s nimi seznámil/a.
                                    </>
                                }
                                isChecked={values.confirmation}
                                onChange={(e) => setFieldValue("confirmation", e.target.checked)}
                                error={touched.confirmation && !!errors.confirmation}
                            />
                            <TextField
                                type="text"
                                id="website-url"
                                label="website-url"
                                placeholder="website-url"
                                autoComplete="nope"
                                tabIndex={-1}
                                value={values["website-url"]}
                                onChange={(e) => setFieldValue("website-url", e.target.value)}
                                className={styles["hfield"]}
                            />
                        </div>
                        <Button
                            type="submit"
                            color="primary"
                            size="lg"
                            form="registrationForm"
                            className={styles["form-button"]}
                            iconEnd={<ArrowRightIcon color="white" />}
                        >
                            {text.auth.signUpButton}
                        </Button>
                    </Form>
                )}
            </Formik>
            <ConditionsDialog show={showConditions} onClose={() => setShowConditions(false)} />
        </>
    );
};
