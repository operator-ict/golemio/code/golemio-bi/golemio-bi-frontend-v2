import React from "react";

import { ConditionsText } from "@/(public)/auth/(components)/ConditionsText";
import { Modal } from "@/components/Modal";
import text from "@/textContent/cs.json";

import styles from "./ConditionsDialog.module.scss";

type ConditionsDialogProps = {
    show: boolean;
    onClose: () => void;
};

export const ConditionsDialog = ({ show, onClose }: ConditionsDialogProps) => {
    return (
        <Modal show={show} onClose={onClose} label={text.usageConditions.mainTitle}>
            <div className={styles["dialog-frame"]}>
                <div className={styles["dialog-text-area"]}>
                    <ConditionsText />
                </div>
            </div>
        </Modal>
    );
};
