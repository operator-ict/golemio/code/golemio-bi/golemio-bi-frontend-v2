"use client";
import { Form, Formik, FormikProps } from "formik";
import Link from "next/link";
import { useSearchParams } from "next/navigation";
import { signIn } from "next-auth/react";
import React, { useState } from "react";
import * as Yup from "yup";

import { checkLoggedIn } from "@/actions/formAuthActions";
import Button from "@/components/Button";
import { NotificationType } from "@/components/NotificationBanner";
import TextField from "@/components/TextField";
import { ArrowRightIcon } from "@/icons/ArrowRightIcon";
import { EyeIcon } from "@/icons/EyeIcon";
import { EyeOffIcon } from "@/icons/EyeOffIcon";
import { EmailIcon } from "@/icons/formIcons/EmailIcon";
import { PasswordIcon } from "@/icons/formIcons/PasswordIcon";
import { getLogger } from "@/logging/logger";
import text from "@/textContent/cs.json";
import { emailScrambler } from "@/utils/emailScrambler";
import { showToast } from "@/utils/showToast";
import { emailSchema, passwordSchema, websiteUrlSchema } from "@/utils/validation-schemas";

import styles from "../Form.module.scss";

type Values = {
    email: string;
    password: string;
    "website-url": string;
};

type Result =
    | {
          error: string | null;
          status: number;
          ok: boolean;
          url: string | null;
      }
    | undefined;

const logger = getLogger("SignInForm");

const signInValSchema = Yup.object({
    ...emailSchema.fields,
    ...passwordSchema.fields,
    ...websiteUrlSchema.fields,
});

const signInInitValues = {
    email: "",
    password: "",
    "website-url": "",
};

export const SignInForm = () => {
    const [passVisible, setPassVisible] = useState(false);
    const fromUrl = useSearchParams().get("from");
    const flowUrl = useSearchParams().get("flow");

    const handleSignIn = async (email: string, password: string, fromUrl?: string | null, flow?: string | null) => {
        if (!email || !password) {
            showToast("error" as NotificationType, "Email and password are required.");
            return;
        }

        const result = await signIn("golemiobi-auth", {
            email,
            password,
            flow,
            redirect: false,
        });

        if (!result?.ok) {
            handleSignInError(result);
            return;
        }

        showToast("info" as NotificationType, "Přihlášení proběhlo úspěšně.");
        const loggedInResult = await checkLoggedIn();

        if (!loggedInResult?.success) {
            showToast("error" as NotificationType, `Uživatel nepřihlášen, chyba: ${loggedInResult?.error}`);
            return;
        }

        logger.debug({ provider: "client", function: "handleSignIn", message: `User ${emailScrambler(email)} logged in` });
        window.location.replace(fromUrl ?? "/");
    };

    const handleSignInError = (result: Result) => {
        if (result?.status === 401) {
            logger.error({
                provider: "client",
                function: "handleSignInError",
                message: `Wrong signin credentials, result error ${result.error} | status: ${result.status}`,
            });
            showToast("error" as NotificationType, "Přihlášení se nezdařilo, chybné přihlašovací údaje.");
        } else {
            logger.error({
                provider: "client",
                function: "handleSignInError",
                message: `Signin failed, result error ${result?.error}`,
            });
            showToast("error" as NotificationType, `Přihlášení se nezdařilo, chyba: ${result?.error}`);
        }
    };

    return (
        <Formik
            initialValues={signInInitValues}
            validationSchema={signInValSchema}
            validateOnBlur={true}
            onSubmit={async (values) => {
                if (values.email && values.password) {
                    await handleSignIn(values.email, values.password, fromUrl, flowUrl);
                }
            }}
        >
            {({ values, errors, touched, setFieldValue }: FormikProps<Values>) => (
                <Form id="signInForm" className={styles["form"]}>
                    <div className={styles["fields"]}>
                        <TextField
                            type="text"
                            id="email"
                            label="E-mail"
                            placeholder="E-mail"
                            autoComplete="email"
                            value={values.email}
                            onChange={(e) => setFieldValue("email", e.target.value)}
                            className={styles["field"]}
                            error={touched.email && !!errors.email}
                            errorMessage={errors.email?.toString()}
                            iconStart={<EmailIcon color="gray-middle" />}
                        />
                        <TextField
                            type={passVisible ? "text" : "password"}
                            id="password"
                            label="Heslo"
                            placeholder="Heslo"
                            autoComplete="current-password"
                            value={values.password}
                            onChange={(e) => setFieldValue("password", e.target.value)}
                            className={styles["field"]}
                            error={touched.password && !!errors.password}
                            errorMessage={errors.password?.toString()}
                            iconStart={<PasswordIcon color="gray-middle" />}
                            iconEnd={passVisible ? <EyeIcon color="gray-middle" /> : <EyeOffIcon color="gray-middle" />}
                            onIconClick={() => setPassVisible(!passVisible)}
                        />
                        <TextField
                            type="text"
                            id="website-url"
                            label="website-url"
                            placeholder="website-url"
                            autoComplete="nope"
                            tabIndex={-1}
                            value={values["website-url"]}
                            onChange={(e) => setFieldValue("website-url", e.target.value)}
                            className={styles["hfield"]}
                        />
                        <Link href="/auth/forgot-password" className={styles["link"]}>
                            {text.auth.forgottenPassword}
                        </Link>
                    </div>
                    <Button
                        type="submit"
                        color="primary"
                        size="lg"
                        form="signInForm"
                        className={styles["form-button"]}
                        iconEnd={<ArrowRightIcon color="white" />}
                    >
                        {text.auth.signInButton}
                    </Button>
                </Form>
            )}
        </Formik>
    );
};
