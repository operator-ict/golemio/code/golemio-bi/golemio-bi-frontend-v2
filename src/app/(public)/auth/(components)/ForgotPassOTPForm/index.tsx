"use client";
import { useRouter, useSearchParams } from "next/navigation";
import React from "react";
import toast from "react-hot-toast";

import { forgottenPassCodeReq } from "@/actions/formAuthActions";
import NotificationBanner, { NotificationType } from "@/components/NotificationBanner";
import text from "@/textContent/cs.json";

import { OTPForm } from "../OTPForm";

export const ForgotPassOTPForm = () => {
    const router = useRouter();
    const flowId = useSearchParams().get("flow") ?? "";

    const handleSubmit = async (code: string) => {
        const result = await forgottenPassCodeReq(code, flowId);
        if (result?.success) {
            toast.custom(<NotificationBanner type={NotificationType.Info} text={text.auth.resetEmailStatusSuccess} />);
            router.push(`/auth/reset-password/${flowId}?flow=${result.settingsToken}`);
        } else {
            toast.custom(
                <NotificationBanner type={NotificationType.Error} text={`Chyba při verifikaci kódu: ${result?.error}`} />
            );
            router.push(`/auth/forgot-password`);
        }
    };

    return <OTPForm onSubmit={handleSubmit} flowId={flowId} />;
};
