"use client";
import { useRouter, useSearchParams } from "next/navigation";
import React from "react";
import toast from "react-hot-toast";

import { verifyEmail } from "@/actions/formAuthActions";
import NotificationBanner, { NotificationType } from "@/components/NotificationBanner";
import text from "@/textContent/cs.json";

import { OTPForm } from "../OTPForm";

export const RegistrationOTPForm = () => {
    const router = useRouter();
    const continueId = useSearchParams().get("continueId") ?? "";

    const handleSubmit = async (code: string) => {
        const validateCode = await verifyEmail(code, continueId);
        if (validateCode.success) {
            toast.custom(<NotificationBanner type={NotificationType.Info} text={text.auth.verifySuccess} />);
            router.push("/auth/sign-in");
        } else {
            toast.custom(<NotificationBanner type={NotificationType.Error} text={`Chyba při ověření: ${validateCode.error}`} />);
            router.push("/auth/request-access");
        }
    };

    return <OTPForm onSubmit={handleSubmit} flowId={continueId} />;
};
