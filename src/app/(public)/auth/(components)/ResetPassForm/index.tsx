"use client";
import { Form, Formik, FormikProps } from "formik";
import { useRouter, useSearchParams } from "next/navigation";
import React, { useEffect, useState } from "react";
import toast from "react-hot-toast";
import * as Yup from "yup";

import { EyeIcon } from "@/(components)/icons/EyeIcon";
import { EyeOffIcon } from "@/(components)/icons/EyeOffIcon";
import { forgottenPassReset } from "@/actions/formAuthActions";
import Button from "@/components/Button";
import { ArrowRightIcon } from "@/components/icons/ArrowRightIcon";
import { PasswordIcon } from "@/components/icons/formIcons/PasswordIcon";
import NotificationBanner, { NotificationType } from "@/components/NotificationBanner";
import TextField from "@/components/TextField";
import text from "@/textContent/cs.json";
import { ResetPassFormProps } from "@/types/formTypes";
import { confirmAddressSchema, passwordSchema } from "@/utils/validation-schemas";

import styles from "../Form.module.scss";

type Values = {
    password: string;
    "confirm-address": string;
};

const ResetPassValSchema = Yup.object({
    ...passwordSchema.fields,
    ...confirmAddressSchema.fields,
});

const ResetPassInitValues = {
    password: "",
    "confirm-address": "",
};

export const ResetPassForm = ({ sessionToken, disabled, error }: ResetPassFormProps) => {
    const router = useRouter();
    const queryParams = useSearchParams();
    const flowId = queryParams.get("flow") ?? "";

    const [passVisible, setPassVisible] = useState(false);

    useEffect(() => {
        if (error) {
            toast.custom(<NotificationBanner type={NotificationType.Error} text={`Chyba při ověření: ${error}`} />);
        }
    }, [error]);

    return (
        <Formik
            initialValues={ResetPassInitValues}
            validationSchema={ResetPassValSchema}
            validateOnBlur={true}
            onSubmit={async (values) => {
                if (values.password) {
                    const result = await forgottenPassReset(flowId, sessionToken, values.password);
                    if (result?.success) {
                        toast.custom(
                            <NotificationBanner type={NotificationType.Info} text={text.auth.resetPasswordStatusSuccess} />
                        );
                        router.push("/auth/sign-in");
                    } else {
                        toast.custom(<NotificationBanner type={NotificationType.Error} text={`Error: ${result.error}`} />);
                    }
                }
            }}
        >
            {({ values, errors, touched, setFieldValue }: FormikProps<Values>) => (
                <Form id="resetPassForm" className={styles["form"]}>
                    <div className={styles["fields"]}>
                        <TextField
                            type={passVisible ? "text" : "password"}
                            id="password"
                            label="Nové heslo"
                            placeholder="Nové heslo"
                            autoComplete="nope"
                            value={values.password}
                            onChange={(e) => setFieldValue("password", e.target.value)}
                            className={styles["field"]}
                            error={touched.password && !!errors.password}
                            errorMessage={errors.password?.toString()}
                            disabled={disabled}
                            iconStart={<PasswordIcon color="gray-middle" />}
                            iconEnd={passVisible ? <EyeIcon color="gray-middle" /> : <EyeOffIcon color="gray-middle" />}
                            onIconClick={() => setPassVisible(!passVisible)}
                        />
                        <TextField
                            type="text"
                            id="confirm-address"
                            label="confirm-address"
                            placeholder="Adresa"
                            autoComplete="nope"
                            tabIndex={-1}
                            value={values["confirm-address"]}
                            onChange={(e) => {
                                setFieldValue("confirm-address", e.target.value);
                                toast.custom(
                                    <NotificationBanner type={NotificationType.Error} text={"Hidden field cought in honeypot"} />
                                );
                            }}
                            className={styles["hfield"]}
                        />
                    </div>
                    <Button
                        type="submit"
                        color="primary"
                        size="lg"
                        form="resetPassForm"
                        className={styles["form-button"]}
                        iconEnd={<ArrowRightIcon color="white" />}
                        disabled={disabled}
                    >
                        {text.auth.resetButton}
                    </Button>
                </Form>
            )}
        </Formik>
    );
};
