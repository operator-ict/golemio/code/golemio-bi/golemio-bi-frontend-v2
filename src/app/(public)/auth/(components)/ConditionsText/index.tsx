import parse from "html-react-parser";
import DOMPurify from "isomorphic-dompurify";
import React from "react";

import { Paragraph } from "@/components/Paragraph";
import { Title } from "@/components/Title";
import text from "@/textContent/cs.json";

export const ConditionsText = () => {
    const { sanitize } = DOMPurify;
    return (
        <>
            <Title type="t3m">{text.usageConditions.title01}</Title>
            <Paragraph type="c1">{text.usageConditions.par11}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par12}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par13}</Paragraph>
            <Title type="t3m">{text.usageConditions.title02}</Title>
            <Paragraph type="c1">{text.usageConditions.par21}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par22}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par23}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par24}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par25}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par26}</Paragraph>
            <Title type="t3m">{text.usageConditions.title03}</Title>
            <Paragraph type="c1">{parse(sanitize(text.usageConditions.par31))}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par32}</Paragraph>
            <Title type="t3m">{text.usageConditions.title04}</Title>
            <Paragraph type="c1">{text.usageConditions.par41}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par42}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par43}</Paragraph>
            <Paragraph type="c1">{parse(sanitize(text.usageConditions.par44))}</Paragraph>
            <Title type="t3m">{text.usageConditions.title05}</Title>
            <Paragraph type="c1">{text.usageConditions.par51}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par52}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par53}</Paragraph>
            <Paragraph type="c1">{parse(sanitize(text.usageConditions.par54))}</Paragraph>
            <Title type="t3m">{text.usageConditions.title06}</Title>
            <Paragraph type="c1">{text.usageConditions.par61}</Paragraph>
            <Title type="t3m">{text.usageConditions.title07}</Title>
            <Paragraph type="c1">{text.usageConditions.par71}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par72}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par73}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par74}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par75}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par76}</Paragraph>
            <Title type="t3m">{text.usageConditions.title08}</Title>
            <Paragraph type="c1">{text.usageConditions.par81}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par82}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par83}</Paragraph>
            <Paragraph type="c1">{text.usageConditions.par84}</Paragraph>
        </>
    );
};
