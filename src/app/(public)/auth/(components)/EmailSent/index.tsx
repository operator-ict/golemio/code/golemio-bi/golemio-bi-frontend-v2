"use client";
import { useRouter } from "next/navigation";
import React from "react";

import { HomeIcon } from "@/(components)/icons/HomeIcon";
import Button from "@/components/Button";
import { ArrowLeftIcon } from "@/icons/ArrowLeftIcon";
import text from "@/textContent/cs.json";

import styles from "../Form.module.scss";

type EmailSentProps = {
    home?: boolean;
};

export const EmailSent = ({ home }: EmailSentProps) => {
    const router = useRouter();
    return (
        <div className={styles["form"]}>
            <Button
                type="submit"
                color="tertiary"
                size="lg"
                form="signInForm"
                className={styles["form-button"]}
                iconStart={home ? <HomeIcon color="black" /> : <ArrowLeftIcon color="black" />}
                onClick={home ? () => router.push("/") : () => router.back()}
            >
                {!home ? text.button.back : text.navBar.home}
            </Button>
        </div>
    );
};
