"use client";
import parse from "html-react-parser";
import DOMPurify from "isomorphic-dompurify";
import React from "react";

import { ConditionsText } from "@/(public)/auth/(components)/ConditionsText";
import { UserConditionsForm } from "@/(public)/auth/(components)/UserConditionForm";
import { Modal } from "@/components/Modal";
import { useAgreement } from "@/context/AgreementContext";
import text from "@/textContent/cs.json";

import styles from "./ConditionsDialogStrict.module.scss";

export const ConditionsDialogStrict = () => {
    const { isAgreed } = useAgreement();
    const { sanitize } = DOMPurify;
    return (
        <Modal show={!isAgreed} label={text.usageConditions.mainTitle}>
            <div className={styles["dialog-container"]}>
                <div className={styles["dialog-header-text"]}>{parse(sanitize(text.usageConditions.letter))}</div>
                <div className={styles["dialog-text-frame"]}>
                    <div className={styles["dialog-text-area"]}>
                        <ConditionsText />
                    </div>
                </div>
                <UserConditionsForm />
            </div>
        </Modal>
    );
};
