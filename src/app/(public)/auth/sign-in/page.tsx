import Link from "next/link";
import { redirect } from "next/navigation";
import { config } from "src/config";

import { getLogger } from "@/(logging)/logger";
import { TokenService } from "@/(services)/token-service";
import { Heading } from "@/components/Heading";
import text from "@/textContent/cs.json";

import publiclayout from "../../PublicLayout.module.scss";
import styles from "../(components)/Form.module.scss";
import { SignInForm } from "../(components)/SignInForm";

const logger = getLogger("SignInPage");

const SignInPage = async () => {
    const isLoggedIn = await TokenService.getIsLoggedIn();
    if (isLoggedIn) {
        logger.debug({ provider: "server", page: "SignInPage", message: "User is already logged in" });
        redirect(`${config.NEXTAUTH_URL}`);
    }
    return (
        <div className={publiclayout["form-container"]}>
            <div className={publiclayout["form-container-top"]}>
                <Heading tag="h4">{text.auth.siginIn}</Heading>
                <span>
                    {text.auth.signUpOffer}
                    <Link href="/auth/request-access" className={styles["link"]}>
                        {text.auth.signUpOfferLink}
                    </Link>
                </span>
            </div>
            <SignInForm />
        </div>
    );
};

export default SignInPage;
