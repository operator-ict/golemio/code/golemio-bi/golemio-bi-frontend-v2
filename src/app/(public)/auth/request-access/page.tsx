import Link from "next/link";

import { Heading } from "@/components/Heading";
import text from "@/textContent/cs.json";

import publiclayout from "../../PublicLayout.module.scss";
import styles from "../(components)/Form.module.scss";
import { RegistrationForm } from "../(components)/RegistrationForm";
import { RegistrationOTPForm } from "../(components)/RegistrationOTPForm";
import { organizations } from "./organizations-list";

type PageProps = {
    searchParams: {
        status: string;
    };
};

const SignUpPage = ({ searchParams }: PageProps) => {
    const emailSent = searchParams.status === "success";

    return (
        <div className={publiclayout["form-container"]}>
            <div className={publiclayout["form-container-top"]}>
                <Heading tag="h4">{emailSent ? text.auth.accessRequest : text.auth.signUp}</Heading>
                <span>
                    {emailSent ? text.auth.accessRequestInfo : text.auth.signInOffer}
                    {!emailSent && (
                        <Link href="/auth/sign-in" className={styles["link"]}>
                            {text.auth.signInOfferLink}
                        </Link>
                    )}
                </span>
            </div>
            {emailSent ? <RegistrationOTPForm /> : <RegistrationForm organizations={organizations} />}
        </div>
    );
};
export default SignUpPage;
