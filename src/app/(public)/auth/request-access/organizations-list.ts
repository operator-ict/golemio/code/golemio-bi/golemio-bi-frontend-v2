export const organizations = [
    "AVE CZ odpadové hospodářství s.r.o.",
    "České vysoké učení technické v Praze",
    "Deloitte Advisory s.r.o.",
    "Dopravní podnik hlavního města Prahy, akciová společnost",
    "Integrovaná doprava Středočeského kraje, příspěvková organizace",
    "Institut plánování a rozvoje hlavního města Prahy",
    "Komwag, podnik čistoty a údržby města, a.s.",
    "Krajský úřad Středočeského kraje",
    "Kreativní Praha z. ú.",
    "Marius Pedersen a.s.",
    "Nextbike Czech Republic s.r.o.",
    "Operátor ICT, a.s.",
    "Prague City Tourism",
    "Magistrát hlavního města Prahy",
    "Městská část Praha 1",
    "Městská část Praha 2",
    "Městská část Praha 3",
    "Městská část Praha 4",
    "Městská část Praha 5",
    "Městská část Praha 6",
    "Městská část Praha 7",
    "Městská část Praha 8",
    "Městská část Praha 9",
    "Městská část Praha 10",
    "Městská část Praha 11",
    "Městská část Praha 12",
    "Městská část Praha 13",
    "Městská část Praha 14",
    "Městská část Praha 15",
    "Městská část Praha 16",
    "Městská část Praha 17",
    "Městská část Praha 18",
    "Městská část Praha 19",
    "Městská část Praha 20",
    "Městská část Praha 21",
    "Městská část Praha 22",
    "Pražská energetika, a.s.",
    "Pražské služby, a.s.",
    "Pražské vodovody a kanalizace, a.s.",
    "Rekola Bikesharing s.r.o.",
    "Regionální organizátor pražské integrované dopravy",
    "Technická správa komunikací hlavního města Prahy, a.s.",
    "TRADE CENTRE PRAHA a.s.",
];
