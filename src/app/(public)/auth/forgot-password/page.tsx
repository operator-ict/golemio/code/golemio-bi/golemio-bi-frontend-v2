import Link from "next/link";

import { Heading } from "@/(components)/Heading";
import authLayout from "@/public/PublicLayout.module.scss";
import text from "@/textContent/cs.json";

import { ForgotPassForm } from "../(components)/ForgotPassForm/Index";
import { ForgotPassOTPForm } from "../(components)/ForgotPassOTPForm";
import styles from "../(components)/Form.module.scss";

type PageProps = {
    searchParams: {
        codeSent: string;
    };
};

const ForgotPasswordPage = ({ searchParams }: PageProps) => {
    const codeSent = searchParams.codeSent === "success";

    return (
        <div className={authLayout["form-container"]}>
            <div className={authLayout["form-container-top"]}>
                <Heading tag="h4">{codeSent ? text.auth.resetEmailStatusSuccess : text.auth.reset}</Heading>
                <span>
                    {codeSent ? text.auth.resetEmailStatusSuccessInfo : text.auth.loginOffer}
                    {!codeSent && (
                        <Link href="/auth/sign-in" className={styles["link"]}>
                            {text.auth.loginOfferLink}
                        </Link>
                    )}
                </span>
            </div>
            {codeSent ? <ForgotPassOTPForm /> : <ForgotPassForm />}
        </div>
    );
};

export default ForgotPasswordPage;
