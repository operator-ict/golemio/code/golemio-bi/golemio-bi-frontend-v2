import { verifyResetCodeReq } from "@/actions/formAuthActions";
import { Heading } from "@/components/Heading";
import publiclayout from "@/public/PublicLayout.module.scss";
import text from "@/textContent/cs.json";

import { ResetPassForm } from "../../(components)/ResetPassForm";

type PageProps = {
    params: { code: string };
};

const ResetPasswordPage = async ({ params }: PageProps) => {
    const response = await verifyResetCodeReq(params.code);

    const valid = response.success === true;
    const error = response.error;

    return (
        <div className={publiclayout["form-container"]}>
            <Heading tag="h4">{text.auth.setNewPassword}</Heading>
            <ResetPassForm sessionToken={response.sessionToken || ""} disabled={!valid} error={error} />
        </div>
    );
};

export default ResetPasswordPage;
