import { Configuration, FrontendApi, IdentityApi } from "@ory/client-fetch";

import { config } from "../../../config";

export const getOryKratosFrontendApi = () => {
    return new FrontendApi(
        new Configuration({
            basePath: config.ORY_USER_PROFILE_URL,
        })
    );
};

export const getOryKratosIdentityApi = () => {
    return new IdentityApi(
        new Configuration({
            basePath: config.ORY_IDENTITY_URL,
        })
    );
};
