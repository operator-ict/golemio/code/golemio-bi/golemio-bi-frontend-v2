import { ResponseError, VerifiableIdentityAddress } from "@ory/client-fetch";
import { NextAuthOptions, Session, User } from "next-auth";
import { AdapterSession, AdapterUser } from "next-auth/adapters";
import { JWT } from "next-auth/jwt";
import CredentialsProvider from "next-auth/providers/credentials";

import { getOryKratosFrontendApi, getOryKratosIdentityApi } from "@/api/auth/OryApisProvider";
import { getLogger } from "@/logging/logger";
import { ApiClient } from "@/services/api-client";
import { emailScrambler } from "@/utils/emailScrambler";
import { getErrorMessage } from "@/utils/getErrorMessage";

import { config } from "../../../../config";

const logger = getLogger("next-auth");

const logData = {
    provider: "server",
    object: "authOptions",
    method: "authorize",
};

export type SessionWithUser = AdapterSession & Session & { user: AdapterUser };

export const authOptions: NextAuthOptions = {
    providers: [
        CredentialsProvider({
            name: "GolemioBI auth",
            id: "golemiobi-auth",
            type: "credentials",
            credentials: {
                email: { label: "Email", type: "text", placeholder: "username@mail.com", required: true },
                password: { label: "Password", type: "password", required: true },
                flow: { type: "text", required: true },
            },
            authorize: async (credentials: Record<"email" | "password" | "flow", string> | undefined): Promise<User | null> => {
                const { email, password: pwdPlainText, flow } = credentials || {};

                if (!email || !pwdPlainText) {
                    throw new Error("Required parameters not defined when calling login.");
                }

                if (!flow) {
                    throw new Error("Unknown login flow");
                }

                const oryFrontendApi = getOryKratosFrontendApi();

                try {
                    const data = await oryFrontendApi.updateLoginFlow({
                        flow,
                        updateLoginFlowBody: {
                            method: "password",
                            identifier: email,
                            password: pwdPlainText,
                        },
                    });

                    // We don't support 2FA/login redirects yet
                    if (!data.continue_with) {
                        const { session_token: token, session } = data;

                        if (token && session.identity?.state === "active") {
                            const emailVerifiedDateTime = getEmailVerifiedDateTime(session.identity.verifiable_addresses);

                            logger.debug({
                                ...logData,
                                message: `User ${emailScrambler(session.identity.traits.email)} logged in`,
                            });

                            const permissions = await ApiClient.post<boolean[]>(
                                `/permissions`,
                                JSON.stringify([
                                    {
                                        namespace: "groups",
                                        object: "admin",
                                        relation: "member",
                                        subjectId: session.identity.id,
                                    },
                                ]),
                                { "Content-Type": "application/json", "x-access-token": token }
                            );

                            return {
                                email: session.identity.traits.email,
                                token,
                                id: session.identity.id,
                                emailVerified: emailVerifiedDateTime,
                                admin: permissions.payload && permissions.payload[0],
                            };
                        }

                        logger.warn({ ...logData, message: `Token missing or user not active` });
                    }

                    throw new Error("Unexpected auth flow error");
                } catch (error: unknown) {
                    if (error instanceof ResponseError) {
                        if (error.response.status === 400) {
                            logger.warn({ ...logData, message: `Invalid credentials: ${error.message}` });
                            throw new Error(`Invalid credentials: ${error.message}`);
                        } else if (error.response.status === 410) {
                            logger.warn({ ...logData, message: `Invalid login flow: ${error.message}` });
                            throw new Error("Invalid login flow");
                        }
                    }

                    logger.error({ ...logData, error: `Authorization error: ${getErrorMessage(error)}` });
                    throw new Error(`Next-auth authorize: ${getErrorMessage(error)}`);
                }
            },
        }),
    ],
    pages: {
        signIn: "/auth/sign-in",
        error: "/auth/sign-in",
    },
    session: {
        strategy: "jwt",
        maxAge: 24 * 60 * 60,
    },
    cookies: {
        sessionToken: {
            name: `${config.ENVIRONMENT === "production" ? "__Secure-GBI-" : ""}next-auth.session-token`,
            options: {
                httpOnly: true,
                sameSite: "lax",
                path: "/",
                secure: config.ENVIRONMENT === "production" ? true : false,
                maxAge: 24 * 60 * 60,
            },
        },
        callbackUrl: {
            name: `${config.ENVIRONMENT === "production" ? "__Secure-GBI-" : ""}next-auth.callback-url`,
            options: {
                httpOnly: true,
                sameSite: "lax",
                path: "/",
                secure: config.ENVIRONMENT === "production" ? true : false,
            },
        },
        csrfToken: {
            name: `${config.ENVIRONMENT === "production" ? "__Host-GBI-" : ""}next-auth.csrf-token`,
            options: {
                httpOnly: true,
                sameSite: "lax",
                path: "/",
                secure: true,
            },
        },
    },
    callbacks: {
        jwt: async ({ token, user }: { token: JWT; user?: User }) => {
            if (user && isExtendedUser(user)) {
                token.token = user.token;
            }

            return { ...token, ...user };
        },
        session: async ({ session, token }) => {
            if (!token || !isExtendedToken(token)) {
                return session;
            }

            const extendedSession = session as SessionWithUser & {
                status: string;
                user: AdapterUser & { token: string };
            };
            const oryFrontendApi = getOryKratosFrontendApi();

            try {
                const data = await oryFrontendApi.toSession({ xSessionToken: token.token as string });

                if (!data.active || !data.expires_at || new Date(data.expires_at) < new Date()) {
                    extendedSession.status = "expired";
                    return extendedSession;
                }

                extendedSession.status = "authenticated";

                if (data.identity && data.identity.state === "active") {
                    const emailVerifiedDateTime = getEmailVerifiedDateTime(data.identity.verifiable_addresses);
                    let name = data.identity.traits.email;

                    if (data.identity.traits.firstName || data.identity.traits.lastName) {
                        name = `${
                            data.identity.traits.firstName
                                ? `${data.identity.traits.firstName} ${data.identity.traits.lastName}`.trim()
                                : data.identity.traits.lastName
                        }`;
                    }

                    extendedSession.user = {
                        id: data.identity.id,
                        email: data.identity.traits.email,
                        emailVerified: emailVerifiedDateTime,
                        name,
                        token: token.token as string,
                    };
                }

                // TODO: This is a workaround as the session extension via Kratos frontend API is not working
                // Try extending the session lifetime so the user doesn't get logged out in the middle of their session
                // without re-authenticating the user
                try {
                    const oryIdentityApi = getOryKratosIdentityApi();
                    await oryIdentityApi.extendSession({ id: data.id });
                } catch (error) {
                    logger.info({ ...logData, message: `Error extending session: ${error}` });
                }
            } catch (error) {
                extendedSession.status = "expired";
                logger.warn({ ...logData, message: `Error fetching session: ${error}` });
            }
            return { ...extendedSession, token: token.token, isLoggedIn: true, id: token.id, admin: token.admin };
        },
        redirect: ({ url, baseUrl }) => {
            // Allows relative callback URLs
            if (url.startsWith("/")) return `${baseUrl}${url}`;
            // Allows callback URLs on the same origin
            else if (new URL(url).origin === baseUrl) return url;
            return baseUrl;
        },
    },
    events: {
        signOut: async (message) => {
            if ("token" in message && message.token) {
                if (!isExtendedToken(message.token)) {
                    return;
                }

                const oryFrontendApi = getOryKratosFrontendApi();

                try {
                    const data = await oryFrontendApi.toSession({ xSessionToken: message.token.token as string });

                    if (data.active && (!data.expires_at || new Date(data.expires_at) > new Date())) {
                        await oryFrontendApi.performNativeLogout({
                            performNativeLogoutBody: {
                                session_token: message.token.token as string,
                            },
                        });
                    }
                } catch (error) {
                    logger.warn({ ...logData, message: `Error signing out: ${error}` });
                    throw new Error(`Next-auth signOut: ${error}`);
                }
            }
        },
    },
};

function getEmailVerifiedDateTime(addresses: VerifiableIdentityAddress[] = []): Date | null {
    const verifiedAddresses = addresses.filter((address) => address.via === "email" && address.verified);
    return verifiedAddresses.length > 0 && verifiedAddresses[0].verified_at ? new Date(verifiedAddresses[0].verified_at) : null;
}

function isExtendedUser(user: User): user is User & { sessionToken: string } {
    return "token" in user && typeof user.token === "string";
}

function isExtendedToken(token: JWT): token is JWT & { sessionToken: string } {
    return typeof token.token === "string";
}
