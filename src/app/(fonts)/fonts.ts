import { Barlow, Golos_Text } from "next/font/google";

export const barlow = Barlow({ weight: ["400", "500", "700"], subsets: ["latin"], variable: "--font-barlow", display: "swap" });
export const golosText = Golos_Text({
    weight: ["400", "500", "700"],
    subsets: ["latin"],
    variable: "--font-golos-text",
    display: "swap",
});
