import { decode, JWT } from "next-auth/jwt";

import { getOryKratosFrontendApi } from "@/api/auth/OryApisProvider";
import { getLogger } from "@/logging/logger";
import { getErrorMessage } from "@/utils/getErrorMessage";

import { config } from "../../config";
const logger = getLogger("checkTokenExpiration");

export async function decodeSessionToken(sessionToken: string) {
    const logData = { provider: "server", function: "decodeSessionToken" };

    let decodedToken: JWT;
    try {
        decodedToken = (await decode({ token: sessionToken, secret: config.NEXTAUTH_SECRET })) as JWT;

        const oryFrontendApi = getOryKratosFrontendApi();
        const data = await oryFrontendApi.toSession({ xSessionToken: decodedToken.token });

        const isAdmin = decodedToken.admin;
        const isExpired = data.expires_at ? (new Date(data.expires_at).valueOf() as number) < Date.now() : true;
        const userId = data.identity?.id;
        const token = decodedToken.token;

        logger.debug({ ...logData, isAdmin, isExpired, userId });

        return { isAdmin, isExpired, userId, token };
    } catch (error) {
        logger.error({ ...logData, error: getErrorMessage(error) });
        throw error;
    }
}
