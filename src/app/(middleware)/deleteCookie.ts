import type { NextResponse } from "next/server";

const MAX_AGE_EXPIRED = 0;
const DELETED_COOKIE_VALUE = "deleted";
const COOKIE_OPTIONS = {
    maxAge: MAX_AGE_EXPIRED,
    path: "/",
    secure: true,
    sameSite: true,
};

export const deleteCookie = (response: NextResponse, cookieName: string): NextResponse => {
    response.cookies.set(cookieName, DELETED_COOKIE_VALUE, COOKIE_OPTIONS);
    return response;
};
