import { config } from "../../config";

export const sessionCookie = () => {
    const sessionCookie = `${config.ENVIRONMENT === "production" ? "__Secure-GBI-" : ""}next-auth.session-token`;
    return sessionCookie;
};
