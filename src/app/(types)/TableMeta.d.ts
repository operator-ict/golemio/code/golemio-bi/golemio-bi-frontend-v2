/* eslint-disable @typescript-eslint/no-unused-vars */

import { TableMeta } from "@tanstack/react-table";

declare module "@tanstack/react-table" {
    export interface TableMeta<TData> {
        currentUserID?: string;
    }
}
