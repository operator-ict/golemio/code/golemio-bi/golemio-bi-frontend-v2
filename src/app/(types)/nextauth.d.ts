/* eslint-disable @typescript-eslint/no-unused-vars */
import { Session } from "next-auth";
import { JWT } from "next-auth/jwt";

declare module "next-auth" {
    interface Session {
        isLoggedIn: boolean;
        token?: string;
        error?: string;
        id?: string;
        user?: User;
        admin?: boolean;
    }
    interface User {
        email?: string;
        emailVerified: Date | null;
        token?: string;
        id?: string;
        name?: string;
        admin?: boolean;
    }
}

declare module "next-auth/jwt" {
    interface JWT {
        name?: string | null;
        email?: string | null;
        picture?: string | null;
        token?: string;
        sub?: string;
        iat?: number;
        exp?: number;
        jti?: string;
    }
}
