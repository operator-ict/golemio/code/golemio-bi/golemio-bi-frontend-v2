export type ApiGetRequestParams = {
    pagination?: { page?: number; pageSize?: number };
    filters?: object;
    clientRoute?: string;
    headerSearch?: string;
    search?: string;
    limit?: string;
    page?: string;
    orderBy?: string;
    orderDir?: "asc" | "desc";
};

export type ResponseEntity<T> = {
    status: number;
    payload?: T;
    error?: string;
    count?: string | null;
};

export type Result = {
    success?: boolean;
    error?: string;
    continueId?: string;
    status?: number;
};

export type ResetPassFormProps = {
    sessionToken: string;
    disabled: boolean;
    error?: string;
};

export type AccessRequest = {
    name: string;
    surname: string;
    email: string;
    password: string;
    organization: string;
    conditions_accepted: boolean;
};

export type AccessDashboardRequest = {
    userId: string;
    email: string;
    dashboardUrl: string;
};

export type AccessRequestResponse = { isOk: boolean; errors?: object };

export type formThumbnail = {
    file: File | null;
    imageUrl: string;
};

export type MetadataFormValues = {
    title: string;
    description: string;
    client_route: string;
    component: string;
    type: string;
    tags: string[];
    thumbnail: formThumbnail;
    data: string;
    roles: string[];
    garant: string;
};

export type CreateMetadataResponse = { id: string };

export type UserProfileResponse = {
    id: string;
    name: string;
    surname: string;
    email: string;
};
