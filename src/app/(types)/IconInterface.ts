/* eslint-disable prettier/prettier */
export interface IiCon {
    color:
        | "primary"
        | "secondary"
        | "tertiary"
        | "black"
        | "white"
        | "yellow"
        | "gray-middle"
        | "gray-dark"
        | "gray-darker"
        | "black-gray"
        | "error"
        | "warning";
    direction?: "up" | "down" | "left" | "right";
}
