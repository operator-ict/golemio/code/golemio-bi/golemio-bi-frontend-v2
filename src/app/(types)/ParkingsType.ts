import { Coordinate, IGeoData } from "./FeaturesTypes";

export interface IParkingCollection {
    features: IGeoData<IParkingProperties>[];
    type: string;
}

export interface IParkingProperties {
    id: string;
    source: string;
    source_id: string;
    data_provider: string;
    name: string;
    category: string;
    date_modified: string;
    address_formatted: string;
    address: IAdress;
    area_served: string | null;
    web_app_payment_url: string | null;
    android_app_payment_url: string | null;
    ios_app_payment_url: string | null;
    total_spot_number: number;
    tariff_id: string;
    valid_from: string | null;
    valid_to: string | null;
    parking_type: string;
    zone_type: string | null;
    centroid: ICentroid;
    available_spots_last_updated: string;
    available_spots_number: number;
    icon?: string;
}

export interface IAdress {
    address_country: string;
    address_formatted: string;
    address_locality: string;
    address_region: string;
    postal_code: string;
    street_address: string;
}

export interface ICentroid {
    type: string;
    coordinates: Coordinate;
}
