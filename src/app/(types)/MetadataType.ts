/* eslint-disable prettier/prettier */

export type DataType = "dashboard" | "application" | "export" | "analysis";

export type DataComponent =
    | "iframeComponent"
    | "powerbiComponent"
    | "attachmentComponent"
    | "parkingsMap"
    | "dataExport"
    | "mapComponent"
    | "sortedWasteMap";

export interface IMetadata {
    tags: [
        {
            _id: string;
            title: string;
        },
    ];
    _id: string;
    route: string;
    client_route: string;
    type: DataType;
    component: DataComponent;
    title: string;
    description: string;
    data: IEmbedData;
    roles: Role[];
    garant: string;
    thumbnail: {
        content_type: string;
        url: string;
    };
}

export interface Role {
    id: string;
    label: string;
}

export type NewDataType = "dashboard" | "application";

export type NewComponentType = "powerbiComponent" | "parkingsMap" | "mapComponent" | "sortedWasteMap";

export interface IMetadataCreate {
    title: string;
    description: string;
    client_route: string | undefined;
    tags: string[];
    component: string;
    type: string;
    data: string;
}
export interface IData {
    districtsRoute?: string;
    measurementsRoute?: string;
    report_id?: string;
    workspace_id?: string;
    url?: string;
}

export interface IEmbedData {
    report_id: string;
    workspace_id: string;
}
export interface IEmbedMetadata {
    accessToken: string;
    embedUrl: IEmbedUrl[];
    expiry: string;
    status: number;
}

export interface IEmbedUrl {
    reportId: string;
    reportName: string;
    embedUrl: string;
}

export type MetadataExistsResponse = {
    exists: boolean;
};
