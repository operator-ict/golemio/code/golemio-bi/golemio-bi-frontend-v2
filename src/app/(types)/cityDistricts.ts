export interface ICityDistrictsFeatureCollection {
    features: ICityDistrictsFeature[];
    type: string;
}

export interface ICityDistrictsFeature {
    geometry: ICityDistrictsGeometry;
    properties: ICityDistrictsProperties;
    type: string;
}
export interface ICityDistrictsGeometry {
    coordinates: number[][];
    type: string;
}

export interface ICityDistrictsProperties {
    id: number;
    name: string;
    slug: string;
    updated_at: string;
}
