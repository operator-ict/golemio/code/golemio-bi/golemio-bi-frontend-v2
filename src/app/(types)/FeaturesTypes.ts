export type Coordinate = [number, number];

export interface IGeometry {
    coordinates: Coordinate;
    type: string;
}

export interface IGeoData<T> {
    geometry: IGeometry;
    properties: T;
    type: string;
}

export interface IFeatureCollection {
    features: IGeoData<IFeatureProperties>[];
    type: string;
}
export interface IFeatureProperties {
    accessibility: IAccessibility;
    containers: IFeatureContainer[];
    district: string;
    id: number;
    is_monitored: boolean;
    name: string;
    station_number: string;
    updated_at: string;
    icon?: string;
}

export interface IAccessibility {
    description: string;
    id: number;
}

export interface IFeatureContainer {
    cleaning_frequency: IFeatureCleaningFrequency;
    container_type: string;
    trash_type: IFeatureTrashType;
    ksnko_id: number;
    container_id: number;
    is_monitored: boolean;
    last_measurement: IFeatureMeasurements;
    sensor_code: string;
}
export interface IFeatureCleaningFrequency {
    duration: string;
    frequency: number;
    id: number;
    pick_days: string;
    next_pick: string;
}

export interface IFeatureMeasurements {
    measured_at_utc: string;
    percent_calculated: number;
    prediction_utc: string;
}

export interface IFeatureTrashType {
    description: string;
    id: number;
}
