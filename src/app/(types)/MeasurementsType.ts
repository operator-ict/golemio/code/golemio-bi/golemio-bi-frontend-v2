export interface IMeasurementsId {
    id: string;
    sensor_code: string;
    percent_calculated: number;
    upturned: number;
    temperature: number;
    battery_status: number;
    measured_at_utc: string;
    prediction_utc: string;
    firealarm: number;
    updated_at: number;
}

export interface IMeasurementsIdResponse {
    status: number;
    data?: IMeasurementsId[];
    error?: string | null;
}
