export type UserProfileType = {
    email: string;
    id: string;
    name: string | null;
    surname: string | null;
};
