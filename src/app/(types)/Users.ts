export interface Role {
    created_at: string | null;
    default: boolean;
    description: string;
    id: number;
    isParent: boolean;
    label: string;
    parent_id: string | null;
    parent_name: string | null;
    updated_at: string | null;
    rateLimit: string | null;
    count: string;
}

export interface User {
    id: string;
    name: string;
    surname: string;
    email: string;
    organization: string;
    group_name: string;
    roleIds: string[];
    roles: UserRole[];
    created_at: string;
    updated_at: string;
    last_activity: string;
    invitation_accepted_at: string;
}

export type Garant = Omit<User, "group_name" | "roleIds" | "roles">;

export interface UserRole {
    id: string;
    label: string;
    description: string;
    assigned: boolean;
}

export interface RoleCreate {
    label: string;
    description: string;
    dashboards: string[];
    isPublic: boolean;
    updated_at: string;
    id: string;
}

export interface UpdateRole {
    id: string;
    label: string;
    description: string;
    deleted_at: string | null;
    created_by_user_id: string;
    updated_by_user_id: string | null;
    created_at: string;
    updated_at: string;
    dashboards: RoleDashboards[];
    users: User[];
}

export interface RoleDashboards {
    id: string;
    title: string;
}
