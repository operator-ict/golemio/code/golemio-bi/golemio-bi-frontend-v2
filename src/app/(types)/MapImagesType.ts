export type MapImageType = [string, HTMLImageElement];
export type MapImagesType = MapImageType[] | MapImageType;

export interface ICustomIcons {
    images: MapImagesType;
    markerIcon: string;
    clusterIcon?: string;
    activeIcon?: string;
    sensorIcon?: string;
    activeSensorIcon?: string;
}
