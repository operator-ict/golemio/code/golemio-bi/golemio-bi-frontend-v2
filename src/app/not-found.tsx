import { getServerSession } from "next-auth";
import { Suspense } from "react";

import Loading from "./(private)/loading";
import NotFoundPrivate from "./(private)/not-found";
import NotFoundPublic from "./(public)/not-found";
import { authOptions } from "./api/auth/[...nextauth]/authOptions";

const NotFound = async () => {
    const session = await getServerSession(authOptions);

    return <Suspense fallback={<Loading />}>{session?.token ? <NotFoundPrivate /> : <NotFoundPublic />}</Suspense>;
};

export default NotFound;
