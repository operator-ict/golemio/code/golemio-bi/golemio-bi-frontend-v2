"use client";
import { useEffect, useState } from "react";

export const useScrollHandler = () => {
    const [isShrunk, setShrunk] = useState<boolean>(false);

    useEffect(() => {
        const handler = () => {
            setShrunk((isShrunk) => {
                if (!isShrunk && (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50)) {
                    return true;
                }

                if (isShrunk && document.body.scrollTop < 4 && document.documentElement.scrollTop < 4) {
                    return false;
                }

                return isShrunk;
            });
        };

        window.addEventListener("scroll", handler);
        return () => window.removeEventListener("scroll", handler);
    }, []);

    return isShrunk;
};
