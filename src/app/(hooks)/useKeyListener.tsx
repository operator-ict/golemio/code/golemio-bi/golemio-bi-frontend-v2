import { useEffect } from "react";

const useKeyListener = (keyListenersMap: Map<string, (e: KeyboardEvent) => void>) => {
    useEffect(() => {
        const keyListener = (e: KeyboardEvent) => {
            const listener = keyListenersMap.get(e.code);
            if (listener) {
                listener(e);
            }
        };

        document.addEventListener("keydown", keyListener);
        return () => {
            document.removeEventListener("keydown", keyListener);
        };
    });
};

export default useKeyListener;
