"use client";

import { useSearchParams } from "next/navigation";

export const useUpdatedQuery = (key: string, value: string) => {
    const searchParams = useSearchParams();
    const updatedQuery = new URLSearchParams(searchParams.toString());
    updatedQuery.set(key, value);
    return updatedQuery.toString();
};
