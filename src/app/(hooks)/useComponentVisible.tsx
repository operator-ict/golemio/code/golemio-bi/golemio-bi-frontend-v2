"use client";
import { useEffect, useRef } from "react";

export const useComponentVisible = <T extends HTMLElement = HTMLDivElement>(onOutside: () => void) => {
    const ref = useRef<T>(null);

    const handleClickOutside = ({ target }: MouseEvent) => {
        if (ref.current && !ref.current.contains(target as Node) && !ref.current.parentNode?.contains(target as Node)) {
            onOutside();
        }
    };

    useEffect(() => {
        document.addEventListener("click", handleClickOutside, true);
        return () => {
            document.removeEventListener("click", handleClickOutside, true);
        };
    });

    return { ref };
};
