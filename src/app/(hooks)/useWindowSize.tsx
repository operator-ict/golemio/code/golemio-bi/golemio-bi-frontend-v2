"use client";
import { useEffect, useState } from "react";
export interface ISize {
    width: number | undefined;
    height: number | undefined;
}

// Hook
function useWindowSize(): ISize {
    // Initialize state with undefined width/height so server and client renders match
    // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
    const [windowSize, setWindowSize] = useState<ISize>({
        width: undefined,
        height: undefined,
    });

    useEffect(() => {
        let timeoutId: ReturnType<typeof setTimeout>;

        function handleResize() {
            clearTimeout(timeoutId);

            timeoutId = setTimeout(() => {
                setWindowSize({
                    width: window.innerWidth,
                    height: window.innerHeight,
                });
            }, 350);
        }

        window.addEventListener("resize", handleResize);

        handleResize();

        return () => {
            window.removeEventListener("resize", handleResize);
            clearTimeout(timeoutId);
        };
    }, []);

    return windowSize;
}

export default useWindowSize;
