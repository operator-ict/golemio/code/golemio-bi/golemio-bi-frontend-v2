import { RefObject, useCallback } from "react";

const useTabKeyHandler = (ref: RefObject<HTMLElement>, selector: string) => {
    const handleTabKey = useCallback(
        (e: KeyboardEvent) => {
            if (ref.current) {
                const focusableElements = ref.current.querySelectorAll<HTMLElement>(selector);
                const firstElement = focusableElements[0];
                const lastElement = focusableElements[focusableElements.length - 1];
                const activeElements = Array.from(focusableElements).filter((el) => document.activeElement === el);

                if (activeElements.length === 0) {
                    firstElement?.focus();
                    e.preventDefault();
                }

                if (!e.shiftKey && document.activeElement === lastElement) {
                    firstElement?.focus();
                    e.preventDefault();
                }

                if (e.shiftKey && document.activeElement === firstElement) {
                    lastElement?.focus();
                    e.preventDefault();
                }
            }
        },
        [ref, selector]
    );

    return handleTabKey;
};

export default useTabKeyHandler;
