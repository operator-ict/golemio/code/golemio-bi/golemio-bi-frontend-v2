"use client";

import { useSearchParams } from "next/navigation";

export const useRemoveParam = (param: string) => {
    const query = useSearchParams();
    const updatedQuery = new URLSearchParams(query.toString());
    updatedQuery.delete(param);
    return updatedQuery.toString();
};
