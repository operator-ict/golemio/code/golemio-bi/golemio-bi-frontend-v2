import { getLogger } from "@/logging/logger";
import { getErrorMessage } from "@/utils/getErrorMessage";

import { pca } from "./msalClient";
import { config } from "./utils";

const logger = getLogger("authentication");

const loginRequest = {
    scopes: [`${config.scope}/.default`],
    username: config.pbiUsername ? config.pbiUsername : "",
    password: config.pbiPassword ? config.pbiPassword : "",
};

const logData = { provider: "server", function: "getAccessToken" };

const getAccessToken = async function () {
    try {
        const authResult = await pca.acquireTokenByUsernamePassword(loginRequest);
        logger.debug({ ...logData, msg: "Successfully acquired token" });
        return authResult;
    } catch (error) {
        logger.error({ ...logData, error: getErrorMessage(error) });
        return error;
    }
};

export { getAccessToken };
