// Properties for embedding the report

class EmbedConfig {
    embedToken!: {
        token: string;
        expiration: string;
    };
    reportId = "";
    embedUrl = "";
}

export { EmbedConfig };
