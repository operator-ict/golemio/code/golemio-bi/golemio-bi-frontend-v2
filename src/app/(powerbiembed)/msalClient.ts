import { PublicClientApplication } from "@azure/msal-node";

import { config } from "./utils";

const msalConfig = {
    auth: {
        clientId: config.clientId ? config.clientId : "",
        authority: `https://login.microsoftonline.com/${config.pbiTenantId}`,
        clientSecret: config.pbiClientSecret ? config.pbiClientSecret : "",
        knownAuthorities: [],
    },
};

export const pca = new PublicClientApplication(msalConfig);
