interface IConfig {
    authenticationMode: string;
    authorityUri: string;
    scope: string;
    apiUrl: string;
    clientId?: string;
    pbiUsername?: string;
    pbiPassword?: string;
    pbiTenantId?: string;
    pbiClientSecret?: string;
    workspaceId?: string;
    reportId?: string;
}

const config: IConfig = {
    authenticationMode: "MasterUser",
    authorityUri: "https://login.microsoftonline.com/common/v2.0",
    scope: "https://analysis.windows.net/powerbi/api",
    apiUrl: "https://api.powerbi.com/",

    clientId: process.env.POWER_BI_CLIENT_ID,
    pbiUsername: process.env.POWER_BI_USERNAME,
    pbiPassword: process.env.POWER_BI_PASSWORD,
    pbiTenantId: process.env.POWER_BI_TENANT_ID,
    pbiClientSecret: process.env.POWER_BI_CLIENT_SECRET,
};

function getAuthHeader(accessToken: string) {
    // Function to append Bearer against the Access Token
    return "Bearer ".concat(accessToken);
}

export { config, getAuthHeader };
