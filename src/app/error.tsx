"use client";

import { useEffect } from "react";

import { Heading } from "@/(components)/Heading";
import { ServerErrorIcon } from "@/(components)/icons/ServerError";
import { Paragraph } from "@/(components)/Paragraph";
import layout from "@/layout.module.scss";
import text from "@/textContent/cs.json";
import { getErrorMessage } from "@/utils/getErrorMessage";

import PublicLayout from "./(public)/layout";
import styles from "./ErrorPages.module.scss";

// eslint-disable-next-line react/function-component-definition
export default function Error({ error }: { error: Error & { digest?: string }; reset: () => void }) {
    useEffect(() => {
        console.error(getErrorMessage(error));
    }, [error]);

    return (
        <PublicLayout>
            <main className={layout.main} id="main">
                <section className={` ${styles["error-page-container"]}`}>
                    <div className={`${styles["error-image-container"]}`}>
                        <ServerErrorIcon />
                    </div>
                    <Heading tag="h1" type="h4-h3">
                        {text.serverErrorPage.title}
                    </Heading>
                    <Paragraph type={"p1"}>{text.serverErrorPage.subTitle}</Paragraph>
                </section>
                <div id="portal" />
            </main>
        </PublicLayout>
    );
}
