/* eslint-disable max-len */
import pino, { Logger } from "pino";
import { config } from "src/config";

export const getLogger = (name: string): Logger => {
    return pino({ name, level: config.NEXT_PUBLIC_LOG_LEVEL || "info" });
};

// from now on, please format the logger messages like this (former logs wil be refactored):

// INSIDE TRY BLOCK
// logger.debug({ provider: "server/client", class: "class name, if present", method: "method name, if present" or function: "function name used as server action", requestUrl: "if present" message: "...some debug message... with optional status" });

// logger.error({ provider: "server/client", class: "class name, if present", method: "method name, if present" or function: "function name used as server action", requestUrl: "if present" message: "...some debug message... with response error and status" });

// INSIDE CATCH BLOCK
// logger.error({ provider: "server/client", class: "class name, if present", method: "method name, if present" or function: "function name used as server action", requestUrl: "if present" error: "caught error" });
