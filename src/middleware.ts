import { NextURL } from "next/dist/server/web/next-url";
import { NextRequest, NextResponse } from "next/server";

import { decodeSessionToken } from "@/(middleware)/decodeSessionToken";
import { deleteCookie } from "@/(middleware)/deleteCookie";
import { sessionCookie } from "@/(middleware)/sessionCookies";
import { getOryKratosFrontendApi } from "@/api/auth/OryApisProvider";
import { getLogger } from "@/logging/logger";
import { PermissionsClient } from "@/services/permissions-client";
import { getErrorMessage } from "@/utils/getErrorMessage";
import { deleteOrySessionCookie, getOrySessionCookie } from "@/utils/oryCookies";

export { default } from "next-auth/middleware";

const logger = getLogger("next-middleware");

const logData = {
    provider: "server",
    function: "middleware",
};

export const config = {
    matcher: [
        "/",
        "/dashboards/:path*",
        "/dashboard/:path*",
        "/maps/:path*",
        "/edit-metadata/:path*",
        "/create-metadata",
        "/users",
        "/roles",
        "/auth/sign-in",
        "/auth/request-access",
        "/auth/forgot-password",
        "/auth/reset-password/:path*",
        "/((?!_next/static|_next/image|favicon.ico|api/auth|api/).*)",
    ],
};

export async function middleware(req: NextRequest) {
    const url = req.nextUrl.clone();

    const reqCookies = req.cookies;
    const sessionCookies = sessionCookie();
    const sessionToken = reqCookies.get(sessionCookies)?.value;

    const signInUrl = new URL("/auth/sign-in", url);
    signInUrl.searchParams.set("from", url.pathname);
    logger.debug({ ...logData, message: `Session token: ${sessionToken}` });
    signInUrl.searchParams.set("from", url.pathname);

    if (url.pathname === "/status") {
        return NextResponse.next();
    }

    const isAuthPage = url.pathname.startsWith("/auth");
    if (!sessionToken && !isAuthPage) {
        return NextResponse.redirect(signInUrl);
    }

    if (sessionToken && isAuthPage) {
        return NextResponse.redirect(new URL("/", url));
    }

    if (url.pathname === "/auth/sign-in") {
        if (sessionToken) {
            return handleExistingSession(url, sessionCookies, sessionToken, signInUrl);
        }

        const flowToken = url.searchParams.get("flow");
        const isLoginFlowValid = flowToken ? await validateLoginFlow(flowToken) : false;

        if (!isLoginFlowValid) {
            const newFlowToken = await initLoginFlow();
            url.searchParams.set("flow", newFlowToken);
            return NextResponse.redirect(url);
        }

        return NextResponse.next();
    }

    if (url.pathname === "/auth/request-access") {
        if (sessionToken) {
            return handleExistingSession(url, sessionCookies, sessionToken, signInUrl);
        }

        const flowToken = url.searchParams.get("flow");
        //logger.debug(`Registration Flow token: ${flowToken}`);
        const isRegistrationFlowValid = flowToken ? await validateRegistrationFlow(flowToken) : false;
        //logger.debug(`Registration Flow is valid: ${isRegistrationFlowValid}`);
        if (!isRegistrationFlowValid) {
            const newFlowToken = await initRegistrationFlow();
            //logger.debug(`New Registration Flow token: ${newFlowToken}`);
            url.searchParams.set("flow", newFlowToken);
            return NextResponse.redirect(url);
        }
        return NextResponse.next();
    }

    if (url.pathname === "/auth/forgot-password") {
        if (sessionToken) {
            return handleExistingSession(url, sessionCookies, sessionToken, signInUrl);
        }

        // do not refresh token if in second step of flow
        if (url.searchParams.get("codeSent") === "success") {
            return;
        }

        const flowToken = url.searchParams.get("flow");
        const isRecoveryFlowValid = flowToken ? await validateRecoveryFlow(flowToken) : false;

        if (!isRecoveryFlowValid) {
            const newFlowToken = await initRecoveryFlow();
            url.searchParams.set("flow", newFlowToken);
            return NextResponse.redirect(url);
        }

        return NextResponse.next();
    }

    if (url.pathname.startsWith("/auth/reset-password")) {
        const settingsFlowToken = url.searchParams.get("flow");
        const oryToken = getOrySessionCookie();

        const isSettingsFlowValid =
            settingsFlowToken && oryToken ? await validateSettingsFlow(settingsFlowToken, oryToken) : false;

        if (!isSettingsFlowValid) {
            deleteOrySessionCookie();
            return NextResponse.redirect(signInUrl);
        }

        return NextResponse.next();
    }

    if (!sessionToken) {
        return NextResponse.redirect(signInUrl);
    }

    return handleExistingSession(url, sessionCookies, sessionToken, signInUrl);
}

export async function initLoginFlow(): Promise<string> {
    const oryFrontendApi = getOryKratosFrontendApi();

    try {
        const data = await oryFrontendApi.createNativeLoginFlow();
        logger.debug({ ...logData, message: `Created login flow: ${data.id}` });
        return data.id;
    } catch (error) {
        logger.warn({ ...logData, message: `Error creating login flow: ${error}` });
        throw new Error("Failed to initiate login flow");
    }
}

export async function validateLoginFlow(token: string): Promise<boolean> {
    const ory = getOryKratosFrontendApi();

    try {
        const data = await ory.getLoginFlow({ id: token });
        return data.expires_at ? new Date(data.expires_at) > new Date() : false;
    } catch (error) {
        logger.error({ ...logData, error: `Login flow is invalid: ${error}` });
        return false;
    }
}

export async function initRegistrationFlow(): Promise<string> {
    const ory = getOryKratosFrontendApi();

    try {
        const data = await ory.createNativeRegistrationFlow();
        return data.id;
    } catch (error) {
        logger.error({ ...logData, error: `Error creating registration flow: ${error}` });
        throw new Error("Failed to initiate registration flow");
    }
}

export async function validateRegistrationFlow(token: string): Promise<boolean> {
    const ory = getOryKratosFrontendApi();
    try {
        const data = await ory.getRegistrationFlow({ id: token });
        return data.expires_at ? new Date(data.expires_at) > new Date() : false;
    } catch (error) {
        logger.error({ ...logData, error: `Registration flow is invalid: ${error}` });
        return false;
    }
}

export async function initRecoveryFlow(): Promise<string> {
    const oryFrontendApi = getOryKratosFrontendApi();

    try {
        const data = await oryFrontendApi.createNativeRecoveryFlow();
        return data.id;
    } catch (error) {
        logger.error({ ...logData, error: `Error creating recovery flow: ${error}` });
        throw new Error("Failed to initiate recovery flow");
    }
}

export async function validateRecoveryFlow(token: string): Promise<boolean> {
    const ory = getOryKratosFrontendApi();

    try {
        const data = await ory.getRecoveryFlow({ id: token });
        return data.expires_at ? new Date(data.expires_at) > new Date() : false;
    } catch (error) {
        logger.error({ ...logData, error: `Recovery flow is invalid: ${error}` });
        return false;
    }
}

export async function validateSettingsFlow(token: string, sessionToken?: string): Promise<boolean> {
    const ory = getOryKratosFrontendApi();

    try {
        // const recoveryFlow = await ory.getRecoveryFlow({ id: token });
        const data = await ory.getSettingsFlow({ id: token, xSessionToken: sessionToken });
        return data.expires_at ? new Date(data.expires_at) > new Date() : false;
    } catch (error) {
        logger.error({ ...logData, error: `Settings flow is invalid: ${error}` });
        return false;
    }
}

async function handleExistingSession(url: NextURL, sessionCookies: string, sessionToken: string, signInUrl: URL) {
    const isDashboardPages = url.pathname.startsWith("/create-metadata") || url.pathname.startsWith("/edit-metadata");
    const isRolesPage = url.pathname.startsWith("/roles");
    const isUsersPage = url.pathname.startsWith("/users");

    const logData = {
        provider: "server",
        function: "handleExistingSession",
        url: url.pathname,
    };

    try {
        const { isExpired, userId, token } = await decodeSessionToken(sessionToken);

        if (isExpired) {
            const response = NextResponse.redirect(signInUrl);
            return deleteCookie(response, sessionCookies);
        }

        if (isDashboardPages && token && userId) {
            try {
                const canEditDashboards = await PermissionsClient.checkPermissions("dashboards", "edit", token, userId);
                logger.debug({ ...logData, canEditDashboards });
                return canEditDashboards ? NextResponse.next() : NextResponse.rewrite(new URL("/not-found", url));
            } catch (error) {
                logger.error({ ...logData, error: getErrorMessage(error) });
                return NextResponse.rewrite(new URL("/not-found", url));
            }
        }

        if (isRolesPage && token && userId) {
            try {
                const isDatarOrAdmin = await PermissionsClient.checkPermissions("dashboards", "viewAdmin", token, userId);
                logger.debug({ ...logData, isDatarOrAdmin });
                return isDatarOrAdmin ? NextResponse.next() : NextResponse.rewrite(new URL("/not-found", url));
            } catch (error) {
                logger.error({ ...logData, error: getErrorMessage(error) });
                return NextResponse.rewrite(new URL("/not-found", url));
            }
        }

        if (isUsersPage && token && userId) {
            try {
                const isGarantOrAdmin = await PermissionsClient.checkPermissions("users", "editRole", token, userId);
                logger.debug({ ...logData, isGarantOrAdmin });
                return isGarantOrAdmin ? NextResponse.next() : NextResponse.rewrite(new URL("/not-found", url));
            } catch (error) {
                logger.error({ ...logData, error: getErrorMessage(error) });
                return NextResponse.rewrite(new URL("/not-found", url));
            }
        }
    } catch {
        const response = NextResponse.redirect(signInUrl);
        return deleteCookie(response, sessionCookies);
    }

    return NextResponse.next();
}
