import React from "react";

const Image: React.FC<React.ImgHTMLAttributes<HTMLImageElement>> = (props) => {
    return <Image alt="" {...props} />;
};

export default Image;
