import "@testing-library/jest-dom";

import React, { ReactNode } from "react";

const MockLayout = ({ children }: { children: ReactNode }) => {
    return (
        <div>
            <div id="portal"></div>
            {children}
        </div>
    );
};

export default MockLayout;
