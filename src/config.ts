interface Config {
    ENVIRONMENT: string;
    NEXT_PUBLIC_API_URL: string;
    NEXT_PUBLIC_URL: string;
    NEXT_PUBLIC_LOG_LEVEL: string;
    NOTIFICATION_BANNER_TEXT: string;
    NOTIFICATION_BANNER_SEVERITY: "info" | "warning" | "error";
    NEXT_PUBLIC_MAPBOX_TOKEN: string;
    NEXTAUTH_SECRET: string;
    NEXTAUTH_URL: string;
    NEXT_PUBLIC_GTM_ID: string;
    ORY_USER_PROFILE_URL: string;
    ORY_IDENTITY_URL: string;
}

export const config: Config = {
    ENVIRONMENT: process.env.ENVIRONMENT ?? "development",
    NEXT_PUBLIC_API_URL: process.env.NEXT_PUBLIC_API_URL ?? "",
    NEXT_PUBLIC_URL: process.env.NEXT_PUBLIC_URL ?? "",
    NEXT_PUBLIC_LOG_LEVEL: process.env.NEXT_PUBLIC_LOG_LEVEL ?? "info",
    NOTIFICATION_BANNER_TEXT: process.env.NOTIFICATION_BANNER_TEXT ?? "",
    NOTIFICATION_BANNER_SEVERITY: (process.env.NOTIFICATION_BANNER_SEVERITY as "info" | "warning" | "error") ?? "",
    NEXT_PUBLIC_MAPBOX_TOKEN: process.env.NEXT_PUBLIC_MAPBOX_TOKEN ?? "",
    NEXTAUTH_SECRET: process.env.NEXTAUTH_SECRET ?? "",
    NEXTAUTH_URL: process.env.NEXTAUTH_URL ?? "",
    NEXT_PUBLIC_GTM_ID: process.env.NEXT_PUBLIC_GTM_ID ?? "",
    ORY_USER_PROFILE_URL: process.env.ORY_USER_PROFILE_URL ?? "",
    ORY_IDENTITY_URL: process.env.ORY_IDENTITY_URL ?? "",
};
