import { getUserAgreementStatus } from "@/actions/formAuthActions";
import { fetchAgreementStatus } from "@/utils/agreementStatus";

// Mock getUserAgreementStatus function
jest.mock("@/actions/formAuthActions", () => ({
    getUserAgreementStatus: jest.fn(),
}));

jest.mock("@/utils/getErrorMessage", () => ({
    getErrorMessage: jest.fn(),
}));

describe("fetchAgreementStatus", () => {
    it("should return true if agreement-status is true and the response is successful", async () => {
        // Mock the return value of getUserAgreementStatus
        (getUserAgreementStatus as jest.Mock).mockResolvedValue({
            success: true,
            "agreement-status": true,
        });

        const result = await fetchAgreementStatus();
        expect(result).toBe(true);
    });

    it("should return false if agreement-status is false even if the response is successful", async () => {
        (getUserAgreementStatus as jest.Mock).mockResolvedValue({
            success: true,
            "agreement-status": false,
        });

        const result = await fetchAgreementStatus();
        expect(result).toBe(false);
    });

    it("should return false if the response is not successful", async () => {
        (getUserAgreementStatus as jest.Mock).mockResolvedValue({
            success: false,
        });

        const result = await fetchAgreementStatus();
        expect(result).toBe(false);
    });

    it("should return false if getUserAgreementStatus throws an error", async () => {
        (getUserAgreementStatus as jest.Mock).mockRejectedValue(new Error("API Error"));

        const result = await fetchAgreementStatus();
        expect(result).toBe(false);
    });
});
