/* eslint-disable @typescript-eslint/no-explicit-any */
import { imageWidthAndHeight } from "@/utils/imageWidthAndHeight";

describe("imageWidthAndHeight", () => {
    let mockBlob: Blob;
    let mockFileReaderInstance: Partial<FileReader>;
    let mockImage: HTMLImageElement;

    beforeEach(() => {
        // Mock a Blob file
        mockBlob = new Blob(["dummy content"], { type: "image/png" });

        // Mock the FileReader instance
        mockFileReaderInstance = {
            readAsDataURL: jest.fn(),
            result: "data:image/png;base64,dummybase64data",
            onload: null,
            onerror: null,
            addEventListener: jest.fn(),
            removeEventListener: jest.fn(),
        };

        // Assign the FileReader static properties
        Object.assign(global, {
            FileReader: jest.fn().mockImplementation(() => mockFileReaderInstance),
        });

        Object.defineProperty(global.FileReader, "EMPTY", { value: 0 });
        Object.defineProperty(global.FileReader, "LOADING", { value: 1 });
        Object.defineProperty(global.FileReader, "DONE", { value: 2 });

        // Mock the Image constructor and onload behavior
        mockImage = {
            src: "",
            width: 200,
            height: 100,
            onload: null,
            setAttribute: jest.fn(),
        } as unknown as HTMLImageElement;

        global.Image = jest.fn(() => mockImage) as any;
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it("should return image dimensions when the image is loaded successfully", async () => {
        const promise = imageWidthAndHeight(mockBlob);

        // Simulate the FileReader `onload` event
        const progressEvent = {
            target: {
                result: "data:image/png;base64,dummybase64data",
            },
        } as ProgressEvent<FileReader>;
        (mockFileReaderInstance.onload as any)(progressEvent);

        // Simulate the Image `onload` event
        mockImage.onload?.(new Event("load"));

        const result = await promise;
        // Check the dimensions
        expect(result).toEqual({ width: 200, height: 100 });
    });

    it("should handle a failure in loading the image", async () => {
        const promise = imageWidthAndHeight(mockBlob);

        // Simulate the FileReader `onload` event
        const progressEvent = {
            target: {
                result: "data:image/png;base64,dummybase64data",
            },
        } as ProgressEvent<FileReader>;
        (mockFileReaderInstance.onload as any)(progressEvent);

        // Simulate the Image `onload` event with null width and height (error case)
        mockImage.width = null as any;
        mockImage.height = null as any;
        mockImage.onload?.(new Event("load"));

        const result = await promise;

        // Check that dimensions are null
        expect(result).toEqual({ width: null, height: null });
    });
});
