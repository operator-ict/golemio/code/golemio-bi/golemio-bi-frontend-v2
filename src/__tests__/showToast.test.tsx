import React from "react";
import toast from "react-hot-toast";

import NotificationBanner, { NotificationType } from "@/(components)/NotificationBanner";
import { showToast } from "@/utils/showToast";

// Mock the `toast.custom` function
jest.mock("react-hot-toast", () => ({
    custom: jest.fn(),
}));

describe("showToast", () => {
    it("should call toast.custom with the correct NotificationBanner", () => {
        const mockType = NotificationType.Info;
        const mockText = "This is a success message";

        showToast(mockType, mockText);

        // Verify that toast.custom was called with the correct component and props
        expect(toast.custom).toHaveBeenCalledTimes(1);
        expect(toast.custom).toHaveBeenCalledWith(<NotificationBanner type={mockType} text={mockText} />);
    });
});
