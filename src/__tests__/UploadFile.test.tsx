import { fireEvent, render, screen } from "@testing-library/react";
import { useRouter } from "next/navigation";

import UploadFile from "@/components/UploadFile";
import text from "@/textContent/cs.json";

beforeAll(() => {
    global.URL.createObjectURL = jest.fn((file: Blob) => `mocked-url/${(file as File).name}`);
});

jest.mock("next/navigation", () => ({
    useRouter: jest.fn(),
}));

// Mock next/image
jest.mock("next/image", () => ({
    __esModule: true,
    default: ({ src, alt }: { src: string; alt: string }) => {
        return <img src={src} alt={alt} />;
    },
}));
jest.mock("@/images/AddFile.png", () => "mocked-addfile-image.png");
jest.mock("@/images/DropFile.png", () => "mocked-dropfile-image.png");

describe("UploadFile Component", () => {
    const mockOnFileSelected = jest.fn();

    beforeEach(() => {
        mockOnFileSelected.mockClear();
        (useRouter as jest.Mock).mockReturnValue({
            push: jest.fn(),
        });
    });

    it("renders the component without errors", () => {
        render(<UploadFile onFileSelected={mockOnFileSelected} />);

        // Check for the title
        expect(screen.getByText(text.uploadFile.miniature)).toBeInTheDocument();

        // Check for the label (associated with hidden input)
        const fileInputLabel = screen.getByText(/Vyberte/i);
        expect(fileInputLabel).toBeInTheDocument();
    });

    it("allows file selection and calls onFileSelected", () => {
        render(<UploadFile onFileSelected={mockOnFileSelected} />);

        const file = new File(["dummy content"], "example.png", { type: "image/png" });

        // Query hidden input by test id
        const fileInput = screen.getByTestId("file-input");
        fireEvent.change(fileInput, { target: { files: [file] } });

        expect(mockOnFileSelected).toHaveBeenCalledWith(file, `mocked-url/${file.name}`);
    });

    it("shows a thumbnail when a file is selected", () => {
        render(<UploadFile onFileSelected={mockOnFileSelected} />);

        const file = new File(["dummy content"], "example.png", { type: "image/png" });

        // Get hidden input by test id
        const fileInput = screen.getByTestId("file-input");
        fireEvent.change(fileInput, { target: { files: [file] } });

        // Expect the thumbnail to be rendered
        const thumbnail = screen.getByAltText("dashboard thumbnail");
        expect(thumbnail).toBeInTheDocument();
    });

    it("allows removal of selected file when not loading", async () => {
        // Render the UploadFile component
        render(<UploadFile onFileSelected={mockOnFileSelected} />);

        // Simulate a file being selected
        const fileInput = screen.getByTestId("file-input");
        const file = new File(["file-content"], "example.png", { type: "image/png" });
        fireEvent.change(fileInput, { target: { files: [file] } });

        // Verify the file is now displayed in the DOM
        const fileName = screen.getByText("example.png");
        expect(fileName).toBeInTheDocument();

        // Use findByTestId to wait for the remove button to appear
        const removeButton = screen.getByAltText("Odstranit obrázek");
        fireEvent.click(removeButton);

        // Verify the file is removed from the DOM
        expect(mockOnFileSelected).toHaveBeenCalledWith(null, "");
        expect(fileName).not.toBeInTheDocument();
    });
});
