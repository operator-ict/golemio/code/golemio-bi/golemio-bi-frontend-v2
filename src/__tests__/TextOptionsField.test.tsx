import { fireEvent, render, screen } from "@testing-library/react";

import TextOptionsField from "@/components/TextOptionsField";

describe("TextOptionsField", () => {
    const mockOptions = ["Option 1", "Option 2", "Option 3"];
    const mockOnChange = jest.fn();

    beforeEach(() => {
        jest.clearAllMocks();
    });

    it("renders the component with label, input, and icons", () => {
        render(
            <TextOptionsField
                label="Test Label"
                value="Test"
                id="test"
                options={mockOptions}
                iconStart={<span>Start Icon</span>}
                iconEnd={<span>End Icon</span>}
            />
        );

        expect(screen.getByLabelText("Test Label")).toBeInTheDocument();
        expect(screen.getByText("Start Icon")).toBeInTheDocument();
        expect(screen.getByText("End Icon")).toBeInTheDocument();
    });

    it("shows filtered options when typing", () => {
        render(<TextOptionsField label="Test Label" value="Test" id="test" options={mockOptions} onChange={mockOnChange} />);

        const input = screen.getByLabelText("Test Label");
        fireEvent.change(input, { target: { value: "Option 1" } });

        expect(screen.getByText("Option 1")).toBeInTheDocument();
        expect(screen.queryByText("Option 2")).not.toBeInTheDocument();
        expect(screen.queryByText("Option 3")).not.toBeInTheDocument();
    });

    it("calls onChange when an option is selected", () => {
        render(<TextOptionsField label="Test Label" value="Test" id="test" options={mockOptions} onChange={mockOnChange} />);

        const input = screen.getByLabelText("Test Label");
        fireEvent.change(input, { target: { value: "Option" } });

        const option = screen.getByText("Option 1");
        fireEvent.click(option);

        expect(mockOnChange).toHaveBeenCalledWith(expect.objectContaining({ target: { value: "Option 1" } }));
        expect(input).toHaveValue("Option 1");
    });

    it("hides dropdown when clicking outside", () => {
        render(<TextOptionsField label="Test Label" value="Test" id="test" options={mockOptions} />);

        const input = screen.getByLabelText("Test Label");
        fireEvent.change(input, { target: { value: "Option" } });

        expect(screen.getByText("Option 1")).toBeVisible();

        fireEvent.click(document.body);

        expect(screen.queryByText("Option 1")).not.toBeInTheDocument();
    });

    it("handles Enter key to select an option", () => {
        render(<TextOptionsField label="Test Label" value="Test" id="test" options={mockOptions} onChange={mockOnChange} />);

        const input = screen.getByLabelText("Test Label");
        fireEvent.change(input, { target: { value: "Option" } });

        const option = screen.getByText("Option 1");
        fireEvent.keyDown(option, { code: "Enter" });

        expect(mockOnChange).toHaveBeenCalledWith(expect.objectContaining({ target: { value: "Option 1" } }));
    });

    it("handles Escape key to hide dropdown", () => {
        render(<TextOptionsField label="Test Label" value="Test" id="test" options={mockOptions} />);

        const input = screen.getByLabelText("Test Label");
        fireEvent.change(input, { target: { value: "Option" } });

        expect(screen.getByText("Option 1")).toBeVisible();

        fireEvent.keyDown(document.body, { code: "Escape" });

        expect(screen.queryByText("Option 1")).not.toBeInTheDocument();
    });

    it("handles Tab key to change options", () => {
        render(<TextOptionsField label="Test Label" value="Test" id="test" options={mockOptions} />);

        const input = screen.getByLabelText("Test Label");
        fireEvent.change(input, { target: { value: "Option" } });

        expect(screen.getByText("Option 1")).toBeVisible();

        fireEvent.keyDown(document.body, { code: "Tab" });

        expect(screen.queryByText("Option 1")).toHaveFocus();

        fireEvent.keyDown(document.body, { code: "Tab", shiftKey: true });

        expect(screen.queryByText("Option 3")).toHaveFocus();

        fireEvent.keyDown(document.body, { code: "Tab" });

        expect(screen.queryByText("Option 1")).toHaveFocus();
    });

    it("shows dropdown when input is clicked", () => {
        render(<TextOptionsField label="Test Label" value="Test" id="test" options={mockOptions} />);

        const input = screen.getByLabelText("Test Label");
        fireEvent.click(input);

        expect(screen.getByText("Option 1")).toBeVisible();
    });

    it("shows error message if error prop is true", () => {
        render(
            <TextOptionsField
                label="Test Label"
                value="Test"
                id="test"
                options={mockOptions}
                error={true}
                errorMessage="Error message"
            />
        );

        expect(screen.getByText("Error message")).toBeInTheDocument();
    });

    it("shows info message if info prop is true", () => {
        render(<TextOptionsField label="Test Label" options={mockOptions} info={true} infoMessage="Info message" />);

        expect(screen.getByText("Info message")).toBeInTheDocument();
    });

    it("disables input and prevents interaction if disabled prop is true", () => {
        render(<TextOptionsField label="Test Label" value="Test" id="test" options={mockOptions} disabled={true} />);

        const input = screen.getByLabelText("Test Label");
        expect(input).toBeDisabled();
    });
});
