import { getFileDimensions } from "@/utils/getFileDimensions";

describe("getFileDimensions", () => {
    const mockCreateObjectURL = jest.fn();
    const mockRevokeObjectURL = jest.fn();

    beforeAll(() => {
        // Mocking window.URL.createObjectURL and window.URL.revokeObjectURL
        Object.defineProperty(window.URL, "createObjectURL", { value: mockCreateObjectURL });
        Object.defineProperty(window.URL, "revokeObjectURL", { value: mockRevokeObjectURL });
    });

    afterEach(() => {
        // Clear mocks after each test
        jest.clearAllMocks();
    });

    it("should return the correct width and height of the image", async () => {
        const mockFile = new Blob(); // Mock a Blob file
        const mockUrl = "blob:http://example.com"; // Mock the object URL

        // Mock URL.createObjectURL
        mockCreateObjectURL.mockReturnValue(mockUrl);

        // Mock the Image element
        const mockImageElement = new Image();

        // Mock the naturalWidth and naturalHeight properties using Object.defineProperty
        Object.defineProperty(mockImageElement, "naturalWidth", { value: 200 });
        Object.defineProperty(mockImageElement, "naturalHeight", { value: 100 });

        // Spy on the Image constructor
        jest.spyOn(global, "Image").mockImplementation(() => mockImageElement);

        // Trigger the load event manually
        setTimeout(() => mockImageElement.onload && mockImageElement.onload(new Event("load")), 0);

        const result = await getFileDimensions(mockFile);

        expect(result).toEqual({ width: 200, height: 100 });
        expect(mockCreateObjectURL).toHaveBeenCalledWith(mockFile);
        expect(mockRevokeObjectURL).toHaveBeenCalledWith(mockUrl);
    });

    it("should reject if the image fails to load", async () => {
        const mockFile = new Blob();
        const mockUrl = "blob:http://example.com";

        // Mock URL.createObjectURL
        mockCreateObjectURL.mockReturnValue(mockUrl);

        // Mock the Image element
        const mockImageElement = new Image();

        // Spy on the Image constructor
        jest.spyOn(global, "Image").mockImplementation(() => mockImageElement);

        // Trigger the error event manually
        setTimeout(() => mockImageElement.onerror && mockImageElement.onerror(new Event("error")), 0);

        // Await for the promise rejection
        await expect(getFileDimensions(mockFile)).rejects.toThrow("Image failed to load");
        expect(mockCreateObjectURL).toHaveBeenCalledWith(mockFile);
        expect(mockRevokeObjectURL).toHaveBeenCalledWith(mockUrl);
    });
});
