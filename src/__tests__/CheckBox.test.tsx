import "@testing-library/jest-dom";

import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import { CheckBox } from "@/components/CheckBox";

// Mos styles for component
jest.mock("@/components/CheckBox/CheckBox.module.scss", () => ({
    "checkbox-wrapper": "checkbox-wrapper",
    "label-content": "label-content",
}));

describe("CheckBox Component", () => {
    it("renders with the correct label", () => {
        render(<CheckBox id="test-checkbox" label="Test Label" name="test" onClick={jest.fn()} checked={false} />);

        const labelElement = screen.getByText("Test Label");
        expect(labelElement).toBeInTheDocument();
    });

    it("initially renders as unchecked when checked is false", () => {
        render(<CheckBox id="test-checkbox" label="Test Label" name="test" onClick={jest.fn()} checked={false} />);

        const inputElement = screen.getByRole("checkbox");
        expect(inputElement).not.toBeChecked();
    });

    it("initially renders as checked when checked is true", () => {
        render(<CheckBox id="test-checkbox" label="Test Label" name="test" onClick={jest.fn()} checked={true} />);

        const inputElement = screen.getByRole("checkbox");
        expect(inputElement).toBeChecked();
    });

    it("calls the onClick function when checkbox is clicked", () => {
        const onClickMock = jest.fn();
        render(<CheckBox id="test-checkbox" label="Test Label" name="test" onClick={onClickMock} checked={false} />);

        const inputElement = screen.getByRole("checkbox");
        fireEvent.click(inputElement);

        expect(onClickMock).toHaveBeenCalled();
    });

    it("updates isChecked state when clicked", () => {
        const onClickMock = jest.fn();
        render(<CheckBox id="test-checkbox" label="Test Label" name="test" onClick={onClickMock} checked={false} />);

        const inputElement = screen.getByRole("checkbox");
        expect(inputElement).not.toBeChecked();

        fireEvent.click(inputElement);

        expect(inputElement).toBeChecked();
    });

    it("unchecks the checkbox when checked prop changes to false", () => {
        const { rerender } = render(
            <CheckBox id="test-checkbox" label="Test Label" name="test" onClick={jest.fn()} checked={true} />
        );

        const inputElement = screen.getByRole("checkbox");
        expect(inputElement).toBeChecked();

        rerender(<CheckBox id="test-checkbox" label="Test Label" name="test" onClick={jest.fn()} checked={false} />);

        expect(inputElement).not.toBeChecked();
    });
});
