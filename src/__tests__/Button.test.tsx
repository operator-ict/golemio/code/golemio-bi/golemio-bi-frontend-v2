import "@testing-library/jest-dom";

import { fireEvent, render, screen } from "@testing-library/react";
import { useRouter } from "next/navigation";
import React from "react";

import Button from "@/components/Button";

// Mock the Next.js useRouter hook
jest.mock("next/navigation", () => ({
    useRouter: jest.fn(),
}));

const mockedUseRouter = useRouter as jest.Mock;

describe("Button Component", () => {
    beforeEach(() => {
        mockedUseRouter.mockReturnValue({ push: jest.fn() });
    });

    test("renders button with label and children", () => {
        render(
            <Button label="Click Me" color="primary">
                Child Content
            </Button>
        );
        expect(screen.getByText("Click Me")).toBeInTheDocument();
        expect(screen.getByText("Child Content")).toBeInTheDocument();
    });

    test("applies correct classes based on props", () => {
        const { container } = render(
            <Button color="secondary" size="lg" square circle>
                Test Button
            </Button>
        );
        const button = container.querySelector("button");
        expect(button).toHaveClass("button-color_secondary");
        expect(button).toHaveClass("button-size_lg");
        expect(button).toHaveClass("button-square");
        expect(button).toHaveClass("button-circle");
    });

    test("renders iconStart and iconEnd correctly", () => {
        render(
            <Button
                iconStart={<span data-testid="icon-start">Start</span>}
                iconEnd={<span data-testid="icon-end">End</span>}
                color="primary"
            >
                With Icons
            </Button>
        );
        expect(screen.getByTestId("icon-start")).toBeInTheDocument();
        expect(screen.getByTestId("icon-end")).toBeInTheDocument();
    });

    test("handles button click", () => {
        const handleClick = jest.fn();
        render(
            <Button onClick={handleClick} color="primary">
                Click Me
            </Button>
        );
        fireEvent.click(screen.getByText("Click Me"));
        expect(handleClick).toHaveBeenCalled();
    });

    test("navigates to URL when url prop is provided", () => {
        const push = jest.fn();
        mockedUseRouter.mockReturnValue({ push });
        render(
            <Button url="/some-url" color="primary">
                Navigate
            </Button>
        );
        fireEvent.click(screen.getByText("Navigate"));
        expect(push).toHaveBeenCalledWith("/some-url");
    });

    test("renders button with correct aria-label when label is provided", () => {
        render(
            <Button label="Accessible Button" color="primary">
                Button
            </Button>
        );
        expect(screen.getByRole("button")).toHaveAttribute("aria-label", "Accessible Button");
    });

    test("does not render label if hideLabel is true", () => {
        render(
            <Button label="Hidden Label" color="primary" hideLabel>
                Button
            </Button>
        );
        expect(screen.queryByText("Hidden Label")).not.toBeInTheDocument();
    });

    test("renders with correct id", () => {
        render(
            <Button id="test-id" color="primary">
                Button
            </Button>
        );
        expect(screen.getByRole("button")).toHaveAttribute("id", "test-id");
    });
});
