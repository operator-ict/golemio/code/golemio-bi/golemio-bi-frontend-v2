import "@testing-library/jest-dom";

import { render } from "@testing-library/react";
import React from "react";

import { Card } from "@/components/Card";

// Mock styles for a component
jest.mock("./Card.module.scss", () => ({
    card: "card",
    card_horizontal: "card_horizontal",
    card_active: "card_active",
}));

describe("Card Component", () => {
    it('renders a div when tag is "div"', () => {
        const { container } = render(<Card tag="div" className="test-class" horizontal="true" />);
        const cardElement = container.firstChild;
        expect(cardElement).toBeInTheDocument();
        expect(cardElement).toHaveClass("card test-class card_horizontal");
    });

    it('renders a button when tag is "button"', () => {
        const { container } = render(<Card tag="button" className="test-class" horizontal="false" label="Click me" />);
        const cardElement = container.firstChild;
        expect(cardElement).toBeInTheDocument();
        expect(cardElement).toHaveClass("card test-class card_active");
        expect(cardElement).toHaveAttribute("aria-label", "Click me");
    });

    it('renders an anchor when tag is "a" and href is provided', () => {
        const { container } = render(<Card tag="a" href="https://example.com" className="test-class" horizontal="false" />);
        const cardElement = container.firstChild;
        expect(cardElement).toBeInTheDocument();
        expect(cardElement).toHaveAttribute("href", "https://example.com");
        expect(cardElement).toHaveClass("card test-class card_active");
    });

    it("applies horizontal class when horizontal prop is true", () => {
        const { container } = render(<Card tag="div" className="test-class" horizontal="true" />);
        const cardElement = container.firstChild;
        expect(cardElement).toHaveClass("card_horizontal");
    });

    it("does not apply horizontal class when horizontal prop is false", () => {
        const { container } = render(<Card tag="div" className="test-class" horizontal="false" />);
        const cardElement = container.firstChild;
        expect(cardElement).not.toHaveClass("card_horizontal");
    });
});
