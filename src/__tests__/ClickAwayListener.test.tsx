import { fireEvent, render } from "@testing-library/react";
import React from "react";

import ClickAwayListener from "@/utils/ClickAwayListener";

describe("ClickAwayListener", () => {
    it("should call onClickAway when clicking outside of the component", () => {
        const handleClickAway = jest.fn();

        render(
            <ClickAwayListener onClickAway={handleClickAway}>
                <div>Inside Component</div>
            </ClickAwayListener>
        );

        // Simulate clicking outside the component
        fireEvent.mouseDown(document.body);

        expect(handleClickAway).toHaveBeenCalledTimes(1);
    });

    it("should not call onClickAway when clicking inside the component", () => {
        const handleClickAway = jest.fn();

        const { getByText } = render(
            <ClickAwayListener onClickAway={handleClickAway}>
                <div>Inside Component</div>
            </ClickAwayListener>
        );

        // Simulate clicking inside the component
        fireEvent.mouseDown(getByText("Inside Component"));

        expect(handleClickAway).not.toHaveBeenCalled();
    });
});
