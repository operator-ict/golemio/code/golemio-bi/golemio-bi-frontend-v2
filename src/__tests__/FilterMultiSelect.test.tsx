import "@testing-library/jest-dom";

import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import FilterMultiSelect from "@/components/FilterMultiSelect";

// Mock-function for reset button
jest.mock("@/(components)/ResetFiltersButton", () => ({
    FilterReset: jest.fn(({ onClick }) => <button onClick={onClick}>Reset</button>),
}));

describe("FilterMultiSelect", () => {
    const optionList = [
        { value: "option1", label: "Option 1", count: 5 },
        { value: "option2", label: "Option 2", count: 0 }, // disabled
        { value: "option3", label: "Option 3", count: 2 },
    ];

    const mockOnChange = jest.fn();

    afterEach(() => {
        jest.clearAllMocks();
    });

    it("renders the options correctly", () => {
        render(<FilterMultiSelect onChange={mockOnChange} optionList={optionList} values={[]} title="Filter Options" />);

        // Checking that options are displayed
        optionList.forEach((option) => {
            const checkbox = screen.getByLabelText(option.label);
            expect(checkbox).toBeInTheDocument();
            if (option.count === 0) {
                expect(checkbox).toBeDisabled();
            } else {
                expect(checkbox).not.toBeDisabled();
            }
        });
    });

    it("calls onChange when an option is selected", () => {
        render(<FilterMultiSelect onChange={mockOnChange} optionList={optionList} values={[]} title="Filter Options" />);

        // Clicking on the first checkbox
        const checkbox1 = screen.getByLabelText("Option 1");
        fireEvent.click(checkbox1);

        expect(mockOnChange).toHaveBeenCalledWith(["option1"]);
    });

    it("calls onChange when an option is deselected", () => {
        render(<FilterMultiSelect onChange={mockOnChange} optionList={optionList} values={["option1"]} title="Filter Options" />);

        // Clicking on the first checkbox (unchecking)
        const checkbox1 = screen.getByLabelText("Option 1");
        fireEvent.click(checkbox1);

        expect(mockOnChange).toHaveBeenCalledWith([]);
    });

    it("renders the reset button if resetButton prop is provided", () => {
        const mockReset = jest.fn();

        render(
            <FilterMultiSelect
                onChange={mockOnChange}
                optionList={optionList}
                values={[]}
                title="Filter Options"
                resetButton={mockReset}
            />
        );

        const resetButton = screen.getByText("Reset");
        expect(resetButton).toBeInTheDocument();

        // Pressing the reset button
        fireEvent.click(resetButton);
        expect(mockReset).toHaveBeenCalled();
    });

    it("does not render the reset button if resetButton prop is not provided", () => {
        render(<FilterMultiSelect onChange={mockOnChange} optionList={optionList} values={[]} title="Filter Options" />);

        const resetButton = screen.queryByText("Reset");
        expect(resetButton).not.toBeInTheDocument();
    });
});
