import { fireEvent, render, screen } from "@testing-library/react";
import { useRouter, useSearchParams } from "next/navigation";
import { usePlausible } from "next-plausible";

import { SearchForm } from "@/components/SearchForm";

// Mock next/navigation and plausible
jest.mock("next/navigation", () => ({
    useRouter: jest.fn(),
    useSearchParams: jest.fn(),
}));
jest.mock("next-plausible", () => ({
    usePlausible: jest.fn(),
}));

describe("SearchForm", () => {
    const pushMock = jest.fn();
    const plausibleMock = jest.fn();
    const mockSearchParams = new URLSearchParams();

    beforeEach(() => {
        (useRouter as jest.Mock).mockReturnValue({ push: pushMock });
        (useSearchParams as jest.Mock).mockReturnValue(mockSearchParams);
        (usePlausible as jest.Mock).mockReturnValue(plausibleMock);
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it("should render the search input with a placeholder", () => {
        render(<SearchForm placeholder="Search..." />);
        const inputElement = screen.getByPlaceholderText("Search...");
        expect(inputElement).toBeInTheDocument();
    });

    it("should update the input value when typing", () => {
        render(<SearchForm placeholder="Search..." />);
        const inputElement = screen.getByPlaceholderText("Search...");
        fireEvent.change(inputElement, { target: { value: "test search" } });
        expect(inputElement).toHaveValue("test search");
    });

    it("should clear the input value when reset button is clicked", () => {
        render(<SearchForm placeholder="Search..." />);
        const inputElement = screen.getByPlaceholderText("Search...");

        // Set input value so that the reset button appears
        fireEvent.change(inputElement, { target: { value: "test search" } });
        const resetButton = screen.getByTestId("reset-button");

        expect(inputElement).toHaveValue("test search");

        fireEvent.click(resetButton);

        // Expect input to be cleared
        expect(inputElement).toHaveValue("");
        expect(pushMock).toHaveBeenCalledWith("?");
    });

    it("should trigger a search when the Enter key is pressed", () => {
        render(<SearchForm placeholder="Search..." />);
        const inputElement = screen.getByPlaceholderText("Search...");

        fireEvent.change(inputElement, { target: { value: "test search" } });
        fireEvent.keyDown(inputElement, { key: "Enter", code: "Enter" });

        // Normalize the result by replacing "+" with "%20"
        const normalizedURL = pushMock.mock.calls[0][0].replace(/\+/g, "%20");
        expect(normalizedURL).toBe("?search=test%20search");
    });

    it("should handle favorite tag in the URL", () => {
        mockSearchParams.set("fav", "true");

        render(<SearchForm placeholder="Search..." />);
        const inputElement = screen.getByPlaceholderText("Search...");

        fireEvent.change(inputElement, { target: { value: "test search" } });
        fireEvent.keyDown(inputElement, { key: "Enter", code: "Enter" });

        // Normalize the result by replacing "+" with "%20"
        const normalizedURL = pushMock.mock.calls[0][0].replace(/\+/g, "%20");
        expect(normalizedURL).toBe("?search=test%20search&fav=true");
    });
});
