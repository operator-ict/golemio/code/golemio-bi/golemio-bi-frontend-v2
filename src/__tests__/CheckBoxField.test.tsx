import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import CheckBoxField from "@/components/CheckBoxField";

import styles from "../CheckBoxField.module.scss";

describe("CheckBoxField Component", () => {
    it("should render the checkbox with label", () => {
        const label = "Test Checkbox";
        const onChange = jest.fn();

        render(<CheckBoxField id="test-checkbox" label={label} isChecked={false} onChange={onChange} />);

        const checkbox = screen.getByRole("checkbox", { name: label });
        const labelText = screen.getByText(label);

        // Ensure the checkbox and label are rendered
        expect(checkbox).toBeInTheDocument();
        expect(labelText).toBeInTheDocument();

        // Check the initial state of the checkbox
        expect(checkbox).not.toBeChecked();
    });

    it("should call onChange and toggle checked state when checkbox is clicked", () => {
        const onChange = jest.fn();
        const label = "Test Checkbox";

        render(<CheckBoxField id="test-checkbox" label={label} isChecked={false} onChange={onChange} />);

        const checkbox = screen.getByRole("checkbox", { name: label });

        // Simulate clicking the checkbox
        fireEvent.click(checkbox);

        // Ensure onChange is called
        expect(onChange).toHaveBeenCalledTimes(1);

        // Ensure the checkbox is checked after the click
        expect(checkbox).toBeChecked();
    });

    it("should apply the error class when error prop is true", () => {
        const label = "Test Checkbox";
        const onChange = jest.fn();

        render(<CheckBoxField id="test-checkbox" label={label} isChecked={false} onChange={onChange} error />);

        const checkbox = screen.getByRole("checkbox", { name: label });
        const labelElement = screen.getByText(label);

        // Check that the error class is applied to the checkbox and label
        expect(checkbox).toHaveClass(styles["check-box__invalid"]);
        expect(labelElement).toHaveClass(styles["error"]);
    });

    it("should toggle the checked state based on the isChecked prop", () => {
        const onChange = jest.fn();
        const label = "Test Checkbox";

        const { rerender } = render(<CheckBoxField id="test-checkbox" label={label} isChecked={false} onChange={onChange} />);

        const checkbox = screen.getByRole("checkbox", { name: label });

        // Initially unchecked
        expect(checkbox).not.toBeChecked();

        // Rerender with isChecked true
        rerender(<CheckBoxField id="test-checkbox" label={label} isChecked={true} onChange={onChange} />);

        // Ensure the checkbox is checked after rerender
        expect(checkbox).toBeChecked();
    });
});
