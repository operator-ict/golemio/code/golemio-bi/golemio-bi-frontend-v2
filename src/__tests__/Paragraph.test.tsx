import { render, screen } from "@testing-library/react";
import React from "react";

import { Paragraph } from "@/components/Paragraph";

import styles from "../Paragraph.module.scss";

describe("Paragraph component", () => {
    it("renders with the correct type class", () => {
        render(<Paragraph type="p1" />);

        const paragraphElement = screen.getByRole("paragraph");

        // Ensure the correct class for the type is applied
        expect(paragraphElement).toHaveClass(styles.paragraph);
        expect(paragraphElement).toHaveClass(styles["paragraph__p1"]);
    });

    it("applies the 'centered' class when centered prop is true", () => {
        render(<Paragraph type="p2" centered />);

        const paragraphElement = screen.getByRole("paragraph");

        // Check that the 'centered' class is applied
        expect(paragraphElement).toHaveClass(styles.centered);
    });

    it("applies additional classes passed via className", () => {
        const customClass = "custom-class";
        render(<Paragraph type="p3" className={customClass} />);

        const paragraphElement = screen.getByRole("paragraph");

        // Ensure that the custom class is applied along with the type class
        expect(paragraphElement).toHaveClass(styles.paragraph);
        expect(paragraphElement).toHaveClass(styles["paragraph__p3"]);
        expect(paragraphElement).toHaveClass(customClass);
    });

    it("forwards the ref to the paragraph element", () => {
        const ref = React.createRef<HTMLParagraphElement>();
        render(<Paragraph type="p4" ref={ref} />);

        // Ensure the ref is forwarded correctly
        expect(ref.current).toBeInstanceOf(HTMLParagraphElement);
    });
});
