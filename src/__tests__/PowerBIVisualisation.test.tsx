import "@testing-library/jest-dom";

import { act, fireEvent, render, screen, waitFor } from "@testing-library/react";
import { useRouter } from "next/navigation";
import { Report } from "powerbi-client";
import { PowerBIEmbed } from "powerbi-client-react";
import React from "react";

import { IVisualization, PowerBIVisualisation } from "@/components/PowerBIVisualisation";

// Mock next/router useRouter
jest.mock("next/navigation", () => ({
    useRouter: jest.fn(),
}));

// Mock PowerBIEmbed component from powerbi-client-react
jest.mock("powerbi-client-react", () => ({
    PowerBIEmbed: jest.fn(() => <div data-testid="powerbi-embed" />),
}));

describe("PowerBIVisualisation component", () => {
    const mockVisualization: IVisualization = {
        reportId: ["12345"],
        workspaceId: "workspace-id",
        embedUrl: "https://app.powerbi.com/reportEmbed",
        accessToken: "fake-access-token",
        tokenExpiration: new Date(Date.now() + 10 * 60 * 1000).toISOString(), // 10 minutes in the future
        visualisationType: "report",
        filtersVisible: true,
        filtersExpanded: false,
        navContentPaneEnabled: true,
        background: "transparent",
    };

    it("renders the PowerBI embed component", () => {
        render(<PowerBIVisualisation visualization={mockVisualization} />);

        const powerBIEmbed = screen.getByTestId("powerbi-embed");
        expect(powerBIEmbed).toBeInTheDocument();
    });

    it("calls report fullscreen when the fullscreen button is clicked", () => {
        const mockReport = {
            fullscreen: jest.fn(),
        } as unknown as Report;

        // Store the getEmbeddedComponent function
        let getEmbeddedComponentFn: (embedObject: Report) => void;
        (PowerBIEmbed as jest.Mock).mockImplementation(({ getEmbeddedComponent }) => {
            getEmbeddedComponentFn = getEmbeddedComponent;
            return <div data-testid="powerbi-embed" />;
        });

        render(<PowerBIVisualisation visualization={mockVisualization} />);

        // Call getEmbeddedComponent outside of the render phase
        act(() => {
            getEmbeddedComponentFn(mockReport);
        });

        const fullscreenButton = screen.getByRole("button", { name: /full size/i });
        fireEvent.click(fullscreenButton);

        expect(mockReport.fullscreen).toHaveBeenCalledTimes(1);
    });

    it("refreshes the router when the token is about to expire", async () => {
        const mockRouter = { refresh: jest.fn() };
        (useRouter as jest.Mock).mockReturnValue(mockRouter);

        const expiringTokenVisualization = {
            ...mockVisualization,
            tokenExpiration: new Date(Date.now() + 1 * 60 * 1000).toISOString(), // 1 minute in the future
        };

        jest.useFakeTimers();

        // Mock Date.now() to return a fixed time
        const now = Date.now();
        jest.spyOn(Date, "now").mockImplementation(() => now);

        // Store and call getEmbeddedComponent
        let getEmbeddedComponentFn: (embedObject: Report) => void;
        (PowerBIEmbed as jest.Mock).mockImplementation(({ getEmbeddedComponent }) => {
            getEmbeddedComponentFn = getEmbeddedComponent;
            return <div data-testid="powerbi-embed" />;
        });

        render(<PowerBIVisualisation visualization={expiringTokenVisualization} />);

        // Call getEmbeddedComponent to set the Report in the component
        act(() => {
            getEmbeddedComponentFn({} as Report);
        });

        // Fast-forward time to trigger the interval
        jest.advanceTimersByTime(60000); // Advance by 60 seconds

        await waitFor(() => {
            expect(mockRouter.refresh).toHaveBeenCalled();
        });

        jest.useRealTimers(); // Restore real timers after the test
    });
});
