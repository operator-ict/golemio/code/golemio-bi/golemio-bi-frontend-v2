import { fireEvent, render, screen } from "@testing-library/react";

import { Dropdown } from "@/components/Dropdown";

describe("Dropdown component", () => {
    const mockHide = jest.fn();

    beforeAll(() => {
        global.ResizeObserver = jest.fn().mockImplementation(() => ({
            observe: jest.fn(),
            unobserve: jest.fn(),
            disconnect: jest.fn(),
        }));
    });

    const renderDropdown = (visible = true, filter = false, hide = mockHide) => {
        return render(
            <Dropdown
                visible={visible}
                hide={hide}
                filter={filter}
                contents={<div data-testid="dropdown-contents">Dropdown Content</div>}
            >
                <button>Toggle Dropdown</button>
            </Dropdown>
        );
    };

    it("should render dropdown contents when visible is true", () => {
        renderDropdown(true);
        expect(screen.getByTestId("dropdown-contents")).toBeInTheDocument();
    });

    it("should not render dropdown contents when visible is false", () => {
        renderDropdown(false);
        expect(screen.queryByTestId("dropdown-contents")).not.toBeInTheDocument();
    });

    it("should call hide function when clicked outside the dropdown", () => {
        renderDropdown(true);

        fireEvent.click(document);
        expect(mockHide).toHaveBeenCalledTimes(1);
    });

    it("should apply filter class when filter is true", () => {
        renderDropdown(true, true);
        const dropdown = screen.getByTestId("dropdown-contents").parentElement;
        expect(dropdown).toHaveClass("dropdown-filter");
    });

    it("should apply regular dropdown class when filter is false", () => {
        renderDropdown(true, false);
        const dropdown = screen.getByTestId("dropdown-contents").parentElement;
        expect(dropdown).toHaveClass("dropdown");
    });
});
