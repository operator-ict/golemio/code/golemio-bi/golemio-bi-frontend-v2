import "@testing-library/jest-dom";

import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import { Radio } from "@/components/Radio";

describe("Radio component", () => {
    // Mock the onClick handler
    const mockOnClick = jest.fn();

    const defaultProps = {
        id: "radio-1",
        label: "Option 1",
        name: "radio-group",
        checked: false,
        onClick: mockOnClick,
    };

    afterEach(() => {
        jest.clearAllMocks();
    });

    it("renders correctly with the label and unchecked state", () => {
        render(<Radio {...defaultProps} />);

        // Check that the radio input and label are rendered correctly
        const radioInput = screen.getByRole("radio", { name: "Option 1" });
        expect(radioInput).toBeInTheDocument();
        expect(radioInput).toHaveAttribute("name", "radio-group");
        expect(radioInput).not.toBeChecked();

        const label = screen.getByText("Option 1");
        expect(label).toBeInTheDocument();
    });

    it("calls the onClick handler when the radio button is clicked", () => {
        render(<Radio {...defaultProps} />);

        const radioInput = screen.getByRole("radio", { name: "Option 1" });

        // Simulate a user click
        fireEvent.click(radioInput);

        // Ensure that the mock onClick function is called
        expect(mockOnClick).toHaveBeenCalledTimes(1);
    });

    it("updates the checked state when clicked", () => {
        const { rerender } = render(<Radio {...defaultProps} checked={false} />);

        const radioInput = screen.getByRole("radio", { name: "Option 1" });

        // Initially, it should not be checked
        expect(radioInput).not.toBeChecked();

        // Simulate clicking the radio button
        fireEvent.click(radioInput);

        rerender(<Radio {...defaultProps} checked={false} />);

        // Check that the input is now checked after being clicked
        expect(radioInput).toBeChecked();
    });

    it("updates the checked state when the checked prop changes", () => {
        const { rerender } = render(<Radio {...defaultProps} checked={false} />);

        const radioInput = screen.getByRole("radio", { name: "Option 1" });

        // Initially, it should not be checked
        expect(radioInput).not.toBeChecked();

        // Rerender the component with the checked prop set to true
        rerender(<Radio {...defaultProps} checked={true} />);

        // Now it should be checked
        expect(radioInput).toBeChecked();
    });
});
