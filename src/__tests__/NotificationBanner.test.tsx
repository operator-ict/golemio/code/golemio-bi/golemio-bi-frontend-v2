import { render, screen } from "@testing-library/react";
import React from "react";

import NotificationBanner, { NotificationType } from "@/components/NotificationBanner"; // Adjust the path to your actual component

// Mocking the icons
jest.mock("@/icons/InfoIcon", () => ({
    InfoIcon: () => <div data-testid="info-icon" />,
}));
jest.mock("@/icons/WarningIcon", () => ({
    WarningIcon: () => <div data-testid="warning-icon" />,
}));
jest.mock("@/icons/ErrorIcon", () => ({
    ErrorIcon: () => <div data-testid="error-icon" />,
}));

describe("NotificationBanner", () => {
    it("should render Info notification with correct icon and text", () => {
        const mockText = "This is an info message";

        render(<NotificationBanner type={NotificationType.Info} text={mockText} />);

        // Check that the Info icon is rendered
        expect(screen.getByTestId("info-icon")).toBeInTheDocument();

        // Check that the correct text is rendered
        expect(screen.getByText(mockText)).toBeInTheDocument();

        // Check that the correct class is applied
        const bannerElement = screen.getByText(mockText).closest("div");
        expect(bannerElement).toHaveClass("color-info");
    });

    it("should render Warning notification with correct icon and text", () => {
        const mockText = "This is a warning message";

        render(<NotificationBanner type={NotificationType.Warning} text={mockText} />);

        expect(screen.getByTestId("warning-icon")).toBeInTheDocument();

        expect(screen.getByText(mockText)).toBeInTheDocument();

        const bannerElement = screen.getByText(mockText).closest("div");
        expect(bannerElement).toHaveClass("color-warning");
    });

    it("should render Error notification with correct icon and text", () => {
        const mockText = "This is an error message";

        render(<NotificationBanner type={NotificationType.Error} text={mockText} />);

        // Check that the Error icon is rendered
        expect(screen.getByTestId("error-icon")).toBeInTheDocument();

        // Check that the correct text is rendered
        expect(screen.getByText(mockText)).toBeInTheDocument();

        // Check that the correct class is applied
        const bannerElement = screen.getByText(mockText).closest("div");
        expect(bannerElement).toHaveClass("color-error");
    });
});
