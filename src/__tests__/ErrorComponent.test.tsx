import { render, screen } from "@testing-library/react";

import { ErrorComponent } from "@/components/ErrorComponent";

describe("ErrorComponent", () => {
    it("should render default error message if no errorStatusCode is provided", () => {
        render(<ErrorComponent />);
        expect(screen.getByText("Something went wrong")).toBeInTheDocument();
    });

    it("should render 400 error message if errorStatusCode is 400", () => {
        render(<ErrorComponent errorStatusCode={400} />);
        expect(screen.getByText("Bad Request: The server could not understand the request.")).toBeInTheDocument();
    });

    it("should render 401 error message if errorStatusCode is 401", () => {
        render(<ErrorComponent errorStatusCode={401} />);
        expect(
            screen.getByText("Unauthorized: Authentication is required and has failed or has not been provided.")
        ).toBeInTheDocument();
    });

    it("should render 403 error message if errorStatusCode is 403", () => {
        render(<ErrorComponent errorStatusCode={403} />);
        expect(screen.getByText("Forbidden: You don't have permission to access the requested resource.")).toBeInTheDocument();
    });

    it("should render 500 error message if errorStatusCode is 500", () => {
        render(<ErrorComponent errorStatusCode={500} />);
        expect(
            screen.getByText("Internal Server Error: The server has encountered a situation it doesn't know how to handle.")
        ).toBeInTheDocument();
    });

    it("should render custom error message if message prop is provided", () => {
        const customMessage = "Custom error occurred.";
        render(<ErrorComponent message={customMessage} />);
        expect(screen.getByText(customMessage)).toBeInTheDocument();
    });

    it("should apply the correct CSS class", () => {
        const { container } = render(<ErrorComponent />);
        expect(container.firstChild).toHaveClass("error-container");
    });
});
