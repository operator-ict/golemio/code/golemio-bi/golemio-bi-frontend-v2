import "@testing-library/jest-dom";

import { flexRender, useReactTable } from "@tanstack/react-table";
import { render, screen } from "@testing-library/react";
import { useRouter } from "next/navigation";

import { DataTable } from "@/components/TableComponent/Table";

jest.mock("next/navigation", () => ({
    useRouter: jest.fn(),
    useSearchParams: jest.fn().mockReturnValue({
        get: jest.fn(),
    }),
}));

jest.mock("@tanstack/react-table", () => ({
    useReactTable: jest.fn(),
    getCoreRowModel: jest.fn(),
    getPaginationRowModel: jest.fn(),
    getSortedRowModel: jest.fn(),
    getFilteredRowModel: jest.fn(),
    flexRender: jest.fn(),
}));

jest.mock("../app/(components)/TablePagination", () => jest.fn(() => <div>Pagination</div>));

describe("DataTable Component", () => {
    const mockRouterPush = jest.fn();

    beforeEach(() => {
        (useRouter as jest.Mock).mockReturnValue({
            push: mockRouterPush,
        });

        // Mock flexRender to return the column header and cell content
        (flexRender as jest.Mock).mockImplementation((content) => {
            return typeof content === "function" ? content() : content;
        });

        // Mock the behavior of the table instance from react-table
        (useReactTable as jest.Mock).mockReturnValue({
            getHeaderGroups: jest.fn().mockReturnValue([
                {
                    id: "headerGroup1",
                    headers: [
                        {
                            id: "header1",
                            isPlaceholder: false,
                            column: { columnDef: { header: "Header 1" }, getCanSort: jest.fn() },
                            getContext: jest.fn(),
                            getSize: jest.fn().mockReturnValue(100),
                        },
                    ],
                },
            ]),
            getRowModel: jest.fn().mockReturnValue({
                rows: [],
            }),
            getSortedRowModel: jest.fn().mockReturnValue({
                rows: [],
            }),
            getFilteredRowModel: jest.fn().mockReturnValue({
                rows: [],
            }),
        });
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    const columns = [{ header: "Header 1", accessorKey: "col1", column: { getCanSort: () => true } }];

    it("renders table headers and rows", () => {
        (useReactTable as jest.Mock).mockReturnValue({
            getHeaderGroups: jest.fn().mockReturnValue([
                {
                    id: "headerGroup1",
                    headers: [
                        {
                            id: "header1",
                            isPlaceholder: false,
                            column: { columnDef: { header: "Header 1" }, getCanSort: jest.fn() },
                            getContext: jest.fn(),
                            getSize: jest.fn().mockReturnValue(100),
                        },
                    ],
                },
            ]),
            getRowModel: jest.fn().mockReturnValue({
                rows: [
                    {
                        id: "row1",
                        getIsSelected: jest.fn().mockReturnValue(false),
                        getVisibleCells: jest.fn().mockReturnValue([
                            {
                                id: "cell1",
                                column: { columnDef: { cell: "Cell Content" } },
                                getContext: jest.fn(),
                            },
                        ]),
                    },
                ],
            }),
        });
        const data = [{ col1: "Data 1" }];

        render(<DataTable columns={columns} data={data} limit={5} page={1} />);

        // Check if header is rendered
        expect(screen.getByText("Header 1")).toBeInTheDocument();

        // Check if cell content is rendered
        expect(screen.getByText("Cell Content")).toBeInTheDocument();
    });

    it("displays no results message when no data is available", () => {
        render(<DataTable columns={columns} data={[]} limit={1} page={1} />);

        expect(screen.getByText("No results.")).toBeInTheDocument();
    });
});
