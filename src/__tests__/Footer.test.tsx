/* eslint-disable react/display-name */
import { render, screen } from "@testing-library/react";
import { memo } from "react";

import Footer from "@/components/Footer";
import text from "@/textContent/cs.json";

// Mock child components
jest.mock("@/components/Footer/Logos", () => ({
    __esModule: true,
    default: memo(() => <div>Mocked Logos</div>),
}));
jest.mock("@/components/CookieConsent", () => ({ CookieBanner: memo(() => <div>Mocked CookieBanner</div>) }));
jest.mock("@/components/Footer/AgreementLinks", () => ({
    __esModule: true,
    default: memo(() => <div>Mocked AgreementLinks</div>),
}));

describe("Footer Component", () => {
    test("renders Footer component", () => {
        render(<Footer />);

        const footerElement = screen.getByRole("contentinfo");
        expect(footerElement).toBeInTheDocument();
    });

    test("renders Conditions component with correct link", () => {
        render(<Footer />);

        // Check if the link to the API documentation is present
        const apiLink = screen.getByRole("link", { name: /api/i });
        expect(apiLink).toBeInTheDocument();
        expect(apiLink).toHaveAttribute("href", "https://api.golemio.cz/v2/docs/openapi");
    });

    test("renders mocked child components", () => {
        render(<Footer />);

        // Check if the mocked child components are rendered
        expect(screen.getByText("Mocked Logos")).toBeInTheDocument();
        expect(screen.getByText("Mocked CookieBanner")).toBeInTheDocument();
        expect(screen.getByText("Mocked AgreementLinks")).toBeInTheDocument();
    });

    test("renders footer text", () => {
        render(<Footer />);

        // Check if the footer text is rendered
        const footerText = screen.getByText(text.footer.dataPlatform);
        expect(footerText).toBeInTheDocument();
    });
});
