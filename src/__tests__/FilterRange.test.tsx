import "@testing-library/jest-dom";

import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import FilterRange from "@/components/FilterRange";

describe("FilterRange", () => {
    const mockOnChange = jest.fn();
    const defaultProps = {
        onChange: mockOnChange,
        values: [20, 80],
        min: 0,
        max: 100,
        unit: "°C",
        disabled: false,
    };

    afterEach(() => {
        jest.clearAllMocks();
    });

    it("renders the range slider with initial values", () => {
        render(<FilterRange title={""} {...defaultProps} />);

        // Check that the sliders are rendered with the correct initial values
        const minSlider = screen.getByTestId("from-slider");
        const maxSlider = screen.getByTestId("to-slider");

        expect(minSlider).toHaveValue("20");
        expect(maxSlider).toHaveValue("80");

        // Check the displayed values of the minimum and maximum ranges
        const leftValue = screen.getByText("20°C");
        const rightValue = screen.getByText("80°C");

        expect(leftValue).toBeInTheDocument();
        expect(rightValue).toBeInTheDocument();
    });

    it("calls onChange when sliders are changed", () => {
        render(<FilterRange title={""} {...defaultProps} />);

        const minSlider = screen.getByTestId("from-slider");
        const maxSlider = screen.getByTestId("to-slider");

        // Change the minimum value of the slider
        fireEvent.change(minSlider, { target: { value: "30" } });
        expect(mockOnChange).toHaveBeenCalledWith(30, 80);

        // Change the maximum value of the slider
        fireEvent.change(maxSlider, { target: { value: "90" } });
        expect(mockOnChange).toHaveBeenCalledWith(30, 90);
    });

    it("disables sliders when disabled prop is true", () => {
        render(<FilterRange title={""} {...defaultProps} disabled={true} />);

        const minSlider = screen.getByTestId("from-slider");
        const maxSlider = screen.getByTestId("to-slider");

        expect(minSlider).toBeDisabled();
        expect(maxSlider).toBeDisabled();
    });

    it("prevents min slider from exceeding max slider value", () => {
        render(<FilterRange title={""} {...defaultProps} />);

        const minSlider = screen.getByTestId("from-slider");

        // Trying to set the minimum value greater than the maximum
        fireEvent.change(minSlider, { target: { value: "85" } });

        // Expect the minimum value to be limited to maxVal - 1
        expect(mockOnChange).toHaveBeenCalledWith(79, 80); // 80 - 1 = 79
    });

    it("prevents max slider from being lower than min slider value", () => {
        render(<FilterRange title={""} {...defaultProps} />);

        const maxSlider = screen.getByTestId("to-slider");

        // Trying to set the maximum value to less than the minimum
        fireEvent.change(maxSlider, { target: { value: "10" } });

        // Expect the maximum value to be limited to minVal + 1
        expect(mockOnChange).toHaveBeenCalledWith(20, 21); // 20 + 1 = 21
    });
});
