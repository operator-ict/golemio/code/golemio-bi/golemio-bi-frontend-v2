import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import { Chip } from "@/components/Chip";

// Mock DeleteChipIcon
jest.mock("@/icons/formIcons/DeleteChipIcon", () => ({
    __esModule: true,
    default: () => <svg data-testid="delete-icon" />,
}));

describe("Chip Component", () => {
    it("should render a button with the correct label", () => {
        const label = "Test Button Chip";
        render(<Chip tag="button" label={label} id="1" />);

        const buttonElement = screen.getByRole("button", { name: label });

        // Ensure the button is rendered with the correct text
        expect(buttonElement).toBeInTheDocument();
    });

    it("should render a div with the correct label", () => {
        const label = "Test Div Chip";
        render(<Chip tag="div" label={label} id="2" />);

        const divElement = screen.getByText(label).closest("div");

        // Ensure the div is rendered with the correct text
        expect(divElement).toBeInTheDocument();
    });

    it("should call onClick when button is clicked", () => {
        const onClick = jest.fn();
        const label = "Clickable Chip";
        render(<Chip tag="button" label={label} id="3" onClick={onClick} />);

        const buttonElement = screen.getByRole("button", { name: label });

        fireEvent.click(buttonElement);

        // Ensure onClick handler is called
        expect(onClick).toHaveBeenCalledTimes(1);
    });

    it("should render the delete icon on the left and call onDelete when clicked", () => {
        const onDelete = jest.fn();
        const label = "Deletable Chip";
        render(<Chip tag="div" label={label} id="4" deleteIconLeft onDelete={onDelete} />);

        const deleteButton = screen.getByTestId("delete-icon");

        // Ensure the delete icon is rendered
        expect(deleteButton).toBeInTheDocument();

        fireEvent.click(deleteButton);

        // Ensure onDelete handler is called with the correct ID
        expect(onDelete).toHaveBeenCalledWith("4");
    });

    it("should render the delete icon on the right and call onDelete when clicked", () => {
        const onDelete = jest.fn();
        const label = "Deletable Chip";
        render(<Chip tag="div" label={label} id="5" deleteIconRight onDelete={onDelete} />);

        const deleteButton = screen.getByTestId("delete-icon");

        // Ensure the delete icon is rendered
        expect(deleteButton).toBeInTheDocument();

        fireEvent.click(deleteButton);

        // Ensure onDelete handler is called with the correct ID
        expect(onDelete).toHaveBeenCalledWith("5");
    });
});
