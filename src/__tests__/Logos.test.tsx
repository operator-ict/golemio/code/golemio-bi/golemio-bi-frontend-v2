import { render, screen } from "@testing-library/react";

import Logos from "@/components/Footer/Logos";

// Mock child component
jest.mock("@/components/Footer/LogoIcon/LogoIcon", () => ({
    __esModule: true,
    default: ({ label, url, link }: { label: string; url: string; link: string }) => (
        <a href={link}>
            <img src={url} alt={label} />
        </a>
    ),
}));

describe("Logos Component", () => {
    test("renders Logos component", () => {
        render(<Logos />);

        // Check if the logos container is rendered
        const logoLink = screen.getByRole("img", { name: /Logo OICT/i }).closest("a") as HTMLAnchorElement;
        expect(logoLink).toBeInTheDocument();
        expect(logoLink).toHaveAttribute("href", "https://operatorict.cz");
        const logosContainer = logoLink.closest("div") as HTMLDivElement;
        expect(logosContainer).toBeInTheDocument();
        expect(logosContainer).toHaveClass("logos-container");
    });

    test("renders all logos with correct attributes", () => {
        render(<Logos />);

        // Check if the logos are rendered with correct attributes
        const logos = [
            { name: "Logo OICT", link: "https://operatorict.cz" },
            { name: "Logo Golemio", link: "https://golemio.cz" },
            { name: "Logo Portál Hl.m.P.", link: "https://praha.eu" },
        ];

        logos.forEach((logo) => {
            const logoElement = screen.getByRole("img", { name: logo.name });
            expect(logoElement).toBeInTheDocument();
            expect(logoElement).toHaveAttribute("alt", expect.stringContaining(logo.name));
            expect(logoElement.closest("a")).toHaveAttribute("href", logo.link);
        });
    });
});
