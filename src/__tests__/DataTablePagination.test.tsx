/* eslint-enable @typescript-eslint/no-explicit-any */
import { Table } from "@tanstack/react-table";
import { fireEvent, render, screen } from "@testing-library/react";
import { useRouter } from "next/navigation";
import React from "react";

import DataTablePagination from "@/components/TablePagination";

jest.mock("next/navigation", () => ({
    useRouter: jest.fn(),
}));

// Define a mock implementation for the table object
const mockTable = {
    getState: jest.fn(() => ({ pagination: { pageIndex: 0, pageSize: 10 } })),
    getPageCount: jest.fn(() => 10),
    setPageIndex: jest.fn(),
    getCanPreviousPage: jest.fn(() => false),
    getCanNextPage: jest.fn(() => true),
    previousPage: jest.fn(),
    nextPage: jest.fn(),
    setPageSize: jest.fn(),
    getFilteredRowModel: jest.fn().mockReturnValue({ rows: { length: 100 } }),
};

const sampleProps = {
    table: mockTable as unknown as Table<any>,
    limit: 10,
};

describe("DataTablePagination component", () => {
    beforeEach(() => {
        jest.clearAllMocks();
        // Mock useRouter return value
        (useRouter as jest.Mock).mockReturnValue({
            route: "/",
            push: jest.fn(),
            query: {},
            asPath: "/",
            pathname: "/",
        });
    });

    it("renders pagination component with page size options", () => {
        render(<DataTablePagination {...sampleProps} />);

        expect(screen.getByText("Rows on page")).toBeInTheDocument();
        const pageSizeSelect = screen.getByTestId("page-size-select");
        expect(pageSizeSelect).toBeInTheDocument();
        expect(pageSizeSelect).toHaveValue("10");
        expect(screen.getByText("1-10 of 100")).toBeInTheDocument();
    });

    it("handles next page button click", () => {
        render(<DataTablePagination {...sampleProps} />);

        const nextPageButton = screen.getByLabelText("Next Page");
        fireEvent.click(nextPageButton);

        expect(mockTable.nextPage).toHaveBeenCalledTimes(1);
    });

    it("disables previous page button when on the first page", () => {
        // Simulate first page
        mockTable.getCanPreviousPage.mockReturnValue(false);

        render(<DataTablePagination {...sampleProps} />);

        const prevPageButton = screen.getByLabelText("Previous Page");
        expect(prevPageButton).toBeDisabled();
    });

    it("handles changing page size", () => {
        render(<DataTablePagination {...sampleProps} />);

        const pageSizeSelect = screen.getByTestId("page-size-select");
        fireEvent.change(pageSizeSelect, { target: { value: "20" } });
        expect(mockTable.setPageSize).toHaveBeenCalledWith(20);
    });

    it("displays ellipsis correctly when there are many pages", () => {
        mockTable.getState.mockReturnValue({ pagination: { pageIndex: 5, pageSize: 10 } });
        mockTable.getPageCount.mockReturnValue(100);

        render(<DataTablePagination {...sampleProps} />);

        const ellipsisElements = screen.getAllByText("...");
        expect(ellipsisElements.length).toBeGreaterThan(0);
    });
});
