/* eslint-disable @typescript-eslint/no-explicit-any */
import { fireEvent, render, screen } from "@testing-library/react";
import { usePlausible } from "next-plausible";
import React from "react";

import SearchableDropdown from "@/components/SearchableDropdown";
import { useEnv } from "@/context/EnvProvider";
import { useComponentVisible } from "@/hooks/useComponentVisible";
import { IMetadata } from "@/types/MetadataType";

// Mock necessary modules and hooks
jest.mock("next-plausible", () => ({
    __esModule: true,
    usePlausible: jest.fn(),
}));

jest.mock("@/context/EnvProvider", () => ({
    __esModule: true,
    useEnv: jest.fn(),
}));

jest.mock("@/hooks/useComponentVisible", () => ({
    __esModule: true,
    useComponentVisible: jest.fn(),
}));

jest.mock("@/components/ListItemComponent", () => ({
    __esModule: true,
    default: (props: any) => <li onClick={() => props.selectOption(props.option)}>{props.option.title}</li>,
}));

jest.mock("@/components/SearchInput", () => ({
    __esModule: true,
    SearchInput: (props: any) => <input placeholder={props.placeholder} value={props.value} onChange={props.onChange} />,
}));

jest.mock("@/icons/ChevronIcon", () => ({
    __esModule: true,
    ChevronIcon: () => <svg data-testid="chevron-icon" />,
}));

describe("SearchableDropdown Component", () => {
    const options: IMetadata[] = [
        {
            _id: "1",
            title: "Option 1",
            description: "Description 1",
            data: {
                report_id: "report_id",
                workspace_id: "workspace_id",
            },
            tags: [{ _id: "tag1", title: "Tag 1" }],
            route: "/option1",
            client_route: "/client/option1",
            type: "dashboard",
            component: "powerbiComponent",
            thumbnail: {
                content_type: "image/png",
                url: "https://example.com/thumbnail1.png",
            },
            roles: [{ id: "2", label: "role-label" }],
        },
    ];

    const handleChangeMock = jest.fn();
    const plausibleMock = jest.fn();
    const useEnvMock = useEnv as jest.Mock;
    const usePlausibleMock = usePlausible as jest.Mock;
    const useComponentVisibleMock = useComponentVisible as jest.Mock;

    beforeEach(() => {
        jest.clearAllMocks();
        useEnvMock.mockReturnValue({ API_URL: "http://api.example.com" });
        usePlausibleMock.mockReturnValue(plausibleMock);
        const ref = { current: null };
        useComponentVisibleMock.mockReturnValue({ ref });
    });

    it("renders without crashing", () => {
        render(
            <SearchableDropdown
                options={options}
                id="test-dropdown"
                selectedVal={null}
                handleChange={handleChangeMock}
                title="Select an Option"
            />
        );
        expect(screen.getByRole("button", { name: /Select an Option/i })).toBeInTheDocument();
    });

    test("renders the component correctly", () => {
        render(<SearchableDropdown id="test-dropdown" options={options} selectedVal={null} handleChange={handleChangeMock} />);

        const button = screen.getByTitle("Otevřít dropdown");
        expect(button).toBeInTheDocument();
        expect(button.textContent).toBe("");
    });

    test("opens and closes the dropdown when clicked", () => {
        render(<SearchableDropdown id="test-dropdown" options={options} selectedVal={null} handleChange={handleChangeMock} />);

        const button = screen.getByTitle("Otevřít dropdown");
        fireEvent.click(button);

        expect(screen.getByPlaceholderText("Vyhledat dashboard")).toBeInTheDocument();
        expect(screen.getByTitle("Zavřít dropdown")).toBeInTheDocument();

        fireEvent.click(button);
        expect(screen.queryByPlaceholderText("Vyhledat dashboard")).not.toBeInTheDocument();
    });

    test("selects an option when clicked", () => {
        render(<SearchableDropdown id="test-dropdown" options={options} selectedVal={null} handleChange={handleChangeMock} />);

        fireEvent.click(screen.getByTitle("Otevřít dropdown"));

        const option1 = screen.getByText("Option 1");
        fireEvent.click(option1);

        expect(handleChangeMock).toHaveBeenCalledWith(options[0]);
        expect(plausibleMock).toHaveBeenCalledWith("trackSelectDashboard-title", {
            props: { selectDashboard: "Option 1" },
        });
    });

    test("filters options based on input query", () => {
        render(<SearchableDropdown id="test-dropdown" options={options} selectedVal={null} handleChange={handleChangeMock} />);

        fireEvent.click(screen.getByTitle("Otevřít dropdown"));

        const searchInput = screen.getByPlaceholderText("Vyhledat dashboard");
        fireEvent.change(searchInput, { target: { value: "Option 1" } });

        const option1 = screen.getByText("Option 1");
        expect(option1).toBeInTheDocument();

        const option2 = screen.queryByText("Option 2");
        expect(option2).not.toBeInTheDocument();
    });
});
