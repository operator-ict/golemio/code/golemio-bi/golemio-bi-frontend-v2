import { getFileDimensions } from "@/utils/getFileDimensions";
import { imageRatio, THUMBNAIL_RATIO_HEIGHT, THUMBNAIL_RATIO_WIDTH } from "@/utils/imageRatio";

// Mock the getFileDimensions function
jest.mock("@/utils/getFileDimensions", () => ({
    getFileDimensions: jest.fn(),
}));

describe("imageRatio", () => {
    afterEach(() => {
        // Clear mock calls between tests
        jest.clearAllMocks();
    });

    it("should return true for valid image ratio", async () => {
        (getFileDimensions as jest.Mock).mockResolvedValue({
            width: THUMBNAIL_RATIO_WIDTH,
            height: THUMBNAIL_RATIO_HEIGHT,
        });

        const mockBlob = new Blob();
        const result = await imageRatio(mockBlob);

        expect(result).toBe(true);
        expect(getFileDimensions).toHaveBeenCalledWith(mockBlob);
    });

    it("should return false for invalid image ratio", async () => {
        (getFileDimensions as jest.Mock).mockResolvedValue({
            width: 500,
            height: 500,
        });

        const mockBlob = new Blob();
        const result = await imageRatio(mockBlob);

        expect(result).toBe(false);
        expect(getFileDimensions).toHaveBeenCalledWith(mockBlob);
    });

    it("should return true if no value is passed (undefined case)", async () => {
        const result = await imageRatio(undefined);

        expect(result).toBe(true);
        expect(getFileDimensions).not.toHaveBeenCalled();
    });

    it("should return false if getFileDimensions fails or returns invalid dimensions", async () => {
        (getFileDimensions as jest.Mock).mockRejectedValueOnce(new Error("Failed to load image"));

        const mockBlob = new Blob();

        const result = await imageRatio(mockBlob);

        expect(result).toBe(false); // Теперь ожидается false
        expect(getFileDimensions).toHaveBeenCalledWith(mockBlob);
    });
});
