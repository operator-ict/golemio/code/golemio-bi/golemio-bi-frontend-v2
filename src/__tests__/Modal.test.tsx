import "@testing-library/jest-dom";

import { fireEvent, render, screen } from "@testing-library/react";
import React from "react";

import { Modal } from "@/components/Modal";

jest.mock("next/navigation", () => ({
    useRouter: jest.fn(),
}));

describe("Modal Component", () => {
    let closeMock: jest.Mock;

    beforeEach(() => {
        closeMock = jest.fn();

        const portal = document.createElement("div");
        portal.setAttribute("id", "portal");
        document.body.appendChild(portal);
    });

    afterEach(() => {
        // Clean up portal after each test
        const portal = document.getElementById("portal");
        if (portal) {
            document.body.removeChild(portal);
        }
    });

    test("should render modal with children content when show is true", () => {
        render(
            <Modal show={true} onClose={closeMock} label="Test Modal">
                <p>Modal Content</p>
            </Modal>
        );

        const modal = screen.getByRole("dialog");
        expect(modal).toBeInTheDocument();
        expect(screen.getByText("Modal Content")).toBeInTheDocument();
        expect(screen.getByText("Test Modal")).toBeInTheDocument();
    });

    test("should not render modal when show is false", () => {
        render(
            <Modal show={false} onClose={closeMock} label="Test Modal">
                <p>Modal Content</p>
            </Modal>
        );

        const modal = screen.queryByRole("dialog");
        expect(modal).not.toBeInTheDocument();
    });

    test("should call onClose when Escape key is pressed", () => {
        render(
            <Modal show={true} onClose={closeMock} label="Test Modal">
                <p>Modal Content</p>
            </Modal>
        );

        fireEvent.keyDown(document, { code: "Escape" });
        expect(closeMock).toHaveBeenCalled();
    });

    test("should trap focus within the modal when Tab is pressed", () => {
        render(
            <Modal show={true} onClose={closeMock} label="Test Modal">
                <button>First Button</button>
                <button>Last Button</button>
            </Modal>
        );

        const firstButton = screen.getByText("First Button");

        firstButton.focus();
        fireEvent.keyDown(document, { code: "Tab" });

        // Should wrap focus back to the first element if needed
        expect(firstButton).toHaveFocus();
    });

    test("should call onClose when the close button is clicked", () => {
        render(
            <Modal show={true} onClose={closeMock} label="Test Modal">
                <p>Modal Content</p>
            </Modal>
        );

        const closeButton = screen.getByRole("button", { name: "Zavřít" });
        fireEvent.click(closeButton);

        expect(closeMock).toHaveBeenCalled();
    });
});
