"use client";

import { useCallback, useEffect, useState } from "react";
import * as CookieConsent from "vanilla-cookieconsent";

declare global {
    interface Window {
        dataLayer: any[];
    }
}

export const useCookieConsent = () => {
    const [analyticsAllowed, setAnalyticsAllowed] = useState(false);

    const checkAnalyticsConsent = useCallback(() => {
        try {
            if (!CookieConsent || typeof CookieConsent.getUserPreferences !== "function") {
                return false;
            }

            const cookieName = "golemiobi-cookieconsent-preferences";
            const rawCookie = window.localStorage.getItem(cookieName);

            if (rawCookie) {
                try {
                    const cookieData = JSON.parse(rawCookie);

                    if (cookieData && cookieData.categories) {
                        const hasAnalytics =
                            cookieData.categories.analytics === true || cookieData.categories.analytics_storage === true;

                        return hasAnalytics;
                    }

                    if (cookieData && cookieData.acceptedCategories && Array.isArray(cookieData.acceptedCategories)) {
                        const hasAnalytics =
                            cookieData.acceptedCategories.includes("analytics") ||
                            cookieData.acceptedCategories.includes("analytics_storage");

                        return hasAnalytics;
                    }
                } catch (e) {
                    console.error("[useCookieConsent] Error parsing localStorage cookie:", e);
                }
            }

            const preferences = CookieConsent.getUserPreferences() as any;

            if (preferences) {
                if (preferences.categories) {
                    const hasAnalytics =
                        preferences.categories.analytics === true || preferences.categories.analytics_storage === true;

                    return hasAnalytics;
                }

                if (preferences.acceptedCategories && Array.isArray(preferences.acceptedCategories)) {
                    const hasAnalytics =
                        preferences.acceptedCategories.includes("analytics") ||
                        preferences.acceptedCategories.includes("analytics_storage");

                    return hasAnalytics;
                }
            }

            return false;
        } catch (e) {
            console.error("[useCookieConsent] Error checking consent:", e);
            return false;
        }
    }, []);

    const updateAnalyticsState = useCallback(() => {
        const isAllowed = checkAnalyticsConsent();

        if (isAllowed !== analyticsAllowed && typeof window !== "undefined") {
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
                event: "analyticsConsentChanged",
                analyticsAllowed: isAllowed,
            });

            window.dataLayer.push({
                event: "consentUpdate",
                analytics_storage: isAllowed ? "granted" : "denied",
            });
        }

        setAnalyticsAllowed(isAllowed);
    }, [checkAnalyticsConsent, analyticsAllowed]);

    useEffect(() => {
        if (typeof window === "undefined") return;

        window.dataLayer = window.dataLayer || [];

        const setupEventListeners = () => {
            let timeoutId: ReturnType<typeof setTimeout> | null = null;

            const handleConsentChange = () => {
                if (timeoutId) {
                    clearTimeout(timeoutId);
                }

                timeoutId = setTimeout(() => {
                    updateAnalyticsState();

                    window.dataLayer.push({
                        event: "consentUpdated",
                        sourceEvent: "cookie_consent_update",
                    });

                    timeoutId = null;
                }, 300);
            };

            window.addEventListener("cc:onConsent", handleConsentChange);
            window.addEventListener("cc:onChange", handleConsentChange);
            window.addEventListener("cc:onFirstAction", handleConsentChange);
            window.addEventListener("cc:onAccept", handleConsentChange);

            window.addEventListener("storage", (event) => {
                if (event.key && event.key.includes("cookieconsent")) {
                    handleConsentChange();
                }
            });

            return () => {
                window.removeEventListener("cc:onConsent", handleConsentChange);
                window.removeEventListener("cc:onChange", handleConsentChange);
                window.removeEventListener("cc:onFirstAction", handleConsentChange);
                window.removeEventListener("cc:onAccept", handleConsentChange);
                window.removeEventListener("storage", handleConsentChange);

                if (timeoutId) {
                    clearTimeout(timeoutId);
                }
            };
        };

        let checkCount = 0;
        const maxChecks = 50;

        const checkInterval = setInterval(() => {
            checkCount++;

            if (CookieConsent && typeof CookieConsent.getUserPreferences === "function") {
                updateAnalyticsState();
                clearInterval(checkInterval);
                setupEventListeners();
            } else if (checkCount >= maxChecks) {
                console.warn("[useCookieConsent] Max check attempts reached, stopping");
                clearInterval(checkInterval);
            }
        }, 100);

        return () => {
            clearInterval(checkInterval);
        };
    }, [checkAnalyticsConsent, updateAnalyticsState]);

    return analyticsAllowed;
};
