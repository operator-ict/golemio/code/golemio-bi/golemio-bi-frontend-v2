"use client";

import GoogleTagManager from "@magicul/next-google-tag-manager";
import PlausibleProvider from "next-plausible";

import { useCookieConsent } from "./useCookieConsent";

interface AnalyticsProvider {
    plausibleDomain: string;
    gtmId: string;
}
export const AnalyticsProvider = ({ plausibleDomain, gtmId }: AnalyticsProvider) => {
    const analyticsAllowed = useCookieConsent();

    if (!analyticsAllowed) return null;
    return (
        <>
            <GoogleTagManager id={gtmId} />
            <PlausibleProvider domain={plausibleDomain} trackOutboundLinks trackLocalhost />
        </>
    );
};
