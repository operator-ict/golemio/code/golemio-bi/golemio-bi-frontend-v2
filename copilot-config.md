# Copilot Configuration

## Next.js App Router Usage

This project uses the Next.js App Router. Please follow these guidelines:

- **Do not use**: `getServerSideProps`, `getStaticProps`, `getInitialProps`.
- **Use**: Server components, middleware, and other features specific to the App Router.

## Start your query example

I'm using the Next.js App Router. Please provide a solution that avoids `getServerSideProps`, `getStaticProps`, and `getInitialProps`. How can I ...?
